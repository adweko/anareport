package com.adweko.webservice;

import com.adweko.controller.RuleCollector;
import com.adweko.controller.RuleController;
import com.adweko.model.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@RestController
public class AnaReportController {
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping("/readdata")
    public HashMap<RuleCollector, List<Report>> readData(@RequestParam(value = "xmlString")String xmlString){

        Service service = new Service();
        List<Report> reports = new ArrayList<>();
        Report report = null;
        HashMap<RuleCollector, List<Report>> reportLog = new HashMap<>();
        try {
            RootElement rootElement = service.readXML(xmlString);
            AdwekoRule bankRule = new AdwekoRule();
            bankRule.checkRule(rootElement, reportLog);

            //RuleCollector.checkAllRules(rootElement);

            RuleController ruleController = new RuleController();
            ruleController.addNewRule(new AdwekoRule());
            ruleController.addNewRule(new HeaderRule());
            ruleController.getRules();

            // default all
            ruleController.getRules().addAll(RuleCollector.getAll());

            for (Rule rule : ruleController.getRules()){
                rule.checkRule(rootElement, reportLog);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        if (reports.size() == 0){
            report = new Report();
            report.setName("Test");
            report.setMessage("OK!");
            reports.add(report);
        }
        return reportLog;
    }
}
