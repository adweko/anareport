package com.adweko.webservice;

import com.adweko.model.RootElement;
import org.sdmx.resources.sdmxml.schemas.v2_1.message.ObjectFactory;
import org.sdmx.resources.sdmxml.schemas.v2_1.message.StructureSpecificDataType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.StringReader;
import java.net.URL;

public class Service {

    public RootElement readXML(String xmlString){
        ObjectFactory objectFactory = new ObjectFactory();
        StructureSpecificDataType structureSpecificDataType1 = objectFactory.createStructureSpecificDataType();
        RootElement tester = new RootElement();
        tester.setStructureSpecificDataType(structureSpecificDataType1);

        try {

            JAXBContext context = JAXBContext.newInstance(RootElement.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            //ClassLoader classLoader = getClass().getClassLoader();
            //URL resource = classLoader.getResource("Bbk_Ancrdt_T1m_Bsp.xml");
            if (xmlString != null && !"".equals(xmlString)) {
                StringReader stringReader = new StringReader(xmlString);
                RootElement rootElement = (RootElement) unmarshaller.unmarshal(stringReader);
                if (rootElement != null) {
                    return rootElement;
                }
            }
            File file = new File("Bbk_Riad_Bsp.xml");

            if (file.exists()) {
                RootElement rootElement = (RootElement) unmarshaller.unmarshal(file);
                if (rootElement != null) {
                    return rootElement;
                }
            }
        }
        catch (JAXBException ex){
            ex.printStackTrace();
        }
        return null;
    }
}
