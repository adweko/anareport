package com.adweko.controller;

import com.adweko.model.Rule;

import java.util.ArrayList;
import java.util.List;

public class RuleController {

    private List<Rule> rules = new ArrayList<>();

    public void addNewRule(Rule rule){
        rules.add(rule);
    }

    public List<Rule> getRules() {
        return rules;
    }
}
