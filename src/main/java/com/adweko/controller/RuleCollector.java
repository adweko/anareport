package com.adweko.controller;

import com.adweko.model.AdwekoRule;
import com.adweko.model.HeaderRule;
import com.adweko.model.RootElement;
import com.adweko.model.Rule;

import java.util.ArrayList;
import java.util.List;

public enum RuleCollector {

    ADWEKO(new AdwekoRule(), 4.0),
    INTEGRITY(new HeaderRule(), 4.1),
    CONTRACT_PARTNER(new HeaderRule(), 4.2),
    CREDIT_DATA(new HeaderRule(), 4.3),
    CONSISTENCY(new HeaderRule(), 4.4),
    ENTITIES(new HeaderRule(), 4.5),
    FILE_ERRORS(new HeaderRule(), 4.6),
    FORMALLY(new HeaderRule(), 4.7);

    private Rule rule;
    private Double section;

    RuleCollector(Rule rule, Double section) {
        this.rule = rule;
        this.section = section;
    }

    public Rule getRule() {
        return rule;
    }

    /*
    public static void checkAllRules(RootElement rootElement){
        for (RuleCollector rc : RuleCollector.values()){
            rc.getRule().checkRule(rootElement);
        }
    }
    */
    public static List<Rule> getAll(){
        List<Rule> rules = new ArrayList<>();
        for (RuleCollector rc : RuleCollector.values()){
            rules.add(rc.getRule());
        }
        return rules;
    }
}
