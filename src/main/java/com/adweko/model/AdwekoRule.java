package com.adweko.model;

import com.adweko.controller.RuleCollector;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AdwekoRule implements Rule {

    private String[] test = {"ANCRDT_T1M", "ANCRDT_T2M", "ANCRDT_T2Q", "RIAD"};

    @Override
    public void checkRule(RootElement rootElement, HashMap<RuleCollector, List<Report>> reportLog) {
        List<Report> reports = new ArrayList<>();
        for (Object dataSet : rootElement.getStructureSpecificDataType().getAny()){
            if (dataSet instanceof ElementNSImpl){
                ElementNSImpl element = (ElementNSImpl) dataSet;
                String attribute = element.getAttribute("data:structureRef");
                if ("BBK_RIAD_HDR_C".equals(attribute)){
                    System.out.println("obs");
                    NamedNodeMap node = element.getFirstChild().getAttributes();
                    Node rprtng_agnt_cd = node.getNamedItem("RPRTNG_AGNT_CD");
                    String nodeValue = rprtng_agnt_cd.getNodeValue();
                    if (!nodeValue.matches("[0-9]+")){
                        Report report = new Report();
                        report.setMessage("Fehler in: " + rprtng_agnt_cd.getNodeName() + ": Only Numbers allowed!");
                        reports.add(report);
                    }
                    Node dt_rfrnc = node.getNamedItem("DT_RFRNC");
                    String date = dt_rfrnc.getNodeValue();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
                    Date checkDate = null;
                    try {
                        checkDate = simpleDateFormat.parse(date);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                    Date now = new Date();
                    if (checkDate.after(now)){
                        Report report = new Report();
                        report.setMessage("Fehler in: " + dt_rfrnc.getNodeName() + ": Date is after now!");
                        reports.add(report);
                    }
                    Node srvy_id = node.getNamedItem("SRVY_ID");
                    String value = srvy_id.getNodeValue();
                    boolean check = false;
                    for (String s : test){
                        if (s.equals(value)){
                            check = true;
                            break;
                        }
                    }
                    if (!check){
                        Report report = new Report();
                        report.setMessage("Fehler in: " + srvy_id.getNodeName() + ": Don't equal!");
                        reports.add(report);
                    }

                }
            }
        }
        reportLog.put(RuleCollector.ADWEKO, reports);
    }
}
