package com.adweko.model;

import com.adweko.controller.RuleCollector;

import java.util.HashMap;
import java.util.List;

public interface Rule {

    void checkRule(RootElement rootElement, HashMap<RuleCollector, List<Report>> reportLog);
}
