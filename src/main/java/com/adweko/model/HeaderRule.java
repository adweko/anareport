package com.adweko.model;

import com.adweko.controller.RuleCollector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HeaderRule implements Rule{
  @Override
  public void checkRule(RootElement rootElement, HashMap<RuleCollector, List<Report>> reportLog) {
    List<Report> reports = new ArrayList<>();

    if(rootElement.getStructureSpecificDataType().getHeader().isTest()){

      Report report = new Report();
      report.setMessage("Achtung, du bist im Testumgebung!!!");
      reports.add(report);

    }

    reportLog.put(RuleCollector.CONTRACT_PARTNER, reports);
  }

}
