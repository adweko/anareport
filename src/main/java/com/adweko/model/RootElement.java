package com.adweko.model;

import org.sdmx.resources.sdmxml.schemas.v2_1.message.StructureSpecificDataType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class RootElement {

    @XmlElement(name = "StructureSpecificData", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", required = true)
    private StructureSpecificDataType structureSpecificDataType;

    public StructureSpecificDataType getStructureSpecificDataType() {
        return structureSpecificDataType;
    }

    public void setStructureSpecificDataType(StructureSpecificDataType structureSpecificDataType) {
        this.structureSpecificDataType = structureSpecificDataType;
    }
}
