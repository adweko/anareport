
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ObjectTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ObjectTypeCodelistType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Any"/>
 *     &lt;enumeration value="Agency"/>
 *     &lt;enumeration value="AgencyScheme"/>
 *     &lt;enumeration value="AttachmentConstraint"/>
 *     &lt;enumeration value="Attribute"/>
 *     &lt;enumeration value="AttributeDescriptor"/>
 *     &lt;enumeration value="Categorisation"/>
 *     &lt;enumeration value="Category"/>
 *     &lt;enumeration value="CategorySchemeMap"/>
 *     &lt;enumeration value="CategoryScheme"/>
 *     &lt;enumeration value="Code"/>
 *     &lt;enumeration value="CodeMap"/>
 *     &lt;enumeration value="Codelist"/>
 *     &lt;enumeration value="CodelistMap"/>
 *     &lt;enumeration value="ComponentMap"/>
 *     &lt;enumeration value="Concept"/>
 *     &lt;enumeration value="ConceptMap"/>
 *     &lt;enumeration value="ConceptScheme"/>
 *     &lt;enumeration value="ConceptSchemeMap"/>
 *     &lt;enumeration value="Constraint"/>
 *     &lt;enumeration value="ConstraintTarget"/>
 *     &lt;enumeration value="ContentConstraint"/>
 *     &lt;enumeration value="Dataflow"/>
 *     &lt;enumeration value="DataConsumer"/>
 *     &lt;enumeration value="DataConsumerScheme"/>
 *     &lt;enumeration value="DataProvider"/>
 *     &lt;enumeration value="DataProviderScheme"/>
 *     &lt;enumeration value="DataSetTarget"/>
 *     &lt;enumeration value="DataStructure"/>
 *     &lt;enumeration value="Dimension"/>
 *     &lt;enumeration value="DimensionDescriptor"/>
 *     &lt;enumeration value="DimensionDescriptorValuesTarget"/>
 *     &lt;enumeration value="GroupDimensionDescriptor"/>
 *     &lt;enumeration value="HierarchicalCode"/>
 *     &lt;enumeration value="HierarchicalCodelist"/>
 *     &lt;enumeration value="Hierarchy"/>
 *     &lt;enumeration value="HybridCodelistMap"/>
 *     &lt;enumeration value="HybridCodeMap"/>
 *     &lt;enumeration value="IdentifiableObjectTarget"/>
 *     &lt;enumeration value="Level"/>
 *     &lt;enumeration value="MeasureDescriptor"/>
 *     &lt;enumeration value="MeasureDimension"/>
 *     &lt;enumeration value="Metadataflow"/>
 *     &lt;enumeration value="MetadataAttribute"/>
 *     &lt;enumeration value="MetadataSet"/>
 *     &lt;enumeration value="MetadataStructure"/>
 *     &lt;enumeration value="MetadataTarget"/>
 *     &lt;enumeration value="Organisation"/>
 *     &lt;enumeration value="OrganisationMap"/>
 *     &lt;enumeration value="OrganisationScheme"/>
 *     &lt;enumeration value="OrganisationSchemeMap"/>
 *     &lt;enumeration value="OrganisationUnit"/>
 *     &lt;enumeration value="OrganisationUnitScheme"/>
 *     &lt;enumeration value="PrimaryMeasure"/>
 *     &lt;enumeration value="Process"/>
 *     &lt;enumeration value="ProcessStep"/>
 *     &lt;enumeration value="ProvisionAgreement"/>
 *     &lt;enumeration value="ReportingCategory"/>
 *     &lt;enumeration value="ReportingCategoryMap"/>
 *     &lt;enumeration value="ReportingTaxonomy"/>
 *     &lt;enumeration value="ReportingTaxonomyMap"/>
 *     &lt;enumeration value="ReportingYearStartDay"/>
 *     &lt;enumeration value="ReportPeriodTarget"/>
 *     &lt;enumeration value="ReportStructure"/>
 *     &lt;enumeration value="StructureMap"/>
 *     &lt;enumeration value="StructureSet"/>
 *     &lt;enumeration value="TimeDimension"/>
 *     &lt;enumeration value="Transition"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ObjectTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum ObjectTypeCodelistType {

    @XmlEnumValue("Any")
    ANY("Any"),
    @XmlEnumValue("Agency")
    AGENCY("Agency"),
    @XmlEnumValue("AgencyScheme")
    AGENCY_SCHEME("AgencyScheme"),
    @XmlEnumValue("AttachmentConstraint")
    ATTACHMENT_CONSTRAINT("AttachmentConstraint"),
    @XmlEnumValue("Attribute")
    ATTRIBUTE("Attribute"),
    @XmlEnumValue("AttributeDescriptor")
    ATTRIBUTE_DESCRIPTOR("AttributeDescriptor"),
    @XmlEnumValue("Categorisation")
    CATEGORISATION("Categorisation"),
    @XmlEnumValue("Category")
    CATEGORY("Category"),
    @XmlEnumValue("CategorySchemeMap")
    CATEGORY_SCHEME_MAP("CategorySchemeMap"),
    @XmlEnumValue("CategoryScheme")
    CATEGORY_SCHEME("CategoryScheme"),
    @XmlEnumValue("Code")
    CODE("Code"),
    @XmlEnumValue("CodeMap")
    CODE_MAP("CodeMap"),
    @XmlEnumValue("Codelist")
    CODELIST("Codelist"),
    @XmlEnumValue("CodelistMap")
    CODELIST_MAP("CodelistMap"),
    @XmlEnumValue("ComponentMap")
    COMPONENT_MAP("ComponentMap"),
    @XmlEnumValue("Concept")
    CONCEPT("Concept"),
    @XmlEnumValue("ConceptMap")
    CONCEPT_MAP("ConceptMap"),
    @XmlEnumValue("ConceptScheme")
    CONCEPT_SCHEME("ConceptScheme"),
    @XmlEnumValue("ConceptSchemeMap")
    CONCEPT_SCHEME_MAP("ConceptSchemeMap"),
    @XmlEnumValue("Constraint")
    CONSTRAINT("Constraint"),
    @XmlEnumValue("ConstraintTarget")
    CONSTRAINT_TARGET("ConstraintTarget"),
    @XmlEnumValue("ContentConstraint")
    CONTENT_CONSTRAINT("ContentConstraint"),
    @XmlEnumValue("Dataflow")
    DATAFLOW("Dataflow"),
    @XmlEnumValue("DataConsumer")
    DATA_CONSUMER("DataConsumer"),
    @XmlEnumValue("DataConsumerScheme")
    DATA_CONSUMER_SCHEME("DataConsumerScheme"),
    @XmlEnumValue("DataProvider")
    DATA_PROVIDER("DataProvider"),
    @XmlEnumValue("DataProviderScheme")
    DATA_PROVIDER_SCHEME("DataProviderScheme"),
    @XmlEnumValue("DataSetTarget")
    DATA_SET_TARGET("DataSetTarget"),
    @XmlEnumValue("DataStructure")
    DATA_STRUCTURE("DataStructure"),
    @XmlEnumValue("Dimension")
    DIMENSION("Dimension"),
    @XmlEnumValue("DimensionDescriptor")
    DIMENSION_DESCRIPTOR("DimensionDescriptor"),
    @XmlEnumValue("DimensionDescriptorValuesTarget")
    DIMENSION_DESCRIPTOR_VALUES_TARGET("DimensionDescriptorValuesTarget"),
    @XmlEnumValue("GroupDimensionDescriptor")
    GROUP_DIMENSION_DESCRIPTOR("GroupDimensionDescriptor"),
    @XmlEnumValue("HierarchicalCode")
    HIERARCHICAL_CODE("HierarchicalCode"),
    @XmlEnumValue("HierarchicalCodelist")
    HIERARCHICAL_CODELIST("HierarchicalCodelist"),
    @XmlEnumValue("Hierarchy")
    HIERARCHY("Hierarchy"),
    @XmlEnumValue("HybridCodelistMap")
    HYBRID_CODELIST_MAP("HybridCodelistMap"),
    @XmlEnumValue("HybridCodeMap")
    HYBRID_CODE_MAP("HybridCodeMap"),
    @XmlEnumValue("IdentifiableObjectTarget")
    IDENTIFIABLE_OBJECT_TARGET("IdentifiableObjectTarget"),
    @XmlEnumValue("Level")
    LEVEL("Level"),
    @XmlEnumValue("MeasureDescriptor")
    MEASURE_DESCRIPTOR("MeasureDescriptor"),
    @XmlEnumValue("MeasureDimension")
    MEASURE_DIMENSION("MeasureDimension"),
    @XmlEnumValue("Metadataflow")
    METADATAFLOW("Metadataflow"),
    @XmlEnumValue("MetadataAttribute")
    METADATA_ATTRIBUTE("MetadataAttribute"),
    @XmlEnumValue("MetadataSet")
    METADATA_SET("MetadataSet"),
    @XmlEnumValue("MetadataStructure")
    METADATA_STRUCTURE("MetadataStructure"),
    @XmlEnumValue("MetadataTarget")
    METADATA_TARGET("MetadataTarget"),
    @XmlEnumValue("Organisation")
    ORGANISATION("Organisation"),
    @XmlEnumValue("OrganisationMap")
    ORGANISATION_MAP("OrganisationMap"),
    @XmlEnumValue("OrganisationScheme")
    ORGANISATION_SCHEME("OrganisationScheme"),
    @XmlEnumValue("OrganisationSchemeMap")
    ORGANISATION_SCHEME_MAP("OrganisationSchemeMap"),
    @XmlEnumValue("OrganisationUnit")
    ORGANISATION_UNIT("OrganisationUnit"),
    @XmlEnumValue("OrganisationUnitScheme")
    ORGANISATION_UNIT_SCHEME("OrganisationUnitScheme"),
    @XmlEnumValue("PrimaryMeasure")
    PRIMARY_MEASURE("PrimaryMeasure"),
    @XmlEnumValue("Process")
    PROCESS("Process"),
    @XmlEnumValue("ProcessStep")
    PROCESS_STEP("ProcessStep"),
    @XmlEnumValue("ProvisionAgreement")
    PROVISION_AGREEMENT("ProvisionAgreement"),
    @XmlEnumValue("ReportingCategory")
    REPORTING_CATEGORY("ReportingCategory"),
    @XmlEnumValue("ReportingCategoryMap")
    REPORTING_CATEGORY_MAP("ReportingCategoryMap"),
    @XmlEnumValue("ReportingTaxonomy")
    REPORTING_TAXONOMY("ReportingTaxonomy"),
    @XmlEnumValue("ReportingTaxonomyMap")
    REPORTING_TAXONOMY_MAP("ReportingTaxonomyMap"),
    @XmlEnumValue("ReportingYearStartDay")
    REPORTING_YEAR_START_DAY("ReportingYearStartDay"),
    @XmlEnumValue("ReportPeriodTarget")
    REPORT_PERIOD_TARGET("ReportPeriodTarget"),
    @XmlEnumValue("ReportStructure")
    REPORT_STRUCTURE("ReportStructure"),
    @XmlEnumValue("StructureMap")
    STRUCTURE_MAP("StructureMap"),
    @XmlEnumValue("StructureSet")
    STRUCTURE_SET("StructureSet"),
    @XmlEnumValue("TimeDimension")
    TIME_DIMENSION("TimeDimension"),
    @XmlEnumValue("Transition")
    TRANSITION("Transition");
    private final String value;

    ObjectTypeCodelistType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ObjectTypeCodelistType fromValue(String v) {
        for (ObjectTypeCodelistType c: ObjectTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
