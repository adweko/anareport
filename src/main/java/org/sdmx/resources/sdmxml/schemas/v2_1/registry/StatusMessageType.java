
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * StatusMessageType carries the text of error messages and/or warnings in response to queries or requests.
 * 
 * <p>Java-Klasse f�r StatusMessageType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StatusMessageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MessageText" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StatusMessageType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="status" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}StatusType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusMessageType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "messageText"
})
public class StatusMessageType {

    @XmlElement(name = "MessageText", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected List<org.sdmx.resources.sdmxml.schemas.v2_1.common.StatusMessageType> messageText;
    @XmlAttribute(name = "status", required = true)
    protected StatusType status;

    /**
     * Gets the value of the messageText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messageText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessageText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link org.sdmx.resources.sdmxml.schemas.v2_1.common.StatusMessageType }
     * 
     * 
     */
    public List<org.sdmx.resources.sdmxml.schemas.v2_1.common.StatusMessageType> getMessageText() {
        if (messageText == null) {
            messageText = new ArrayList<org.sdmx.resources.sdmxml.schemas.v2_1.common.StatusMessageType>();
        }
        return this.messageText;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setStatus(StatusType value) {
        this.status = value;
    }

}
