
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r TextSearchOperatorType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="TextSearchOperatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="contains"/>
 *     &lt;enumeration value="startsWith"/>
 *     &lt;enumeration value="endsWith"/>
 *     &lt;enumeration value="doesNotContain"/>
 *     &lt;enumeration value="doesNotStartWith"/>
 *     &lt;enumeration value="doesNotEndWith"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TextSearchOperatorType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum TextSearchOperatorType {


    /**
     * The text being searched must contain the supplied text.
     * 
     */
    @XmlEnumValue("contains")
    CONTAINS("contains"),

    /**
     * The text being searched must start with the supplied text.
     * 
     */
    @XmlEnumValue("startsWith")
    STARTS_WITH("startsWith"),

    /**
     * The text being searched must end with the supplied text.
     * 
     */
    @XmlEnumValue("endsWith")
    ENDS_WITH("endsWith"),

    /**
     * The text being searched cannot contain the supplied text.
     * 
     */
    @XmlEnumValue("doesNotContain")
    DOES_NOT_CONTAIN("doesNotContain"),

    /**
     * The text being searched cannot start with the supplied text.
     * 
     */
    @XmlEnumValue("doesNotStartWith")
    DOES_NOT_START_WITH("doesNotStartWith"),

    /**
     * The text being searched cannot end with the supplied text.
     * 
     */
    @XmlEnumValue("doesNotEndWith")
    DOES_NOT_END_WITH("doesNotEndWith");
    private final String value;

    TextSearchOperatorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TextSearchOperatorType fromValue(String v) {
        for (TextSearchOperatorType c: TextSearchOperatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
