
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r MetadataTargetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataTargetType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataTargetBaseType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}KeyDescriptorValuesTarget"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataSetTarget"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConstraintContentTarget"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReportPeriodTarget"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}IdentifiableObjectTarget"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataTargetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "keyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget"
})
public class MetadataTargetType
    extends MetadataTargetBaseType
{

    @XmlElements({
        @XmlElement(name = "KeyDescriptorValuesTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = KeyDescriptorValuesTargetType.class),
        @XmlElement(name = "DataSetTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = DataSetTargetType.class),
        @XmlElement(name = "ConstraintContentTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = ConstraintContentTargetType.class),
        @XmlElement(name = "ReportPeriodTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = ReportPeriodTargetType.class),
        @XmlElement(name = "IdentifiableObjectTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = IdentifiableObjectTargetType.class)
    })
    protected List<TargetObject> keyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget;

    /**
     * Gets the value of the keyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the keyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyDescriptorValuesTargetType }
     * {@link DataSetTargetType }
     * {@link ConstraintContentTargetType }
     * {@link ReportPeriodTargetType }
     * {@link IdentifiableObjectTargetType }
     * 
     * 
     */
    public List<TargetObject> getKeyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget() {
        if (keyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget == null) {
            keyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget = new ArrayList<TargetObject>();
        }
        return this.keyDescriptorValuesTargetOrDataSetTargetOrConstraintContentTarget;
    }

}
