
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r StructureReturnDetailType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="StructureReturnDetailType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Stub"/>
 *     &lt;enumeration value="CompleteStub"/>
 *     &lt;enumeration value="Full"/>
 *     &lt;enumeration value="MatchedItems"/>
 *     &lt;enumeration value="CascadedMatchedItems"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StructureReturnDetailType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum
public enum StructureReturnDetailType {


    /**
     * Only the identification information and name should be returned.
     * 
     */
    @XmlEnumValue("Stub")
    STUB("Stub"),

    /**
     * Identification information, name, description, and annotations should be returned.
     * 
     */
    @XmlEnumValue("CompleteStub")
    COMPLETE_STUB("CompleteStub"),

    /**
     * The entire detail of the object should be returned.
     * 
     */
    @XmlEnumValue("Full")
    FULL("Full"),

    /**
     * For an item scheme, only the items matching the item where parameters will be returned. In the case that items are hierarchical, the entire hierarchy leading to the matched item will have to be returned.
     * 
     */
    @XmlEnumValue("MatchedItems")
    MATCHED_ITEMS("MatchedItems"),

    /**
     * For an item scheme, only the items matching the item where parameters, and their hierarchical child items will be returned. In the case that items are hierarchical, the entire hierarchy leading to the matched item will have to be returned.
     * 
     */
    @XmlEnumValue("CascadedMatchedItems")
    CASCADED_MATCHED_ITEMS("CascadedMatchedItems");
    private final String value;

    StructureReturnDetailType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StructureReturnDetailType fromValue(String v) {
        for (StructureReturnDetailType c: StructureReturnDetailType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
