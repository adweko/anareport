
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataReturnDetailsType is a structure for detailing how reference metadata should be returned. Only a default size limit can be specified.
 * 
 * <p>Java-Klasse f�r MetadataReturnDetailsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataReturnDetailsType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReturnDetailsBaseType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataReturnDetailsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class MetadataReturnDetailsType
    extends ReturnDetailsBaseType
{


}
