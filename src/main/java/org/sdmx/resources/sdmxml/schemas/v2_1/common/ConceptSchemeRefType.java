
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * ConceptSchemeRefType provides a reference to a concept scheme via a complete set of reference fields.
 * 
 * <p>Java-Klasse f�r ConceptSchemeRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConceptSchemeRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeRefBaseType">
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeTypeCodelistType" fixed="ConceptScheme" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemePackageTypeCodelistType" fixed="conceptscheme" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConceptSchemeRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class ConceptSchemeRefType
    extends ItemSchemeRefBaseType
{


}
