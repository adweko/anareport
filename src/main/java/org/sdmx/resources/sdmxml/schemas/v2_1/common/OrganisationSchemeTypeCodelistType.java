
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r OrganisationSchemeTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganisationSchemeTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeTypeCodelistType">
 *     &lt;enumeration value="AgencyScheme"/>
 *     &lt;enumeration value="DataConsumerScheme"/>
 *     &lt;enumeration value="DataProviderScheme"/>
 *     &lt;enumeration value="OrganisationUnitScheme"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrganisationSchemeTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ItemSchemeTypeCodelistType.class)
public enum OrganisationSchemeTypeCodelistType {

    @XmlEnumValue("AgencyScheme")
    AGENCY_SCHEME(ItemSchemeTypeCodelistType.AGENCY_SCHEME),
    @XmlEnumValue("DataConsumerScheme")
    DATA_CONSUMER_SCHEME(ItemSchemeTypeCodelistType.DATA_CONSUMER_SCHEME),
    @XmlEnumValue("DataProviderScheme")
    DATA_PROVIDER_SCHEME(ItemSchemeTypeCodelistType.DATA_PROVIDER_SCHEME),
    @XmlEnumValue("OrganisationUnitScheme")
    ORGANISATION_UNIT_SCHEME(ItemSchemeTypeCodelistType.ORGANISATION_UNIT_SCHEME);
    private final ItemSchemeTypeCodelistType value;

    OrganisationSchemeTypeCodelistType(ItemSchemeTypeCodelistType v) {
        value = v;
    }

    public ItemSchemeTypeCodelistType value() {
        return value;
    }

    public static OrganisationSchemeTypeCodelistType fromValue(ItemSchemeTypeCodelistType v) {
        for (OrganisationSchemeTypeCodelistType c: OrganisationSchemeTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
