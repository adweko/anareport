
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;


/**
 * ObsOnlyType defines the structure for an un-grouped observation. Unlike a group observation, an un-grouped must provided a full set of values for every dimension declared in the data structure definition. The observation can contain an observed value and/or a collection of attribute values.
 * 
 * <p>Java-Klasse f�r ObsOnlyType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsOnlyType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="ObsKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType"/>
 *         &lt;element name="ObsValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ObsValueType" minOccurs="0"/>
 *         &lt;element name="Attributes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsOnlyType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", propOrder = {
    "obsKey",
    "obsValue",
    "attributes"
})
public class ObsOnlyType
    extends AnnotableType
{

    @XmlElement(name = "ObsKey", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", required = true)
    protected ValuesType obsKey;
    @XmlElement(name = "ObsValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected ObsValueType obsValue;
    @XmlElement(name = "Attributes", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected ValuesType attributes;

    /**
     * Ruft den Wert der obsKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValuesType }
     *     
     */
    public ValuesType getObsKey() {
        return obsKey;
    }

    /**
     * Legt den Wert der obsKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuesType }
     *     
     */
    public void setObsKey(ValuesType value) {
        this.obsKey = value;
    }

    /**
     * Ruft den Wert der obsValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObsValueType }
     *     
     */
    public ObsValueType getObsValue() {
        return obsValue;
    }

    /**
     * Legt den Wert der obsValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObsValueType }
     *     
     */
    public void setObsValue(ObsValueType value) {
        this.obsValue = value;
    }

    /**
     * Ruft den Wert der attributes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValuesType }
     *     
     */
    public ValuesType getAttributes() {
        return attributes;
    }

    /**
     * Legt den Wert der attributes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuesType }
     *     
     */
    public void setAttributes(ValuesType value) {
        this.attributes = value;
    }

}
