
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * GenericTimeSeriesDataStructureType defines the structural information for a generic time series based data set. The dimension at the observation level is fixed to be TIME_PERIOD.
 * 
 * <p>Java-Klasse f�r GenericTimeSeriesDataStructureType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GenericTimeSeriesDataStructureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}GenericDataStructureType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="ProvisionAgrement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType"/>
 *           &lt;element name="StructureUsage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType"/>
 *           &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureReferenceType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="dimensionAtObservation" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationDimensionType" fixed="TIME_PERIOD" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericTimeSeriesDataStructureType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class GenericTimeSeriesDataStructureType
    extends GenericDataStructureType
{


}
