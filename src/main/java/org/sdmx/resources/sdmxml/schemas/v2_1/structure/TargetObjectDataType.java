
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataType;


/**
 * <p>Java-Klasse f�r TargetObjectDataType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="TargetObjectDataType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataType">
 *     &lt;enumeration value="KeyValues"/>
 *     &lt;enumeration value="IdentifiableReference"/>
 *     &lt;enumeration value="DataSetReference"/>
 *     &lt;enumeration value="AttachmentConstraintReference"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TargetObjectDataType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlEnum(DataType.class)
public enum TargetObjectDataType {

    @XmlEnumValue("KeyValues")
    KEY_VALUES(DataType.KEY_VALUES),
    @XmlEnumValue("IdentifiableReference")
    IDENTIFIABLE_REFERENCE(DataType.IDENTIFIABLE_REFERENCE),
    @XmlEnumValue("DataSetReference")
    DATA_SET_REFERENCE(DataType.DATA_SET_REFERENCE),
    @XmlEnumValue("AttachmentConstraintReference")
    ATTACHMENT_CONSTRAINT_REFERENCE(DataType.ATTACHMENT_CONSTRAINT_REFERENCE);
    private final DataType value;

    TargetObjectDataType(DataType v) {
        value = v;
    }

    public DataType value() {
        return value;
    }

    public static TargetObjectDataType fromValue(DataType v) {
        for (TargetObjectDataType c: TargetObjectDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
