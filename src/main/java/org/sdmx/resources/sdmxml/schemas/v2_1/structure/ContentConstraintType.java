
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ContentConstraintTypeCodeType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ReferencePeriodType;


/**
 * ContentConstraintType describes the details of a content constraint by defining the content regions, key sets, or release information for the constraint attachment objects. Note that if the constraint is for a data provider, then only release calendar information is relevant, as there is no reliable way of determining which key family is being used to frame constraints in terms of cube regions or key sets.
 * 
 * <p>Java-Klasse f�r ContentConstraintType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ContentConstraintType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ContentConstraintBaseType">
 *       &lt;sequence>
 *         &lt;element name="ReleaseCalendar" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReleaseCalendarType" minOccurs="0"/>
 *         &lt;element name="ReferencePeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReferencePeriodType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ContentConstraintTypeCodeType" default="Actual" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContentConstraintType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "releaseCalendar",
    "referencePeriod"
})
public class ContentConstraintType
    extends ContentConstraintBaseType
{

    @XmlElement(name = "ReleaseCalendar", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ReleaseCalendarType releaseCalendar;
    @XmlElement(name = "ReferencePeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ReferencePeriodType referencePeriod;
    @XmlAttribute(name = "type")
    protected ContentConstraintTypeCodeType type;

    /**
     * Ruft den Wert der releaseCalendar-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseCalendarType }
     *     
     */
    public ReleaseCalendarType getReleaseCalendar() {
        return releaseCalendar;
    }

    /**
     * Legt den Wert der releaseCalendar-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseCalendarType }
     *     
     */
    public void setReleaseCalendar(ReleaseCalendarType value) {
        this.releaseCalendar = value;
    }

    /**
     * Ruft den Wert der referencePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencePeriodType }
     *     
     */
    public ReferencePeriodType getReferencePeriod() {
        return referencePeriod;
    }

    /**
     * Legt den Wert der referencePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencePeriodType }
     *     
     */
    public void setReferencePeriod(ReferencePeriodType value) {
        this.referencePeriod = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContentConstraintTypeCodeType }
     *     
     */
    public ContentConstraintTypeCodeType getType() {
        if (type == null) {
            return ContentConstraintTypeCodeType.ACTUAL;
        } else {
            return type;
        }
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContentConstraintTypeCodeType }
     *     
     */
    public void setType(ContentConstraintTypeCodeType value) {
        this.type = value;
    }

}
