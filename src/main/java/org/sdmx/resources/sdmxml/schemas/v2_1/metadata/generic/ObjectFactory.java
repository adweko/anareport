
package org.sdmx.resources.sdmxml.schemas.v2_1.metadata.generic;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.sdmx.resources.sdmxml.schemas.v2_1.metadata.generic package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.sdmx.resources.sdmxml.schemas.v2_1.metadata.generic
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReferenceValueType }
     * 
     */
    public ReferenceValueType createReferenceValueType() {
        return new ReferenceValueType();
    }

    /**
     * Create an instance of {@link ReportedAttributeType }
     * 
     */
    public ReportedAttributeType createReportedAttributeType() {
        return new ReportedAttributeType();
    }

    /**
     * Create an instance of {@link MetadataSetType }
     * 
     */
    public MetadataSetType createMetadataSetType() {
        return new MetadataSetType();
    }

    /**
     * Create an instance of {@link TargetType }
     * 
     */
    public TargetType createTargetType() {
        return new TargetType();
    }

    /**
     * Create an instance of {@link AttributeSetType }
     * 
     */
    public AttributeSetType createAttributeSetType() {
        return new AttributeSetType();
    }

    /**
     * Create an instance of {@link ReportType }
     * 
     */
    public ReportType createReportType() {
        return new ReportType();
    }

}
