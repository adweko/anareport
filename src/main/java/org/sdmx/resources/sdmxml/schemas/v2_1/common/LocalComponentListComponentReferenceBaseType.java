
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * LocalComponentListComponentReferenceBaseType is an abstract base type which provides a simple reference to any type of component in a specific component list where the reference to the structure which defines it are provided in another context, and the component list may or may not be defined in another context.
 * 
 * <p>Java-Klasse f�r LocalComponentListComponentReferenceBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LocalComponentListComponentReferenceBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReferenceType">
 *       &lt;sequence>
 *         &lt;element name="Ref" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalComponentListComponentRefBaseType" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocalComponentListComponentReferenceBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    LocalComponentListComponentReferenceType.class,
    LocalMetadataStructureComponentReferenceType.class,
    LocalDataStructureComponentReferenceType.class,
    LocalComponentReferenceType.class,
    LocalComponentReferenceBaseType.class
})
public abstract class LocalComponentListComponentReferenceBaseType
    extends ReferenceType
{


}
