
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ObjectTypeCodelistType;


/**
 * IdentifiableObjectTargetType defines the structure of an identifiable target object. The identifiable target object has a fixed representation of a reference and can specify a local representation of any item scheme for the purpose of restricting which items may be referenced. The identifiable object target must specify the object type which the target object is meant to reference.
 * 
 * <p>Java-Klasse f�r IdentifiableObjectTargetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="IdentifiableObjectTargetType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}IdentifiableObjectTargetBaseType">
 *       &lt;attribute name="objectType" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentifiableObjectTargetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class IdentifiableObjectTargetType
    extends IdentifiableObjectTargetBaseType
{

    @XmlAttribute(name = "objectType", required = true)
    protected ObjectTypeCodelistType objectType;

    /**
     * Ruft den Wert der objectType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjectTypeCodelistType }
     *     
     */
    public ObjectTypeCodelistType getObjectType() {
        return objectType;
    }

    /**
     * Legt den Wert der objectType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectTypeCodelistType }
     *     
     */
    public void setObjectType(ObjectTypeCodelistType value) {
        this.objectType = value;
    }

}
