
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * SimpleKeyValueType derives from the SimpleValueType, but does not allow for the cascading of value in the hierarchy, as keys are meant to describe a distinct full or partial key.
 * 
 * <p>Java-Klasse f�r SimpleKeyValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SimpleKeyValueType">
 *   &lt;simpleContent>
 *     &lt;restriction base="&lt;http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common>SimpleValueType">
 *     &lt;/restriction>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleKeyValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class SimpleKeyValueType
    extends SimpleValueType
{


}
