
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeSeriesType defines a structure which is used to group a collection of observations which have a key in common, organised by time. The key for a series is every dimension defined in the data structure definition, save the time dimension. In addition to observations, values can be provided for attributes which are associated with the dimensions which make up this series key (so long as the attributes do not specify a group attachment or also have an relationship with the time dimension). It is possible for the series to contain only observations or only attribute values, or both.
 * 
 * <p>Java-Klasse f�r TimeSeriesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeSeriesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}SeriesType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element name="SeriesKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType"/>
 *         &lt;element name="Attributes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType" minOccurs="0"/>
 *         &lt;element name="Obs" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}TimeSeriesObsType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeSeriesType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
public class TimeSeriesType
    extends SeriesType
{


}
