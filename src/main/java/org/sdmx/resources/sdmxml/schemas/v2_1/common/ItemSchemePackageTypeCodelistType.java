
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ItemSchemePackageTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ItemSchemePackageTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PackageTypeCodelistType">
 *     &lt;enumeration value="base"/>
 *     &lt;enumeration value="codelist"/>
 *     &lt;enumeration value="categoryscheme"/>
 *     &lt;enumeration value="conceptscheme"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ItemSchemePackageTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(PackageTypeCodelistType.class)
public enum ItemSchemePackageTypeCodelistType {

    @XmlEnumValue("base")
    BASE(PackageTypeCodelistType.BASE),
    @XmlEnumValue("codelist")
    CODELIST(PackageTypeCodelistType.CODELIST),
    @XmlEnumValue("categoryscheme")
    CATEGORYSCHEME(PackageTypeCodelistType.CATEGORYSCHEME),
    @XmlEnumValue("conceptscheme")
    CONCEPTSCHEME(PackageTypeCodelistType.CONCEPTSCHEME);
    private final PackageTypeCodelistType value;

    ItemSchemePackageTypeCodelistType(PackageTypeCodelistType v) {
        value = v;
    }

    public PackageTypeCodelistType value() {
        return value;
    }

    public static ItemSchemePackageTypeCodelistType fromValue(PackageTypeCodelistType v) {
        for (ItemSchemePackageTypeCodelistType c: ItemSchemePackageTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
