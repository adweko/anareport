
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ObjectReferenceType;


/**
 * InputOrOutputObjectType describes the structure of input or output condition for a process step query. It contains reference to an object, as will as an attribute indicates whether the object should be an input, output, or either of the two to the step.
 * 
 * <p>Java-Klasse f�r InputOrOutputObjectType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InputOrOutputObjectType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}InputOutputTypeCodeType" default="Any" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InputOrOutputObjectType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "objectReference"
})
public class InputOrOutputObjectType {

    @XmlElement(name = "ObjectReference", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected ObjectReferenceType objectReference;
    @XmlAttribute(name = "type")
    protected InputOutputTypeCodeType type;

    /**
     * Ruft den Wert der objectReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceType }
     *     
     */
    public ObjectReferenceType getObjectReference() {
        return objectReference;
    }

    /**
     * Legt den Wert der objectReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceType }
     *     
     */
    public void setObjectReference(ObjectReferenceType value) {
        this.objectReference = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InputOutputTypeCodeType }
     *     
     */
    public InputOutputTypeCodeType getType() {
        if (type == null) {
            return InputOutputTypeCodeType.ANY;
        } else {
            return type;
        }
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InputOutputTypeCodeType }
     *     
     */
    public void setType(InputOutputTypeCodeType value) {
        this.type = value;
    }

}
