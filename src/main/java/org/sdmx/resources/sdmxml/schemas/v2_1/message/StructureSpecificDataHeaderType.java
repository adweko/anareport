
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * StructureSpecificDataHeaderType defines the header structure for a structure specific data message.
 * 
 * <p>Java-Klasse f�r StructureSpecificDataHeaderType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureSpecificDataHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}BaseHeaderType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType"/>
 *         &lt;element name="Test" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Prepared" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}HeaderTimeType"/>
 *         &lt;element name="Sender" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}SenderType"/>
 *         &lt;element name="Receiver" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}PartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Name" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureSpecificDataStructureType" maxOccurs="unbounded"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0"/>
 *         &lt;element name="DataSetAction" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ActionType" minOccurs="0"/>
 *         &lt;element name="DataSetID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Extracted" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ReportingBegin" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationalTimePeriodType" minOccurs="0"/>
 *         &lt;element name="ReportingEnd" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationalTimePeriodType" minOccurs="0"/>
 *         &lt;element name="EmbargoDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureSpecificDataHeaderType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
@XmlSeeAlso({
    StructureSpecificTimeSeriesDataHeaderType.class
})
public class StructureSpecificDataHeaderType
    extends BaseHeaderType
{


}
