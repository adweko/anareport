
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * ReportingYearStartDayType defines the structure of the reporting year start day attribute. The reporting year start day attribute takes its semantic from its concept identity (usually the REPORTING_YEAR_START_DAY concept), yet is always has a fixed identifier (REPORTING_YEAR_START_DAY). The reporting year start day attribute always has a fixed text format, which specifies that the format of its value is always a day and month in the ISO 8601 format of '--MM-DD'. As with any other attribute, an attribute relationship must be specified. this relationship should be carefully selected as it will determin what type of data the data structure definition will allow. For example, if an attribute relationship of none is specified, this will mean the data sets conforming to this data structure definition can only contain data with standard reporting periods where the all reporting periods have the same start day. In this case, data reported as standard reporting periods from two entities with different fiscal year start days could not be contained in the same data set.
 * 
 * <p>Java-Klasse f�r ReportingYearStartDayType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReportingYearStartDayType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}AttributeType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element name="ConceptIdentity" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType"/>
 *         &lt;element name="LocalRepresentation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReportingYearStartDayRepresentationType"/>
 *         &lt;element name="AttributeRelationship" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}AttributeRelationshipType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NCNameIDType" fixed="REPORTING_YEAR_START_DAY" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingYearStartDayType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class ReportingYearStartDayType
    extends AttributeType
{


}
