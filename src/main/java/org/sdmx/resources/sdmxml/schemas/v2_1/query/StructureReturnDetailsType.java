
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * StructureReturnDetailsType defines the structure of the return details for any structural metadata query.
 * 
 * <p>Java-Klasse f�r StructureReturnDetailsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureReturnDetailsType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructureReturnDetailsBaseType">
 *       &lt;sequence>
 *         &lt;element name="References" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReferencesType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="returnMatchedArtefact" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureReturnDetailsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "references"
})
@XmlSeeAlso({
    MaintainableReturnDetailsType.class
})
public class StructureReturnDetailsType
    extends StructureReturnDetailsBaseType
{

    @XmlElement(name = "References", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected ReferencesType references;
    @XmlAttribute(name = "returnMatchedArtefact")
    protected Boolean returnMatchedArtefact;

    /**
     * Ruft den Wert der references-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencesType }
     *     
     */
    public ReferencesType getReferences() {
        return references;
    }

    /**
     * Legt den Wert der references-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencesType }
     *     
     */
    public void setReferences(ReferencesType value) {
        this.references = value;
    }

    /**
     * Ruft den Wert der returnMatchedArtefact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnMatchedArtefact() {
        if (returnMatchedArtefact == null) {
            return true;
        } else {
            return returnMatchedArtefact;
        }
    }

    /**
     * Legt den Wert der returnMatchedArtefact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnMatchedArtefact(Boolean value) {
        this.returnMatchedArtefact = value;
    }

}
