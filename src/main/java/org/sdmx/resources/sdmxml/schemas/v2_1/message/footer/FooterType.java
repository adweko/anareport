
package org.sdmx.resources.sdmxml.schemas.v2_1.message.footer;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * FooterType describes the structure of a message footer. The footer is used to convey any error, information, or warning messages. This is to be used when the message has payload, but also needs to communicate additional information. If an error occurs and no payload is generated, an Error message should be returned.
 * 
 * <p>Java-Klasse f�r FooterType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FooterType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Message" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer}FooterMessageType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FooterType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer", propOrder = {
    "message"
})
public class FooterType {

    @XmlElement(name = "Message", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer", required = true)
    protected List<FooterMessageType> message;

    /**
     * Gets the value of the message property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the message property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FooterMessageType }
     * 
     * 
     */
    public List<FooterMessageType> getMessage() {
        if (message == null) {
            message = new ArrayList<FooterMessageType>();
        }
        return this.message;
    }

}
