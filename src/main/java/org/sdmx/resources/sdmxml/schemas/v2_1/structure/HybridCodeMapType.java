
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnyLocalCodeReferenceType;


/**
 * CodeMapType defines the structure for associating a code from a source codelist to a code in a target codelist. Note that either of these may come from a hierarchical codelist.
 * 
 * <p>Java-Klasse f�r HybridCodeMapType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HybridCodeMapType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnyLocalCodeReferenceType"/>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnyLocalCodeReferenceType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HybridCodeMapType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "source",
    "target"
})
public class HybridCodeMapType
    extends AnnotableType
{

    @XmlElement(name = "Source", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected AnyLocalCodeReferenceType source;
    @XmlElement(name = "Target", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected AnyLocalCodeReferenceType target;

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnyLocalCodeReferenceType }
     *     
     */
    public AnyLocalCodeReferenceType getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnyLocalCodeReferenceType }
     *     
     */
    public void setSource(AnyLocalCodeReferenceType value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der target-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnyLocalCodeReferenceType }
     *     
     */
    public AnyLocalCodeReferenceType getTarget() {
        return target;
    }

    /**
     * Legt den Wert der target-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnyLocalCodeReferenceType }
     *     
     */
    public void setTarget(AnyLocalCodeReferenceType value) {
        this.target = value;
    }

}
