
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataflowQueryType defines the structure of a metadataflow query. The parameters for the query are contained in the MetadataflowWhere element. The References element is used to indicate how objects that reference or are referenced from the matched metadataflow should be returned.
 * 
 * <p>Java-Klasse f�r MetadataflowQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataflowQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructuralMetadataQueryType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableReturnDetailsType"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataflowWhere"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataflowQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class MetadataflowQueryType
    extends StructuralMetadataQueryType
{


}
