
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CategoryReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CategorySchemeReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ObjectReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ObjectTypeCodelistType;


/**
 * CategorisationWhereType contains a set of parameters for a categorisation query. All supplied parameters must be matched in order for an object to satisfy the query.
 * 
 * <p>Java-Klasse f�r CategorisationWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CategorisationWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}CategorisationWhereBaseType">
 *       &lt;sequence>
 *         &lt;element name="CategoryScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategorySchemeReferenceType" minOccurs="0"/>
 *         &lt;element name="TargetCategory" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryReferenceType" minOccurs="0"/>
 *         &lt;element name="ObjectReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType" minOccurs="0"/>
 *         &lt;element name="CategorisedObjectType" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategorisationWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "categoryScheme",
    "targetCategory",
    "objectReference",
    "categorisedObjectType"
})
public class CategorisationWhereType
    extends CategorisationWhereBaseType
{

    @XmlElement(name = "CategoryScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected CategorySchemeReferenceType categoryScheme;
    @XmlElement(name = "TargetCategory", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected CategoryReferenceType targetCategory;
    @XmlElement(name = "ObjectReference", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected ObjectReferenceType objectReference;
    @XmlElement(name = "CategorisedObjectType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    @XmlSchemaType(name = "string")
    protected List<ObjectTypeCodelistType> categorisedObjectType;

    /**
     * Ruft den Wert der categoryScheme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CategorySchemeReferenceType }
     *     
     */
    public CategorySchemeReferenceType getCategoryScheme() {
        return categoryScheme;
    }

    /**
     * Legt den Wert der categoryScheme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CategorySchemeReferenceType }
     *     
     */
    public void setCategoryScheme(CategorySchemeReferenceType value) {
        this.categoryScheme = value;
    }

    /**
     * Ruft den Wert der targetCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CategoryReferenceType }
     *     
     */
    public CategoryReferenceType getTargetCategory() {
        return targetCategory;
    }

    /**
     * Legt den Wert der targetCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryReferenceType }
     *     
     */
    public void setTargetCategory(CategoryReferenceType value) {
        this.targetCategory = value;
    }

    /**
     * Ruft den Wert der objectReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceType }
     *     
     */
    public ObjectReferenceType getObjectReference() {
        return objectReference;
    }

    /**
     * Legt den Wert der objectReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceType }
     *     
     */
    public void setObjectReference(ObjectReferenceType value) {
        this.objectReference = value;
    }

    /**
     * Gets the value of the categorisedObjectType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the categorisedObjectType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategorisedObjectType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectTypeCodelistType }
     * 
     * 
     */
    public List<ObjectTypeCodelistType> getCategorisedObjectType() {
        if (categorisedObjectType == null) {
            categorisedObjectType = new ArrayList<ObjectTypeCodelistType>();
        }
        return this.categorisedObjectType;
    }

}
