
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * MaintainableType is an abstract base type for all maintainable objects.
 * 
 * <p>Java-Klasse f�r MaintainableType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MaintainableType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MaintainableBaseType">
 *       &lt;attGroup ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ExternalReferenceAttributeGroup"/>
 *       &lt;attribute name="agencyID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedNCNameIDType" />
 *       &lt;attribute name="isFinal" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="isExternalReference" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintainableType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlSeeAlso({
    ProvisionAgreementType.class,
    ProcessType.class,
    StructureUsageType.class,
    CategorisationType.class,
    StructureSetBaseType.class,
    ConstraintBaseType.class,
    StructureType.class,
    ItemSchemeType.class,
    HierarchicalCodelistBaseType.class
})
public abstract class MaintainableType
    extends MaintainableBaseType
{

    @XmlAttribute(name = "agencyID", required = true)
    protected String agencyID;
    @XmlAttribute(name = "isFinal")
    protected Boolean isFinal;
    @XmlAttribute(name = "isExternalReference")
    protected Boolean isExternalReference;
    @XmlAttribute(name = "serviceURL")
    @XmlSchemaType(name = "anyURI")
    protected String serviceURL;
    @XmlAttribute(name = "structureURL")
    @XmlSchemaType(name = "anyURI")
    protected String structureURL;

    /**
     * Ruft den Wert der agencyID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyID() {
        return agencyID;
    }

    /**
     * Legt den Wert der agencyID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyID(String value) {
        this.agencyID = value;
    }

    /**
     * Ruft den Wert der isFinal-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsFinal() {
        if (isFinal == null) {
            return false;
        } else {
            return isFinal;
        }
    }

    /**
     * Legt den Wert der isFinal-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsFinal(Boolean value) {
        this.isFinal = value;
    }

    /**
     * Ruft den Wert der isExternalReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsExternalReference() {
        if (isExternalReference == null) {
            return false;
        } else {
            return isExternalReference;
        }
    }

    /**
     * Legt den Wert der isExternalReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsExternalReference(Boolean value) {
        this.isExternalReference = value;
    }

    /**
     * Ruft den Wert der serviceURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceURL() {
        return serviceURL;
    }

    /**
     * Legt den Wert der serviceURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceURL(String value) {
        this.serviceURL = value;
    }

    /**
     * Ruft den Wert der structureURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureURL() {
        return structureURL;
    }

    /**
     * Legt den Wert der structureURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureURL(String value) {
        this.structureURL = value;
    }

}
