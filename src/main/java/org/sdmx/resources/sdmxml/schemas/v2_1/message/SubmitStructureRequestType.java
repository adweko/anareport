
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * SubmitStructureRequestType defines the structure of a registry submit structure request document.
 * 
 * <p>Java-Klasse f�r SubmitStructureRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitStructureRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}RegistryInterfaceType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}BasicHeaderType"/>
 *         &lt;element name="SubmitStructureRequest" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmitStructureRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitStructureRequestType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
public class SubmitStructureRequestType
    extends RegistryInterfaceType
{


}
