
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DataParametersOrType refines the base data parameters to define a set of parameters joined by an "or" condition. Only one of the parameters supplied in an instance of this type can be satisfied to result in a match.
 * 
 * <p>Java-Klasse f�r DataParametersOrType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataParametersOrType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersType">
 *       &lt;sequence>
 *         &lt;element name="DataSetID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Dataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Updated" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="ConceptValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ConceptValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RepresentationValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}CodeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DimensionValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DimensionValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TimeDimensionValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeDimensionValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttributeValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AttributeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PrimaryMeasureValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}PrimaryMeasureValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraintReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TimeFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeDataType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="And" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersAndType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataParametersOrType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class DataParametersOrType
    extends DataParametersType
{


}
