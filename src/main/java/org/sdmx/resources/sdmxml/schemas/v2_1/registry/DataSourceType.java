
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * DataSourceType specifies the properties of a data or metadata source. Either a simple data source, a queryable data source, or both must be specified.
 * 
 * <p>Java-Klasse f�r DataSourceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataSourceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="2">
 *         &lt;element name="SimpleDataSource" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *         &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}QueryableDataSourceType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSourceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "simpleDataSourceOrQueryableDataSource"
})
public class DataSourceType {

    @XmlElements({
        @XmlElement(name = "SimpleDataSource", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = String.class),
        @XmlElement(name = "QueryableDataSource", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = QueryableDataSourceType.class)
    })
    protected List<Object> simpleDataSourceOrQueryableDataSource;

    /**
     * Gets the value of the simpleDataSourceOrQueryableDataSource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the simpleDataSourceOrQueryableDataSource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSimpleDataSourceOrQueryableDataSource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * {@link QueryableDataSourceType }
     * 
     * 
     */
    public List<Object> getSimpleDataSourceOrQueryableDataSource() {
        if (simpleDataSourceOrQueryableDataSource == null) {
            simpleDataSourceOrQueryableDataSource = new ArrayList<Object>();
        }
        return this.simpleDataSourceOrQueryableDataSource;
    }

}
