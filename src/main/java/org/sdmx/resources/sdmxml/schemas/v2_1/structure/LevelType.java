
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * LevelType describes a level in a hierarchical codelist. Where level is defined as a group where codes can be characterised by homogeneous coding, and where the parent of each code in the group is at the same higher level of the hierarchy.
 * 
 * <p>Java-Klasse f�r LevelType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LevelType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}LevelBaseType">
 *       &lt;sequence>
 *         &lt;element name="CodingFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodingTextFormatType" minOccurs="0"/>
 *         &lt;element name="Level" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}LevelType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LevelType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "codingFormat",
    "level"
})
public class LevelType
    extends LevelBaseType
{

    @XmlElement(name = "CodingFormat", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected CodingTextFormatType codingFormat;
    @XmlElement(name = "Level", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected LevelType level;

    /**
     * Ruft den Wert der codingFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodingTextFormatType }
     *     
     */
    public CodingTextFormatType getCodingFormat() {
        return codingFormat;
    }

    /**
     * Legt den Wert der codingFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodingTextFormatType }
     *     
     */
    public void setCodingFormat(CodingTextFormatType value) {
        this.codingFormat = value;
    }

    /**
     * Ruft den Wert der level-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LevelType }
     *     
     */
    public LevelType getLevel() {
        return level;
    }

    /**
     * Legt den Wert der level-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LevelType }
     *     
     */
    public void setLevel(LevelType value) {
        this.level = value;
    }

}
