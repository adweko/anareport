
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataTargetWhereType describes the structure that is used to query for metadata structure definitions containing a metadata target meeting the conditions detailed. Conditions include the identification and the details of the target objects which make up the metadata target.
 * 
 * <p>Java-Klasse f�r MetadataTargetWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataTargetWhereType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ComponentListWhereType">
 *       &lt;sequence>
 *         &lt;sequence>
 *           &lt;element name="Annotation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AnnotationWhereType" minOccurs="0"/>
 *           &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" minOccurs="0"/>
 *           &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *             &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TargetObjectWhere"/>
 *           &lt;/sequence>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataTargetWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class MetadataTargetWhereType
    extends ComponentListWhereType
{


}
