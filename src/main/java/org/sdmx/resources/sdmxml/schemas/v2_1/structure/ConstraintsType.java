
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * ConstraintsType describes the structure of the constraints container. It contains one or more constraint, which can be explicitly detailed or referenced from an external structure document or registry service. This container may contain both attachment and content constraints.
 * 
 * <p>Java-Klasse f�r ConstraintsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConstraintsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}AttachmentConstraintType"/>
 *         &lt;element name="ContentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ContentConstraintType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstraintsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "attachmentConstraintOrContentConstraint"
})
public class ConstraintsType {

    @XmlElements({
        @XmlElement(name = "AttachmentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = AttachmentConstraintType.class),
        @XmlElement(name = "ContentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = ContentConstraintType.class)
    })
    protected List<ConstraintType> attachmentConstraintOrContentConstraint;

    /**
     * Gets the value of the attachmentConstraintOrContentConstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachmentConstraintOrContentConstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachmentConstraintOrContentConstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttachmentConstraintType }
     * {@link ContentConstraintType }
     * 
     * 
     */
    public List<ConstraintType> getAttachmentConstraintOrContentConstraint() {
        if (attachmentConstraintOrContentConstraint == null) {
            attachmentConstraintOrContentConstraint = new ArrayList<ConstraintType>();
        }
        return this.attachmentConstraintOrContentConstraint;
    }

}
