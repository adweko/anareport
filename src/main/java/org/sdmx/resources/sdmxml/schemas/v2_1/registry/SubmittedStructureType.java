
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ActionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MaintainableReferenceType;


/**
 * SubmittedStructureType generally references a submitted structural object. When used in a submit structure request, its purpose is to override the default action or external dependency resolution behavior. If neither of these indicators are set, then it will be ignored. In a submit structure response, it is used to reference a submitted object for the purpose of providing a status for the submission. In this case, the action attribute should be populated in order to echo the requested action.
 * 
 * <p>Java-Klasse f�r SubmittedStructureType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmittedStructureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MaintainableObject" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableReferenceType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="action" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ActionType" />
 *       &lt;attribute name="externalDependencies" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmittedStructureType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "maintainableObject"
})
public class SubmittedStructureType {

    @XmlElement(name = "MaintainableObject", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected MaintainableReferenceType maintainableObject;
    @XmlAttribute(name = "action")
    protected ActionType action;
    @XmlAttribute(name = "externalDependencies")
    protected Boolean externalDependencies;

    /**
     * Ruft den Wert der maintainableObject-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaintainableReferenceType }
     *     
     */
    public MaintainableReferenceType getMaintainableObject() {
        return maintainableObject;
    }

    /**
     * Legt den Wert der maintainableObject-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainableReferenceType }
     *     
     */
    public void setMaintainableObject(MaintainableReferenceType value) {
        this.maintainableObject = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setAction(ActionType value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der externalDependencies-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExternalDependencies() {
        return externalDependencies;
    }

    /**
     * Legt den Wert der externalDependencies-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExternalDependencies(Boolean value) {
        this.externalDependencies = value;
    }

}
