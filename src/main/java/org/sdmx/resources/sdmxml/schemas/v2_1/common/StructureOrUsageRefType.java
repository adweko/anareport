
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * StructureOrUsageRefType is a type for referencing a structure or structure usage.
 * 
 * <p>Java-Klasse f�r StructureOrUsageRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureOrUsageRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureOrUsageRefBaseType">
 *       &lt;attribute name="class" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureOrUsageTypeCodelistType" />
 *       &lt;attribute name="package" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructurePackageTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureOrUsageRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class StructureOrUsageRefType
    extends StructureOrUsageRefBaseType
{


}
