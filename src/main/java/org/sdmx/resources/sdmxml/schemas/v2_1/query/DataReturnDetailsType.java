
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataStructureRequestType;


/**
 * DataReturnDetailsType specifies the specifics of the how data should be returned, including how it should be structured and how many and what type (e.g. active or deleted) observations should be returned.
 * 
 * <p>Java-Klasse f�r DataReturnDetailsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataReturnDetailsType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataReturnDetailsBaseType">
 *       &lt;sequence>
 *         &lt;element name="FirstNObservations" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LastNObservations" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureRequestType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="observationAction" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ObservationActionCodeType" default="Active" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataReturnDetailsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "firstNObservations",
    "lastNObservations",
    "structure"
})
@XmlSeeAlso({
    GenericDataReturnDetailsType.class,
    TimeSeriesDataReturnDetailsType.class
})
public class DataReturnDetailsType
    extends DataReturnDetailsBaseType
{

    @XmlElement(name = "FirstNObservations", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected Integer firstNObservations;
    @XmlElement(name = "LastNObservations", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected Integer lastNObservations;
    @XmlElement(name = "Structure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<DataStructureRequestType> structure;
    @XmlAttribute(name = "observationAction")
    protected ObservationActionCodeType observationAction;

    /**
     * Ruft den Wert der firstNObservations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFirstNObservations() {
        return firstNObservations;
    }

    /**
     * Legt den Wert der firstNObservations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFirstNObservations(Integer value) {
        this.firstNObservations = value;
    }

    /**
     * Ruft den Wert der lastNObservations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLastNObservations() {
        return lastNObservations;
    }

    /**
     * Legt den Wert der lastNObservations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLastNObservations(Integer value) {
        this.lastNObservations = value;
    }

    /**
     * Gets the value of the structure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the structure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataStructureRequestType }
     * 
     * 
     */
    public List<DataStructureRequestType> getStructure() {
        if (structure == null) {
            structure = new ArrayList<DataStructureRequestType>();
        }
        return this.structure;
    }

    /**
     * Ruft den Wert der observationAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObservationActionCodeType }
     *     
     */
    public ObservationActionCodeType getObservationAction() {
        if (observationAction == null) {
            return ObservationActionCodeType.ACTIVE;
        } else {
            return observationAction;
        }
    }

    /**
     * Legt den Wert der observationAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObservationActionCodeType }
     *     
     */
    public void setObservationAction(ObservationActionCodeType value) {
        this.observationAction = value;
    }

}
