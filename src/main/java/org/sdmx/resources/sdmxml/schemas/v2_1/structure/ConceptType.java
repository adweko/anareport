
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ConceptType describes the details of a concept. A concept is defined as a unit of knowledge created by a unique combination of characteristics. If a concept does not specify a TextFormat or a core representation, then the representation of the concept is assumed to be represented by any set of valid characters (corresponding to the xs:string datatype of W3C XML Schema).
 * 
 * <p>Java-Klasse f�r ConceptType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConceptType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConceptBaseType">
 *       &lt;sequence>
 *         &lt;element name="CoreRepresentation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConceptRepresentation" minOccurs="0"/>
 *         &lt;element name="ISOConceptReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ISOConceptReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConceptType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "coreRepresentation",
    "isoConceptReference"
})
public class ConceptType
    extends ConceptBaseType
{

    @XmlElement(name = "CoreRepresentation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ConceptRepresentation coreRepresentation;
    @XmlElement(name = "ISOConceptReference", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ISOConceptReferenceType isoConceptReference;

    /**
     * Ruft den Wert der coreRepresentation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConceptRepresentation }
     *     
     */
    public ConceptRepresentation getCoreRepresentation() {
        return coreRepresentation;
    }

    /**
     * Legt den Wert der coreRepresentation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptRepresentation }
     *     
     */
    public void setCoreRepresentation(ConceptRepresentation value) {
        this.coreRepresentation = value;
    }

    /**
     * Ruft den Wert der isoConceptReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ISOConceptReferenceType }
     *     
     */
    public ISOConceptReferenceType getISOConceptReference() {
        return isoConceptReference;
    }

    /**
     * Legt den Wert der isoConceptReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ISOConceptReferenceType }
     *     
     */
    public void setISOConceptReference(ISOConceptReferenceType value) {
        this.isoConceptReference = value;
    }

}
