
package org.sdmx.resources.sdmxml.schemas.v2_1.metadata.structurespecific;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AttachmentConstraintReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataKeyType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SetReferenceType;


/**
 * ReferenceValueType is an abstract base type that forms the basis of a target reference value. A target reference value will either be a reference to an identifiable object, a data key, a reference to a data set, or a report period. The choice of these options will be refined to only one according to the definition of the target in the metadata structure definition.
 * 
 * <p>Java-Klasse f�r ReferenceValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReferenceValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ObjectReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReferenceType" form="unqualified"/>
 *         &lt;element name="DataKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataKeyType" form="unqualified"/>
 *         &lt;element name="DataSetReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType" form="unqualified"/>
 *         &lt;element name="ConstraintContentReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraintReferenceType" form="unqualified"/>
 *         &lt;element name="ReportPeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationalTimePeriodType" form="unqualified"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific", propOrder = {
    "objectReference",
    "dataKey",
    "dataSetReference",
    "constraintContentReference",
    "reportPeriod"
})
public abstract class ReferenceValueType {

    @XmlElement(name = "ObjectReference")
    protected ReferenceType objectReference;
    @XmlElement(name = "DataKey")
    protected DataKeyType dataKey;
    @XmlElement(name = "DataSetReference")
    protected SetReferenceType dataSetReference;
    @XmlElement(name = "ConstraintContentReference")
    protected AttachmentConstraintReferenceType constraintContentReference;
    @XmlList
    @XmlElement(name = "ReportPeriod")
    protected List<String> reportPeriod;
    @XmlAttribute(name = "id")
    protected String id;

    /**
     * Ruft den Wert der objectReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceType }
     *     
     */
    public ReferenceType getObjectReference() {
        return objectReference;
    }

    /**
     * Legt den Wert der objectReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceType }
     *     
     */
    public void setObjectReference(ReferenceType value) {
        this.objectReference = value;
    }

    /**
     * Ruft den Wert der dataKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataKeyType }
     *     
     */
    public DataKeyType getDataKey() {
        return dataKey;
    }

    /**
     * Legt den Wert der dataKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataKeyType }
     *     
     */
    public void setDataKey(DataKeyType value) {
        this.dataKey = value;
    }

    /**
     * Ruft den Wert der dataSetReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SetReferenceType }
     *     
     */
    public SetReferenceType getDataSetReference() {
        return dataSetReference;
    }

    /**
     * Legt den Wert der dataSetReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SetReferenceType }
     *     
     */
    public void setDataSetReference(SetReferenceType value) {
        this.dataSetReference = value;
    }

    /**
     * Ruft den Wert der constraintContentReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentConstraintReferenceType }
     *     
     */
    public AttachmentConstraintReferenceType getConstraintContentReference() {
        return constraintContentReference;
    }

    /**
     * Legt den Wert der constraintContentReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentConstraintReferenceType }
     *     
     */
    public void setConstraintContentReference(AttachmentConstraintReferenceType value) {
        this.constraintContentReference = value;
    }

    /**
     * Gets the value of the reportPeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportPeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportPeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportPeriod() {
        if (reportPeriod == null) {
            reportPeriod = new ArrayList<String>();
        }
        return this.reportPeriod;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
