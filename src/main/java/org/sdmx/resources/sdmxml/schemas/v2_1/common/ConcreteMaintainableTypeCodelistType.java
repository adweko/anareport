
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ConcreteMaintainableTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ConcreteMaintainableTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableTypeCodelistType">
 *     &lt;enumeration value="AgencyScheme"/>
 *     &lt;enumeration value="AttachmentConstraint"/>
 *     &lt;enumeration value="Categorisation"/>
 *     &lt;enumeration value="CategoryScheme"/>
 *     &lt;enumeration value="Codelist"/>
 *     &lt;enumeration value="ConceptScheme"/>
 *     &lt;enumeration value="ContentConstraint"/>
 *     &lt;enumeration value="Dataflow"/>
 *     &lt;enumeration value="DataConsumerScheme"/>
 *     &lt;enumeration value="DataProviderScheme"/>
 *     &lt;enumeration value="DataStructure"/>
 *     &lt;enumeration value="HierarchicalCodelist"/>
 *     &lt;enumeration value="Metadataflow"/>
 *     &lt;enumeration value="MetadataStructure"/>
 *     &lt;enumeration value="OrganisationUnitScheme"/>
 *     &lt;enumeration value="Process"/>
 *     &lt;enumeration value="ProvisionAgreement"/>
 *     &lt;enumeration value="ReportingTaxonomy"/>
 *     &lt;enumeration value="StructureSet"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ConcreteMaintainableTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(MaintainableTypeCodelistType.class)
public enum ConcreteMaintainableTypeCodelistType {

    @XmlEnumValue("AgencyScheme")
    AGENCY_SCHEME(MaintainableTypeCodelistType.AGENCY_SCHEME),
    @XmlEnumValue("AttachmentConstraint")
    ATTACHMENT_CONSTRAINT(MaintainableTypeCodelistType.ATTACHMENT_CONSTRAINT),
    @XmlEnumValue("Categorisation")
    CATEGORISATION(MaintainableTypeCodelistType.CATEGORISATION),
    @XmlEnumValue("CategoryScheme")
    CATEGORY_SCHEME(MaintainableTypeCodelistType.CATEGORY_SCHEME),
    @XmlEnumValue("Codelist")
    CODELIST(MaintainableTypeCodelistType.CODELIST),
    @XmlEnumValue("ConceptScheme")
    CONCEPT_SCHEME(MaintainableTypeCodelistType.CONCEPT_SCHEME),
    @XmlEnumValue("ContentConstraint")
    CONTENT_CONSTRAINT(MaintainableTypeCodelistType.CONTENT_CONSTRAINT),
    @XmlEnumValue("Dataflow")
    DATAFLOW(MaintainableTypeCodelistType.DATAFLOW),
    @XmlEnumValue("DataConsumerScheme")
    DATA_CONSUMER_SCHEME(MaintainableTypeCodelistType.DATA_CONSUMER_SCHEME),
    @XmlEnumValue("DataProviderScheme")
    DATA_PROVIDER_SCHEME(MaintainableTypeCodelistType.DATA_PROVIDER_SCHEME),
    @XmlEnumValue("DataStructure")
    DATA_STRUCTURE(MaintainableTypeCodelistType.DATA_STRUCTURE),
    @XmlEnumValue("HierarchicalCodelist")
    HIERARCHICAL_CODELIST(MaintainableTypeCodelistType.HIERARCHICAL_CODELIST),
    @XmlEnumValue("Metadataflow")
    METADATAFLOW(MaintainableTypeCodelistType.METADATAFLOW),
    @XmlEnumValue("MetadataStructure")
    METADATA_STRUCTURE(MaintainableTypeCodelistType.METADATA_STRUCTURE),
    @XmlEnumValue("OrganisationUnitScheme")
    ORGANISATION_UNIT_SCHEME(MaintainableTypeCodelistType.ORGANISATION_UNIT_SCHEME),
    @XmlEnumValue("Process")
    PROCESS(MaintainableTypeCodelistType.PROCESS),
    @XmlEnumValue("ProvisionAgreement")
    PROVISION_AGREEMENT(MaintainableTypeCodelistType.PROVISION_AGREEMENT),
    @XmlEnumValue("ReportingTaxonomy")
    REPORTING_TAXONOMY(MaintainableTypeCodelistType.REPORTING_TAXONOMY),
    @XmlEnumValue("StructureSet")
    STRUCTURE_SET(MaintainableTypeCodelistType.STRUCTURE_SET);
    private final MaintainableTypeCodelistType value;

    ConcreteMaintainableTypeCodelistType(MaintainableTypeCodelistType v) {
        value = v;
    }

    public MaintainableTypeCodelistType value() {
        return value;
    }

    public static ConcreteMaintainableTypeCodelistType fromValue(MaintainableTypeCodelistType v) {
        for (ConcreteMaintainableTypeCodelistType c: ConcreteMaintainableTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
