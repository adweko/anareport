
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * GenericTimeSeriesDataQueryType defines the structure of a query for time series only data formatted in the generic format.
 * 
 * <p>Java-Klasse f�r GenericTimeSeriesDataQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GenericTimeSeriesDataQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}GenericDataQueryType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}GenericTimeSeriesDataReturnDetailsType"/>
 *         &lt;element name="DataWhere" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersAndType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericTimeSeriesDataQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class GenericTimeSeriesDataQueryType
    extends GenericDataQueryType
{


}
