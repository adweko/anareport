
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * OrganisationSchemeType describes the structure of an organisation scheme.
 * 
 * <p>Java-Klasse f�r OrganisationSchemeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrganisationSchemeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}OrganisationSchemeBaseType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Organisation"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationSchemeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "organisation"
})
@XmlSeeAlso({
    DataConsumerSchemeType.class,
    AgencySchemeType.class,
    OrganisationUnitSchemeType.class,
    DataProviderSchemeType.class
})
public abstract class OrganisationSchemeType
    extends OrganisationSchemeBaseType
{

    @XmlElementRef(name = "Organisation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends OrganisationType>> organisation;

    /**
     * Gets the value of the organisation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the organisation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrganisation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link DataProviderType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationUnitType }{@code >}
     * {@link JAXBElement }{@code <}{@link AgencyType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataConsumerType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends OrganisationType>> getOrganisation() {
        if (organisation == null) {
            organisation = new ArrayList<JAXBElement<? extends OrganisationType>>();
        }
        return this.organisation;
    }

}
