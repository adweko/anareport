
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r SimpleDataType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SimpleDataType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}BasicComponentDataType">
 *     &lt;enumeration value="String"/>
 *     &lt;enumeration value="Alpha"/>
 *     &lt;enumeration value="AlphaNumeric"/>
 *     &lt;enumeration value="Numeric"/>
 *     &lt;enumeration value="BigInteger"/>
 *     &lt;enumeration value="Integer"/>
 *     &lt;enumeration value="Long"/>
 *     &lt;enumeration value="Short"/>
 *     &lt;enumeration value="Decimal"/>
 *     &lt;enumeration value="Float"/>
 *     &lt;enumeration value="Double"/>
 *     &lt;enumeration value="Boolean"/>
 *     &lt;enumeration value="URI"/>
 *     &lt;enumeration value="Count"/>
 *     &lt;enumeration value="InclusiveValueRange"/>
 *     &lt;enumeration value="ExclusiveValueRange"/>
 *     &lt;enumeration value="Incremental"/>
 *     &lt;enumeration value="ObservationalTimePeriod"/>
 *     &lt;enumeration value="StandardTimePeriod"/>
 *     &lt;enumeration value="BasicTimePeriod"/>
 *     &lt;enumeration value="GregorianTimePeriod"/>
 *     &lt;enumeration value="GregorianYear"/>
 *     &lt;enumeration value="GregorianYearMonth"/>
 *     &lt;enumeration value="GregorianDay"/>
 *     &lt;enumeration value="ReportingTimePeriod"/>
 *     &lt;enumeration value="ReportingYear"/>
 *     &lt;enumeration value="ReportingSemester"/>
 *     &lt;enumeration value="ReportingTrimester"/>
 *     &lt;enumeration value="ReportingQuarter"/>
 *     &lt;enumeration value="ReportingMonth"/>
 *     &lt;enumeration value="ReportingWeek"/>
 *     &lt;enumeration value="ReportingDay"/>
 *     &lt;enumeration value="DateTime"/>
 *     &lt;enumeration value="TimeRange"/>
 *     &lt;enumeration value="Month"/>
 *     &lt;enumeration value="MonthDay"/>
 *     &lt;enumeration value="Day"/>
 *     &lt;enumeration value="Time"/>
 *     &lt;enumeration value="Duration"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SimpleDataType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(BasicComponentDataType.class)
public enum SimpleDataType {

    @XmlEnumValue("String")
    STRING(BasicComponentDataType.STRING),
    @XmlEnumValue("Alpha")
    ALPHA(BasicComponentDataType.ALPHA),
    @XmlEnumValue("AlphaNumeric")
    ALPHA_NUMERIC(BasicComponentDataType.ALPHA_NUMERIC),
    @XmlEnumValue("Numeric")
    NUMERIC(BasicComponentDataType.NUMERIC),
    @XmlEnumValue("BigInteger")
    BIG_INTEGER(BasicComponentDataType.BIG_INTEGER),
    @XmlEnumValue("Integer")
    INTEGER(BasicComponentDataType.INTEGER),
    @XmlEnumValue("Long")
    LONG(BasicComponentDataType.LONG),
    @XmlEnumValue("Short")
    SHORT(BasicComponentDataType.SHORT),
    @XmlEnumValue("Decimal")
    DECIMAL(BasicComponentDataType.DECIMAL),
    @XmlEnumValue("Float")
    FLOAT(BasicComponentDataType.FLOAT),
    @XmlEnumValue("Double")
    DOUBLE(BasicComponentDataType.DOUBLE),
    @XmlEnumValue("Boolean")
    BOOLEAN(BasicComponentDataType.BOOLEAN),
    URI(BasicComponentDataType.URI),
    @XmlEnumValue("Count")
    COUNT(BasicComponentDataType.COUNT),
    @XmlEnumValue("InclusiveValueRange")
    INCLUSIVE_VALUE_RANGE(BasicComponentDataType.INCLUSIVE_VALUE_RANGE),
    @XmlEnumValue("ExclusiveValueRange")
    EXCLUSIVE_VALUE_RANGE(BasicComponentDataType.EXCLUSIVE_VALUE_RANGE),
    @XmlEnumValue("Incremental")
    INCREMENTAL(BasicComponentDataType.INCREMENTAL),
    @XmlEnumValue("ObservationalTimePeriod")
    OBSERVATIONAL_TIME_PERIOD(BasicComponentDataType.OBSERVATIONAL_TIME_PERIOD),
    @XmlEnumValue("StandardTimePeriod")
    STANDARD_TIME_PERIOD(BasicComponentDataType.STANDARD_TIME_PERIOD),
    @XmlEnumValue("BasicTimePeriod")
    BASIC_TIME_PERIOD(BasicComponentDataType.BASIC_TIME_PERIOD),
    @XmlEnumValue("GregorianTimePeriod")
    GREGORIAN_TIME_PERIOD(BasicComponentDataType.GREGORIAN_TIME_PERIOD),
    @XmlEnumValue("GregorianYear")
    GREGORIAN_YEAR(BasicComponentDataType.GREGORIAN_YEAR),
    @XmlEnumValue("GregorianYearMonth")
    GREGORIAN_YEAR_MONTH(BasicComponentDataType.GREGORIAN_YEAR_MONTH),
    @XmlEnumValue("GregorianDay")
    GREGORIAN_DAY(BasicComponentDataType.GREGORIAN_DAY),
    @XmlEnumValue("ReportingTimePeriod")
    REPORTING_TIME_PERIOD(BasicComponentDataType.REPORTING_TIME_PERIOD),
    @XmlEnumValue("ReportingYear")
    REPORTING_YEAR(BasicComponentDataType.REPORTING_YEAR),
    @XmlEnumValue("ReportingSemester")
    REPORTING_SEMESTER(BasicComponentDataType.REPORTING_SEMESTER),
    @XmlEnumValue("ReportingTrimester")
    REPORTING_TRIMESTER(BasicComponentDataType.REPORTING_TRIMESTER),
    @XmlEnumValue("ReportingQuarter")
    REPORTING_QUARTER(BasicComponentDataType.REPORTING_QUARTER),
    @XmlEnumValue("ReportingMonth")
    REPORTING_MONTH(BasicComponentDataType.REPORTING_MONTH),
    @XmlEnumValue("ReportingWeek")
    REPORTING_WEEK(BasicComponentDataType.REPORTING_WEEK),
    @XmlEnumValue("ReportingDay")
    REPORTING_DAY(BasicComponentDataType.REPORTING_DAY),
    @XmlEnumValue("DateTime")
    DATE_TIME(BasicComponentDataType.DATE_TIME),
    @XmlEnumValue("TimeRange")
    TIME_RANGE(BasicComponentDataType.TIME_RANGE),
    @XmlEnumValue("Month")
    MONTH(BasicComponentDataType.MONTH),
    @XmlEnumValue("MonthDay")
    MONTH_DAY(BasicComponentDataType.MONTH_DAY),
    @XmlEnumValue("Day")
    DAY(BasicComponentDataType.DAY),
    @XmlEnumValue("Time")
    TIME(BasicComponentDataType.TIME),
    @XmlEnumValue("Duration")
    DURATION(BasicComponentDataType.DURATION);
    private final BasicComponentDataType value;

    SimpleDataType(BasicComponentDataType v) {
        value = v;
    }

    public BasicComponentDataType value() {
        return value;
    }

    public static SimpleDataType fromValue(BasicComponentDataType v) {
        for (SimpleDataType c: SimpleDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
