
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SimpleDataType;


/**
 * <p>Java-Klasse f�r CodeDataType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="CodeDataType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleDataType">
 *     &lt;enumeration value="String"/>
 *     &lt;enumeration value="Alpha"/>
 *     &lt;enumeration value="AlphaNumeric"/>
 *     &lt;enumeration value="Numeric"/>
 *     &lt;enumeration value="BigInteger"/>
 *     &lt;enumeration value="Integer"/>
 *     &lt;enumeration value="Long"/>
 *     &lt;enumeration value="Short"/>
 *     &lt;enumeration value="Boolean"/>
 *     &lt;enumeration value="URI"/>
 *     &lt;enumeration value="Count"/>
 *     &lt;enumeration value="InclusiveValueRange"/>
 *     &lt;enumeration value="ExclusiveValueRange"/>
 *     &lt;enumeration value="Incremental"/>
 *     &lt;enumeration value="ObservationalTimePeriod"/>
 *     &lt;enumeration value="StandardTimePeriod"/>
 *     &lt;enumeration value="BasicTimePeriod"/>
 *     &lt;enumeration value="GregorianTimePeriod"/>
 *     &lt;enumeration value="GregorianYear"/>
 *     &lt;enumeration value="GregorianYearMonth"/>
 *     &lt;enumeration value="GregorianDay"/>
 *     &lt;enumeration value="ReportingTimePeriod"/>
 *     &lt;enumeration value="ReportingYear"/>
 *     &lt;enumeration value="ReportingSemester"/>
 *     &lt;enumeration value="ReportingTrimester"/>
 *     &lt;enumeration value="ReportingQuarter"/>
 *     &lt;enumeration value="ReportingMonth"/>
 *     &lt;enumeration value="ReportingWeek"/>
 *     &lt;enumeration value="ReportingDay"/>
 *     &lt;enumeration value="Month"/>
 *     &lt;enumeration value="MonthDay"/>
 *     &lt;enumeration value="Day"/>
 *     &lt;enumeration value="Duration"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CodeDataType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlEnum(SimpleDataType.class)
public enum CodeDataType {

    @XmlEnumValue("String")
    STRING(SimpleDataType.STRING),
    @XmlEnumValue("Alpha")
    ALPHA(SimpleDataType.ALPHA),
    @XmlEnumValue("AlphaNumeric")
    ALPHA_NUMERIC(SimpleDataType.ALPHA_NUMERIC),
    @XmlEnumValue("Numeric")
    NUMERIC(SimpleDataType.NUMERIC),
    @XmlEnumValue("BigInteger")
    BIG_INTEGER(SimpleDataType.BIG_INTEGER),
    @XmlEnumValue("Integer")
    INTEGER(SimpleDataType.INTEGER),
    @XmlEnumValue("Long")
    LONG(SimpleDataType.LONG),
    @XmlEnumValue("Short")
    SHORT(SimpleDataType.SHORT),
    @XmlEnumValue("Boolean")
    BOOLEAN(SimpleDataType.BOOLEAN),
    URI(SimpleDataType.URI),
    @XmlEnumValue("Count")
    COUNT(SimpleDataType.COUNT),
    @XmlEnumValue("InclusiveValueRange")
    INCLUSIVE_VALUE_RANGE(SimpleDataType.INCLUSIVE_VALUE_RANGE),
    @XmlEnumValue("ExclusiveValueRange")
    EXCLUSIVE_VALUE_RANGE(SimpleDataType.EXCLUSIVE_VALUE_RANGE),
    @XmlEnumValue("Incremental")
    INCREMENTAL(SimpleDataType.INCREMENTAL),
    @XmlEnumValue("ObservationalTimePeriod")
    OBSERVATIONAL_TIME_PERIOD(SimpleDataType.OBSERVATIONAL_TIME_PERIOD),
    @XmlEnumValue("StandardTimePeriod")
    STANDARD_TIME_PERIOD(SimpleDataType.STANDARD_TIME_PERIOD),
    @XmlEnumValue("BasicTimePeriod")
    BASIC_TIME_PERIOD(SimpleDataType.BASIC_TIME_PERIOD),
    @XmlEnumValue("GregorianTimePeriod")
    GREGORIAN_TIME_PERIOD(SimpleDataType.GREGORIAN_TIME_PERIOD),
    @XmlEnumValue("GregorianYear")
    GREGORIAN_YEAR(SimpleDataType.GREGORIAN_YEAR),
    @XmlEnumValue("GregorianYearMonth")
    GREGORIAN_YEAR_MONTH(SimpleDataType.GREGORIAN_YEAR_MONTH),
    @XmlEnumValue("GregorianDay")
    GREGORIAN_DAY(SimpleDataType.GREGORIAN_DAY),
    @XmlEnumValue("ReportingTimePeriod")
    REPORTING_TIME_PERIOD(SimpleDataType.REPORTING_TIME_PERIOD),
    @XmlEnumValue("ReportingYear")
    REPORTING_YEAR(SimpleDataType.REPORTING_YEAR),
    @XmlEnumValue("ReportingSemester")
    REPORTING_SEMESTER(SimpleDataType.REPORTING_SEMESTER),
    @XmlEnumValue("ReportingTrimester")
    REPORTING_TRIMESTER(SimpleDataType.REPORTING_TRIMESTER),
    @XmlEnumValue("ReportingQuarter")
    REPORTING_QUARTER(SimpleDataType.REPORTING_QUARTER),
    @XmlEnumValue("ReportingMonth")
    REPORTING_MONTH(SimpleDataType.REPORTING_MONTH),
    @XmlEnumValue("ReportingWeek")
    REPORTING_WEEK(SimpleDataType.REPORTING_WEEK),
    @XmlEnumValue("ReportingDay")
    REPORTING_DAY(SimpleDataType.REPORTING_DAY),
    @XmlEnumValue("Month")
    MONTH(SimpleDataType.MONTH),
    @XmlEnumValue("MonthDay")
    MONTH_DAY(SimpleDataType.MONTH_DAY),
    @XmlEnumValue("Day")
    DAY(SimpleDataType.DAY),
    @XmlEnumValue("Duration")
    DURATION(SimpleDataType.DURATION);
    private final SimpleDataType value;

    CodeDataType(SimpleDataType v) {
        value = v;
    }

    public SimpleDataType value() {
        return value;
    }

    public static CodeDataType fromValue(SimpleDataType v) {
        for (CodeDataType c: CodeDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
