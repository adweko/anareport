
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ObservationActionCodeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ObservationActionCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Active"/>
 *     &lt;enumeration value="Added"/>
 *     &lt;enumeration value="Updated"/>
 *     &lt;enumeration value="Deleted"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ObservationActionCodeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum
public enum ObservationActionCodeType {


    /**
     * Active observations, regardless of when they were added or updated will be returned.
     * 
     */
    @XmlEnumValue("Active")
    ACTIVE("Active"),

    /**
     * Only newly added observations will be returned.
     * 
     */
    @XmlEnumValue("Added")
    ADDED("Added"),

    /**
     * Only updated observations will be returned.
     * 
     */
    @XmlEnumValue("Updated")
    UPDATED("Updated"),

    /**
     * Only deleted observations will be returned.
     * 
     */
    @XmlEnumValue("Deleted")
    DELETED("Deleted");
    private final String value;

    ObservationActionCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ObservationActionCodeType fromValue(String v) {
        for (ObservationActionCodeType c: ObservationActionCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
