
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ObjectReferenceType;


/**
 * InputOutputType describes the structure of an input or output to a process step. It provides a reference to the object that is the input or output.
 * 
 * <p>Java-Klasse f�r InputOutputType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InputOutputType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="ObjectReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="localID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InputOutputType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "objectReference"
})
public class InputOutputType
    extends AnnotableType
{

    @XmlElement(name = "ObjectReference", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected ObjectReferenceType objectReference;
    @XmlAttribute(name = "localID")
    protected String localID;

    /**
     * Ruft den Wert der objectReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceType }
     *     
     */
    public ObjectReferenceType getObjectReference() {
        return objectReference;
    }

    /**
     * Legt den Wert der objectReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceType }
     *     
     */
    public void setObjectReference(ObjectReferenceType value) {
        this.objectReference = value;
    }

    /**
     * Ruft den Wert der localID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalID() {
        return localID;
    }

    /**
     * Legt den Wert der localID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalID(String value) {
        this.localID = value;
    }

}
