
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MeasureWhereType describes the structure of a measure query. The primary measure can be queried based on the concept from which it takes its semantic, and the code list it uses as the enumeration of its representation. This is an implicit set of "and" parameters, that is the conditions within this must all be met in order to return a match.
 * 
 * <p>Java-Klasse f�r PrimaryMeasureWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PrimaryMeasureWhereType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataStructureComponentWhereType">
 *       &lt;sequence>
 *         &lt;element name="Annotation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AnnotationWhereType" minOccurs="0"/>
 *         &lt;element name="ConceptIdentity" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" minOccurs="0"/>
 *         &lt;element name="Enumeration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrimaryMeasureWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class PrimaryMeasureWhereType
    extends DataStructureComponentWhereType
{


}
