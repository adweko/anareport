
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * AnnotationType provides for non-documentation notes and annotations to be embedded in data and structure messages. It provides optional fields for providing a title, a type description, a URI, and the text of the annotation.
 * 
 * <p>Java-Klasse f�r AnnotationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AnnotationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AnnotationTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AnnotationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AnnotationURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *         &lt;element name="AnnotationText" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnnotationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", propOrder = {
    "annotationTitle",
    "annotationType",
    "annotationURL",
    "annotationText"
})
public class AnnotationType {

    @XmlElement(name = "AnnotationTitle", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected String annotationTitle;
    @XmlElement(name = "AnnotationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected String annotationType;
    @XmlElement(name = "AnnotationURL", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    @XmlSchemaType(name = "anyURI")
    protected String annotationURL;
    @XmlElement(name = "AnnotationText", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<TextType> annotationText;
    @XmlAttribute(name = "id")
    protected String id;

    /**
     * Ruft den Wert der annotationTitle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationTitle() {
        return annotationTitle;
    }

    /**
     * Legt den Wert der annotationTitle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationTitle(String value) {
        this.annotationTitle = value;
    }

    /**
     * Ruft den Wert der annotationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationType() {
        return annotationType;
    }

    /**
     * Legt den Wert der annotationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationType(String value) {
        this.annotationType = value;
    }

    /**
     * Ruft den Wert der annotationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationURL() {
        return annotationURL;
    }

    /**
     * Legt den Wert der annotationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationURL(String value) {
        this.annotationURL = value;
    }

    /**
     * Gets the value of the annotationText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the annotationText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnnotationText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getAnnotationText() {
        if (annotationText == null) {
            annotationText = new ArrayList<TextType>();
        }
        return this.annotationText;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
