
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r PackageTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PackageTypeCodelistType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="base"/>
 *     &lt;enumeration value="datastructure"/>
 *     &lt;enumeration value="metadatastructure"/>
 *     &lt;enumeration value="process"/>
 *     &lt;enumeration value="registry"/>
 *     &lt;enumeration value="mapping"/>
 *     &lt;enumeration value="codelist"/>
 *     &lt;enumeration value="categoryscheme"/>
 *     &lt;enumeration value="conceptscheme"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PackageTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum PackageTypeCodelistType {

    @XmlEnumValue("base")
    BASE("base"),
    @XmlEnumValue("datastructure")
    DATASTRUCTURE("datastructure"),
    @XmlEnumValue("metadatastructure")
    METADATASTRUCTURE("metadatastructure"),
    @XmlEnumValue("process")
    PROCESS("process"),
    @XmlEnumValue("registry")
    REGISTRY("registry"),
    @XmlEnumValue("mapping")
    MAPPING("mapping"),
    @XmlEnumValue("codelist")
    CODELIST("codelist"),
    @XmlEnumValue("categoryscheme")
    CATEGORYSCHEME("categoryscheme"),
    @XmlEnumValue("conceptscheme")
    CONCEPTSCHEME("conceptscheme");
    private final String value;

    PackageTypeCodelistType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PackageTypeCodelistType fromValue(String v) {
        for (PackageTypeCodelistType c: PackageTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
