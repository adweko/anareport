
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * AgencyType defines the structure of an agency description. The contacts defined for the organisation are specific to the agency role the organisation is serving.
 * 
 * <p>Java-Klasse f�r AgencyType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AgencyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}OrganisationType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Name" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Description" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Contact" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ContactType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NCNameIDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgencyType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class AgencyType
    extends OrganisationType
{


}
