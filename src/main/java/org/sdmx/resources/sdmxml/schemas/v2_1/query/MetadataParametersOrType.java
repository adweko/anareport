
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataParametersOrType refines the base data parameters to define a set of parameters joined by an "or" condition. Only one of the parameters supplied in an instance of this type can be satisfied to result in a match.
 * 
 * <p>Java-Klasse f�r MetadataParametersOrType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataParametersOrType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataParametersType">
 *       &lt;sequence>
 *         &lt;element name="MetadataSetID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MetadataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataStructureReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Metadataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataflowReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Updated" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ConceptValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ConceptValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RepresentationValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}CodeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MetadataTargetValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataTargetValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReportStructureValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReportStructureValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraintReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachedObject" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachedDataKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataKeyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachedDataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachedReportingPeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="And" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataParametersAndType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataParametersOrType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class MetadataParametersOrType
    extends MetadataParametersType
{


}
