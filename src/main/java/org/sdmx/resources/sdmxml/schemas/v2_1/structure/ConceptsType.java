
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ConceptsType describes the structure of the concepts container. It contains one or more stand-alone concept or concept scheme, which can be explicitly detailed or referenced from an external structure document or registry service. This container may contain a mix of both stand-alone concepts and concept schemes.
 * 
 * <p>Java-Klasse f�r ConceptsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConceptsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConceptScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConceptSchemeType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConceptsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "conceptScheme"
})
public class ConceptsType {

    @XmlElement(name = "ConceptScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<ConceptSchemeType> conceptScheme;

    /**
     * Gets the value of the conceptScheme property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conceptScheme property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConceptScheme().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConceptSchemeType }
     * 
     * 
     */
    public List<ConceptSchemeType> getConceptScheme() {
        if (conceptScheme == null) {
            conceptScheme = new ArrayList<ConceptSchemeType>();
        }
        return this.conceptScheme;
    }

}
