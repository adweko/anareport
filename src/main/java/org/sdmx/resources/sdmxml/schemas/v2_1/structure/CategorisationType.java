
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CategoryReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ObjectReferenceType;


/**
 * CategorisationType is defines the structure for a categorisation. A source object is referenced via an object reference and the target category is referenced via the target category.
 * 
 * <p>Java-Klasse f�r CategorisationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CategorisationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MaintainableType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType"/>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryReferenceType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategorisationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "source",
    "target"
})
public class CategorisationType
    extends MaintainableType
{

    @XmlElement(name = "Source", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ObjectReferenceType source;
    @XmlElement(name = "Target", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected CategoryReferenceType target;

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceType }
     *     
     */
    public ObjectReferenceType getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceType }
     *     
     */
    public void setSource(ObjectReferenceType value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der target-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CategoryReferenceType }
     *     
     */
    public CategoryReferenceType getTarget() {
        return target;
    }

    /**
     * Legt den Wert der target-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryReferenceType }
     *     
     */
    public void setTarget(CategoryReferenceType value) {
        this.target = value;
    }

}
