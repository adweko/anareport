
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * HierarchicalCodeRefType references a code from within a hierarchical codelist. Reference fields are required for both the code and the codelist.
 * 
 * <p>Java-Klasse f�r HierarchicalCodeRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HierarchicalCodeRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ContainerChildObjectRefBaseType">
 *       &lt;attribute name="maintainableParentID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="maintainableParentVersion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionType" default="1.0" />
 *       &lt;attribute name="containerID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" fixed="HierarchicalCode" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PackageTypeCodelistType" fixed="codelist" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HierarchicalCodeRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class HierarchicalCodeRefType
    extends ContainerChildObjectRefBaseType
{


}
