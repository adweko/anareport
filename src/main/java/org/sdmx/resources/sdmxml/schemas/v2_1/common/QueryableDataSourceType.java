
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * QueryableDataSourceType describes a data source which is accepts an standard SDMX Query message and responds appropriately.
 * 
 * <p>Java-Klasse f�r QueryableDataSourceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="QueryableDataSourceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataURL" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *         &lt;element name="WSDLURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *         &lt;element name="WADLURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="isRESTDatasource" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isWebServiceDatasource" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryableDataSourceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", propOrder = {
    "dataURL",
    "wsdlurl",
    "wadlurl"
})
@XmlSeeAlso({
    org.sdmx.resources.sdmxml.schemas.v2_1.registry.QueryableDataSourceType.class
})
public class QueryableDataSourceType {

    @XmlElement(name = "DataURL", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", required = true)
    @XmlSchemaType(name = "anyURI")
    protected String dataURL;
    @XmlElement(name = "WSDLURL", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    @XmlSchemaType(name = "anyURI")
    protected String wsdlurl;
    @XmlElement(name = "WADLURL", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    @XmlSchemaType(name = "anyURI")
    protected String wadlurl;
    @XmlAttribute(name = "isRESTDatasource", required = true)
    protected boolean isRESTDatasource;
    @XmlAttribute(name = "isWebServiceDatasource", required = true)
    protected boolean isWebServiceDatasource;

    /**
     * Ruft den Wert der dataURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataURL() {
        return dataURL;
    }

    /**
     * Legt den Wert der dataURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataURL(String value) {
        this.dataURL = value;
    }

    /**
     * Ruft den Wert der wsdlurl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWSDLURL() {
        return wsdlurl;
    }

    /**
     * Legt den Wert der wsdlurl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWSDLURL(String value) {
        this.wsdlurl = value;
    }

    /**
     * Ruft den Wert der wadlurl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWADLURL() {
        return wadlurl;
    }

    /**
     * Legt den Wert der wadlurl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWADLURL(String value) {
        this.wadlurl = value;
    }

    /**
     * Ruft den Wert der isRESTDatasource-Eigenschaft ab.
     * 
     */
    public boolean isIsRESTDatasource() {
        return isRESTDatasource;
    }

    /**
     * Legt den Wert der isRESTDatasource-Eigenschaft fest.
     * 
     */
    public void setIsRESTDatasource(boolean value) {
        this.isRESTDatasource = value;
    }

    /**
     * Ruft den Wert der isWebServiceDatasource-Eigenschaft ab.
     * 
     */
    public boolean isIsWebServiceDatasource() {
        return isWebServiceDatasource;
    }

    /**
     * Legt den Wert der isWebServiceDatasource-Eigenschaft fest.
     * 
     */
    public void setIsWebServiceDatasource(boolean value) {
        this.isWebServiceDatasource = value;
    }

}
