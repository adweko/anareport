
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r DimensionTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="DimensionTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentTypeCodelistType">
 *     &lt;enumeration value="Dimension"/>
 *     &lt;enumeration value="MeasureDimension"/>
 *     &lt;enumeration value="TimeDimension"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DimensionTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ComponentTypeCodelistType.class)
public enum DimensionTypeCodelistType {

    @XmlEnumValue("Dimension")
    DIMENSION(ComponentTypeCodelistType.DIMENSION),
    @XmlEnumValue("MeasureDimension")
    MEASURE_DIMENSION(ComponentTypeCodelistType.MEASURE_DIMENSION),
    @XmlEnumValue("TimeDimension")
    TIME_DIMENSION(ComponentTypeCodelistType.TIME_DIMENSION);
    private final ComponentTypeCodelistType value;

    DimensionTypeCodelistType(ComponentTypeCodelistType v) {
        value = v;
    }

    public ComponentTypeCodelistType value() {
        return value;
    }

    public static DimensionTypeCodelistType fromValue(ComponentTypeCodelistType v) {
        for (DimensionTypeCodelistType c: DimensionTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
