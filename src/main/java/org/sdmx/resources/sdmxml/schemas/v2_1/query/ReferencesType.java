
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.EmptyType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MaintainableObjectTypeListType;


/**
 * ReferencesType defines the structure for indicating which referenced objects should be returned in a structural metadata query. It is possible to return both objects which reference the object(s) matched by the query and objects referenced from the match object(s). The type(s) of reference objects to be returned consists of a choice between None, All, Default, or an explicit list of object types.
 * 
 * <p>Java-Klasse f�r ReferencesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReferencesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="None" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;element name="All" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;element name="Parents" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;element name="ParentsAndSiblings" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;element name="Children" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;element name="Descendants" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;element name="SpecificObjects" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableObjectTypeListType"/>
 *       &lt;/choice>
 *       &lt;attribute name="processConstraints" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="detail" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableReturnDetailType" default="Full" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferencesType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "none",
    "all",
    "parents",
    "parentsAndSiblings",
    "children",
    "descendants",
    "specificObjects"
})
public class ReferencesType {

    @XmlElement(name = "None", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected EmptyType none;
    @XmlElement(name = "All", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected EmptyType all;
    @XmlElement(name = "Parents", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected EmptyType parents;
    @XmlElement(name = "ParentsAndSiblings", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected EmptyType parentsAndSiblings;
    @XmlElement(name = "Children", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected EmptyType children;
    @XmlElement(name = "Descendants", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected EmptyType descendants;
    @XmlElement(name = "SpecificObjects", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected MaintainableObjectTypeListType specificObjects;
    @XmlAttribute(name = "processConstraints")
    protected Boolean processConstraints;
    @XmlAttribute(name = "detail")
    protected MaintainableReturnDetailType detail;

    /**
     * Ruft den Wert der none-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getNone() {
        return none;
    }

    /**
     * Legt den Wert der none-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setNone(EmptyType value) {
        this.none = value;
    }

    /**
     * Ruft den Wert der all-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAll() {
        return all;
    }

    /**
     * Legt den Wert der all-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAll(EmptyType value) {
        this.all = value;
    }

    /**
     * Ruft den Wert der parents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getParents() {
        return parents;
    }

    /**
     * Legt den Wert der parents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setParents(EmptyType value) {
        this.parents = value;
    }

    /**
     * Ruft den Wert der parentsAndSiblings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getParentsAndSiblings() {
        return parentsAndSiblings;
    }

    /**
     * Legt den Wert der parentsAndSiblings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setParentsAndSiblings(EmptyType value) {
        this.parentsAndSiblings = value;
    }

    /**
     * Ruft den Wert der children-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getChildren() {
        return children;
    }

    /**
     * Legt den Wert der children-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setChildren(EmptyType value) {
        this.children = value;
    }

    /**
     * Ruft den Wert der descendants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDescendants() {
        return descendants;
    }

    /**
     * Legt den Wert der descendants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDescendants(EmptyType value) {
        this.descendants = value;
    }

    /**
     * Ruft den Wert der specificObjects-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaintainableObjectTypeListType }
     *     
     */
    public MaintainableObjectTypeListType getSpecificObjects() {
        return specificObjects;
    }

    /**
     * Legt den Wert der specificObjects-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainableObjectTypeListType }
     *     
     */
    public void setSpecificObjects(MaintainableObjectTypeListType value) {
        this.specificObjects = value;
    }

    /**
     * Ruft den Wert der processConstraints-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProcessConstraints() {
        if (processConstraints == null) {
            return false;
        } else {
            return processConstraints;
        }
    }

    /**
     * Legt den Wert der processConstraints-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessConstraints(Boolean value) {
        this.processConstraints = value;
    }

    /**
     * Ruft den Wert der detail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaintainableReturnDetailType }
     *     
     */
    public MaintainableReturnDetailType getDetail() {
        if (detail == null) {
            return MaintainableReturnDetailType.FULL;
        } else {
            return detail;
        }
    }

    /**
     * Legt den Wert der detail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainableReturnDetailType }
     *     
     */
    public void setDetail(MaintainableReturnDetailType value) {
        this.detail = value;
    }

}
