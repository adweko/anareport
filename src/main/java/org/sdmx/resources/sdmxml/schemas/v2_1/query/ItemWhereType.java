
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalItemReferenceType;


/**
 * ItemQueryType is an abstract base type that serves as the basis for a query for an item within an item scheme query. A nested item where is provided to query for items nested within other items. The conditions within an item query are implied to be in an and-query. If an id and a child item where condition are supplied, then both conditions will have to met in order for the item query to return true. If, for instance, a query based on names in multiple languages is required, then multiple instances of the element utilizing this type should be used within an or-query container.
 * 
 * <p>Java-Klasse f�r ItemWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ItemWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}NameableWhereType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="Parent" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalItemReferenceType"/>
 *         &lt;sequence maxOccurs="unbounded">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ItemWhere"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "parent",
    "itemWhere"
})
@XmlSeeAlso({
    OrganisationWhereType.class,
    CodeWhereType.class,
    CategoryWhereType.class,
    ReportingCategoryWhereBaseType.class,
    ConceptWhereBaseType.class
})
public abstract class ItemWhereType
    extends NameableWhereType
{

    @XmlElement(name = "Parent", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected LocalItemReferenceType parent;
    @XmlElementRef(name = "ItemWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ItemWhereType>> itemWhere;

    /**
     * Ruft den Wert der parent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalItemReferenceType }
     *     
     */
    public LocalItemReferenceType getParent() {
        return parent;
    }

    /**
     * Legt den Wert der parent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalItemReferenceType }
     *     
     */
    public void setParent(LocalItemReferenceType value) {
        this.parent = value;
    }

    /**
     * Gets the value of the itemWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CategoryWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link CodeWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ConceptWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ItemWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportingCategoryWhereType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ItemWhereType>> getItemWhere() {
        if (itemWhere == null) {
            itemWhere = new ArrayList<JAXBElement<? extends ItemWhereType>>();
        }
        return this.itemWhere;
    }

}
