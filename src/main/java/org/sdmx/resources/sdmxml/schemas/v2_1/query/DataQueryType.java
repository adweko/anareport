
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * DataQueryType defines the structure of a query for data. This is generally appliable for any data request, but can be refined depending on the type of data being queried (generic or structured, time series specific or not) to the requirements of the requested format.
 * 
 * <p>Java-Klasse f�r DataQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataReturnDetailsType"/>
 *         &lt;element name="DataWhere" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersAndType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "returnDetails",
    "dataWhere"
})
@XmlSeeAlso({
    TimeSeriesDataQueryType.class,
    GenericDataQueryType.class
})
public class DataQueryType {

    @XmlElement(name = "ReturnDetails", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected DataReturnDetailsType returnDetails;
    @XmlElement(name = "DataWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected DataParametersAndType dataWhere;

    /**
     * Ruft den Wert der returnDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataReturnDetailsType }
     *     
     */
    public DataReturnDetailsType getReturnDetails() {
        return returnDetails;
    }

    /**
     * Legt den Wert der returnDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataReturnDetailsType }
     *     
     */
    public void setReturnDetails(DataReturnDetailsType value) {
        this.returnDetails = value;
    }

    /**
     * Ruft den Wert der dataWhere-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataParametersAndType }
     *     
     */
    public DataParametersAndType getDataWhere() {
        return dataWhere;
    }

    /**
     * Legt den Wert der dataWhere-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataParametersAndType }
     *     
     */
    public void setDataWhere(DataParametersAndType value) {
        this.dataWhere = value;
    }

}
