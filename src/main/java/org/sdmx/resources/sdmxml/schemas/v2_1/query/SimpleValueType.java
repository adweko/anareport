
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SimpleOperatorType;


/**
 * SimpleValueType describes the structure of a simple value query. A value is provided as the content in string format.
 * 
 * <p>Java-Klasse f�r SimpleValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SimpleValueType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="operator" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleOperatorType" default="equal" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "value"
})
public class SimpleValueType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "operator")
    protected SimpleOperatorType operator;

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Ruft den Wert der operator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SimpleOperatorType }
     *     
     */
    public SimpleOperatorType getOperator() {
        if (operator == null) {
            return SimpleOperatorType.EQUAL;
        } else {
            return operator;
        }
    }

    /**
     * Legt den Wert der operator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleOperatorType }
     *     
     */
    public void setOperator(SimpleOperatorType value) {
        this.operator = value;
    }

}
