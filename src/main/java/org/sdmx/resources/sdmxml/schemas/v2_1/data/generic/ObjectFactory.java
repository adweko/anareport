
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.sdmx.resources.sdmxml.schemas.v2_1.data.generic package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.sdmx.resources.sdmxml.schemas.v2_1.data.generic
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BaseValueType }
     * 
     */
    public BaseValueType createBaseValueType() {
        return new BaseValueType();
    }

    /**
     * Create an instance of {@link ObsValueType }
     * 
     */
    public ObsValueType createObsValueType() {
        return new ObsValueType();
    }

    /**
     * Create an instance of {@link DataSetType }
     * 
     */
    public DataSetType createDataSetType() {
        return new DataSetType();
    }

    /**
     * Create an instance of {@link TimeSeriesType }
     * 
     */
    public TimeSeriesType createTimeSeriesType() {
        return new TimeSeriesType();
    }

    /**
     * Create an instance of {@link TimeSeriesObsType }
     * 
     */
    public TimeSeriesObsType createTimeSeriesObsType() {
        return new TimeSeriesObsType();
    }

    /**
     * Create an instance of {@link SeriesType }
     * 
     */
    public SeriesType createSeriesType() {
        return new SeriesType();
    }

    /**
     * Create an instance of {@link TimeSeriesDataSetType }
     * 
     */
    public TimeSeriesDataSetType createTimeSeriesDataSetType() {
        return new TimeSeriesDataSetType();
    }

    /**
     * Create an instance of {@link ObsOnlyType }
     * 
     */
    public ObsOnlyType createObsOnlyType() {
        return new ObsOnlyType();
    }

    /**
     * Create an instance of {@link GroupType }
     * 
     */
    public GroupType createGroupType() {
        return new GroupType();
    }

    /**
     * Create an instance of {@link ValuesType }
     * 
     */
    public ValuesType createValuesType() {
        return new ValuesType();
    }

    /**
     * Create an instance of {@link ObsType }
     * 
     */
    public ObsType createObsType() {
        return new ObsType();
    }

    /**
     * Create an instance of {@link ComponentValueType }
     * 
     */
    public ComponentValueType createComponentValueType() {
        return new ComponentValueType();
    }

    /**
     * Create an instance of {@link TimeValueType }
     * 
     */
    public TimeValueType createTimeValueType() {
        return new TimeValueType();
    }

}
