
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ItemSchemeTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ItemSchemeTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableTypeCodelistType">
 *     &lt;enumeration value="AgencyScheme"/>
 *     &lt;enumeration value="CategoryScheme"/>
 *     &lt;enumeration value="Codelist"/>
 *     &lt;enumeration value="ConceptScheme"/>
 *     &lt;enumeration value="DataConsumerScheme"/>
 *     &lt;enumeration value="DataProviderScheme"/>
 *     &lt;enumeration value="OrganisationUnitScheme"/>
 *     &lt;enumeration value="ReportingTaxonomy"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ItemSchemeTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(MaintainableTypeCodelistType.class)
public enum ItemSchemeTypeCodelistType {

    @XmlEnumValue("AgencyScheme")
    AGENCY_SCHEME(MaintainableTypeCodelistType.AGENCY_SCHEME),
    @XmlEnumValue("CategoryScheme")
    CATEGORY_SCHEME(MaintainableTypeCodelistType.CATEGORY_SCHEME),
    @XmlEnumValue("Codelist")
    CODELIST(MaintainableTypeCodelistType.CODELIST),
    @XmlEnumValue("ConceptScheme")
    CONCEPT_SCHEME(MaintainableTypeCodelistType.CONCEPT_SCHEME),
    @XmlEnumValue("DataConsumerScheme")
    DATA_CONSUMER_SCHEME(MaintainableTypeCodelistType.DATA_CONSUMER_SCHEME),
    @XmlEnumValue("DataProviderScheme")
    DATA_PROVIDER_SCHEME(MaintainableTypeCodelistType.DATA_PROVIDER_SCHEME),
    @XmlEnumValue("OrganisationUnitScheme")
    ORGANISATION_UNIT_SCHEME(MaintainableTypeCodelistType.ORGANISATION_UNIT_SCHEME),
    @XmlEnumValue("ReportingTaxonomy")
    REPORTING_TAXONOMY(MaintainableTypeCodelistType.REPORTING_TAXONOMY);
    private final MaintainableTypeCodelistType value;

    ItemSchemeTypeCodelistType(MaintainableTypeCodelistType v) {
        value = v;
    }

    public MaintainableTypeCodelistType value() {
        return value;
    }

    public static ItemSchemeTypeCodelistType fromValue(MaintainableTypeCodelistType v) {
        for (ItemSchemeTypeCodelistType c: ItemSchemeTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
