
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SubmitSubscriptionsResponseType describes the structure of the response to a new subscription submission. A status is provided for each subscription in the request.
 * 
 * <p>Java-Klasse f�r SubmitSubscriptionsResponseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitSubscriptionsResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubscriptionStatus" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubscriptionStatusType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitSubscriptionsResponseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "subscriptionStatus"
})
public class SubmitSubscriptionsResponseType {

    @XmlElement(name = "SubscriptionStatus", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected List<SubscriptionStatusType> subscriptionStatus;

    /**
     * Gets the value of the subscriptionStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subscriptionStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubscriptionStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubscriptionStatusType }
     * 
     * 
     */
    public List<SubscriptionStatusType> getSubscriptionStatus() {
        if (subscriptionStatus == null) {
            subscriptionStatus = new ArrayList<SubscriptionStatusType>();
        }
        return this.subscriptionStatus;
    }

}
