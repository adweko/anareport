
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.QueryableDataSourceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SetReferenceType;


/**
 * ConstraintAttachmentType describes a collection of references to constrainable artefacts.
 * 
 * <p>Java-Klasse f�r ConstraintAttachmentType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConstraintAttachmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType"/>
 *         &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType" maxOccurs="unbounded"/>
 *         &lt;element name="MetadataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType" maxOccurs="unbounded"/>
 *         &lt;element name="SimpleDataSource" type="{http://www.w3.org/2001/XMLSchema}anyURI" maxOccurs="unbounded"/>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element name="DataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="MetadataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataStructureReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="Dataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="Metadataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataflowReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstraintAttachmentType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "content"
})
@XmlSeeAlso({
    AttachmentConstraintAttachmentType.class,
    ContentConstraintAttachmentType.class
})
public abstract class ConstraintAttachmentType {

    @XmlElementRefs({
        @XmlElementRef(name = "ProvisionAgreement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Metadataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "QueryableDataSource", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "SimpleDataSource", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Dataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MetadataSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MetadataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> content;

    /**
     * Ruft das restliche Contentmodell ab. 
     * 
     * <p>
     * Sie rufen diese "catch-all"-Eigenschaft aus folgendem Grund ab: 
     * Der Feldname "QueryableDataSource" wird von zwei verschiedenen Teilen eines Schemas verwendet. Siehe: 
     * Zeile 274 von file:/D:/tmp/anacredit-technisches-meldeschema-version-2-0-data/SDMXStructureConstraint.xsd
     * Zeile 262 von file:/D:/tmp/anacredit-technisches-meldeschema-version-2-0-data/SDMXStructureConstraint.xsd
     * <p>
     * Um diese Eigenschaft zu entfernen, wenden Sie eine Eigenschaftenanpassung f�r eine
     * der beiden folgenden Deklarationen an, um deren Namen zu �ndern: 
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link ProvisionAgreementReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataStructureReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link MetadataflowReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link SetReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link QueryableDataSourceType }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link DataflowReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link SetReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataProviderReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link MetadataStructureReferenceType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<?>>();
        }
        return this.content;
    }

}
