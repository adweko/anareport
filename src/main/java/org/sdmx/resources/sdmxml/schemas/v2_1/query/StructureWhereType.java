
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ConceptReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ItemSchemeReferenceBaseType;


/**
 * StructureWhereType is an abstract base type that serves as the basis for a query for a structure object.
 * 
 * <p>Java-Klasse f�r StructureWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableWhereType">
 *       &lt;sequence>
 *         &lt;element name="UsedConcept" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UsedRepresentation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeReferenceBaseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ComponentListWhere"/>
 *         &lt;/sequence>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ComponentWhere"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "usedConcept",
    "usedRepresentation",
    "componentListWhere",
    "componentWhere"
})
@XmlSeeAlso({
    DataStructureWhereBaseType.class,
    MetadataStructureWhereBaseType.class
})
public abstract class StructureWhereType
    extends MaintainableWhereType
{

    @XmlElement(name = "UsedConcept", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<ConceptReferenceType> usedConcept;
    @XmlElement(name = "UsedRepresentation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<ItemSchemeReferenceBaseType> usedRepresentation;
    @XmlElementRef(name = "ComponentListWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ComponentListWhereType>> componentListWhere;
    @XmlElementRef(name = "ComponentWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ComponentWhereType>> componentWhere;

    /**
     * Gets the value of the usedConcept property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usedConcept property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsedConcept().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConceptReferenceType }
     * 
     * 
     */
    public List<ConceptReferenceType> getUsedConcept() {
        if (usedConcept == null) {
            usedConcept = new ArrayList<ConceptReferenceType>();
        }
        return this.usedConcept;
    }

    /**
     * Gets the value of the usedRepresentation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usedRepresentation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsedRepresentation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemSchemeReferenceBaseType }
     * 
     * 
     */
    public List<ItemSchemeReferenceBaseType> getUsedRepresentation() {
        if (usedRepresentation == null) {
            usedRepresentation = new ArrayList<ItemSchemeReferenceBaseType>();
        }
        return this.usedRepresentation;
    }

    /**
     * Gets the value of the componentListWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the componentListWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponentListWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link MetadataTargetWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link GroupWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportStructureWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ComponentListWhereType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ComponentListWhereType>> getComponentListWhere() {
        if (componentListWhere == null) {
            componentListWhere = new ArrayList<JAXBElement<? extends ComponentListWhereType>>();
        }
        return this.componentListWhere;
    }

    /**
     * Gets the value of the componentWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the componentWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponentWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link DimensionWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link DimensionWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link MeasureDimensionWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ComponentWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link TimeDimensionWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link PrimaryMeasureWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link MetadataAttributeWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link TargetObjectWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link AttributeWhereType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ComponentWhereType>> getComponentWhere() {
        if (componentWhere == null) {
            componentWhere = new ArrayList<JAXBElement<? extends ComponentWhereType>>();
        }
        return this.componentWhere;
    }

}
