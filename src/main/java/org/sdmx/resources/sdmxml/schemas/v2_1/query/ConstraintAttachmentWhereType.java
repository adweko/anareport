
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SetReferenceType;


/**
 * ConstraintAttachmentWhereType describes the structure for querying for a constraint based on the objects it is attached to. A constraint attachment query is implicitly an and-query meaning all of the referenced objects must be part of a constraints attachment in order to return a match. It is treated as a condition within its parent query.
 * 
 * <p>Java-Klasse f�r ConstraintAttachmentWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConstraintAttachmentWhereType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType"/>
 *         &lt;element name="Dataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType"/>
 *         &lt;element name="DataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureReferenceType"/>
 *         &lt;element name="Metadataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataflowReferenceType"/>
 *         &lt;element name="MetadataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataStructureReferenceType"/>
 *         &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType"/>
 *         &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType"/>
 *         &lt;element name="MetadataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType"/>
 *         &lt;element name="DataSourceURL" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstraintAttachmentWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "dataProviderOrDataflowOrDataStructure"
})
public class ConstraintAttachmentWhereType {

    @XmlElementRefs({
        @XmlElementRef(name = "MetadataSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MetadataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataSourceURL", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Metadataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Dataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ProvisionAgreement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> dataProviderOrDataflowOrDataStructure;

    /**
     * Gets the value of the dataProviderOrDataflowOrDataStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataProviderOrDataflowOrDataStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataProviderOrDataflowOrDataStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link SetReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataProviderReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link MetadataStructureReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link DataStructureReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link MetadataflowReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataflowReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link ProvisionAgreementReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link SetReferenceType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getDataProviderOrDataflowOrDataStructure() {
        if (dataProviderOrDataflowOrDataStructure == null) {
            dataProviderOrDataflowOrDataStructure = new ArrayList<JAXBElement<?>>();
        }
        return this.dataProviderOrDataflowOrDataStructure;
    }

}
