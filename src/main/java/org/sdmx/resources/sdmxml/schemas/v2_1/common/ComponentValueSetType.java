
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ComponentValueSetType is an abstract base type which is used to provide a set of value for a referenced component. Implementations of this type will be based on a particular component type and refine the allowed values to reflect the types of values that are possible for that type of component.
 * 
 * <p>Java-Klasse f�r ComponentValueSetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentValueSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="Value" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleValueType" maxOccurs="unbounded"/>
 *         &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType" maxOccurs="unbounded"/>
 *         &lt;element name="DataKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataKeyType" maxOccurs="unbounded"/>
 *         &lt;element name="Object" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType" maxOccurs="unbounded"/>
 *         &lt;element name="TimeRange" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedNCNameIDType" />
 *       &lt;attribute name="include" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentValueSetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", propOrder = {
    "value",
    "dataSet",
    "dataKey",
    "object",
    "timeRange"
})
@XmlSeeAlso({
    CubeRegionKeyType.class,
    DinstinctKeyValueType.class,
    MetadataAttributeValueSetType.class,
    MetadataTargetRegionKeyType.class,
    AttributeValueSetType.class
})
public abstract class ComponentValueSetType {

    @XmlElement(name = "Value", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<SimpleValueType> value;
    @XmlElement(name = "DataSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<SetReferenceType> dataSet;
    @XmlElement(name = "DataKey", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<DataKeyType> dataKey;
    @XmlElement(name = "Object", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<ObjectReferenceType> object;
    @XmlElement(name = "TimeRange", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected TimeRangeValueType timeRange;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "include")
    protected Boolean include;

    /**
     * Gets the value of the value property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the value property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleValueType }
     * 
     * 
     */
    public List<SimpleValueType> getValue() {
        if (value == null) {
            value = new ArrayList<SimpleValueType>();
        }
        return this.value;
    }

    /**
     * Gets the value of the dataSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SetReferenceType }
     * 
     * 
     */
    public List<SetReferenceType> getDataSet() {
        if (dataSet == null) {
            dataSet = new ArrayList<SetReferenceType>();
        }
        return this.dataSet;
    }

    /**
     * Gets the value of the dataKey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataKey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataKeyType }
     * 
     * 
     */
    public List<DataKeyType> getDataKey() {
        if (dataKey == null) {
            dataKey = new ArrayList<DataKeyType>();
        }
        return this.dataKey;
    }

    /**
     * Gets the value of the object property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the object property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceType }
     * 
     * 
     */
    public List<ObjectReferenceType> getObject() {
        if (object == null) {
            object = new ArrayList<ObjectReferenceType>();
        }
        return this.object;
    }

    /**
     * Ruft den Wert der timeRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeRangeValueType }
     *     
     */
    public TimeRangeValueType getTimeRange() {
        return timeRange;
    }

    /**
     * Legt den Wert der timeRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeRangeValueType }
     *     
     */
    public void setTimeRange(TimeRangeValueType value) {
        this.timeRange = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der include-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isInclude() {
        if (include == null) {
            return true;
        } else {
            return include;
        }
    }

    /**
     * Legt den Wert der include-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInclude(Boolean value) {
        this.include = value;
    }

}
