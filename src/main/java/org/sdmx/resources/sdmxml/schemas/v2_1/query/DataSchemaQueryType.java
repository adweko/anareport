
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * DataSchemaQueryType defines the structure of a query for a structured data schema. This query consists of a single data structure which provides the full details of what type of structured data schema should be returned.
 * 
 * <p>Java-Klasse f�r DataSchemaQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataSchemaQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataStructureRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSchemaQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "dataStructure"
})
public class DataSchemaQueryType {

    @XmlElement(name = "DataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected DataStructureRequestType dataStructure;

    /**
     * Ruft den Wert der dataStructure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataStructureRequestType }
     *     
     */
    public DataStructureRequestType getDataStructure() {
        return dataStructure;
    }

    /**
     * Legt den Wert der dataStructure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataStructureRequestType }
     *     
     */
    public void setDataStructure(DataStructureRequestType value) {
        this.dataStructure = value;
    }

}
