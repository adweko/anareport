
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * CubeRegionType defines the structure of a data cube region. This is based on the abstract RegionType and simply refines the key and attribute values to conform with what is applicable for dimensions and attributes, respectively. See the documentation of the base type for more details on how a region is defined.
 * 
 * <p>Java-Klasse f�r CubeRegionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CubeRegionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}RegionType">
 *       &lt;sequence>
 *         &lt;element name="KeyValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CubeRegionKeyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Attribute" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttributeValueSetType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CubeRegionType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class CubeRegionType
    extends RegionType
{


}
