
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * MappedObjectType defines a structure for referencing an object and indicating whether it is the source, target, or either for the purposes of query for structure set containing the referenced object in one of the maps it defines.
 * 
 * <p>Java-Klasse f�r MappedObjectType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MappedObjectType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MappedObjectReferenceType">
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}SourceTargetType" default="Any" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MappedObjectType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class MappedObjectType
    extends MappedObjectReferenceType
{

    @XmlAttribute(name = "type")
    protected SourceTargetType type;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SourceTargetType }
     *     
     */
    public SourceTargetType getType() {
        if (type == null) {
            return SourceTargetType.ANY;
        } else {
            return type;
        }
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SourceTargetType }
     *     
     */
    public void setType(SourceTargetType value) {
        this.type = value;
    }

}
