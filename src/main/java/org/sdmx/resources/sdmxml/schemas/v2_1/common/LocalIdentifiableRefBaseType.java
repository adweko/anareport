
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * LocalIdentifiableRefBaseType is an abstract base type which provides a local reference to any identifiable object.
 * 
 * <p>Java-Klasse f�r LocalIdentifiableRefBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LocalIdentifiableRefBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}RefBaseType">
 *       &lt;attribute name="containerID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedIDType" />
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedIDType" />
 *       &lt;attribute name="local" type="{http://www.w3.org/2001/XMLSchema}boolean" fixed="true" />
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PackageTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocalIdentifiableRefBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    LocalCodelistMapRefType.class,
    LocalLevelRefType.class,
    LocalProcessStepRefType.class,
    LocalItemRefBaseType.class,
    LocalComponentListRefBaseType.class,
    LocalComponentListComponentRefBaseType.class
})
public abstract class LocalIdentifiableRefBaseType
    extends RefBaseType
{


}
