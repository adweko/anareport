
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r UsageStatusType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="UsageStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Mandatory"/>
 *     &lt;enumeration value="Conditional"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UsageStatusType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlEnum
public enum UsageStatusType {


    /**
     * Reporting the associated attribute is mandatory - a value must be supplied.
     * 
     */
    @XmlEnumValue("Mandatory")
    MANDATORY("Mandatory"),

    /**
     * Reporting the associated attribute is not mandatory - a value may  be supplied, but is not required.
     * 
     */
    @XmlEnumValue("Conditional")
    CONDITIONAL("Conditional");
    private final String value;

    UsageStatusType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UsageStatusType fromValue(String v) {
        for (UsageStatusType c: UsageStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
