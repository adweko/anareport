
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r TimeDataType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="TimeDataType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleDataType">
 *     &lt;enumeration value="ObservationalTimePeriod"/>
 *     &lt;enumeration value="StandardTimePeriod"/>
 *     &lt;enumeration value="BasicTimePeriod"/>
 *     &lt;enumeration value="GregorianTimePeriod"/>
 *     &lt;enumeration value="GregorianYear"/>
 *     &lt;enumeration value="GregorianYearMonth"/>
 *     &lt;enumeration value="GregorianDay"/>
 *     &lt;enumeration value="ReportingTimePeriod"/>
 *     &lt;enumeration value="ReportingYear"/>
 *     &lt;enumeration value="ReportingSemester"/>
 *     &lt;enumeration value="ReportingTrimester"/>
 *     &lt;enumeration value="ReportingQuarter"/>
 *     &lt;enumeration value="ReportingMonth"/>
 *     &lt;enumeration value="ReportingWeek"/>
 *     &lt;enumeration value="ReportingDay"/>
 *     &lt;enumeration value="DateTime"/>
 *     &lt;enumeration value="TimeRange"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TimeDataType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(SimpleDataType.class)
public enum TimeDataType {

    @XmlEnumValue("ObservationalTimePeriod")
    OBSERVATIONAL_TIME_PERIOD(SimpleDataType.OBSERVATIONAL_TIME_PERIOD),
    @XmlEnumValue("StandardTimePeriod")
    STANDARD_TIME_PERIOD(SimpleDataType.STANDARD_TIME_PERIOD),
    @XmlEnumValue("BasicTimePeriod")
    BASIC_TIME_PERIOD(SimpleDataType.BASIC_TIME_PERIOD),
    @XmlEnumValue("GregorianTimePeriod")
    GREGORIAN_TIME_PERIOD(SimpleDataType.GREGORIAN_TIME_PERIOD),
    @XmlEnumValue("GregorianYear")
    GREGORIAN_YEAR(SimpleDataType.GREGORIAN_YEAR),
    @XmlEnumValue("GregorianYearMonth")
    GREGORIAN_YEAR_MONTH(SimpleDataType.GREGORIAN_YEAR_MONTH),
    @XmlEnumValue("GregorianDay")
    GREGORIAN_DAY(SimpleDataType.GREGORIAN_DAY),
    @XmlEnumValue("ReportingTimePeriod")
    REPORTING_TIME_PERIOD(SimpleDataType.REPORTING_TIME_PERIOD),
    @XmlEnumValue("ReportingYear")
    REPORTING_YEAR(SimpleDataType.REPORTING_YEAR),
    @XmlEnumValue("ReportingSemester")
    REPORTING_SEMESTER(SimpleDataType.REPORTING_SEMESTER),
    @XmlEnumValue("ReportingTrimester")
    REPORTING_TRIMESTER(SimpleDataType.REPORTING_TRIMESTER),
    @XmlEnumValue("ReportingQuarter")
    REPORTING_QUARTER(SimpleDataType.REPORTING_QUARTER),
    @XmlEnumValue("ReportingMonth")
    REPORTING_MONTH(SimpleDataType.REPORTING_MONTH),
    @XmlEnumValue("ReportingWeek")
    REPORTING_WEEK(SimpleDataType.REPORTING_WEEK),
    @XmlEnumValue("ReportingDay")
    REPORTING_DAY(SimpleDataType.REPORTING_DAY),
    @XmlEnumValue("DateTime")
    DATE_TIME(SimpleDataType.DATE_TIME),
    @XmlEnumValue("TimeRange")
    TIME_RANGE(SimpleDataType.TIME_RANGE);
    private final SimpleDataType value;

    TimeDataType(SimpleDataType v) {
        value = v;
    }

    public SimpleDataType value() {
        return value;
    }

    public static TimeDataType fromValue(SimpleDataType v) {
        for (TimeDataType c: TimeDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
