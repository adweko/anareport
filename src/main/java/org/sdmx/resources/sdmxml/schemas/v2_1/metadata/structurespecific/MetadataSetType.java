
package org.sdmx.resources.sdmxml.schemas.v2_1.metadata.structurespecific;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ActionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TextType;


/**
 * MetadataSetType is an abstract base type the forms the basis for a metadata structure specific metadata set. It is restricted by the metadata structure definition specific schema to meet its needs.
 * 
 * <p>Java-Klasse f�r MetadataSetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataSetType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Name" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0" form="unqualified"/>
 *         &lt;element name="Report" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific}ReportType" maxOccurs="unbounded" form="unqualified"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific}SetAttributeGroup"/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataSetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific", propOrder = {
    "name",
    "dataProvider",
    "report"
})
public abstract class MetadataSetType
    extends AnnotableType
{

    @XmlElement(name = "Name", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<TextType> name;
    @XmlElement(name = "DataProvider")
    protected DataProviderReferenceType dataProvider;
    @XmlElement(name = "Report", required = true)
    protected List<ReportType> report;
    @XmlAttribute(name = "structureRef", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific", required = true)
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object structureRef;
    @XmlAttribute(name = "setID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    protected String setID;
    @XmlAttribute(name = "action", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    protected ActionType action;
    @XmlAttribute(name = "reportingBeginDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    protected List<String> reportingBeginDate;
    @XmlAttribute(name = "reportingEndDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    protected List<String> reportingEndDate;
    @XmlAttribute(name = "validFromDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validFromDate;
    @XmlAttribute(name = "validToDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validToDate;
    @XmlAttribute(name = "publicationYear", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    @XmlSchemaType(name = "gYear")
    protected XMLGregorianCalendar publicationYear;
    @XmlAttribute(name = "publicationPeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    protected List<String> publicationPeriod;

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getName() {
        if (name == null) {
            name = new ArrayList<TextType>();
        }
        return this.name;
    }

    /**
     * Ruft den Wert der dataProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public DataProviderReferenceType getDataProvider() {
        return dataProvider;
    }

    /**
     * Legt den Wert der dataProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public void setDataProvider(DataProviderReferenceType value) {
        this.dataProvider = value;
    }

    /**
     * Gets the value of the report property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the report property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReport().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportType }
     * 
     * 
     */
    public List<ReportType> getReport() {
        if (report == null) {
            report = new ArrayList<ReportType>();
        }
        return this.report;
    }

    /**
     * Ruft den Wert der structureRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getStructureRef() {
        return structureRef;
    }

    /**
     * Legt den Wert der structureRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setStructureRef(Object value) {
        this.structureRef = value;
    }

    /**
     * Ruft den Wert der setID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetID() {
        return setID;
    }

    /**
     * Legt den Wert der setID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetID(String value) {
        this.setID = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setAction(ActionType value) {
        this.action = value;
    }

    /**
     * Gets the value of the reportingBeginDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingBeginDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingBeginDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportingBeginDate() {
        if (reportingBeginDate == null) {
            reportingBeginDate = new ArrayList<String>();
        }
        return this.reportingBeginDate;
    }

    /**
     * Gets the value of the reportingEndDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingEndDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingEndDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportingEndDate() {
        if (reportingEndDate == null) {
            reportingEndDate = new ArrayList<String>();
        }
        return this.reportingEndDate;
    }

    /**
     * Ruft den Wert der validFromDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidFromDate() {
        return validFromDate;
    }

    /**
     * Legt den Wert der validFromDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFromDate(XMLGregorianCalendar value) {
        this.validFromDate = value;
    }

    /**
     * Ruft den Wert der validToDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidToDate() {
        return validToDate;
    }

    /**
     * Legt den Wert der validToDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidToDate(XMLGregorianCalendar value) {
        this.validToDate = value;
    }

    /**
     * Ruft den Wert der publicationYear-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPublicationYear() {
        return publicationYear;
    }

    /**
     * Legt den Wert der publicationYear-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPublicationYear(XMLGregorianCalendar value) {
        this.publicationYear = value;
    }

    /**
     * Gets the value of the publicationPeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the publicationPeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPublicationPeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPublicationPeriod() {
        if (publicationPeriod == null) {
            publicationPeriod = new ArrayList<String>();
        }
        return this.publicationPeriod;
    }

}
