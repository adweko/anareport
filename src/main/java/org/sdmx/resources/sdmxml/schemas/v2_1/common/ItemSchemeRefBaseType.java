
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ItemSchemeRefBaseType is an abstract base type for referencing an item scheme.
 * 
 * <p>Java-Klasse f�r ItemSchemeRefBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ItemSchemeRefBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableRefBaseType">
 *       &lt;attribute name="agencyID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedNCNameIDType" />
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="version" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionType" default="1.0" />
 *       &lt;attribute name="local" type="{http://www.w3.org/2001/XMLSchema}boolean" fixed="false" />
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeTypeCodelistType" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemePackageTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemSchemeRefBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    ReportingTaxonomyRefType.class,
    CategorySchemeRefType.class,
    CodelistRefType.class,
    ItemSchemeRefType.class,
    ConceptSchemeRefType.class,
    OrganisationSchemeRefBaseType.class
})
public abstract class ItemSchemeRefBaseType
    extends MaintainableRefBaseType
{


}
