
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SubmitSubscriptionsRequestType defines the payload of a request message used to submit addtions, updates, or deletions of subscriptions. Subscriptions are submitted to the registry to subscribe to registration and change events for specific registry resources.
 * 
 * <p>Java-Klasse f�r SubmitSubscriptionsRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitSubscriptionsRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubscriptionRequest" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubscriptionRequestType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitSubscriptionsRequestType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "subscriptionRequest"
})
public class SubmitSubscriptionsRequestType {

    @XmlElement(name = "SubscriptionRequest", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected List<SubscriptionRequestType> subscriptionRequest;

    /**
     * Gets the value of the subscriptionRequest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subscriptionRequest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubscriptionRequest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubscriptionRequestType }
     * 
     * 
     */
    public List<SubscriptionRequestType> getSubscriptionRequest() {
        if (subscriptionRequest == null) {
            subscriptionRequest = new ArrayList<SubscriptionRequestType>();
        }
        return this.subscriptionRequest;
    }

}
