
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TimeRangeValueType;


/**
 * VersionableQueryType is an abstract base type that serves as the basis for any query for a versionable object.
 * 
 * <p>Java-Klasse f�r VersionableWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersionableWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}NameableWhereType">
 *       &lt;sequence>
 *         &lt;element name="Version" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionQueryType" minOccurs="0"/>
 *         &lt;element name="VersionTo" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" minOccurs="0"/>
 *         &lt;element name="VersionFrom" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" minOccurs="0"/>
 *         &lt;element name="VersionActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersionableWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "version",
    "versionTo",
    "versionFrom",
    "versionActive"
})
@XmlSeeAlso({
    MaintainableWhereType.class
})
public abstract class VersionableWhereType
    extends NameableWhereType
{

    @XmlElement(name = "Version", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected String version;
    @XmlElement(name = "VersionTo", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected TimeRangeValueType versionTo;
    @XmlElement(name = "VersionFrom", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected TimeRangeValueType versionFrom;
    @XmlElement(name = "VersionActive", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected Boolean versionActive;

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der versionTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeRangeValueType }
     *     
     */
    public TimeRangeValueType getVersionTo() {
        return versionTo;
    }

    /**
     * Legt den Wert der versionTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeRangeValueType }
     *     
     */
    public void setVersionTo(TimeRangeValueType value) {
        this.versionTo = value;
    }

    /**
     * Ruft den Wert der versionFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeRangeValueType }
     *     
     */
    public TimeRangeValueType getVersionFrom() {
        return versionFrom;
    }

    /**
     * Legt den Wert der versionFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeRangeValueType }
     *     
     */
    public void setVersionFrom(TimeRangeValueType value) {
        this.versionFrom = value;
    }

    /**
     * Ruft den Wert der versionActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVersionActive() {
        return versionActive;
    }

    /**
     * Legt den Wert der versionActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVersionActive(Boolean value) {
        this.versionActive = value;
    }

}
