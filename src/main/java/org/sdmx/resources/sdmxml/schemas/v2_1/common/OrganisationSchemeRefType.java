
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * OrganisationSchemeRefType provides a reference to an organisation scheme via a complete set of reference fields. It is required that the class (i.e. the type) of organisation scheme being referenced be specified.
 * 
 * <p>Java-Klasse f�r OrganisationSchemeRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrganisationSchemeRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationSchemeRefBaseType">
 *       &lt;attribute name="class" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationSchemeTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationSchemeRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class OrganisationSchemeRefType
    extends OrganisationSchemeRefBaseType
{


}
