
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * SubmitRegistrationsRequestType defines the structure of a registry submit registration requests document.
 * 
 * <p>Java-Klasse f�r SubmitRegistrationsRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitRegistrationsRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}RegistryInterfaceType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}BasicHeaderType"/>
 *         &lt;element name="SubmitRegistrationsRequest" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmitRegistrationsRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitRegistrationsRequestType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
public class SubmitRegistrationsRequestType
    extends RegistryInterfaceType
{


}
