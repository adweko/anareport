
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r StatusType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="StatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Success"/>
 *     &lt;enumeration value="Warning"/>
 *     &lt;enumeration value="Failure"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StatusType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
@XmlEnum
public enum StatusType {


    /**
     * Query or request successful.
     * 
     */
    @XmlEnumValue("Success")
    SUCCESS("Success"),

    /**
     * Query or request successful, but with warnings.
     * 
     */
    @XmlEnumValue("Warning")
    WARNING("Warning"),

    /**
     * Query or request not successful.
     * 
     */
    @XmlEnumValue("Failure")
    FAILURE("Failure");
    private final String value;

    StatusType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StatusType fromValue(String v) {
        for (StatusType c: StatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
