
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;


/**
 * Registration provides the information needed for data and reference metadata set registration. A data source must be supplied here if not already provided in the provision agreement. The data set or metadata set must be associated with a provision agreement,  a metadata flow, or a dataflow definition. If possible, the provision agreement should be specified. Only in cases where this is not possible should the dataflow or metadata flow be used.
 * 
 * <p>Java-Klasse f�r RegistrationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RegistrationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType"/>
 *         &lt;element name="Datasource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}DataSourceType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="validFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="validTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="lastUpdated" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="indexTimeSeries" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="indexDataSet" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="indexAttributes" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="indexReportingPeriod" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "provisionAgreement",
    "datasource"
})
public class RegistrationType {

    @XmlElement(name = "ProvisionAgreement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected ProvisionAgreementReferenceType provisionAgreement;
    @XmlElement(name = "Datasource", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected DataSourceType datasource;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "validFrom")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validFrom;
    @XmlAttribute(name = "validTo")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validTo;
    @XmlAttribute(name = "lastUpdated")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastUpdated;
    @XmlAttribute(name = "indexTimeSeries")
    protected Boolean indexTimeSeries;
    @XmlAttribute(name = "indexDataSet")
    protected Boolean indexDataSet;
    @XmlAttribute(name = "indexAttributes")
    protected Boolean indexAttributes;
    @XmlAttribute(name = "indexReportingPeriod")
    protected Boolean indexReportingPeriod;

    /**
     * Ruft den Wert der provisionAgreement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProvisionAgreementReferenceType }
     *     
     */
    public ProvisionAgreementReferenceType getProvisionAgreement() {
        return provisionAgreement;
    }

    /**
     * Legt den Wert der provisionAgreement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvisionAgreementReferenceType }
     *     
     */
    public void setProvisionAgreement(ProvisionAgreementReferenceType value) {
        this.provisionAgreement = value;
    }

    /**
     * Ruft den Wert der datasource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataSourceType }
     *     
     */
    public DataSourceType getDatasource() {
        return datasource;
    }

    /**
     * Legt den Wert der datasource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataSourceType }
     *     
     */
    public void setDatasource(DataSourceType value) {
        this.datasource = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der validFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidFrom() {
        return validFrom;
    }

    /**
     * Legt den Wert der validFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFrom(XMLGregorianCalendar value) {
        this.validFrom = value;
    }

    /**
     * Ruft den Wert der validTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidTo() {
        return validTo;
    }

    /**
     * Legt den Wert der validTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidTo(XMLGregorianCalendar value) {
        this.validTo = value;
    }

    /**
     * Ruft den Wert der lastUpdated-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdated() {
        return lastUpdated;
    }

    /**
     * Legt den Wert der lastUpdated-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdated(XMLGregorianCalendar value) {
        this.lastUpdated = value;
    }

    /**
     * Ruft den Wert der indexTimeSeries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIndexTimeSeries() {
        if (indexTimeSeries == null) {
            return false;
        } else {
            return indexTimeSeries;
        }
    }

    /**
     * Legt den Wert der indexTimeSeries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIndexTimeSeries(Boolean value) {
        this.indexTimeSeries = value;
    }

    /**
     * Ruft den Wert der indexDataSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIndexDataSet() {
        if (indexDataSet == null) {
            return false;
        } else {
            return indexDataSet;
        }
    }

    /**
     * Legt den Wert der indexDataSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIndexDataSet(Boolean value) {
        this.indexDataSet = value;
    }

    /**
     * Ruft den Wert der indexAttributes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIndexAttributes() {
        if (indexAttributes == null) {
            return false;
        } else {
            return indexAttributes;
        }
    }

    /**
     * Legt den Wert der indexAttributes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIndexAttributes(Boolean value) {
        this.indexAttributes = value;
    }

    /**
     * Ruft den Wert der indexReportingPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIndexReportingPeriod() {
        if (indexReportingPeriod == null) {
            return false;
        } else {
            return indexReportingPeriod;
        }
    }

    /**
     * Legt den Wert der indexReportingPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIndexReportingPeriod(Boolean value) {
        this.indexReportingPeriod = value;
    }

}
