
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * DataReturnDetailsBaseType is an abstract base type which forms the basis of the DataReturnDetailsType.
 * 
 * <p>Java-Klasse f�r DataReturnDetailsBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataReturnDetailsBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReturnDetailsBaseType">
 *       &lt;attribute name="detail" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataReturnDetailType" default="Full" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataReturnDetailsBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlSeeAlso({
    DataReturnDetailsType.class
})
public abstract class DataReturnDetailsBaseType
    extends ReturnDetailsBaseType
{


}
