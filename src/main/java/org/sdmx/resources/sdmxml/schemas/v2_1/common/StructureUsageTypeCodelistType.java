
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r StructureUsageTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="StructureUsageTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureOrUsageTypeCodelistType">
 *     &lt;enumeration value="Dataflow"/>
 *     &lt;enumeration value="Metadataflow"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StructureUsageTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(StructureOrUsageTypeCodelistType.class)
public enum StructureUsageTypeCodelistType {

    @XmlEnumValue("Dataflow")
    DATAFLOW(StructureOrUsageTypeCodelistType.DATAFLOW),
    @XmlEnumValue("Metadataflow")
    METADATAFLOW(StructureOrUsageTypeCodelistType.METADATAFLOW);
    private final StructureOrUsageTypeCodelistType value;

    StructureUsageTypeCodelistType(StructureOrUsageTypeCodelistType v) {
        value = v;
    }

    public StructureOrUsageTypeCodelistType value() {
        return value;
    }

    public static StructureUsageTypeCodelistType fromValue(StructureOrUsageTypeCodelistType v) {
        for (StructureUsageTypeCodelistType c: StructureUsageTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
