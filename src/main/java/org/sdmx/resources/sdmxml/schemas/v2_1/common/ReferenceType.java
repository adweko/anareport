
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ReferenceType is an abstract base type. It is used as the basis for all references, to all for a top level generic object reference that can be substituted with an explicit reference to any object. Any reference can consist of a Ref (which contains all required reference fields separately) and/or a URN. These must result in the identification of the same object. Note that the Ref and URN elements are local and unqualified in order to allow for refinement of this structure outside of the namespace. This allows any reference to further refined by a different namespace. For example, a metadata structure definition specific metadata set might wish to restrict the URN to only allow for a value from an enumerated list. The general URN structure, for the purpose of mapping the reference fields is as follows: urn:sdmx:org.package-name.class-name=agency-id:(maintainable-parent-object-id[maintainable-parent-object-version].)?(container-object-id.)?object-id([object-version])?.
 * 
 * <p>Java-Klasse f�r ReferenceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="Ref" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}RefBaseType" form="unqualified"/>
 *           &lt;element name="URN" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0" form="unqualified"/>
 *         &lt;/sequence>
 *         &lt;element name="URN" type="{http://www.w3.org/2001/XMLSchema}anyURI" form="unqualified"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", propOrder = {
    "content"
})
@XmlSeeAlso({
    LocalCodelistMapReferenceType.class,
    URNReferenceType.class,
    LocalLevelReferenceType.class,
    LocalIdentifiableReferenceType.class,
    ObjectReferenceType.class,
    LocalComponentListComponentReferenceBaseType.class,
    LocalComponentListReferenceType.class,
    ChildObjectReferenceType.class,
    AnyLocalCodeReferenceType.class,
    ContainerChildObjectReferenceType.class,
    LocalItemReferenceType.class,
    MaintainableReferenceBaseType.class
})
public abstract class ReferenceType {

    @XmlElementRefs({
        @XmlElementRef(name = "URN", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Ref", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> content;

    /**
     * Ruft das restliche Contentmodell ab. 
     * 
     * <p>
     * Sie rufen diese "catch-all"-Eigenschaft aus folgendem Grund ab: 
     * Der Feldname "URN" wird von zwei verschiedenen Teilen eines Schemas verwendet. Siehe: 
     * Zeile 121 von file:/D:/tmp/anacredit-technisches-meldeschema-version-2-0-data/SDMXCommonReferences.xsd
     * Zeile 115 von file:/D:/tmp/anacredit-technisches-meldeschema-version-2-0-data/SDMXCommonReferences.xsd
     * <p>
     * Um diese Eigenschaft zu entfernen, wenden Sie eine Eigenschaftenanpassung f�r eine
     * der beiden folgenden Deklarationen an, um deren Namen zu �ndern: 
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link RefBaseType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<?>>();
        }
        return this.content;
    }

}
