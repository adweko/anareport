
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r DimensionTypeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="DimensionTypeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Dimension"/>
 *     &lt;enumeration value="MeasureDimension"/>
 *     &lt;enumeration value="TimeDimension"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DimensionTypeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum DimensionTypeType {


    /**
     * An ordinary dimension.
     * 
     */
    @XmlEnumValue("Dimension")
    DIMENSION("Dimension"),

    /**
     * A measure dimension.
     * 
     */
    @XmlEnumValue("MeasureDimension")
    MEASURE_DIMENSION("MeasureDimension"),

    /**
     * The time dimension.
     * 
     */
    @XmlEnumValue("TimeDimension")
    TIME_DIMENSION("TimeDimension");
    private final String value;

    DimensionTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DimensionTypeType fromValue(String v) {
        for (DimensionTypeType c: DimensionTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
