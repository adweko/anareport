
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * SimpleComponentTextFormatType is a restricted version of the BasicComponentTextFormatType that does not allow for multi-lingual values.
 * 
 * <p>Java-Klasse f�r SimpleComponentTextFormatType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SimpleComponentTextFormatType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}BasicComponentTextFormatType">
 *       &lt;attribute name="textType" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleDataType" default="String" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleComponentTextFormatType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlSeeAlso({
    CodededTextFormatType.class,
    CodingTextFormatType.class,
    TimeTextFormatType.class,
    NonFacetedTextFormatType.class
})
public class SimpleComponentTextFormatType
    extends BasicComponentTextFormatType
{


}
