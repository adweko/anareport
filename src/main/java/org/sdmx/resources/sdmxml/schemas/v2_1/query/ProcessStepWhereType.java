
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ProcessStepWhereType defines a set of parameters for matching a category. All supplied parameters must be matched in order for an object to satisfy the query. In addition to the base item parameters, there are also parameters for matching based on the objects which serve as the input or output to the process step.
 * 
 * <p>Java-Klasse f�r ProcessStepWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProcessStepWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}IdentifiableWhereType">
 *       &lt;sequence>
 *         &lt;element name="InputOrOutputObject" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}InputOrOutputObjectType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProcessStepWhere" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ProcessStepWhereType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessStepWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "inputOrOutputObject",
    "processStepWhere"
})
public class ProcessStepWhereType
    extends IdentifiableWhereType
{

    @XmlElement(name = "InputOrOutputObject", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<InputOrOutputObjectType> inputOrOutputObject;
    @XmlElement(name = "ProcessStepWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<ProcessStepWhereType> processStepWhere;

    /**
     * Gets the value of the inputOrOutputObject property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inputOrOutputObject property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInputOrOutputObject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InputOrOutputObjectType }
     * 
     * 
     */
    public List<InputOrOutputObjectType> getInputOrOutputObject() {
        if (inputOrOutputObject == null) {
            inputOrOutputObject = new ArrayList<InputOrOutputObjectType>();
        }
        return this.inputOrOutputObject;
    }

    /**
     * Gets the value of the processStepWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the processStepWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcessStepWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessStepWhereType }
     * 
     * 
     */
    public List<ProcessStepWhereType> getProcessStepWhere() {
        if (processStepWhere == null) {
            processStepWhere = new ArrayList<ProcessStepWhereType>();
        }
        return this.processStepWhere;
    }

}
