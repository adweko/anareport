
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * AnnotationWhereType defines the structure for querying the details of an annotation.
 * 
 * <p>Java-Klasse f�r AnnotationWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AnnotationWhereType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryStringType" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryStringType" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnnotationWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "type",
    "title",
    "text"
})
public class AnnotationWhereType {

    @XmlElement(name = "Type", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected QueryStringType type;
    @XmlElement(name = "Title", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected QueryStringType title;
    @XmlElement(name = "Text", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected QueryTextType text;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QueryStringType }
     *     
     */
    public QueryStringType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryStringType }
     *     
     */
    public void setType(QueryStringType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QueryStringType }
     *     
     */
    public QueryStringType getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryStringType }
     *     
     */
    public void setTitle(QueryStringType value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QueryTextType }
     *     
     */
    public QueryTextType getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryTextType }
     *     
     */
    public void setText(QueryTextType value) {
        this.text = value;
    }

}
