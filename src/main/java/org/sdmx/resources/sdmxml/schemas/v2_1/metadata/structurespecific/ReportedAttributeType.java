
package org.sdmx.resources.sdmxml.schemas.v2_1.metadata.structurespecific;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TextType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.XHTMLType;


/**
 * ReportedAttributeType is an abstract base type that forms the basis for a metadata structure specific metadata attribute. A value for the attribute can be supplied as either a single value, or multi-lingual text values (either structured or unstructured). An optional set of child metadata attributes is also available if the metadata attribute definition defines nested metadata attributes. The metadata structure definition specific schema will refine this type for each metadata attribute such that the content can be validation against what is defined in the metadata structure definition.
 * 
 * <p>Java-Klasse f�r ReportedAttributeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReportedAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Text" maxOccurs="unbounded"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructuredText" maxOccurs="unbounded"/>
 *         &lt;/choice>
 *         &lt;element name="AttributeSet" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0" form="unqualified"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="isMetadataAttribute" type="{http://www.w3.org/2001/XMLSchema}boolean" fixed="true" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportedAttributeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific", propOrder = {
    "text",
    "structuredText",
    "attributeSet"
})
public abstract class ReportedAttributeType
    extends AnnotableType
{

    @XmlElement(name = "Text", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<TextType> text;
    @XmlElement(name = "StructuredText", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<XHTMLType> structuredText;
    @XmlElement(name = "AttributeSet")
    protected Object attributeSet;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "value")
    @XmlSchemaType(name = "anySimpleType")
    protected String value;
    @XmlAttribute(name = "isMetadataAttribute", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/structurespecific")
    protected Boolean isMetadataAttribute;

    /**
     * Text is used to supply parallel multi-lingual textual values for the reported metadata attribute. This will be used if the text format of the metadata attribute has a type of string and the multi-lingual value is set to true.Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * StructuredText is used to supply parallel multi-lingual structured (as XHTML) textual values for the reported metadata attribute. This will be used if the text format of the metadata attribute has a type of XHTML and the multi-lingual value is set to true. If the multi-lingual flag is not set to true, it is expected that the maximum occurrence of this will be refined to be 1 in the metadata structure definition specific schema.Gets the value of the structuredText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the structuredText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStructuredText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XHTMLType }
     * 
     * 
     */
    public List<XHTMLType> getStructuredText() {
        if (structuredText == null) {
            structuredText = new ArrayList<XHTMLType>();
        }
        return this.structuredText;
    }

    /**
     * Ruft den Wert der attributeSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAttributeSet() {
        return attributeSet;
    }

    /**
     * Legt den Wert der attributeSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAttributeSet(Object value) {
        this.attributeSet = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Ruft den Wert der isMetadataAttribute-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsMetadataAttribute() {
        if (isMetadataAttribute == null) {
            return true;
        } else {
            return isMetadataAttribute;
        }
    }

    /**
     * Legt den Wert der isMetadataAttribute-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsMetadataAttribute(Boolean value) {
        this.isMetadataAttribute = value;
    }

}
