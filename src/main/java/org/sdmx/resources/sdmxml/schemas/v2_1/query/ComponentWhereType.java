
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ConceptReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ItemSchemeReferenceBaseType;


/**
 * ComponentWhereType is an abstract base type that serves as the basis for a query for a component within a component list where or a structure query. A concept identity and a local representation condition are available to seek a component that utilizes a particular concept or representation scheme. The conditions within a component query are implied to be in an and-query. If an id and a concept identity condition are supplied, then both conditions will have to met in order for the component query to return true. If, for instance, a query based on names in multiple languages is required, then multiple instances of the element utilizing this type should be used within an or-query container.
 * 
 * <p>Java-Klasse f�r ComponentWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}IdentifiableWhereType">
 *       &lt;sequence>
 *         &lt;element name="ConceptIdentity" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" minOccurs="0"/>
 *         &lt;element name="Enumeration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeReferenceBaseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "conceptIdentity",
    "enumeration"
})
@XmlSeeAlso({
    MeasureDimensionWhereBaseType.class,
    DataStructureComponentWhereType.class,
    MetadataAttributeWhereBaseType.class,
    TargetObjectWhereBaseType.class
})
public abstract class ComponentWhereType
    extends IdentifiableWhereType
{

    @XmlElement(name = "ConceptIdentity", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected ConceptReferenceType conceptIdentity;
    @XmlElement(name = "Enumeration", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected ItemSchemeReferenceBaseType enumeration;

    /**
     * Ruft den Wert der conceptIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConceptReferenceType }
     *     
     */
    public ConceptReferenceType getConceptIdentity() {
        return conceptIdentity;
    }

    /**
     * Legt den Wert der conceptIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptReferenceType }
     *     
     */
    public void setConceptIdentity(ConceptReferenceType value) {
        this.conceptIdentity = value;
    }

    /**
     * Ruft den Wert der enumeration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ItemSchemeReferenceBaseType }
     *     
     */
    public ItemSchemeReferenceBaseType getEnumeration() {
        return enumeration;
    }

    /**
     * Legt den Wert der enumeration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemSchemeReferenceBaseType }
     *     
     */
    public void setEnumeration(ItemSchemeReferenceBaseType value) {
        this.enumeration = value;
    }

}
