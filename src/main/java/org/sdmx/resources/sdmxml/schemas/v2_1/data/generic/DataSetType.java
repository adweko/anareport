
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ActionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;


/**
 * DataSetType defines the structure of the generic data set. Data is organised into either a collection of series (grouped observations) or a collection of un-grouped observations. The organisation used is dependent on the structure specification in the header of the data message (which is referenced via the structureRef attribute). The structure specification states which data occurs at the observation level. If this dimension is "AllDimensions" then the data set must consist of a collection of un-grouped observations; otherwise the data set will contain a collection of series with the observations in the series disambiguated by the specified dimension at the observation level. This data set is capable of containing data (observed values) and/or documentation (attribute values). It is assumed that each series or un-grouped observation will be distinct in its purpose. For example, if series contains both data and documentation, it assumed that each series will have a unique key. If the series contains only data or only documentation, then it is possible that another series with the same key might exist, but with not with the same purpose (i.e. to provide data or documentation) as the first series.
 * 
 * <p>Java-Klasse f�r DataSetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataSetType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0"/>
 *         &lt;element name="Attributes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType" minOccurs="0"/>
 *         &lt;element name="Group" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}GroupType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="Series" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}SeriesType" maxOccurs="unbounded"/>
 *           &lt;element name="Obs" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ObsOnlyType" maxOccurs="unbounded"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetAttributeGroup"/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", propOrder = {
    "dataProvider",
    "attributes",
    "group",
    "series",
    "obs"
})
@XmlSeeAlso({
    TimeSeriesDataSetType.class
})
public class DataSetType
    extends AnnotableType
{

    @XmlElement(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected DataProviderReferenceType dataProvider;
    @XmlElement(name = "Attributes", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected ValuesType attributes;
    @XmlElement(name = "Group", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected List<GroupType> group;
    @XmlElement(name = "Series", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected List<SeriesType> series;
    @XmlElement(name = "Obs", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected List<ObsOnlyType> obs;
    @XmlAttribute(name = "structureRef", required = true)
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object structureRef;
    @XmlAttribute(name = "setID")
    protected String setID;
    @XmlAttribute(name = "action")
    protected ActionType action;
    @XmlAttribute(name = "reportingBeginDate")
    protected List<String> reportingBeginDate;
    @XmlAttribute(name = "reportingEndDate")
    protected List<String> reportingEndDate;
    @XmlAttribute(name = "validFromDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validFromDate;
    @XmlAttribute(name = "validToDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validToDate;
    @XmlAttribute(name = "publicationYear")
    @XmlSchemaType(name = "gYear")
    protected XMLGregorianCalendar publicationYear;
    @XmlAttribute(name = "publicationPeriod")
    protected List<String> publicationPeriod;

    /**
     * Ruft den Wert der dataProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public DataProviderReferenceType getDataProvider() {
        return dataProvider;
    }

    /**
     * Legt den Wert der dataProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public void setDataProvider(DataProviderReferenceType value) {
        this.dataProvider = value;
    }

    /**
     * Ruft den Wert der attributes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValuesType }
     *     
     */
    public ValuesType getAttributes() {
        return attributes;
    }

    /**
     * Legt den Wert der attributes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuesType }
     *     
     */
    public void setAttributes(ValuesType value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the group property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the group property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupType }
     * 
     * 
     */
    public List<GroupType> getGroup() {
        if (group == null) {
            group = new ArrayList<GroupType>();
        }
        return this.group;
    }

    /**
     * Gets the value of the series property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the series property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SeriesType }
     * 
     * 
     */
    public List<SeriesType> getSeries() {
        if (series == null) {
            series = new ArrayList<SeriesType>();
        }
        return this.series;
    }

    /**
     * Gets the value of the obs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the obs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObsOnlyType }
     * 
     * 
     */
    public List<ObsOnlyType> getObs() {
        if (obs == null) {
            obs = new ArrayList<ObsOnlyType>();
        }
        return this.obs;
    }

    /**
     * Ruft den Wert der structureRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getStructureRef() {
        return structureRef;
    }

    /**
     * Legt den Wert der structureRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setStructureRef(Object value) {
        this.structureRef = value;
    }

    /**
     * Ruft den Wert der setID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetID() {
        return setID;
    }

    /**
     * Legt den Wert der setID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetID(String value) {
        this.setID = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setAction(ActionType value) {
        this.action = value;
    }

    /**
     * Gets the value of the reportingBeginDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingBeginDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingBeginDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportingBeginDate() {
        if (reportingBeginDate == null) {
            reportingBeginDate = new ArrayList<String>();
        }
        return this.reportingBeginDate;
    }

    /**
     * Gets the value of the reportingEndDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingEndDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingEndDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportingEndDate() {
        if (reportingEndDate == null) {
            reportingEndDate = new ArrayList<String>();
        }
        return this.reportingEndDate;
    }

    /**
     * Ruft den Wert der validFromDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidFromDate() {
        return validFromDate;
    }

    /**
     * Legt den Wert der validFromDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFromDate(XMLGregorianCalendar value) {
        this.validFromDate = value;
    }

    /**
     * Ruft den Wert der validToDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidToDate() {
        return validToDate;
    }

    /**
     * Legt den Wert der validToDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidToDate(XMLGregorianCalendar value) {
        this.validToDate = value;
    }

    /**
     * Ruft den Wert der publicationYear-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPublicationYear() {
        return publicationYear;
    }

    /**
     * Legt den Wert der publicationYear-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPublicationYear(XMLGregorianCalendar value) {
        this.publicationYear = value;
    }

    /**
     * Gets the value of the publicationPeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the publicationPeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPublicationPeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPublicationPeriod() {
        if (publicationPeriod == null) {
            publicationPeriod = new ArrayList<String>();
        }
        return this.publicationPeriod;
    }

}
