
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The Hierarchy is an abstract type that provides for a classification structure of referenced codes arranged in levels of detail from the broadest to the most detailed level. The levels in which the code exist can be formal or informal.
 * 
 * <p>Java-Klasse f�r HierarchyType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HierarchyType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HierarchyBaseType">
 *       &lt;sequence>
 *         &lt;element name="HierarchicalCode" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HierarchicalCodeType" maxOccurs="unbounded"/>
 *         &lt;element name="Level" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}LevelType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="leveled" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HierarchyType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "hierarchicalCode",
    "level"
})
public class HierarchyType
    extends HierarchyBaseType
{

    @XmlElement(name = "HierarchicalCode", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected List<HierarchicalCodeType> hierarchicalCode;
    @XmlElement(name = "Level", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected LevelType level;
    @XmlAttribute(name = "leveled")
    protected Boolean leveled;

    /**
     * Gets the value of the hierarchicalCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hierarchicalCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHierarchicalCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HierarchicalCodeType }
     * 
     * 
     */
    public List<HierarchicalCodeType> getHierarchicalCode() {
        if (hierarchicalCode == null) {
            hierarchicalCode = new ArrayList<HierarchicalCodeType>();
        }
        return this.hierarchicalCode;
    }

    /**
     * Ruft den Wert der level-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LevelType }
     *     
     */
    public LevelType getLevel() {
        return level;
    }

    /**
     * Legt den Wert der level-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LevelType }
     *     
     */
    public void setLevel(LevelType value) {
        this.level = value;
    }

    /**
     * Ruft den Wert der leveled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isLeveled() {
        if (leveled == null) {
            return false;
        } else {
            return leveled;
        }
    }

    /**
     * Legt den Wert der leveled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLeveled(Boolean value) {
        this.leveled = value;
    }

}
