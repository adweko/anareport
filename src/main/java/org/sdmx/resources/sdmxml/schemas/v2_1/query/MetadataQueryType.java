
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataQueryType defines the structure of a reference metadata query. Reference metadata is queried as individual reports. The result of this query will be a collection of metadata sets, with only the relevant metadata reports contained within them. If no report level parameters are specified, then the query will result in entire metadata sets being returned.
 * 
 * <p>Java-Klasse f�r MetadataQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataReturnDetailsType"/>
 *         &lt;element name="MetadataParameters" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataParametersAndType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "returnDetails",
    "metadataParameters"
})
public class MetadataQueryType {

    @XmlElement(name = "ReturnDetails", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected MetadataReturnDetailsType returnDetails;
    @XmlElement(name = "MetadataParameters", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected MetadataParametersAndType metadataParameters;

    /**
     * Ruft den Wert der returnDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MetadataReturnDetailsType }
     *     
     */
    public MetadataReturnDetailsType getReturnDetails() {
        return returnDetails;
    }

    /**
     * Legt den Wert der returnDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MetadataReturnDetailsType }
     *     
     */
    public void setReturnDetails(MetadataReturnDetailsType value) {
        this.returnDetails = value;
    }

    /**
     * Ruft den Wert der metadataParameters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MetadataParametersAndType }
     *     
     */
    public MetadataParametersAndType getMetadataParameters() {
        return metadataParameters;
    }

    /**
     * Legt den Wert der metadataParameters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MetadataParametersAndType }
     *     
     */
    public void setMetadataParameters(MetadataParametersAndType value) {
        this.metadataParameters = value;
    }

}
