
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ProcessStepType describes the structure of a process step. A nested process step is automatically sub-ordinate, and followed as the next step. If the following step is conditional, it should be referenced in a transition.
 * 
 * <p>Java-Klasse f�r ProcessStepType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProcessStepType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ProcessStepBaseType">
 *       &lt;sequence>
 *         &lt;element name="Input" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}InputOutputType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Output" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}InputOutputType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Computation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ComputationType" minOccurs="0"/>
 *         &lt;element name="Transition" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}TransitionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProcessStep" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ProcessStepType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessStepType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "input",
    "output",
    "computation",
    "transition",
    "processStep"
})
public class ProcessStepType
    extends ProcessStepBaseType
{

    @XmlElement(name = "Input", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<InputOutputType> input;
    @XmlElement(name = "Output", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<InputOutputType> output;
    @XmlElement(name = "Computation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ComputationType computation;
    @XmlElement(name = "Transition", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<TransitionType> transition;
    @XmlElement(name = "ProcessStep", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<ProcessStepType> processStep;

    /**
     * Gets the value of the input property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the input property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInput().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InputOutputType }
     * 
     * 
     */
    public List<InputOutputType> getInput() {
        if (input == null) {
            input = new ArrayList<InputOutputType>();
        }
        return this.input;
    }

    /**
     * Gets the value of the output property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the output property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOutput().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InputOutputType }
     * 
     * 
     */
    public List<InputOutputType> getOutput() {
        if (output == null) {
            output = new ArrayList<InputOutputType>();
        }
        return this.output;
    }

    /**
     * Ruft den Wert der computation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComputationType }
     *     
     */
    public ComputationType getComputation() {
        return computation;
    }

    /**
     * Legt den Wert der computation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComputationType }
     *     
     */
    public void setComputation(ComputationType value) {
        this.computation = value;
    }

    /**
     * Gets the value of the transition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransitionType }
     * 
     * 
     */
    public List<TransitionType> getTransition() {
        if (transition == null) {
            transition = new ArrayList<TransitionType>();
        }
        return this.transition;
    }

    /**
     * Gets the value of the processStep property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the processStep property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcessStep().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessStepType }
     * 
     * 
     */
    public List<ProcessStepType> getProcessStep() {
        if (processStep == null) {
            processStep = new ArrayList<ProcessStepType>();
        }
        return this.processStep;
    }

}
