
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeSeriesDataReturnDetailsType specifies the specifics of the how data should be returned as it pertains to a request for time series only oriented data in the structured format.
 * 
 * <p>Java-Klasse f�r TimeSeriesDataReturnDetailsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeSeriesDataReturnDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataReturnDetailsType">
 *       &lt;sequence>
 *         &lt;element name="FirstNObservations" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LastNObservations" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeSeriesDataStructureRequestType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="observationAction" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ObservationActionCodeType" default="Active" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeSeriesDataReturnDetailsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class TimeSeriesDataReturnDetailsType
    extends DataReturnDetailsType
{


}
