
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * OrganisationUnitSchemeRefType contains a set of reference fields for an organisation unit scheme.
 * 
 * <p>Java-Klasse f�r OrganisationUnitSchemeRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrganisationUnitSchemeRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationSchemeRefBaseType">
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationSchemeTypeCodelistType" fixed="OrganisationUnitScheme" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationUnitSchemeRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class OrganisationUnitSchemeRefType
    extends OrganisationSchemeRefBaseType
{


}
