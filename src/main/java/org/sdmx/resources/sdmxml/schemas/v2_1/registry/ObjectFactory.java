
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CategoryReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.sdmx.resources.sdmxml.schemas.v2_1.registry package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MetadataRegistrationEventsTypeRegistrationID_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "RegistrationID");
    private final static QName _MetadataRegistrationEventsTypeMetadataStructureDefinitionReference_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "MetadataStructureDefinitionReference");
    private final static QName _MetadataRegistrationEventsTypeProvisionAgreement_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "ProvisionAgreement");
    private final static QName _MetadataRegistrationEventsTypeCategory_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "Category");
    private final static QName _MetadataRegistrationEventsTypeMetadataflowReference_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "MetadataflowReference");
    private final static QName _MetadataRegistrationEventsTypeDataProvider_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "DataProvider");
    private final static QName _StructuralRepositoryEventsTypeAttachmentConstraint_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "AttachmentConstraint");
    private final static QName _StructuralRepositoryEventsTypeDataflow_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "Dataflow");
    private final static QName _StructuralRepositoryEventsTypeOrganisationUnitScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "OrganisationUnitScheme");
    private final static QName _StructuralRepositoryEventsTypeStructureSet_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "StructureSet");
    private final static QName _StructuralRepositoryEventsTypeMetadataflow_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "Metadataflow");
    private final static QName _StructuralRepositoryEventsTypeConceptScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "ConceptScheme");
    private final static QName _StructuralRepositoryEventsTypeCategorisation_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "Categorisation");
    private final static QName _StructuralRepositoryEventsTypeKeyFamily_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "KeyFamily");
    private final static QName _StructuralRepositoryEventsTypeDataConsmerScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "DataConsmerScheme");
    private final static QName _StructuralRepositoryEventsTypeReportingTaxonomy_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "ReportingTaxonomy");
    private final static QName _StructuralRepositoryEventsTypeDataProviderScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "DataProviderScheme");
    private final static QName _StructuralRepositoryEventsTypeMetadataStructureDefinition_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "MetadataStructureDefinition");
    private final static QName _StructuralRepositoryEventsTypeContentConstraint_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "ContentConstraint");
    private final static QName _StructuralRepositoryEventsTypeProcess_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "Process");
    private final static QName _StructuralRepositoryEventsTypeHierarchicalCodelist_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "HierarchicalCodelist");
    private final static QName _StructuralRepositoryEventsTypeAgencyScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "AgencyScheme");
    private final static QName _StructuralRepositoryEventsTypeCategoryScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "CategoryScheme");
    private final static QName _StructuralRepositoryEventsTypeCodelist_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "Codelist");
    private final static QName _DataRegistrationEventsTypeKeyFamilyReference_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "KeyFamilyReference");
    private final static QName _DataRegistrationEventsTypeDataflowReference_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", "DataflowReference");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.sdmx.resources.sdmxml.schemas.v2_1.registry
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NotifyRegistryEventType }
     * 
     */
    public NotifyRegistryEventType createNotifyRegistryEventType() {
        return new NotifyRegistryEventType();
    }

    /**
     * Create an instance of {@link NotificationURLType }
     * 
     */
    public NotificationURLType createNotificationURLType() {
        return new NotificationURLType();
    }

    /**
     * Create an instance of {@link StructuralEventType }
     * 
     */
    public StructuralEventType createStructuralEventType() {
        return new StructuralEventType();
    }

    /**
     * Create an instance of {@link IdentifiableQueryType }
     * 
     */
    public IdentifiableQueryType createIdentifiableQueryType() {
        return new IdentifiableQueryType();
    }

    /**
     * Create an instance of {@link SubmitStructureRequestType }
     * 
     */
    public SubmitStructureRequestType createSubmitStructureRequestType() {
        return new SubmitStructureRequestType();
    }

    /**
     * Create an instance of {@link SubmitRegistrationsResponseType }
     * 
     */
    public SubmitRegistrationsResponseType createSubmitRegistrationsResponseType() {
        return new SubmitRegistrationsResponseType();
    }

    /**
     * Create an instance of {@link SubmittedStructureType }
     * 
     */
    public SubmittedStructureType createSubmittedStructureType() {
        return new SubmittedStructureType();
    }

    /**
     * Create an instance of {@link VersionableQueryType }
     * 
     */
    public VersionableQueryType createVersionableQueryType() {
        return new VersionableQueryType();
    }

    /**
     * Create an instance of {@link SubmissionResultType }
     * 
     */
    public SubmissionResultType createSubmissionResultType() {
        return new SubmissionResultType();
    }

    /**
     * Create an instance of {@link RegistrationRequestType }
     * 
     */
    public RegistrationRequestType createRegistrationRequestType() {
        return new RegistrationRequestType();
    }

    /**
     * Create an instance of {@link SubmitStructureResponseType }
     * 
     */
    public SubmitStructureResponseType createSubmitStructureResponseType() {
        return new SubmitStructureResponseType();
    }

    /**
     * Create an instance of {@link SubscriptionType }
     * 
     */
    public SubscriptionType createSubscriptionType() {
        return new SubscriptionType();
    }

    /**
     * Create an instance of {@link QueryableDataSourceType }
     * 
     */
    public QueryableDataSourceType createQueryableDataSourceType() {
        return new QueryableDataSourceType();
    }

    /**
     * Create an instance of {@link MaintainableQueryType }
     * 
     */
    public MaintainableQueryType createMaintainableQueryType() {
        return new MaintainableQueryType();
    }

    /**
     * Create an instance of {@link QueryResultType }
     * 
     */
    public QueryResultType createQueryResultType() {
        return new QueryResultType();
    }

    /**
     * Create an instance of {@link ValidityPeriodType }
     * 
     */
    public ValidityPeriodType createValidityPeriodType() {
        return new ValidityPeriodType();
    }

    /**
     * Create an instance of {@link MetadataRegistrationEventsType }
     * 
     */
    public MetadataRegistrationEventsType createMetadataRegistrationEventsType() {
        return new MetadataRegistrationEventsType();
    }

    /**
     * Create an instance of {@link DataRegistrationEventsType }
     * 
     */
    public DataRegistrationEventsType createDataRegistrationEventsType() {
        return new DataRegistrationEventsType();
    }

    /**
     * Create an instance of {@link DataSourceType }
     * 
     */
    public DataSourceType createDataSourceType() {
        return new DataSourceType();
    }

    /**
     * Create an instance of {@link RegistrationType }
     * 
     */
    public RegistrationType createRegistrationType() {
        return new RegistrationType();
    }

    /**
     * Create an instance of {@link VersionableObjectEventType }
     * 
     */
    public VersionableObjectEventType createVersionableObjectEventType() {
        return new VersionableObjectEventType();
    }

    /**
     * Create an instance of {@link QuerySubscriptionResponseType }
     * 
     */
    public QuerySubscriptionResponseType createQuerySubscriptionResponseType() {
        return new QuerySubscriptionResponseType();
    }

    /**
     * Create an instance of {@link SimpleDataSourceType }
     * 
     */
    public SimpleDataSourceType createSimpleDataSourceType() {
        return new SimpleDataSourceType();
    }

    /**
     * Create an instance of {@link QueryRegistrationResponseType }
     * 
     */
    public QueryRegistrationResponseType createQueryRegistrationResponseType() {
        return new QueryRegistrationResponseType();
    }

    /**
     * Create an instance of {@link EventSelectorType }
     * 
     */
    public EventSelectorType createEventSelectorType() {
        return new EventSelectorType();
    }

    /**
     * Create an instance of {@link StructuralRepositoryEventsType }
     * 
     */
    public StructuralRepositoryEventsType createStructuralRepositoryEventsType() {
        return new StructuralRepositoryEventsType();
    }

    /**
     * Create an instance of {@link SubmitSubscriptionsResponseType }
     * 
     */
    public SubmitSubscriptionsResponseType createSubmitSubscriptionsResponseType() {
        return new SubmitSubscriptionsResponseType();
    }

    /**
     * Create an instance of {@link IdentifiableObjectEventType }
     * 
     */
    public IdentifiableObjectEventType createIdentifiableObjectEventType() {
        return new IdentifiableObjectEventType();
    }

    /**
     * Create an instance of {@link QueryRegistrationRequestType }
     * 
     */
    public QueryRegistrationRequestType createQueryRegistrationRequestType() {
        return new QueryRegistrationRequestType();
    }

    /**
     * Create an instance of {@link SubscriptionRequestType }
     * 
     */
    public SubscriptionRequestType createSubscriptionRequestType() {
        return new SubscriptionRequestType();
    }

    /**
     * Create an instance of {@link ResultType }
     * 
     */
    public ResultType createResultType() {
        return new ResultType();
    }

    /**
     * Create an instance of {@link RegistrationStatusType }
     * 
     */
    public RegistrationStatusType createRegistrationStatusType() {
        return new RegistrationStatusType();
    }

    /**
     * Create an instance of {@link SubscriptionStatusType }
     * 
     */
    public SubscriptionStatusType createSubscriptionStatusType() {
        return new SubscriptionStatusType();
    }

    /**
     * Create an instance of {@link SubmitRegistrationsRequestType }
     * 
     */
    public SubmitRegistrationsRequestType createSubmitRegistrationsRequestType() {
        return new SubmitRegistrationsRequestType();
    }

    /**
     * Create an instance of {@link StatusMessageType }
     * 
     */
    public StatusMessageType createStatusMessageType() {
        return new StatusMessageType();
    }

    /**
     * Create an instance of {@link QuerySubscriptionRequestType }
     * 
     */
    public QuerySubscriptionRequestType createQuerySubscriptionRequestType() {
        return new QuerySubscriptionRequestType();
    }

    /**
     * Create an instance of {@link MaintainableEventType }
     * 
     */
    public MaintainableEventType createMaintainableEventType() {
        return new MaintainableEventType();
    }

    /**
     * Create an instance of {@link SubmitSubscriptionsRequestType }
     * 
     */
    public SubmitSubscriptionsRequestType createSubmitSubscriptionsRequestType() {
        return new SubmitSubscriptionsRequestType();
    }

    /**
     * Create an instance of {@link RegistrationEventType }
     * 
     */
    public RegistrationEventType createRegistrationEventType() {
        return new RegistrationEventType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "RegistrationID", scope = MetadataRegistrationEventsType.class)
    public JAXBElement<String> createMetadataRegistrationEventsTypeRegistrationID(String value) {
        return new JAXBElement<String>(_MetadataRegistrationEventsTypeRegistrationID_QNAME, String.class, MetadataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainableEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "MetadataStructureDefinitionReference", scope = MetadataRegistrationEventsType.class)
    public JAXBElement<MaintainableEventType> createMetadataRegistrationEventsTypeMetadataStructureDefinitionReference(MaintainableEventType value) {
        return new JAXBElement<MaintainableEventType>(_MetadataRegistrationEventsTypeMetadataStructureDefinitionReference_QNAME, MaintainableEventType.class, MetadataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisionAgreementReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "ProvisionAgreement", scope = MetadataRegistrationEventsType.class)
    public JAXBElement<ProvisionAgreementReferenceType> createMetadataRegistrationEventsTypeProvisionAgreement(ProvisionAgreementReferenceType value) {
        return new JAXBElement<ProvisionAgreementReferenceType>(_MetadataRegistrationEventsTypeProvisionAgreement_QNAME, ProvisionAgreementReferenceType.class, MetadataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "Category", scope = MetadataRegistrationEventsType.class)
    public JAXBElement<CategoryReferenceType> createMetadataRegistrationEventsTypeCategory(CategoryReferenceType value) {
        return new JAXBElement<CategoryReferenceType>(_MetadataRegistrationEventsTypeCategory_QNAME, CategoryReferenceType.class, MetadataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainableEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "MetadataflowReference", scope = MetadataRegistrationEventsType.class)
    public JAXBElement<MaintainableEventType> createMetadataRegistrationEventsTypeMetadataflowReference(MaintainableEventType value) {
        return new JAXBElement<MaintainableEventType>(_MetadataRegistrationEventsTypeMetadataflowReference_QNAME, MaintainableEventType.class, MetadataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataProviderReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "DataProvider", scope = MetadataRegistrationEventsType.class)
    public JAXBElement<DataProviderReferenceType> createMetadataRegistrationEventsTypeDataProvider(DataProviderReferenceType value) {
        return new JAXBElement<DataProviderReferenceType>(_MetadataRegistrationEventsTypeDataProvider_QNAME, DataProviderReferenceType.class, MetadataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "AttachmentConstraint", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeAttachmentConstraint(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeAttachmentConstraint_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "Dataflow", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeDataflow(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeDataflow_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "OrganisationUnitScheme", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeOrganisationUnitScheme(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeOrganisationUnitScheme_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "StructureSet", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeStructureSet(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeStructureSet_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "Metadataflow", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeMetadataflow(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeMetadataflow_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "ConceptScheme", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeConceptScheme(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeConceptScheme_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "ProvisionAgreement", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeProvisionAgreement(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_MetadataRegistrationEventsTypeProvisionAgreement_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentifiableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "Categorisation", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<IdentifiableObjectEventType> createStructuralRepositoryEventsTypeCategorisation(IdentifiableObjectEventType value) {
        return new JAXBElement<IdentifiableObjectEventType>(_StructuralRepositoryEventsTypeCategorisation_QNAME, IdentifiableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "KeyFamily", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeKeyFamily(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeKeyFamily_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "DataConsmerScheme", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeDataConsmerScheme(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeDataConsmerScheme_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "ReportingTaxonomy", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeReportingTaxonomy(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeReportingTaxonomy_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "DataProviderScheme", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeDataProviderScheme(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeDataProviderScheme_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "MetadataStructureDefinition", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeMetadataStructureDefinition(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeMetadataStructureDefinition_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "ContentConstraint", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeContentConstraint(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeContentConstraint_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "Process", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeProcess(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeProcess_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "HierarchicalCodelist", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeHierarchicalCodelist(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeHierarchicalCodelist_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "AgencyScheme", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeAgencyScheme(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeAgencyScheme_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "CategoryScheme", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeCategoryScheme(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeCategoryScheme_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "Codelist", scope = StructuralRepositoryEventsType.class)
    public JAXBElement<VersionableObjectEventType> createStructuralRepositoryEventsTypeCodelist(VersionableObjectEventType value) {
        return new JAXBElement<VersionableObjectEventType>(_StructuralRepositoryEventsTypeCodelist_QNAME, VersionableObjectEventType.class, StructuralRepositoryEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainableEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "KeyFamilyReference", scope = DataRegistrationEventsType.class)
    public JAXBElement<MaintainableEventType> createDataRegistrationEventsTypeKeyFamilyReference(MaintainableEventType value) {
        return new JAXBElement<MaintainableEventType>(_DataRegistrationEventsTypeKeyFamilyReference_QNAME, MaintainableEventType.class, DataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainableEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "DataflowReference", scope = DataRegistrationEventsType.class)
    public JAXBElement<MaintainableEventType> createDataRegistrationEventsTypeDataflowReference(MaintainableEventType value) {
        return new JAXBElement<MaintainableEventType>(_DataRegistrationEventsTypeDataflowReference_QNAME, MaintainableEventType.class, DataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "RegistrationID", scope = DataRegistrationEventsType.class)
    public JAXBElement<String> createDataRegistrationEventsTypeRegistrationID(String value) {
        return new JAXBElement<String>(_MetadataRegistrationEventsTypeRegistrationID_QNAME, String.class, DataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisionAgreementReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "ProvisionAgreement", scope = DataRegistrationEventsType.class)
    public JAXBElement<ProvisionAgreementReferenceType> createDataRegistrationEventsTypeProvisionAgreement(ProvisionAgreementReferenceType value) {
        return new JAXBElement<ProvisionAgreementReferenceType>(_MetadataRegistrationEventsTypeProvisionAgreement_QNAME, ProvisionAgreementReferenceType.class, DataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "Category", scope = DataRegistrationEventsType.class)
    public JAXBElement<CategoryReferenceType> createDataRegistrationEventsTypeCategory(CategoryReferenceType value) {
        return new JAXBElement<CategoryReferenceType>(_MetadataRegistrationEventsTypeCategory_QNAME, CategoryReferenceType.class, DataRegistrationEventsType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataProviderReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", name = "DataProvider", scope = DataRegistrationEventsType.class)
    public JAXBElement<DataProviderReferenceType> createDataRegistrationEventsTypeDataProvider(DataProviderReferenceType value) {
        return new JAXBElement<DataProviderReferenceType>(_MetadataRegistrationEventsTypeDataProvider_QNAME, DataProviderReferenceType.class, DataRegistrationEventsType.class, value);
    }

}
