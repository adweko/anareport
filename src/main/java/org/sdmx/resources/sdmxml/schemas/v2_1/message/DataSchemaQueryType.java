
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DataSchemaQueryType defines the structure of a data schema query message.
 * 
 * <p>Java-Klasse f�r DataSchemaQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataSchemaQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}MessageType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}BasicHeaderType"/>
 *         &lt;element name="Query" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataSchemaQueryType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSchemaQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
public class DataSchemaQueryType
    extends MessageType
{


}
