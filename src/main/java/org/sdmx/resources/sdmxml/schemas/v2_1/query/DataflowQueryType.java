
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DataflowQueryType defines the structure of a dataflow query. The parameters for the query are contained in the DataflowWhere element. The References element is used to indicate how objects that reference or are referenced from the matched dataflow should be returned.
 * 
 * <p>Java-Klasse f�r DataflowQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataflowQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructuralMetadataQueryType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableReturnDetailsType"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataflowWhere"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataflowQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class DataflowQueryType
    extends StructuralMetadataQueryType
{


}
