
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeValueType is a derivation of the BaseValueType which is used to provide a value for the time dimension. Since the identifier for the time dimension is fixed, the component reference for this structure is fixed. Note that this means that it is not necessary to provide a value in an instance as the fixed value will be provided in the post validation information set.
 * 
 * <p>Java-Klasse f�r TimeValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}BaseValueType">
 *       &lt;attribute name="id" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NCNameIDType" fixed="TIME_PERIOD" />
 *       &lt;attribute name="value" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationalTimePeriodType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
public class TimeValueType
    extends BaseValueType
{


}
