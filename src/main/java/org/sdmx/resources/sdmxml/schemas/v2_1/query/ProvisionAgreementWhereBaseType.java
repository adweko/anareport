
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ProvisionAgreementWhereBaseType is an abstract base type which forms the basis for the ProvisionAgreementWhereType.
 * 
 * <p>Java-Klasse f�r ProvisionAgreementWhereBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProvisionAgreementWhereBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableWhereType">
 *       &lt;sequence>
 *         &lt;element name="Annotation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AnnotationWhereType" minOccurs="0"/>
 *         &lt;element name="URN" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *         &lt;element name="Version" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionQueryType" minOccurs="0"/>
 *         &lt;element name="VersionTo" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" minOccurs="0"/>
 *         &lt;element name="VersionFrom" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" minOccurs="0"/>
 *         &lt;element name="VersionActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AgencyID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryNestedIDType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableTypeCodelistType" fixed="ProvisionAgreement" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvisionAgreementWhereBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlSeeAlso({
    ProvisionAgreementWhereType.class
})
public abstract class ProvisionAgreementWhereBaseType
    extends MaintainableWhereType
{


}
