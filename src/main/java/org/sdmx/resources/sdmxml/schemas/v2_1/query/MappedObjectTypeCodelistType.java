
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ConcreteMaintainableTypeCodelistType;


/**
 * <p>Java-Klasse f�r MappedObjectTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="MappedObjectTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConcreteMaintainableTypeCodelistType">
 *     &lt;enumeration value="AgencyScheme"/>
 *     &lt;enumeration value="CategoryScheme"/>
 *     &lt;enumeration value="Codelist"/>
 *     &lt;enumeration value="ConceptScheme"/>
 *     &lt;enumeration value="Dataflow"/>
 *     &lt;enumeration value="DataConsumerScheme"/>
 *     &lt;enumeration value="DataProviderScheme"/>
 *     &lt;enumeration value="DataStructure"/>
 *     &lt;enumeration value="HierarchicalCodelist"/>
 *     &lt;enumeration value="Metadataflow"/>
 *     &lt;enumeration value="MetadataStructure"/>
 *     &lt;enumeration value="OrganisationUnitScheme"/>
 *     &lt;enumeration value="ReportingTaxonomy"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MappedObjectTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum(ConcreteMaintainableTypeCodelistType.class)
public enum MappedObjectTypeCodelistType {

    @XmlEnumValue("AgencyScheme")
    AGENCY_SCHEME(ConcreteMaintainableTypeCodelistType.AGENCY_SCHEME),
    @XmlEnumValue("CategoryScheme")
    CATEGORY_SCHEME(ConcreteMaintainableTypeCodelistType.CATEGORY_SCHEME),
    @XmlEnumValue("Codelist")
    CODELIST(ConcreteMaintainableTypeCodelistType.CODELIST),
    @XmlEnumValue("ConceptScheme")
    CONCEPT_SCHEME(ConcreteMaintainableTypeCodelistType.CONCEPT_SCHEME),
    @XmlEnumValue("Dataflow")
    DATAFLOW(ConcreteMaintainableTypeCodelistType.DATAFLOW),
    @XmlEnumValue("DataConsumerScheme")
    DATA_CONSUMER_SCHEME(ConcreteMaintainableTypeCodelistType.DATA_CONSUMER_SCHEME),
    @XmlEnumValue("DataProviderScheme")
    DATA_PROVIDER_SCHEME(ConcreteMaintainableTypeCodelistType.DATA_PROVIDER_SCHEME),
    @XmlEnumValue("DataStructure")
    DATA_STRUCTURE(ConcreteMaintainableTypeCodelistType.DATA_STRUCTURE),
    @XmlEnumValue("HierarchicalCodelist")
    HIERARCHICAL_CODELIST(ConcreteMaintainableTypeCodelistType.HIERARCHICAL_CODELIST),
    @XmlEnumValue("Metadataflow")
    METADATAFLOW(ConcreteMaintainableTypeCodelistType.METADATAFLOW),
    @XmlEnumValue("MetadataStructure")
    METADATA_STRUCTURE(ConcreteMaintainableTypeCodelistType.METADATA_STRUCTURE),
    @XmlEnumValue("OrganisationUnitScheme")
    ORGANISATION_UNIT_SCHEME(ConcreteMaintainableTypeCodelistType.ORGANISATION_UNIT_SCHEME),
    @XmlEnumValue("ReportingTaxonomy")
    REPORTING_TAXONOMY(ConcreteMaintainableTypeCodelistType.REPORTING_TAXONOMY);
    private final ConcreteMaintainableTypeCodelistType value;

    MappedObjectTypeCodelistType(ConcreteMaintainableTypeCodelistType v) {
        value = v;
    }

    public ConcreteMaintainableTypeCodelistType value() {
        return value;
    }

    public static MappedObjectTypeCodelistType fromValue(ConcreteMaintainableTypeCodelistType v) {
        for (MappedObjectTypeCodelistType c: MappedObjectTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
