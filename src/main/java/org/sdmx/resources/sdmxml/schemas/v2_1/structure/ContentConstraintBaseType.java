
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ContentConstraintBaseType is an abstract base type that forms the basis for the ContentConstraintType.
 * 
 * <p>Java-Klasse f�r ContentConstraintBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ContentConstraintBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConstraintType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Name" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Description" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ConstraintAttachment" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ContentConstraintAttachmentType" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="DataKeySet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataKeySetType"/>
 *           &lt;element name="MetadataKeySet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataKeySetType"/>
 *           &lt;element name="CubeRegion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CubeRegionType"/>
 *           &lt;element name="MetadataTargetRegion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataTargetRegionType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContentConstraintBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlSeeAlso({
    ContentConstraintType.class
})
public abstract class ContentConstraintBaseType
    extends ConstraintType
{


}
