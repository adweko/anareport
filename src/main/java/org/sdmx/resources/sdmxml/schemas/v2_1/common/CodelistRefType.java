
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * CodelistRefType provides a reference to a codelist via a complete set of reference fields.
 * 
 * <p>Java-Klasse f�r CodelistRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CodelistRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeRefBaseType">
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeTypeCodelistType" fixed="Codelist" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemePackageTypeCodelistType" fixed="codelist" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodelistRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class CodelistRefType
    extends ItemSchemeRefBaseType
{


}
