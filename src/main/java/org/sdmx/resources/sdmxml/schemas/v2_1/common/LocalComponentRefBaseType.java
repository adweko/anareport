
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * LocalComponentRefBaseType is an abstract base type which provides a local reference to a component object.
 * 
 * <p>Java-Klasse f�r LocalComponentRefBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LocalComponentRefBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalComponentListComponentRefBaseType">
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocalComponentRefBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    LocalTargetObjectRefType.class,
    LocalComponentRefType.class,
    LocalPrimaryMeasureRefType.class,
    LocalDimensionRefType.class
})
public abstract class LocalComponentRefBaseType
    extends LocalComponentListComponentRefBaseType
{


}
