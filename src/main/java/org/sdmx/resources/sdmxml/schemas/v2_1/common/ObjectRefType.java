
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * ObjectRefType contains a set of reference fields for the purpose of referencing any object. This cannot be a local reference, therefore the agency identifier is required. It is also required that the class and package be supplied for the referenced object such that a complete URN reference can be built from the values provided. Note that this is not capable of fully validating that all necessary fields are supplied for a given object type.
 * 
 * <p>Java-Klasse f�r ObjectRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObjectRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}RefBaseType">
 *       &lt;attribute name="agencyID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedNCNameIDType" />
 *       &lt;attribute name="local" type="{http://www.w3.org/2001/XMLSchema}boolean" fixed="false" />
 *       &lt;attribute name="class" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" />
 *       &lt;attribute name="package" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PackageTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class ObjectRefType
    extends RefBaseType
{


}
