
package org.sdmx.resources.sdmxml.schemas.v2_1.message.footer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r SeverityCodeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SeverityCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Error"/>
 *     &lt;enumeration value="Warning"/>
 *     &lt;enumeration value="Information"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SeverityCodeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer")
@XmlEnum
public enum SeverityCodeType {


    /**
     * Error indicates the status message coresponds to an error.
     * 
     */
    @XmlEnumValue("Error")
    ERROR("Error"),

    /**
     * Warning indicates that the status message corresponds to a warning.
     * 
     */
    @XmlEnumValue("Warning")
    WARNING("Warning"),

    /**
     * Information indicates that the status message corresponds to an informational message.
     * 
     */
    @XmlEnumValue("Information")
    INFORMATION("Information");
    private final String value;

    SeverityCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SeverityCodeType fromValue(String v) {
        for (SeverityCodeType c: SeverityCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
