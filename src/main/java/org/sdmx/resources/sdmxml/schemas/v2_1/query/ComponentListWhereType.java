
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ComponentListWhereType is an abstract base type that serves as the basis for a query for a component list within a structure query. A list of component where children are provided to query for the list's child components. The conditions within a component list query are implied to be in an and-query. If an id and a child component where condition are supplied, then both conditions will have to met in order for the component list query to return true. If, for instance, a query based on names in multiple languages is required, then multiple instances of the element utilizing this type should be used within an or-query container.
 * 
 * <p>Java-Klasse f�r ComponentListWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentListWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}IdentifiableWhereType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ComponentWhere"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentListWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "componentWhere"
})
@XmlSeeAlso({
    ReportStructureWhereType.class,
    MetadataTargetWhereType.class,
    GroupWhereBaseType.class
})
public abstract class ComponentListWhereType
    extends IdentifiableWhereType
{

    @XmlElementRef(name = "ComponentWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ComponentWhereType>> componentWhere;

    /**
     * Gets the value of the componentWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the componentWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponentWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link DimensionWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link DimensionWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link MeasureDimensionWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ComponentWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link TimeDimensionWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link PrimaryMeasureWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link MetadataAttributeWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link TargetObjectWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link AttributeWhereType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ComponentWhereType>> getComponentWhere() {
        if (componentWhere == null) {
            componentWhere = new ArrayList<JAXBElement<? extends ComponentWhereType>>();
        }
        return this.componentWhere;
    }

}
