
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r AnyQueryType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="AnyQueryType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Any"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AnyQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum
public enum AnyQueryType {

    @XmlEnumValue("Any")
    ANY("Any");
    private final String value;

    AnyQueryType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AnyQueryType fromValue(String v) {
        for (AnyQueryType c: AnyQueryType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
