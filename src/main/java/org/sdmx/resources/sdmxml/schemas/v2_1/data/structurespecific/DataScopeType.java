
package org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r DataScopeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="DataScopeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DataStructure"/>
 *     &lt;enumeration value="ConstrainedDataStructure"/>
 *     &lt;enumeration value="Dataflow"/>
 *     &lt;enumeration value="ProvisionAgreement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DataScopeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
@XmlEnum
public enum DataScopeType {


    /**
     * The data set conforms simply to the data structure definition as it is defined, without regard to any constraints.
     * 
     */
    @XmlEnumValue("DataStructure")
    DATA_STRUCTURE("DataStructure"),

    /**
     * The data set conforms to the known allowable content constraints applied to the data structure definition.
     * 
     */
    @XmlEnumValue("ConstrainedDataStructure")
    CONSTRAINED_DATA_STRUCTURE("ConstrainedDataStructure"),

    /**
     * The data set conforms to the known allowable content constraints applied to the dataflow.
     * 
     */
    @XmlEnumValue("Dataflow")
    DATAFLOW("Dataflow"),

    /**
     * The data set conforms to the known allowable content constraints applied to the provision agreement.
     * 
     */
    @XmlEnumValue("ProvisionAgreement")
    PROVISION_AGREEMENT("ProvisionAgreement");
    private final String value;

    DataScopeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DataScopeType fromValue(String v) {
        for (DataScopeType c: DataScopeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
