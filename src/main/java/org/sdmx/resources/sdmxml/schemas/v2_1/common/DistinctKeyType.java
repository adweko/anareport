
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * DistinctKeyType is an abstract base type which is a special type of region that only defines a distinct region of data or metadata. For each component defined in the region, only a single values is provided. However, the same rules that apply to the general region about unstated components being wild carded apply here as well. Thus, this type can define a distinct full or partial key for data or metadata.
 * 
 * <p>Java-Klasse f�r DistinctKeyType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DistinctKeyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}RegionType">
 *       &lt;sequence>
 *         &lt;element name="KeyValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DinstinctKeyValueType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="include" type="{http://www.w3.org/2001/XMLSchema}boolean" fixed="true" />
 *       &lt;anyAttribute namespace=''/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DistinctKeyType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    DataKeyType.class,
    MetadataKeyType.class
})
public abstract class DistinctKeyType
    extends RegionType
{


}
