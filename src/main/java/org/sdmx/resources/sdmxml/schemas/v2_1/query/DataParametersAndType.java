
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DataParametersAndType refines the base data parameters to define a set of parameters joined by an "and" conditions. All of the parameters supplied in an instance of this type must be satisfied to result in a match. As a result of this condition, the maximum occurrence of some parameters has been reduced so as to not allow for impossible conditions to be specified (for example data cannot be matched is it is specified that the data set identifier should be "xyz" and the data identifier should be "abc".
 * 
 * <p>Java-Klasse f�r DataParametersAndType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataParametersAndType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersType">
 *       &lt;sequence>
 *         &lt;element name="DataSetID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0"/>
 *         &lt;element name="DataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureReferenceType" minOccurs="0"/>
 *         &lt;element name="Dataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType" minOccurs="0"/>
 *         &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Updated" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="ConceptValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ConceptValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RepresentationValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}CodeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DimensionValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DimensionValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TimeDimensionValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeDimensionValueType" minOccurs="0"/>
 *         &lt;element name="AttributeValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AttributeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PrimaryMeasureValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}PrimaryMeasureValueType" minOccurs="0"/>
 *         &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraintReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TimeFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeDataType" minOccurs="0"/>
 *         &lt;element name="Or" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersOrType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataParametersAndType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class DataParametersAndType
    extends DataParametersType
{


}
