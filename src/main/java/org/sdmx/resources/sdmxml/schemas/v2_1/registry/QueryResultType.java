
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * QueryResultType describes the structure of a query result for a single data source. Either a data result or metadata result is detailed, depending on the data source.
 * 
 * <p>Java-Klasse f�r QueryResultType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="QueryResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="DataResult" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}ResultType"/>
 *           &lt;element name="MetadataResult" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}ResultType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="timeSeriesMatch" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryResultType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "dataResult",
    "metadataResult"
})
public class QueryResultType {

    @XmlElement(name = "DataResult", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected ResultType dataResult;
    @XmlElement(name = "MetadataResult", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected ResultType metadataResult;
    @XmlAttribute(name = "timeSeriesMatch", required = true)
    protected boolean timeSeriesMatch;

    /**
     * Ruft den Wert der dataResult-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResultType }
     *     
     */
    public ResultType getDataResult() {
        return dataResult;
    }

    /**
     * Legt den Wert der dataResult-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultType }
     *     
     */
    public void setDataResult(ResultType value) {
        this.dataResult = value;
    }

    /**
     * Ruft den Wert der metadataResult-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResultType }
     *     
     */
    public ResultType getMetadataResult() {
        return metadataResult;
    }

    /**
     * Legt den Wert der metadataResult-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultType }
     *     
     */
    public void setMetadataResult(ResultType value) {
        this.metadataResult = value;
    }

    /**
     * Ruft den Wert der timeSeriesMatch-Eigenschaft ab.
     * 
     */
    public boolean isTimeSeriesMatch() {
        return timeSeriesMatch;
    }

    /**
     * Legt den Wert der timeSeriesMatch-Eigenschaft fest.
     * 
     */
    public void setTimeSeriesMatch(boolean value) {
        this.timeSeriesMatch = value;
    }

}
