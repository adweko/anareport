
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ComponentListType is an abstract base type for all component lists. It contains a collection of components. Concrete types should restrict this to specific concrete components.
 * 
 * <p>Java-Klasse f�r ComponentListType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentListType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}IdentifiableType">
 *       &lt;sequence>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Component"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentListType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "component"
})
@XmlSeeAlso({
    MeasureListType.class,
    DimensionListBaseType.class,
    AttributeListBaseType.class,
    MetadataTargetBaseType.class,
    GroupBaseType.class,
    ReportStructureBaseType.class
})
public abstract class ComponentListType
    extends IdentifiableType
{

    @XmlElementRef(name = "Component", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ComponentType>> component;

    /**
     * Gets the value of the component property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the component property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link MetadataAttributeType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataSetTargetType }{@code >}
     * {@link JAXBElement }{@code <}{@link AttributeType }{@code >}
     * {@link JAXBElement }{@code <}{@link IdentifiableObjectTargetType }{@code >}
     * {@link JAXBElement }{@code <}{@link MeasureDimensionType }{@code >}
     * {@link JAXBElement }{@code <}{@link ConstraintContentTargetType }{@code >}
     * {@link JAXBElement }{@code <}{@link KeyDescriptorValuesTargetType }{@code >}
     * {@link JAXBElement }{@code <}{@link TimeDimensionType }{@code >}
     * {@link JAXBElement }{@code <}{@link ComponentType }{@code >}
     * {@link JAXBElement }{@code <}{@link GroupDimensionType }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportingYearStartDayType }{@code >}
     * {@link JAXBElement }{@code <}{@link PrimaryMeasureType }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportPeriodTargetType }{@code >}
     * {@link JAXBElement }{@code <}{@link DimensionType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ComponentType>> getComponent() {
        if (component == null) {
            component = new ArrayList<JAXBElement<? extends ComponentType>>();
        }
        return this.component;
    }

}
