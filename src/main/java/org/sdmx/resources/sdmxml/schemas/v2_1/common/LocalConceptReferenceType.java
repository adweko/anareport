
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * LocalConceptReferenceType provides a simple references to a concept where the identification of the concept scheme which defines it is contained in another context.
 * 
 * <p>Java-Klasse f�r LocalConceptReferenceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LocalConceptReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalItemReferenceType">
 *       &lt;sequence>
 *         &lt;element name="Ref" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalConceptRefType" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocalConceptReferenceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class LocalConceptReferenceType
    extends LocalItemReferenceType
{


}
