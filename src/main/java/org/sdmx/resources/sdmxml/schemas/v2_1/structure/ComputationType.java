
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TextType;


/**
 * ComputationType describes a computation in a process.
 * 
 * <p>Java-Klasse f�r ComputationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComputationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Description" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="localID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="softwarePackage" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="softwareLanguage" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="softwareVersion" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComputationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "description"
})
public class ComputationType
    extends AnnotableType
{

    @XmlElement(name = "Description", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", required = true)
    protected List<TextType> description;
    @XmlAttribute(name = "localID")
    protected String localID;
    @XmlAttribute(name = "softwarePackage")
    protected String softwarePackage;
    @XmlAttribute(name = "softwareLanguage")
    protected String softwareLanguage;
    @XmlAttribute(name = "softwareVersion")
    protected String softwareVersion;

    /**
     * Description describe the computation in any form desired by the user (these are informational rather than machine-actionable), and so may be supplied in multiple, parallel-language versions, Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getDescription() {
        if (description == null) {
            description = new ArrayList<TextType>();
        }
        return this.description;
    }

    /**
     * Ruft den Wert der localID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalID() {
        return localID;
    }

    /**
     * Legt den Wert der localID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalID(String value) {
        this.localID = value;
    }

    /**
     * Ruft den Wert der softwarePackage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoftwarePackage() {
        return softwarePackage;
    }

    /**
     * Legt den Wert der softwarePackage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoftwarePackage(String value) {
        this.softwarePackage = value;
    }

    /**
     * Ruft den Wert der softwareLanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoftwareLanguage() {
        return softwareLanguage;
    }

    /**
     * Legt den Wert der softwareLanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoftwareLanguage(String value) {
        this.softwareLanguage = value;
    }

    /**
     * Ruft den Wert der softwareVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoftwareVersion() {
        return softwareVersion;
    }

    /**
     * Legt den Wert der softwareVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoftwareVersion(String value) {
        this.softwareVersion = value;
    }

}
