
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.GenericMetadataStructureType;


/**
 * MetadataSchemaQueryType defines the structure of a query for a structured metadata schema. This query consists of a single metadata structure which simply provides a reference to a metadata structure.
 * 
 * <p>Java-Klasse f�r MetadataSchemaQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataSchemaQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MetadataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}GenericMetadataStructureType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataSchemaQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "metadataStructure"
})
public class MetadataSchemaQueryType {

    @XmlElement(name = "MetadataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected GenericMetadataStructureType metadataStructure;

    /**
     * Ruft den Wert der metadataStructure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GenericMetadataStructureType }
     *     
     */
    public GenericMetadataStructureType getMetadataStructure() {
        return metadataStructure;
    }

    /**
     * Legt den Wert der metadataStructure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericMetadataStructureType }
     *     
     */
    public void setMetadataStructure(GenericMetadataStructureType value) {
        this.metadataStructure = value;
    }

}
