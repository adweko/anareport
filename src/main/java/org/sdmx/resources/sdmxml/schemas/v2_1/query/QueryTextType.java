
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TextType;


/**
 * QueryTextType describes the structure of a textual query value. A language must be specified if parallel multi-lingual values are available, otherwise it is ignored.
 * 
 * <p>Java-Klasse f�r QueryTextType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="QueryTextType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common>TextType">
 *       &lt;attribute name="operator" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TextOperatorType" default="equal" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryTextType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class QueryTextType
    extends TextType
{

    @XmlAttribute(name = "operator")
    protected String operator;

    /**
     * Ruft den Wert der operator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperator() {
        if (operator == null) {
            return "equal";
        } else {
            return operator;
        }
    }

    /**
     * Legt den Wert der operator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperator(String value) {
        this.operator = value;
    }

}
