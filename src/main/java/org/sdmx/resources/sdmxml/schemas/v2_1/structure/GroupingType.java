
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * GroupType is an abstract base type for specific structure groupings. It contains a collection of component lists. Concrete instances of this should restrict to specific concrete component lists.
 * 
 * <p>Java-Klasse f�r GroupingType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GroupingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ComponentList"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupingType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "componentList"
})
@XmlSeeAlso({
    DataStructureComponentsBaseType.class,
    MetadataStructureComponentsBaseType.class
})
public abstract class GroupingType {

    @XmlElementRef(name = "ComponentList", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ComponentListType>> componentList;

    /**
     * Gets the value of the componentList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the componentList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponentList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link ReportStructureType }{@code >}
     * {@link JAXBElement }{@code <}{@link ComponentListType }{@code >}
     * {@link JAXBElement }{@code <}{@link DimensionListType }{@code >}
     * {@link JAXBElement }{@code <}{@link GroupType }{@code >}
     * {@link JAXBElement }{@code <}{@link MeasureListType }{@code >}
     * {@link JAXBElement }{@code <}{@link AttributeListType }{@code >}
     * {@link JAXBElement }{@code <}{@link MetadataTargetType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ComponentListType>> getComponentList() {
        if (componentList == null) {
            componentList = new ArrayList<JAXBElement<? extends ComponentListType>>();
        }
        return this.componentList;
    }

}
