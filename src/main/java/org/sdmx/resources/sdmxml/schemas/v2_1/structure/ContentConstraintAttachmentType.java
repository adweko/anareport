
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * ContentConstraintAttachmentType defines the structure for specifying the target object(s) of a content constraint.
 * 
 * <p>Java-Klasse f�r ContentConstraintAttachmentType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ContentConstraintAttachmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConstraintAttachmentType">
 *       &lt;choice>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType"/>
 *         &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType"/>
 *         &lt;element name="MetadataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType"/>
 *         &lt;element name="SimpleDataSource" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element name="DataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="MetadataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataStructureReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="Dataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="Metadataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataflowReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType" maxOccurs="unbounded"/>
 *             &lt;element name="QueryableDataSource" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContentConstraintAttachmentType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class ContentConstraintAttachmentType
    extends ConstraintAttachmentType
{


}
