
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * AttributeListType describes the attribute descriptor for the data structure definition.
 * 
 * <p>Java-Klasse f�r AttributeListType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttributeListType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}AttributeListBaseType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Attribute"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReportingYearStartDay"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeListType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "attributeOrReportingYearStartDay"
})
public class AttributeListType
    extends AttributeListBaseType
{

    @XmlElements({
        @XmlElement(name = "Attribute", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure"),
        @XmlElement(name = "ReportingYearStartDay", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = ReportingYearStartDayType.class)
    })
    protected List<AttributeType> attributeOrReportingYearStartDay;

    /**
     * Gets the value of the attributeOrReportingYearStartDay property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeOrReportingYearStartDay property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeOrReportingYearStartDay().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeType }
     * {@link ReportingYearStartDayType }
     * 
     * 
     */
    public List<AttributeType> getAttributeOrReportingYearStartDay() {
        if (attributeOrReportingYearStartDay == null) {
            attributeOrReportingYearStartDay = new ArrayList<AttributeType>();
        }
        return this.attributeOrReportingYearStartDay;
    }

}
