
package org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import de.bundesbank.statistik.anacredit.ack.v1.ObsBBKANCRDTACKHDRC;
import de.bundesbank.statistik.anacredit.ack.v1.ObsBBKANCRDTACKMSSGIDC;
import de.bundesbank.statistik.anacredit.ack.v1.ObsBBKANCRDTVLDACKC;
import de.bundesbank.statistik.anacredit.ack.v1.ObsBBKANCRDTVLDACKXMLC;
import de.bundesbank.statistik.anacredit.rmndr.v1.ObsBBKANCRDTRMNDHDRC;
import de.bundesbank.statistik.anacredit.rmndr.v1.ObsBBKANCRDTRMNDRC;
import de.bundesbank.statistik.anacredit.t1m.v2.ObsBBKANCRDTENTTYINSTRMNTC;
import de.bundesbank.statistik.anacredit.t1m.v2.ObsBBKANCRDTFNNCLC;
import de.bundesbank.statistik.anacredit.t1m.v2.ObsBBKANCRDTINSTRMNTC;
import de.bundesbank.statistik.anacredit.t1m.v2.ObsBBKANCRDTJNTLBLTSC;
import de.bundesbank.statistik.anacredit.t2m.v2.ObsBBKANCRDTENTTYDFLTC;
import de.bundesbank.statistik.anacredit.t2m.v2.ObsBBKANCRDTENTTYRSKC;
import de.bundesbank.statistik.anacredit.t2m.v2.ObsBBKANCRDTINSTRMNTPRTCTNRCVDC;
import de.bundesbank.statistik.anacredit.t2m.v2.ObsBBKANCRDTPRTCTNPRVDRC;
import de.bundesbank.statistik.anacredit.t2m.v2.ObsBBKANCRDTPRTCTNRCVDC;
import de.bundesbank.statistik.anacredit.t2q.v2.ObsBBKANCRDTACCNTNGC;
import de.bundesbank.statistik.riad.v2.ObsBBKANCRDTENTTYCHNGECDC;
import de.bundesbank.statistik.riad.v2.ObsBBKANCRDTENTTYRFRNCC;
import de.bundesbank.statistik.riad.v2.ObsBBKRIADHDRC;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;


/**
 * 
 * 			
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:xs="http://www.w3.org/2001/XMLSchema"&gt;ObsType is the abstract type which defines the structure of a grouped or un-grouped observation. The observation must be provided a key, which is either a value for the dimension which is declared to be at the observation level if the observation is grouped, or a full set of values for all dimensions in the data structure definition if the observation is un-grouped. This key should disambiguate the observation within the context in which it is defined (e.g. there should not be another observation with the same dimension value in a series). The observation can contain an observed value and/or attribute values.&lt;/p&gt;
 * </pre>
 * 
 * 			
 * 			
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:xs="http://www.w3.org/2001/XMLSchema"&gt;Data structure definition schemas will drive a type or types based on this that is specific to the data structure definition and the variation of the format being expressed in the schema. The dimension value(s) which make up the key and the attribute values associated with the key dimension(s) or the primary measure will be represented with XML attributes. This is specified in the content model with the declaration of anyAttributes in the "local" namespace. The derived observation type will refine this structure so that the attributes are explicit. The XML attributes will be given a name based on the attribute's identifier. These XML attributes will be unqualified (meaning they do not have a namespace associated with them). The dimension XML attribute(s) will be required while the attribute XML attributes will be optional. To allow for generic processing, it is required that the only unqualified XML attributes in the derived observation type be for the observation dimension(s) and attributes declared in the data structure definition. If additional attributes are required, these should be qualified with a namespace so that a generic application can easily distinguish them as not being meant to represent a data structure definition dimension or attribute.&lt;/p&gt;
 * </pre>
 * 
 * 			
 * 			
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:xs="http://www.w3.org/2001/XMLSchema"&gt;If the data structure definition specific schema requires that explicit measures be used (only possible when the measure dimension is specified at the observation), then there will be types derived for each measure defined by the measure dimension. In this case, the types will be specific to each measure, which is to say that the representation of the primary measure (i.e. the observed value) will be restricted to that which is specified by the specific measure.&lt;/p&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java-Klasse f�r ObsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="TIME_PERIOD" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationalTimePeriodType" />
 *       &lt;attribute name="REPORTING_YEAR_START_DAY" type="{http://www.w3.org/2001/XMLSchema}gMonthDay" />
 *       &lt;attribute name="OBS_VALUE" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;anyAttribute namespace=''/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
@XmlSeeAlso({
    ObsBBKANCRDTVLDACKC.class,
    ObsBBKANCRDTACKMSSGIDC.class,
    ObsBBKANCRDTACKHDRC.class,
    ObsBBKANCRDTVLDACKXMLC.class,
    TimeSeriesObsType.class,
    ObsBBKANCRDTRMNDRC.class,
    ObsBBKANCRDTRMNDHDRC.class,
    ObsBBKANCRDTJNTLBLTSC.class,
    ObsBBKANCRDTINSTRMNTC.class,
    de.bundesbank.statistik.anacredit.t1m.v2.ObsBBKANCRDTHDRC.class,
    ObsBBKANCRDTFNNCLC.class,
    ObsBBKANCRDTENTTYINSTRMNTC.class,
    de.bundesbank.statistik.anacredit.t2m.v2.ObsBBKANCRDTHDRC.class,
    ObsBBKANCRDTPRTCTNPRVDRC.class,
    ObsBBKANCRDTPRTCTNRCVDC.class,
    ObsBBKANCRDTENTTYRSKC.class,
    ObsBBKANCRDTINSTRMNTPRTCTNRCVDC.class,
    ObsBBKANCRDTENTTYDFLTC.class,
    ObsBBKANCRDTACCNTNGC.class,
    de.bundesbank.statistik.anacredit.t2q.v2.ObsBBKANCRDTHDRC.class,
    ObsBBKANCRDTENTTYCHNGECDC.class,
    ObsBBKRIADHDRC.class,
    ObsBBKANCRDTENTTYRFRNCC.class
})
public abstract class ObsType
    extends AnnotableType
{

    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "TIME_PERIOD")
    protected List<String> timeperiod;
    @XmlAttribute(name = "REPORTING_YEAR_START_DAY")
    @XmlSchemaType(name = "gMonthDay")
    protected XMLGregorianCalendar reportingyearstartday;
    @XmlAttribute(name = "OBS_VALUE")
    @XmlSchemaType(name = "anySimpleType")
    protected String obsvalue;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the timeperiod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timeperiod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTIMEPERIOD().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTIMEPERIOD() {
        if (timeperiod == null) {
            timeperiod = new ArrayList<String>();
        }
        return this.timeperiod;
    }

    /**
     * Ruft den Wert der reportingyearstartday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getREPORTINGYEARSTARTDAY() {
        return reportingyearstartday;
    }

    /**
     * Legt den Wert der reportingyearstartday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setREPORTINGYEARSTARTDAY(XMLGregorianCalendar value) {
        this.reportingyearstartday = value;
    }

    /**
     * Ruft den Wert der obsvalue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBSVALUE() {
        return obsvalue;
    }

    /**
     * Legt den Wert der obsvalue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBSVALUE(String value) {
        this.obsvalue = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
