
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataStructureComponentsType describes the structure of the grouping of the sets of the components that make up the metadata structure definition. At a minimum, a full target identifier and at least one report structure must be defined.
 * 
 * <p>Java-Klasse f�r MetadataStructureComponentsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataStructureComponentsType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataStructureComponentsBaseType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataTarget" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReportStructure" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataStructureComponentsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "metadataTarget",
    "reportStructure"
})
public class MetadataStructureComponentsType
    extends MetadataStructureComponentsBaseType
{

    @XmlElement(name = "MetadataTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected List<MetadataTargetType> metadataTarget;
    @XmlElement(name = "ReportStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected List<ReportStructureType> reportStructure;

    /**
     * Gets the value of the metadataTarget property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadataTarget property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadataTarget().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadataTargetType }
     * 
     * 
     */
    public List<MetadataTargetType> getMetadataTarget() {
        if (metadataTarget == null) {
            metadataTarget = new ArrayList<MetadataTargetType>();
        }
        return this.metadataTarget;
    }

    /**
     * Gets the value of the reportStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportStructureType }
     * 
     * 
     */
    public List<ReportStructureType> getReportStructure() {
        if (reportStructure == null) {
            reportStructure = new ArrayList<ReportStructureType>();
        }
        return this.reportStructure;
    }

}
