
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * DataStructureWhereType defines the parameters of a data structure definition query. In addition to querying based on the identification, it is also possible to search for data structure definitions based on information about its components.
 * 
 * <p>Java-Klasse f�r DataStructureWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataStructureWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataStructureWhereBaseType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AttributeWhere" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DimensionWhere" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MeasureDimensionWhere" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeDimensionWhere" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}PrimaryMeasureWhere" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataStructureWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "attributeWhere",
    "dimensionWhere",
    "measureDimensionWhere",
    "timeDimensionWhere",
    "primaryMeasureWhere"
})
public class DataStructureWhereType
    extends DataStructureWhereBaseType
{

    @XmlElement(name = "AttributeWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<AttributeWhereType> attributeWhere;
    @XmlElement(name = "DimensionWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<DimensionWhereType> dimensionWhere;
    @XmlElement(name = "MeasureDimensionWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected MeasureDimensionWhereType measureDimensionWhere;
    @XmlElement(name = "TimeDimensionWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected TimeDimensionWhereType timeDimensionWhere;
    @XmlElement(name = "PrimaryMeasureWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected PrimaryMeasureWhereType primaryMeasureWhere;

    /**
     * Gets the value of the attributeWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeWhereType }
     * 
     * 
     */
    public List<AttributeWhereType> getAttributeWhere() {
        if (attributeWhere == null) {
            attributeWhere = new ArrayList<AttributeWhereType>();
        }
        return this.attributeWhere;
    }

    /**
     * Gets the value of the dimensionWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dimensionWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDimensionWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DimensionWhereType }
     * 
     * 
     */
    public List<DimensionWhereType> getDimensionWhere() {
        if (dimensionWhere == null) {
            dimensionWhere = new ArrayList<DimensionWhereType>();
        }
        return this.dimensionWhere;
    }

    /**
     * Ruft den Wert der measureDimensionWhere-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasureDimensionWhereType }
     *     
     */
    public MeasureDimensionWhereType getMeasureDimensionWhere() {
        return measureDimensionWhere;
    }

    /**
     * Legt den Wert der measureDimensionWhere-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureDimensionWhereType }
     *     
     */
    public void setMeasureDimensionWhere(MeasureDimensionWhereType value) {
        this.measureDimensionWhere = value;
    }

    /**
     * Ruft den Wert der timeDimensionWhere-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeDimensionWhereType }
     *     
     */
    public TimeDimensionWhereType getTimeDimensionWhere() {
        return timeDimensionWhere;
    }

    /**
     * Legt den Wert der timeDimensionWhere-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeDimensionWhereType }
     *     
     */
    public void setTimeDimensionWhere(TimeDimensionWhereType value) {
        this.timeDimensionWhere = value;
    }

    /**
     * Ruft den Wert der primaryMeasureWhere-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PrimaryMeasureWhereType }
     *     
     */
    public PrimaryMeasureWhereType getPrimaryMeasureWhere() {
        return primaryMeasureWhere;
    }

    /**
     * Legt den Wert der primaryMeasureWhere-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PrimaryMeasureWhereType }
     *     
     */
    public void setPrimaryMeasureWhere(PrimaryMeasureWhereType value) {
        this.primaryMeasureWhere = value;
    }

}
