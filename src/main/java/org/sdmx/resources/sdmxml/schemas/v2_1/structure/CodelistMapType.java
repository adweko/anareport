
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * CodelistMapType defines the structure of a map which identifies relationships between codes in different codelists.
 * 
 * <p>Java-Klasse f�r CodelistMapType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CodelistMapType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ItemSchemeMapType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Name" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Description" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistReferenceType"/>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistReferenceType"/>
 *         &lt;sequence maxOccurs="unbounded">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodeMap"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodelistMapType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class CodelistMapType
    extends ItemSchemeMapType
{


}
