
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ComponentReferenceType is an abstract base type used for referencing components within a structure definition. It consists of a URN and/or a complete set of reference fields (structure agency, structure id, structure version, component list id, and component id).
 * 
 * <p>Java-Klasse f�r ComponentReferenceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ContainerChildObjectReferenceType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="Ref" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentRefBaseType" form="unqualified"/>
 *           &lt;element name="URN" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0" form="unqualified"/>
 *         &lt;/sequence>
 *         &lt;element name="URN" type="{http://www.w3.org/2001/XMLSchema}anyURI" form="unqualified"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentReferenceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    ReportPeriodTargetReferenceType.class,
    TimeDimensionReferenceType.class,
    KeyDescriptorValuesTargetReferenceType.class,
    PrimaryMeasureReferenceType.class,
    MeasureDimensionReferenceType.class,
    ConstraintTargetReferenceType.class,
    IdentifiableObjectTargetReferenceType.class,
    DataSetTargetReferenceType.class,
    MetadataAttributeReferenceType.class,
    DimensionReferenceType.class,
    AttributeReferenceType.class
})
public abstract class ComponentReferenceType
    extends ContainerChildObjectReferenceType
{


}
