
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * NotifyRegistryEventType defines the structure of a registry notification document.
 * 
 * <p>Java-Klasse f�r NotifyRegistryEventType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NotifyRegistryEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}RegistryInterfaceType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}BasicHeaderType"/>
 *         &lt;element name="NotifyRegistryEvent" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}NotifyRegistryEventType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NotifyRegistryEventType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
public class NotifyRegistryEventType
    extends RegistryInterfaceType
{


}
