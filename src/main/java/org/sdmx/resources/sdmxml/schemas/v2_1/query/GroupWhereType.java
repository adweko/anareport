
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AttachmentConstraintReferenceType;


/**
 * GroupWhereType defines the parameters querying for a data structure definition based a group meeting the conditions detailed. Parameters include identification, dimensions used in the group, and the group's referenced attachment constraint. This is an implicit set of "and" parameters, that is the conditions within this must all be met in order to return a match.
 * 
 * <p>Java-Klasse f�r GroupWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GroupWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}GroupWhereBaseType">
 *       &lt;sequence>
 *         &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraintReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "attachmentConstraint"
})
public class GroupWhereType
    extends GroupWhereBaseType
{

    @XmlElement(name = "AttachmentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected AttachmentConstraintReferenceType attachmentConstraint;

    /**
     * Ruft den Wert der attachmentConstraint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentConstraintReferenceType }
     *     
     */
    public AttachmentConstraintReferenceType getAttachmentConstraint() {
        return attachmentConstraint;
    }

    /**
     * Legt den Wert der attachmentConstraint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentConstraintReferenceType }
     *     
     */
    public void setAttachmentConstraint(AttachmentConstraintReferenceType value) {
        this.attachmentConstraint = value;
    }

}
