
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * QueryableDataSourceType describes a queryable data source, and add a fixed attribute for ensuring only one queryable source can be provided.
 * 
 * <p>Java-Klasse f�r QueryableDataSourceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="QueryableDataSourceType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}QueryableDataSourceType">
 *       &lt;attribute name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" fixed="QUERY" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryableDataSourceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
public class QueryableDataSourceType
    extends org.sdmx.resources.sdmxml.schemas.v2_1.common.QueryableDataSourceType
{

    @XmlAttribute(name = "TYPE")
    protected String type;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTYPE() {
        if (type == null) {
            return "QUERY";
        } else {
            return type;
        }
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTYPE(String value) {
        this.type = value;
    }

}
