
package org.sdmx.resources.sdmxml.schemas.v2_1.metadata.generic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * AttributeSetType defines the structure for a collection of reported metadata attributes.
 * 
 * <p>Java-Klasse f�r AttributeSetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttributeSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReportedAttribute" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/generic}ReportedAttributeType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeSetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/generic", propOrder = {
    "reportedAttribute"
})
public class AttributeSetType {

    @XmlElement(name = "ReportedAttribute", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/generic", required = true)
    protected List<ReportedAttributeType> reportedAttribute;

    /**
     * Gets the value of the reportedAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportedAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportedAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportedAttributeType }
     * 
     * 
     */
    public List<ReportedAttributeType> getReportedAttribute() {
        if (reportedAttribute == null) {
            reportedAttribute = new ArrayList<ReportedAttributeType>();
        }
        return this.reportedAttribute;
    }

}
