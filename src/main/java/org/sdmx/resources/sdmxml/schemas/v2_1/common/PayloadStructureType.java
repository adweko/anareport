
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * PayloadStructureType is an abstract base type used to define the structural information for data or metadata sets. A reference to the structure is provided (either explicitly or through a reference to a structure usage).
 * 
 * <p>Java-Klasse f�r PayloadStructureType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PayloadStructureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="ProvisionAgrement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType"/>
 *           &lt;element name="StructureUsage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureUsageReferenceBaseType"/>
 *           &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureReferenceBaseType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ExternalReferenceAttributeGroup"/>
 *       &lt;attribute name="structureID" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="schemaURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" />
 *       &lt;attribute name="namespace" type="{http://www.w3.org/2001/XMLSchema}anyURI" />
 *       &lt;attribute name="dimensionAtObservation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationDimensionType" />
 *       &lt;attribute name="explicitMeasures" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayloadStructureType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", propOrder = {
    "provisionAgrement",
    "structureUsage",
    "structure"
})
@XmlSeeAlso({
    MetadataStructureType.class,
    DataStructureType.class
})
public class PayloadStructureType {

    @XmlElement(name = "ProvisionAgrement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected ProvisionAgreementReferenceType provisionAgrement;
    @XmlElement(name = "StructureUsage", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected StructureUsageReferenceBaseType structureUsage;
    @XmlElement(name = "Structure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected StructureReferenceBaseType structure;
    @XmlAttribute(name = "structureID", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String structureID;
    @XmlAttribute(name = "schemaURL")
    @XmlSchemaType(name = "anyURI")
    protected String schemaURL;
    @XmlAttribute(name = "namespace")
    @XmlSchemaType(name = "anyURI")
    protected String namespace;
    @XmlAttribute(name = "dimensionAtObservation")
    protected String dimensionAtObservation;
    @XmlAttribute(name = "explicitMeasures")
    protected Boolean explicitMeasures;
    @XmlAttribute(name = "serviceURL")
    @XmlSchemaType(name = "anyURI")
    protected String serviceURL;
    @XmlAttribute(name = "structureURL")
    @XmlSchemaType(name = "anyURI")
    protected String structureURL;

    /**
     * Ruft den Wert der provisionAgrement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProvisionAgreementReferenceType }
     *     
     */
    public ProvisionAgreementReferenceType getProvisionAgrement() {
        return provisionAgrement;
    }

    /**
     * Legt den Wert der provisionAgrement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvisionAgreementReferenceType }
     *     
     */
    public void setProvisionAgrement(ProvisionAgreementReferenceType value) {
        this.provisionAgrement = value;
    }

    /**
     * Ruft den Wert der structureUsage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureUsageReferenceBaseType }
     *     
     */
    public StructureUsageReferenceBaseType getStructureUsage() {
        return structureUsage;
    }

    /**
     * Legt den Wert der structureUsage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureUsageReferenceBaseType }
     *     
     */
    public void setStructureUsage(StructureUsageReferenceBaseType value) {
        this.structureUsage = value;
    }

    /**
     * Ruft den Wert der structure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureReferenceBaseType }
     *     
     */
    public StructureReferenceBaseType getStructure() {
        return structure;
    }

    /**
     * Legt den Wert der structure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureReferenceBaseType }
     *     
     */
    public void setStructure(StructureReferenceBaseType value) {
        this.structure = value;
    }

    /**
     * Ruft den Wert der structureID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureID() {
        return structureID;
    }

    /**
     * Legt den Wert der structureID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureID(String value) {
        this.structureID = value;
    }

    /**
     * Ruft den Wert der schemaURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaURL() {
        return schemaURL;
    }

    /**
     * Legt den Wert der schemaURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaURL(String value) {
        this.schemaURL = value;
    }

    /**
     * Ruft den Wert der namespace-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Legt den Wert der namespace-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNamespace(String value) {
        this.namespace = value;
    }

    /**
     * Ruft den Wert der dimensionAtObservation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDimensionAtObservation() {
        return dimensionAtObservation;
    }

    /**
     * Legt den Wert der dimensionAtObservation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDimensionAtObservation(String value) {
        this.dimensionAtObservation = value;
    }

    /**
     * Ruft den Wert der explicitMeasures-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExplicitMeasures() {
        return explicitMeasures;
    }

    /**
     * Legt den Wert der explicitMeasures-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExplicitMeasures(Boolean value) {
        this.explicitMeasures = value;
    }

    /**
     * Ruft den Wert der serviceURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceURL() {
        return serviceURL;
    }

    /**
     * Legt den Wert der serviceURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceURL(String value) {
        this.serviceURL = value;
    }

    /**
     * Ruft den Wert der structureURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureURL() {
        return structureURL;
    }

    /**
     * Legt den Wert der structureURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureURL(String value) {
        this.structureURL = value;
    }

}
