
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TransitionRefType provides for a reference to a transition definition in process step through its id.
 * 
 * <p>Java-Klasse f�r TransitionRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TransitionRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ContainerChildObjectRefBaseType">
 *       &lt;attribute name="containerID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedIDType" />
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" fixed="Transition" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PackageTypeCodelistType" fixed="process" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransitionRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class TransitionRefType
    extends ContainerChildObjectRefBaseType
{


}
