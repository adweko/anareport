
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ObjectTypeListType provides a means for enumerating object types.
 * 
 * <p>Java-Klasse f�r ObjectTypeListType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObjectTypeListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Any" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Agency" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AgencyScheme" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraint" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Attribute" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttributeDescriptor" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Categorisation" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Category" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategorySchemeMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryScheme" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Code" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodeMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Codelist" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Concept" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptScheme" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptSchemeMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ContentConstraint" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Dataflow" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataConsumer" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataConsumerScheme" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProvider" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderScheme" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataSetTarget" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructure" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Dimension" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DimensionDescriptor" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DimensionDescriptorValuesTarget" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}GroupDimensionDescriptor" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}HierarchicalCode" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}HierarchicalCodelist" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Hierarchy" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}HybridCodelistMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}HybridCodeMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IdentifiableObjectTarget" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Level" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MeasureDescriptor" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MeasureDimension" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Metadataflow" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataAttribute" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataSet" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataStructure" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataTarget" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationSchemeMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationUnit" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationUnitScheme" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PrimaryMeasure" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Process" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProcessStep" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreement" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReportingCategory" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReportingCategoryMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReportingTaxonomy" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReportingTaxonomyMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReportPeriodTarget" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReportStructure" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureMap" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureSet" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeDimension" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Transition" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectTypeListType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", propOrder = {

})
@XmlSeeAlso({
    MaintainableObjectTypeListType.class
})
public class ObjectTypeListType {

    @XmlElement(name = "Any", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType any;
    @XmlElement(name = "Agency", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType agency;
    @XmlElement(name = "AgencyScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType agencyScheme;
    @XmlElement(name = "AttachmentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType attachmentConstraint;
    @XmlElement(name = "Attribute", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType attribute;
    @XmlElement(name = "AttributeDescriptor", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType attributeDescriptor;
    @XmlElement(name = "Categorisation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType categorisation;
    @XmlElement(name = "Category", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType category;
    @XmlElement(name = "CategorySchemeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType categorySchemeMap;
    @XmlElement(name = "CategoryScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType categoryScheme;
    @XmlElement(name = "Code", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType code;
    @XmlElement(name = "CodeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType codeMap;
    @XmlElement(name = "Codelist", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType codelist;
    @XmlElement(name = "CodelistMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType codelistMap;
    @XmlElement(name = "ComponentMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType componentMap;
    @XmlElement(name = "Concept", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType concept;
    @XmlElement(name = "ConceptMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType conceptMap;
    @XmlElement(name = "ConceptScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType conceptScheme;
    @XmlElement(name = "ConceptSchemeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType conceptSchemeMap;
    @XmlElement(name = "ContentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType contentConstraint;
    @XmlElement(name = "Dataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dataflow;
    @XmlElement(name = "DataConsumer", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dataConsumer;
    @XmlElement(name = "DataConsumerScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dataConsumerScheme;
    @XmlElement(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dataProvider;
    @XmlElement(name = "DataProviderScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dataProviderScheme;
    @XmlElement(name = "DataSetTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dataSetTarget;
    @XmlElement(name = "DataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dataStructure;
    @XmlElement(name = "Dimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dimension;
    @XmlElement(name = "DimensionDescriptor", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dimensionDescriptor;
    @XmlElement(name = "DimensionDescriptorValuesTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType dimensionDescriptorValuesTarget;
    @XmlElement(name = "GroupDimensionDescriptor", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType groupDimensionDescriptor;
    @XmlElement(name = "HierarchicalCode", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType hierarchicalCode;
    @XmlElement(name = "HierarchicalCodelist", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType hierarchicalCodelist;
    @XmlElement(name = "Hierarchy", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType hierarchy;
    @XmlElement(name = "HybridCodelistMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType hybridCodelistMap;
    @XmlElement(name = "HybridCodeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType hybridCodeMap;
    @XmlElement(name = "IdentifiableObjectTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType identifiableObjectTarget;
    @XmlElement(name = "Level", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType level;
    @XmlElement(name = "MeasureDescriptor", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType measureDescriptor;
    @XmlElement(name = "MeasureDimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType measureDimension;
    @XmlElement(name = "Metadataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType metadataflow;
    @XmlElement(name = "MetadataAttribute", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType metadataAttribute;
    @XmlElement(name = "MetadataSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType metadataSet;
    @XmlElement(name = "MetadataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType metadataStructure;
    @XmlElement(name = "MetadataTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType metadataTarget;
    @XmlElement(name = "OrganisationMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType organisationMap;
    @XmlElement(name = "OrganisationSchemeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType organisationSchemeMap;
    @XmlElement(name = "OrganisationUnit", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType organisationUnit;
    @XmlElement(name = "OrganisationUnitScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType organisationUnitScheme;
    @XmlElement(name = "PrimaryMeasure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType primaryMeasure;
    @XmlElement(name = "Process", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType process;
    @XmlElement(name = "ProcessStep", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType processStep;
    @XmlElement(name = "ProvisionAgreement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType provisionAgreement;
    @XmlElement(name = "ReportingCategory", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType reportingCategory;
    @XmlElement(name = "ReportingCategoryMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType reportingCategoryMap;
    @XmlElement(name = "ReportingTaxonomy", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType reportingTaxonomy;
    @XmlElement(name = "ReportingTaxonomyMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType reportingTaxonomyMap;
    @XmlElement(name = "ReportPeriodTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType reportPeriodTarget;
    @XmlElement(name = "ReportStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType reportStructure;
    @XmlElement(name = "StructureMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType structureMap;
    @XmlElement(name = "StructureSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType structureSet;
    @XmlElement(name = "TimeDimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType timeDimension;
    @XmlElement(name = "Transition", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected EmptyType transition;

    /**
     * Ruft den Wert der any-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAny() {
        return any;
    }

    /**
     * Legt den Wert der any-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAny(EmptyType value) {
        this.any = value;
    }

    /**
     * Ruft den Wert der agency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAgency() {
        return agency;
    }

    /**
     * Legt den Wert der agency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAgency(EmptyType value) {
        this.agency = value;
    }

    /**
     * Ruft den Wert der agencyScheme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAgencyScheme() {
        return agencyScheme;
    }

    /**
     * Legt den Wert der agencyScheme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAgencyScheme(EmptyType value) {
        this.agencyScheme = value;
    }

    /**
     * Ruft den Wert der attachmentConstraint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAttachmentConstraint() {
        return attachmentConstraint;
    }

    /**
     * Legt den Wert der attachmentConstraint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAttachmentConstraint(EmptyType value) {
        this.attachmentConstraint = value;
    }

    /**
     * Ruft den Wert der attribute-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAttribute() {
        return attribute;
    }

    /**
     * Legt den Wert der attribute-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAttribute(EmptyType value) {
        this.attribute = value;
    }

    /**
     * Ruft den Wert der attributeDescriptor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAttributeDescriptor() {
        return attributeDescriptor;
    }

    /**
     * Legt den Wert der attributeDescriptor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAttributeDescriptor(EmptyType value) {
        this.attributeDescriptor = value;
    }

    /**
     * Ruft den Wert der categorisation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getCategorisation() {
        return categorisation;
    }

    /**
     * Legt den Wert der categorisation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setCategorisation(EmptyType value) {
        this.categorisation = value;
    }

    /**
     * Ruft den Wert der category-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getCategory() {
        return category;
    }

    /**
     * Legt den Wert der category-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setCategory(EmptyType value) {
        this.category = value;
    }

    /**
     * Ruft den Wert der categorySchemeMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getCategorySchemeMap() {
        return categorySchemeMap;
    }

    /**
     * Legt den Wert der categorySchemeMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setCategorySchemeMap(EmptyType value) {
        this.categorySchemeMap = value;
    }

    /**
     * Ruft den Wert der categoryScheme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getCategoryScheme() {
        return categoryScheme;
    }

    /**
     * Legt den Wert der categoryScheme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setCategoryScheme(EmptyType value) {
        this.categoryScheme = value;
    }

    /**
     * Ruft den Wert der code-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getCode() {
        return code;
    }

    /**
     * Legt den Wert der code-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setCode(EmptyType value) {
        this.code = value;
    }

    /**
     * Ruft den Wert der codeMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getCodeMap() {
        return codeMap;
    }

    /**
     * Legt den Wert der codeMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setCodeMap(EmptyType value) {
        this.codeMap = value;
    }

    /**
     * Ruft den Wert der codelist-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getCodelist() {
        return codelist;
    }

    /**
     * Legt den Wert der codelist-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setCodelist(EmptyType value) {
        this.codelist = value;
    }

    /**
     * Ruft den Wert der codelistMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getCodelistMap() {
        return codelistMap;
    }

    /**
     * Legt den Wert der codelistMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setCodelistMap(EmptyType value) {
        this.codelistMap = value;
    }

    /**
     * Ruft den Wert der componentMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getComponentMap() {
        return componentMap;
    }

    /**
     * Legt den Wert der componentMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setComponentMap(EmptyType value) {
        this.componentMap = value;
    }

    /**
     * Ruft den Wert der concept-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getConcept() {
        return concept;
    }

    /**
     * Legt den Wert der concept-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setConcept(EmptyType value) {
        this.concept = value;
    }

    /**
     * Ruft den Wert der conceptMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getConceptMap() {
        return conceptMap;
    }

    /**
     * Legt den Wert der conceptMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setConceptMap(EmptyType value) {
        this.conceptMap = value;
    }

    /**
     * Ruft den Wert der conceptScheme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getConceptScheme() {
        return conceptScheme;
    }

    /**
     * Legt den Wert der conceptScheme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setConceptScheme(EmptyType value) {
        this.conceptScheme = value;
    }

    /**
     * Ruft den Wert der conceptSchemeMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getConceptSchemeMap() {
        return conceptSchemeMap;
    }

    /**
     * Legt den Wert der conceptSchemeMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setConceptSchemeMap(EmptyType value) {
        this.conceptSchemeMap = value;
    }

    /**
     * Ruft den Wert der contentConstraint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getContentConstraint() {
        return contentConstraint;
    }

    /**
     * Legt den Wert der contentConstraint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setContentConstraint(EmptyType value) {
        this.contentConstraint = value;
    }

    /**
     * Ruft den Wert der dataflow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDataflow() {
        return dataflow;
    }

    /**
     * Legt den Wert der dataflow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDataflow(EmptyType value) {
        this.dataflow = value;
    }

    /**
     * Ruft den Wert der dataConsumer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDataConsumer() {
        return dataConsumer;
    }

    /**
     * Legt den Wert der dataConsumer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDataConsumer(EmptyType value) {
        this.dataConsumer = value;
    }

    /**
     * Ruft den Wert der dataConsumerScheme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDataConsumerScheme() {
        return dataConsumerScheme;
    }

    /**
     * Legt den Wert der dataConsumerScheme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDataConsumerScheme(EmptyType value) {
        this.dataConsumerScheme = value;
    }

    /**
     * Ruft den Wert der dataProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDataProvider() {
        return dataProvider;
    }

    /**
     * Legt den Wert der dataProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDataProvider(EmptyType value) {
        this.dataProvider = value;
    }

    /**
     * Ruft den Wert der dataProviderScheme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDataProviderScheme() {
        return dataProviderScheme;
    }

    /**
     * Legt den Wert der dataProviderScheme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDataProviderScheme(EmptyType value) {
        this.dataProviderScheme = value;
    }

    /**
     * Ruft den Wert der dataSetTarget-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDataSetTarget() {
        return dataSetTarget;
    }

    /**
     * Legt den Wert der dataSetTarget-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDataSetTarget(EmptyType value) {
        this.dataSetTarget = value;
    }

    /**
     * Ruft den Wert der dataStructure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDataStructure() {
        return dataStructure;
    }

    /**
     * Legt den Wert der dataStructure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDataStructure(EmptyType value) {
        this.dataStructure = value;
    }

    /**
     * Ruft den Wert der dimension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDimension() {
        return dimension;
    }

    /**
     * Legt den Wert der dimension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDimension(EmptyType value) {
        this.dimension = value;
    }

    /**
     * Ruft den Wert der dimensionDescriptor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDimensionDescriptor() {
        return dimensionDescriptor;
    }

    /**
     * Legt den Wert der dimensionDescriptor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDimensionDescriptor(EmptyType value) {
        this.dimensionDescriptor = value;
    }

    /**
     * Ruft den Wert der dimensionDescriptorValuesTarget-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getDimensionDescriptorValuesTarget() {
        return dimensionDescriptorValuesTarget;
    }

    /**
     * Legt den Wert der dimensionDescriptorValuesTarget-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setDimensionDescriptorValuesTarget(EmptyType value) {
        this.dimensionDescriptorValuesTarget = value;
    }

    /**
     * Ruft den Wert der groupDimensionDescriptor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getGroupDimensionDescriptor() {
        return groupDimensionDescriptor;
    }

    /**
     * Legt den Wert der groupDimensionDescriptor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setGroupDimensionDescriptor(EmptyType value) {
        this.groupDimensionDescriptor = value;
    }

    /**
     * Ruft den Wert der hierarchicalCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getHierarchicalCode() {
        return hierarchicalCode;
    }

    /**
     * Legt den Wert der hierarchicalCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setHierarchicalCode(EmptyType value) {
        this.hierarchicalCode = value;
    }

    /**
     * Ruft den Wert der hierarchicalCodelist-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getHierarchicalCodelist() {
        return hierarchicalCodelist;
    }

    /**
     * Legt den Wert der hierarchicalCodelist-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setHierarchicalCodelist(EmptyType value) {
        this.hierarchicalCodelist = value;
    }

    /**
     * Ruft den Wert der hierarchy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getHierarchy() {
        return hierarchy;
    }

    /**
     * Legt den Wert der hierarchy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setHierarchy(EmptyType value) {
        this.hierarchy = value;
    }

    /**
     * Ruft den Wert der hybridCodelistMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getHybridCodelistMap() {
        return hybridCodelistMap;
    }

    /**
     * Legt den Wert der hybridCodelistMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setHybridCodelistMap(EmptyType value) {
        this.hybridCodelistMap = value;
    }

    /**
     * Ruft den Wert der hybridCodeMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getHybridCodeMap() {
        return hybridCodeMap;
    }

    /**
     * Legt den Wert der hybridCodeMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setHybridCodeMap(EmptyType value) {
        this.hybridCodeMap = value;
    }

    /**
     * Ruft den Wert der identifiableObjectTarget-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getIdentifiableObjectTarget() {
        return identifiableObjectTarget;
    }

    /**
     * Legt den Wert der identifiableObjectTarget-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setIdentifiableObjectTarget(EmptyType value) {
        this.identifiableObjectTarget = value;
    }

    /**
     * Ruft den Wert der level-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getLevel() {
        return level;
    }

    /**
     * Legt den Wert der level-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setLevel(EmptyType value) {
        this.level = value;
    }

    /**
     * Ruft den Wert der measureDescriptor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getMeasureDescriptor() {
        return measureDescriptor;
    }

    /**
     * Legt den Wert der measureDescriptor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setMeasureDescriptor(EmptyType value) {
        this.measureDescriptor = value;
    }

    /**
     * Ruft den Wert der measureDimension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getMeasureDimension() {
        return measureDimension;
    }

    /**
     * Legt den Wert der measureDimension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setMeasureDimension(EmptyType value) {
        this.measureDimension = value;
    }

    /**
     * Ruft den Wert der metadataflow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getMetadataflow() {
        return metadataflow;
    }

    /**
     * Legt den Wert der metadataflow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setMetadataflow(EmptyType value) {
        this.metadataflow = value;
    }

    /**
     * Ruft den Wert der metadataAttribute-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getMetadataAttribute() {
        return metadataAttribute;
    }

    /**
     * Legt den Wert der metadataAttribute-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setMetadataAttribute(EmptyType value) {
        this.metadataAttribute = value;
    }

    /**
     * Ruft den Wert der metadataSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getMetadataSet() {
        return metadataSet;
    }

    /**
     * Legt den Wert der metadataSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setMetadataSet(EmptyType value) {
        this.metadataSet = value;
    }

    /**
     * Ruft den Wert der metadataStructure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getMetadataStructure() {
        return metadataStructure;
    }

    /**
     * Legt den Wert der metadataStructure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setMetadataStructure(EmptyType value) {
        this.metadataStructure = value;
    }

    /**
     * Ruft den Wert der metadataTarget-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getMetadataTarget() {
        return metadataTarget;
    }

    /**
     * Legt den Wert der metadataTarget-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setMetadataTarget(EmptyType value) {
        this.metadataTarget = value;
    }

    /**
     * Ruft den Wert der organisationMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getOrganisationMap() {
        return organisationMap;
    }

    /**
     * Legt den Wert der organisationMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setOrganisationMap(EmptyType value) {
        this.organisationMap = value;
    }

    /**
     * Ruft den Wert der organisationSchemeMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getOrganisationSchemeMap() {
        return organisationSchemeMap;
    }

    /**
     * Legt den Wert der organisationSchemeMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setOrganisationSchemeMap(EmptyType value) {
        this.organisationSchemeMap = value;
    }

    /**
     * Ruft den Wert der organisationUnit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getOrganisationUnit() {
        return organisationUnit;
    }

    /**
     * Legt den Wert der organisationUnit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setOrganisationUnit(EmptyType value) {
        this.organisationUnit = value;
    }

    /**
     * Ruft den Wert der organisationUnitScheme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getOrganisationUnitScheme() {
        return organisationUnitScheme;
    }

    /**
     * Legt den Wert der organisationUnitScheme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setOrganisationUnitScheme(EmptyType value) {
        this.organisationUnitScheme = value;
    }

    /**
     * Ruft den Wert der primaryMeasure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getPrimaryMeasure() {
        return primaryMeasure;
    }

    /**
     * Legt den Wert der primaryMeasure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setPrimaryMeasure(EmptyType value) {
        this.primaryMeasure = value;
    }

    /**
     * Ruft den Wert der process-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getProcess() {
        return process;
    }

    /**
     * Legt den Wert der process-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setProcess(EmptyType value) {
        this.process = value;
    }

    /**
     * Ruft den Wert der processStep-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getProcessStep() {
        return processStep;
    }

    /**
     * Legt den Wert der processStep-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setProcessStep(EmptyType value) {
        this.processStep = value;
    }

    /**
     * Ruft den Wert der provisionAgreement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getProvisionAgreement() {
        return provisionAgreement;
    }

    /**
     * Legt den Wert der provisionAgreement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setProvisionAgreement(EmptyType value) {
        this.provisionAgreement = value;
    }

    /**
     * Ruft den Wert der reportingCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getReportingCategory() {
        return reportingCategory;
    }

    /**
     * Legt den Wert der reportingCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setReportingCategory(EmptyType value) {
        this.reportingCategory = value;
    }

    /**
     * Ruft den Wert der reportingCategoryMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getReportingCategoryMap() {
        return reportingCategoryMap;
    }

    /**
     * Legt den Wert der reportingCategoryMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setReportingCategoryMap(EmptyType value) {
        this.reportingCategoryMap = value;
    }

    /**
     * Ruft den Wert der reportingTaxonomy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getReportingTaxonomy() {
        return reportingTaxonomy;
    }

    /**
     * Legt den Wert der reportingTaxonomy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setReportingTaxonomy(EmptyType value) {
        this.reportingTaxonomy = value;
    }

    /**
     * Ruft den Wert der reportingTaxonomyMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getReportingTaxonomyMap() {
        return reportingTaxonomyMap;
    }

    /**
     * Legt den Wert der reportingTaxonomyMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setReportingTaxonomyMap(EmptyType value) {
        this.reportingTaxonomyMap = value;
    }

    /**
     * Ruft den Wert der reportPeriodTarget-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getReportPeriodTarget() {
        return reportPeriodTarget;
    }

    /**
     * Legt den Wert der reportPeriodTarget-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setReportPeriodTarget(EmptyType value) {
        this.reportPeriodTarget = value;
    }

    /**
     * Ruft den Wert der reportStructure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getReportStructure() {
        return reportStructure;
    }

    /**
     * Legt den Wert der reportStructure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setReportStructure(EmptyType value) {
        this.reportStructure = value;
    }

    /**
     * Ruft den Wert der structureMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getStructureMap() {
        return structureMap;
    }

    /**
     * Legt den Wert der structureMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setStructureMap(EmptyType value) {
        this.structureMap = value;
    }

    /**
     * Ruft den Wert der structureSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getStructureSet() {
        return structureSet;
    }

    /**
     * Legt den Wert der structureSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setStructureSet(EmptyType value) {
        this.structureSet = value;
    }

    /**
     * Ruft den Wert der timeDimension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getTimeDimension() {
        return timeDimension;
    }

    /**
     * Legt den Wert der timeDimension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setTimeDimension(EmptyType value) {
        this.timeDimension = value;
    }

    /**
     * Ruft den Wert der transition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getTransition() {
        return transition;
    }

    /**
     * Legt den Wert der transition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setTransition(EmptyType value) {
        this.transition = value;
    }

}
