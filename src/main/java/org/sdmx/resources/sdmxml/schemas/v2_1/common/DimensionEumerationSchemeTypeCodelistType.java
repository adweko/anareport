
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r DimensionEumerationSchemeTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="DimensionEumerationSchemeTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeTypeCodelistType">
 *     &lt;enumeration value="Codelist"/>
 *     &lt;enumeration value="ConceptScheme"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DimensionEumerationSchemeTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ItemSchemeTypeCodelistType.class)
public enum DimensionEumerationSchemeTypeCodelistType {

    @XmlEnumValue("Codelist")
    CODELIST(ItemSchemeTypeCodelistType.CODELIST),
    @XmlEnumValue("ConceptScheme")
    CONCEPT_SCHEME(ItemSchemeTypeCodelistType.CONCEPT_SCHEME);
    private final ItemSchemeTypeCodelistType value;

    DimensionEumerationSchemeTypeCodelistType(ItemSchemeTypeCodelistType v) {
        value = v;
    }

    public ItemSchemeTypeCodelistType value() {
        return value;
    }

    public static DimensionEumerationSchemeTypeCodelistType fromValue(ItemSchemeTypeCodelistType v) {
        for (DimensionEumerationSchemeTypeCodelistType c: DimensionEumerationSchemeTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
