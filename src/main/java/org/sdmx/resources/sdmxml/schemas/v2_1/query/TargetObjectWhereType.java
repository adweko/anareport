
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ObjectTypeCodelistType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TargetObjectTypeCodelistType;


/**
 * TargetObjectWhereType describes the structure of a target object query. A target object can be queried based on its identification, its type (i.e. data set target, key descriptor values target, report period target, or identifiable object target), and in the case of an identifiable object target, an item scheme which enumerates the possible values and/or the class of the target object reference.
 * 
 * <p>Java-Klasse f�r TargetObjectWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TargetObjectWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TargetObjectWhereBaseType">
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TargetObjectTypeCodelistType" />
 *       &lt;attribute name="targetClass" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetObjectWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class TargetObjectWhereType
    extends TargetObjectWhereBaseType
{

    @XmlAttribute(name = "type")
    protected TargetObjectTypeCodelistType type;
    @XmlAttribute(name = "targetClass")
    protected ObjectTypeCodelistType targetClass;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TargetObjectTypeCodelistType }
     *     
     */
    public TargetObjectTypeCodelistType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TargetObjectTypeCodelistType }
     *     
     */
    public void setType(TargetObjectTypeCodelistType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der targetClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjectTypeCodelistType }
     *     
     */
    public ObjectTypeCodelistType getTargetClass() {
        return targetClass;
    }

    /**
     * Legt den Wert der targetClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectTypeCodelistType }
     *     
     */
    public void setTargetClass(ObjectTypeCodelistType value) {
        this.targetClass = value;
    }

}
