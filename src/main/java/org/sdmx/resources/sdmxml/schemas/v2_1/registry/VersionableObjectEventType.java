
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.EmptyType;


/**
 * VersionableObjectEventType describes the structure of a reference to a versionable object's events. Either all instances of the object matching the inherited criteria, a specific instance, or specific instances of the object may be selected.
 * 
 * <p>Java-Klasse f�r VersionableObjectEventType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersionableObjectEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="All" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;element name="URN" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *         &lt;sequence>
 *           &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}IDQueryType"/>
 *           &lt;element name="Version" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionQueryType"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersionableObjectEventType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "all",
    "urn",
    "id",
    "version"
})
public class VersionableObjectEventType {

    @XmlElement(name = "All", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected EmptyType all;
    @XmlElement(name = "URN", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    @XmlSchemaType(name = "anyURI")
    protected String urn;
    @XmlElement(name = "ID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", defaultValue = "%")
    protected String id;
    @XmlList
    @XmlElement(name = "Version", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", defaultValue = "%")
    protected List<String> version;

    /**
     * Ruft den Wert der all-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAll() {
        return all;
    }

    /**
     * Legt den Wert der all-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAll(EmptyType value) {
        this.all = value;
    }

    /**
     * Ruft den Wert der urn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURN() {
        return urn;
    }

    /**
     * Legt den Wert der urn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURN(String value) {
        this.urn = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the version property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getVersion() {
        if (version == null) {
            version = new ArrayList<String>();
        }
        return this.version;
    }

}
