
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CodelistReferenceType;


/**
 * HierarchicalCodelistWhereType contains the parameters of a hierarchical codelist query. All supplied parameters must be matched in order for an object to satisfy the query.
 * 
 * <p>Java-Klasse f�r HierarchicalCodelistWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HierarchicalCodelistWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}HierarchicalCodelistWhereBaseType">
 *       &lt;sequence>
 *         &lt;element name="IncludedCodelist" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HierarchicalCodelistWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "includedCodelist"
})
public class HierarchicalCodelistWhereType
    extends HierarchicalCodelistWhereBaseType
{

    @XmlElement(name = "IncludedCodelist", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<CodelistReferenceType> includedCodelist;

    /**
     * Gets the value of the includedCodelist property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedCodelist property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludedCodelist().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodelistReferenceType }
     * 
     * 
     */
    public List<CodelistReferenceType> getIncludedCodelist() {
        if (includedCodelist == null) {
            includedCodelist = new ArrayList<CodelistReferenceType>();
        }
        return this.includedCodelist;
    }

}
