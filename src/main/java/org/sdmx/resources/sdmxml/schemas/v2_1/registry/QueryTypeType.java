
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r QueryTypeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="QueryTypeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="DataSets"/>
 *     &lt;enumeration value="MetadataSets"/>
 *     &lt;enumeration value="AllSets"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "QueryTypeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
@XmlEnum
public enum QueryTypeType {


    /**
     * Only references data sets should be returned.
     * 
     */
    @XmlEnumValue("DataSets")
    DATA_SETS("DataSets"),

    /**
     * Only references to metadata sets should be returned.
     * 
     */
    @XmlEnumValue("MetadataSets")
    METADATA_SETS("MetadataSets"),

    /**
     * References to both data sets and metadata sets should be returned.
     * 
     */
    @XmlEnumValue("AllSets")
    ALL_SETS("AllSets");
    private final String value;

    QueryTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QueryTypeType fromValue(String v) {
        for (QueryTypeType c: QueryTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
