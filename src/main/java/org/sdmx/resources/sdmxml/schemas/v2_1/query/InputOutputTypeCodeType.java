
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r InputOutputTypeCodeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="InputOutputTypeCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Input"/>
 *     &lt;enumeration value="Output"/>
 *     &lt;enumeration value="Any"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InputOutputTypeCodeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum
public enum InputOutputTypeCodeType {


    /**
     * Input - referenced object is an input to the process step.
     * 
     */
    @XmlEnumValue("Input")
    INPUT("Input"),

    /**
     * Output - referenced object is an output to the process step.
     * 
     */
    @XmlEnumValue("Output")
    OUTPUT("Output"),

    /**
     * Any - referenced object is either an input or an output to the process step.
     * 
     */
    @XmlEnumValue("Any")
    ANY("Any");
    private final String value;

    InputOutputTypeCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InputOutputTypeCodeType fromValue(String v) {
        for (InputOutputTypeCodeType c: InputOutputTypeCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
