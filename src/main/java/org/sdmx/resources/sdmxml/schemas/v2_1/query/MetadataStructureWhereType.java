
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataStructureWhereType defines the parameters of a metadata structure definition query. In addition to querying based on the identification, it is also possible to search for metadata structure definitions based on information about its components.
 * 
 * <p>Java-Klasse f�r MetadataStructureWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataStructureWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataStructureWhereBaseType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataTargetWhere" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TargetObjectWhere" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReportStructureWhere" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataAttributeWhere" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataStructureWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "metadataTargetWhere",
    "targetObjectWhere",
    "reportStructureWhere",
    "metadataAttributeWhere"
})
public class MetadataStructureWhereType
    extends MetadataStructureWhereBaseType
{

    @XmlElement(name = "MetadataTargetWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<MetadataTargetWhereType> metadataTargetWhere;
    @XmlElement(name = "TargetObjectWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<TargetObjectWhereType> targetObjectWhere;
    @XmlElement(name = "ReportStructureWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<ReportStructureWhereType> reportStructureWhere;
    @XmlElement(name = "MetadataAttributeWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<MetadataAttributeWhereType> metadataAttributeWhere;

    /**
     * Gets the value of the metadataTargetWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadataTargetWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadataTargetWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadataTargetWhereType }
     * 
     * 
     */
    public List<MetadataTargetWhereType> getMetadataTargetWhere() {
        if (metadataTargetWhere == null) {
            metadataTargetWhere = new ArrayList<MetadataTargetWhereType>();
        }
        return this.metadataTargetWhere;
    }

    /**
     * Gets the value of the targetObjectWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the targetObjectWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTargetObjectWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TargetObjectWhereType }
     * 
     * 
     */
    public List<TargetObjectWhereType> getTargetObjectWhere() {
        if (targetObjectWhere == null) {
            targetObjectWhere = new ArrayList<TargetObjectWhereType>();
        }
        return this.targetObjectWhere;
    }

    /**
     * Gets the value of the reportStructureWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportStructureWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportStructureWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportStructureWhereType }
     * 
     * 
     */
    public List<ReportStructureWhereType> getReportStructureWhere() {
        if (reportStructureWhere == null) {
            reportStructureWhere = new ArrayList<ReportStructureWhereType>();
        }
        return this.reportStructureWhere;
    }

    /**
     * Gets the value of the metadataAttributeWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadataAttributeWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadataAttributeWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadataAttributeWhereType }
     * 
     * 
     */
    public List<MetadataAttributeWhereType> getMetadataAttributeWhere() {
        if (metadataAttributeWhere == null) {
            metadataAttributeWhere = new ArrayList<MetadataAttributeWhereType>();
        }
        return this.metadataAttributeWhere;
    }

}
