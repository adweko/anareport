
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * AttachmentConstraintType describes the details of an attachment constraint by defining the data or metadata key sets or component regions that attributes or reference metadata may be attached in the constraint attachment objects.
 * 
 * <p>Java-Klasse f�r AttachmentConstraintType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttachmentConstraintType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConstraintType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Name" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Description" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ConstraintAttachment" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}AttachmentConstraintAttachmentType" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="DataKeySet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataKeySetType"/>
 *           &lt;element name="MetadataKeySet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataKeySetType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachmentConstraintType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class AttachmentConstraintType
    extends ConstraintType
{


}
