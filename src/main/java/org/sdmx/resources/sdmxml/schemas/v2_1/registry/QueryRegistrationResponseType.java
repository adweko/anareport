
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * QueryRegistrationResponseType describes the structure of a registration query response. It provides a status for the request, and if successful, the resulting data and/or metadata results.
 * 
 * <p>Java-Klasse f�r QueryRegistrationResponseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="QueryRegistrationResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusMessage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}StatusMessageType"/>
 *         &lt;element name="QueryResult" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}QueryResultType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryRegistrationResponseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "statusMessage",
    "queryResult"
})
public class QueryRegistrationResponseType {

    @XmlElement(name = "StatusMessage", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected StatusMessageType statusMessage;
    @XmlElement(name = "QueryResult", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected List<QueryResultType> queryResult;

    /**
     * Ruft den Wert der statusMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StatusMessageType }
     *     
     */
    public StatusMessageType getStatusMessage() {
        return statusMessage;
    }

    /**
     * Legt den Wert der statusMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusMessageType }
     *     
     */
    public void setStatusMessage(StatusMessageType value) {
        this.statusMessage = value;
    }

    /**
     * Gets the value of the queryResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the queryResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQueryResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryResultType }
     * 
     * 
     */
    public List<QueryResultType> getQueryResult() {
        if (queryResult == null) {
            queryResult = new ArrayList<QueryResultType>();
        }
        return this.queryResult;
    }

}
