
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * StructuresType describes the structure of the container for all structural metadata components. The structural components may be explicitly detailed, or referenced from an external structure document or registry service. Best practices dictate that, at a minimum, any structural component that is referenced by another structural component be included by reference.
 * 
 * <p>Java-Klasse f�r StructuresType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructuresType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrganisationSchemes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}OrganisationSchemesType" minOccurs="0"/>
 *         &lt;element name="Dataflows" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataflowsType" minOccurs="0"/>
 *         &lt;element name="Metadataflows" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataflowsType" minOccurs="0"/>
 *         &lt;element name="CategorySchemes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CategorySchemesType" minOccurs="0"/>
 *         &lt;element name="Categorisations" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CategorisationsType" minOccurs="0"/>
 *         &lt;element name="Codelists" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodelistsType" minOccurs="0"/>
 *         &lt;element name="HierarchicalCodelists" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HierarchicalCodelistsType" minOccurs="0"/>
 *         &lt;element name="Concepts" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConceptsType" minOccurs="0"/>
 *         &lt;element name="MetadataStructures" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataStructuresType" minOccurs="0"/>
 *         &lt;element name="DataStructures" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataStructuresType" minOccurs="0"/>
 *         &lt;element name="StructureSets" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}StructureSetsType" minOccurs="0"/>
 *         &lt;element name="ReportingTaxonomies" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReportingTaxonomiesType" minOccurs="0"/>
 *         &lt;element name="Processes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ProcessesType" minOccurs="0"/>
 *         &lt;element name="Constraints" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConstraintsType" minOccurs="0"/>
 *         &lt;element name="ProvisionAgreements" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ProvisionAgreementsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuresType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "organisationSchemes",
    "dataflows",
    "metadataflows",
    "categorySchemes",
    "categorisations",
    "codelists",
    "hierarchicalCodelists",
    "concepts",
    "metadataStructures",
    "dataStructures",
    "structureSets",
    "reportingTaxonomies",
    "processes",
    "constraints",
    "provisionAgreements"
})
public class StructuresType {

    @XmlElement(name = "OrganisationSchemes", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected OrganisationSchemesType organisationSchemes;
    @XmlElement(name = "Dataflows", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected DataflowsType dataflows;
    @XmlElement(name = "Metadataflows", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected MetadataflowsType metadataflows;
    @XmlElement(name = "CategorySchemes", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected CategorySchemesType categorySchemes;
    @XmlElement(name = "Categorisations", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected CategorisationsType categorisations;
    @XmlElement(name = "Codelists", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected CodelistsType codelists;
    @XmlElement(name = "HierarchicalCodelists", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected HierarchicalCodelistsType hierarchicalCodelists;
    @XmlElement(name = "Concepts", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ConceptsType concepts;
    @XmlElement(name = "MetadataStructures", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected MetadataStructuresType metadataStructures;
    @XmlElement(name = "DataStructures", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected DataStructuresType dataStructures;
    @XmlElement(name = "StructureSets", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected StructureSetsType structureSets;
    @XmlElement(name = "ReportingTaxonomies", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ReportingTaxonomiesType reportingTaxonomies;
    @XmlElement(name = "Processes", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ProcessesType processes;
    @XmlElement(name = "Constraints", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ConstraintsType constraints;
    @XmlElement(name = "ProvisionAgreements", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ProvisionAgreementsType provisionAgreements;

    /**
     * Ruft den Wert der organisationSchemes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationSchemesType }
     *     
     */
    public OrganisationSchemesType getOrganisationSchemes() {
        return organisationSchemes;
    }

    /**
     * Legt den Wert der organisationSchemes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationSchemesType }
     *     
     */
    public void setOrganisationSchemes(OrganisationSchemesType value) {
        this.organisationSchemes = value;
    }

    /**
     * Ruft den Wert der dataflows-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataflowsType }
     *     
     */
    public DataflowsType getDataflows() {
        return dataflows;
    }

    /**
     * Legt den Wert der dataflows-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataflowsType }
     *     
     */
    public void setDataflows(DataflowsType value) {
        this.dataflows = value;
    }

    /**
     * Ruft den Wert der metadataflows-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MetadataflowsType }
     *     
     */
    public MetadataflowsType getMetadataflows() {
        return metadataflows;
    }

    /**
     * Legt den Wert der metadataflows-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MetadataflowsType }
     *     
     */
    public void setMetadataflows(MetadataflowsType value) {
        this.metadataflows = value;
    }

    /**
     * Ruft den Wert der categorySchemes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CategorySchemesType }
     *     
     */
    public CategorySchemesType getCategorySchemes() {
        return categorySchemes;
    }

    /**
     * Legt den Wert der categorySchemes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CategorySchemesType }
     *     
     */
    public void setCategorySchemes(CategorySchemesType value) {
        this.categorySchemes = value;
    }

    /**
     * Ruft den Wert der categorisations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CategorisationsType }
     *     
     */
    public CategorisationsType getCategorisations() {
        return categorisations;
    }

    /**
     * Legt den Wert der categorisations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CategorisationsType }
     *     
     */
    public void setCategorisations(CategorisationsType value) {
        this.categorisations = value;
    }

    /**
     * Ruft den Wert der codelists-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodelistsType }
     *     
     */
    public CodelistsType getCodelists() {
        return codelists;
    }

    /**
     * Legt den Wert der codelists-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodelistsType }
     *     
     */
    public void setCodelists(CodelistsType value) {
        this.codelists = value;
    }

    /**
     * Ruft den Wert der hierarchicalCodelists-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HierarchicalCodelistsType }
     *     
     */
    public HierarchicalCodelistsType getHierarchicalCodelists() {
        return hierarchicalCodelists;
    }

    /**
     * Legt den Wert der hierarchicalCodelists-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HierarchicalCodelistsType }
     *     
     */
    public void setHierarchicalCodelists(HierarchicalCodelistsType value) {
        this.hierarchicalCodelists = value;
    }

    /**
     * Ruft den Wert der concepts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConceptsType }
     *     
     */
    public ConceptsType getConcepts() {
        return concepts;
    }

    /**
     * Legt den Wert der concepts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptsType }
     *     
     */
    public void setConcepts(ConceptsType value) {
        this.concepts = value;
    }

    /**
     * Ruft den Wert der metadataStructures-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MetadataStructuresType }
     *     
     */
    public MetadataStructuresType getMetadataStructures() {
        return metadataStructures;
    }

    /**
     * Legt den Wert der metadataStructures-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MetadataStructuresType }
     *     
     */
    public void setMetadataStructures(MetadataStructuresType value) {
        this.metadataStructures = value;
    }

    /**
     * Ruft den Wert der dataStructures-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataStructuresType }
     *     
     */
    public DataStructuresType getDataStructures() {
        return dataStructures;
    }

    /**
     * Legt den Wert der dataStructures-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataStructuresType }
     *     
     */
    public void setDataStructures(DataStructuresType value) {
        this.dataStructures = value;
    }

    /**
     * Ruft den Wert der structureSets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureSetsType }
     *     
     */
    public StructureSetsType getStructureSets() {
        return structureSets;
    }

    /**
     * Legt den Wert der structureSets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureSetsType }
     *     
     */
    public void setStructureSets(StructureSetsType value) {
        this.structureSets = value;
    }

    /**
     * Ruft den Wert der reportingTaxonomies-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReportingTaxonomiesType }
     *     
     */
    public ReportingTaxonomiesType getReportingTaxonomies() {
        return reportingTaxonomies;
    }

    /**
     * Legt den Wert der reportingTaxonomies-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportingTaxonomiesType }
     *     
     */
    public void setReportingTaxonomies(ReportingTaxonomiesType value) {
        this.reportingTaxonomies = value;
    }

    /**
     * Ruft den Wert der processes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProcessesType }
     *     
     */
    public ProcessesType getProcesses() {
        return processes;
    }

    /**
     * Legt den Wert der processes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessesType }
     *     
     */
    public void setProcesses(ProcessesType value) {
        this.processes = value;
    }

    /**
     * Ruft den Wert der constraints-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConstraintsType }
     *     
     */
    public ConstraintsType getConstraints() {
        return constraints;
    }

    /**
     * Legt den Wert der constraints-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstraintsType }
     *     
     */
    public void setConstraints(ConstraintsType value) {
        this.constraints = value;
    }

    /**
     * Ruft den Wert der provisionAgreements-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProvisionAgreementsType }
     *     
     */
    public ProvisionAgreementsType getProvisionAgreements() {
        return provisionAgreements;
    }

    /**
     * Legt den Wert der provisionAgreements-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvisionAgreementsType }
     *     
     */
    public void setProvisionAgreements(ProvisionAgreementsType value) {
        this.provisionAgreements = value;
    }

}
