
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * RefBaseType is an abstract base type the defines the basis for any set of complete reference fields. This should be refined by derived types so that only the necessary fields are available and required as necessary. This can be used for both full and local references (when some of the values are implied from another context). A local reference is indicated with the local attribute. The values in this type correspond directly to the components of the URN structure, and thus can be used to compose a URN when the local attribute value is false. As this is the case, any reference components which are not part of the URN structure should not be present in the derived types.
 * 
 * <p>Java-Klasse f�r RefBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RefBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="agencyID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedNCNameIDType" />
 *       &lt;attribute name="maintainableParentID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="maintainableParentVersion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionType" />
 *       &lt;attribute name="containerID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedIDType" />
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedIDType" />
 *       &lt;attribute name="version" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionType" />
 *       &lt;attribute name="local" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PackageTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    ObjectRefType.class,
    AnyLocalCodeRefType.class,
    ContainerChildObjectRefBaseType.class,
    ChildObjectRefBaseType.class,
    LocalIdentifiableRefBaseType.class,
    MaintainableRefBaseType.class
})
public class RefBaseType {

    @XmlAttribute(name = "agencyID")
    protected String agencyID;
    @XmlAttribute(name = "maintainableParentID")
    protected String maintainableParentID;
    @XmlAttribute(name = "maintainableParentVersion")
    protected String maintainableParentVersion;
    @XmlAttribute(name = "containerID")
    protected String containerID;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "version")
    protected String version;
    @XmlAttribute(name = "local")
    protected Boolean local;
    @XmlAttribute(name = "class")
    protected ObjectTypeCodelistType clazz;
    @XmlAttribute(name = "package")
    protected PackageTypeCodelistType _package;

    /**
     * Ruft den Wert der agencyID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyID() {
        return agencyID;
    }

    /**
     * Legt den Wert der agencyID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyID(String value) {
        this.agencyID = value;
    }

    /**
     * Ruft den Wert der maintainableParentID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaintainableParentID() {
        return maintainableParentID;
    }

    /**
     * Legt den Wert der maintainableParentID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaintainableParentID(String value) {
        this.maintainableParentID = value;
    }

    /**
     * Ruft den Wert der maintainableParentVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaintainableParentVersion() {
        return maintainableParentVersion;
    }

    /**
     * Legt den Wert der maintainableParentVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaintainableParentVersion(String value) {
        this.maintainableParentVersion = value;
    }

    /**
     * Ruft den Wert der containerID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContainerID() {
        return containerID;
    }

    /**
     * Legt den Wert der containerID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContainerID(String value) {
        this.containerID = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der local-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLocal() {
        return local;
    }

    /**
     * Legt den Wert der local-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLocal(Boolean value) {
        this.local = value;
    }

    /**
     * Ruft den Wert der clazz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjectTypeCodelistType }
     *     
     */
    public ObjectTypeCodelistType getClazz() {
        return clazz;
    }

    /**
     * Legt den Wert der clazz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectTypeCodelistType }
     *     
     */
    public void setClazz(ObjectTypeCodelistType value) {
        this.clazz = value;
    }

    /**
     * Ruft den Wert der package-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PackageTypeCodelistType }
     *     
     */
    public PackageTypeCodelistType getPackage() {
        return _package;
    }

    /**
     * Legt den Wert der package-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageTypeCodelistType }
     *     
     */
    public void setPackage(PackageTypeCodelistType value) {
        this._package = value;
    }

}
