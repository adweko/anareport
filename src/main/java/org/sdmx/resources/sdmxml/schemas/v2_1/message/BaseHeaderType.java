
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ActionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.PayloadStructureType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TextType;


/**
 * BaseHeaderType in an abstract base type that defines the basis for all message headers. Specific message formats will refine this
 * 
 * <p>Java-Klasse f�r BaseHeaderType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BaseHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType"/>
 *         &lt;element name="Test" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Prepared" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}HeaderTimeType"/>
 *         &lt;element name="Sender" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}SenderType"/>
 *         &lt;element name="Receiver" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}PartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Name" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PayloadStructureType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0"/>
 *         &lt;element name="DataSetAction" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ActionType" minOccurs="0"/>
 *         &lt;element name="DataSetID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Extracted" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ReportingBegin" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationalTimePeriodType" minOccurs="0"/>
 *         &lt;element name="ReportingEnd" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationalTimePeriodType" minOccurs="0"/>
 *         &lt;element name="EmbargoDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseHeaderType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", propOrder = {
    "id",
    "test",
    "prepared",
    "sender",
    "receiver",
    "name",
    "structure",
    "dataProvider",
    "dataSetAction",
    "dataSetID",
    "extracted",
    "reportingBegin",
    "reportingEnd",
    "embargoDate",
    "source"
})
@XmlSeeAlso({
    GenericDataHeaderType.class,
    StructureSpecificMetadataHeaderType.class,
    GenericMetadataHeaderType.class,
    StructureSpecificDataHeaderType.class,
    BasicHeaderType.class,
    StructureHeaderType.class
})
public abstract class BaseHeaderType {

    @XmlElement(name = "ID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", required = true)
    protected String id;
    @XmlElement(name = "Test", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", defaultValue = "false")
    protected boolean test;
    @XmlElement(name = "Prepared", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", required = true)
    protected String prepared;
    @XmlElement(name = "Sender", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", required = true)
    protected SenderType sender;
    @XmlElement(name = "Receiver", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    protected List<PartyType> receiver;
    @XmlElement(name = "Name", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<TextType> name;
    @XmlElement(name = "Structure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    protected List<PayloadStructureType> structure;
    @XmlElement(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    protected DataProviderReferenceType dataProvider;
    @XmlElement(name = "DataSetAction", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    @XmlSchemaType(name = "NMTOKEN")
    protected ActionType dataSetAction;
    @XmlElement(name = "DataSetID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    protected List<String> dataSetID;
    @XmlElement(name = "Extracted", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar extracted;
    @XmlList
    @XmlElement(name = "ReportingBegin", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    protected List<String> reportingBegin;
    @XmlList
    @XmlElement(name = "ReportingEnd", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    protected List<String> reportingEnd;
    @XmlElement(name = "EmbargoDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar embargoDate;
    @XmlElement(name = "Source", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
    protected List<TextType> source;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der test-Eigenschaft ab.
     * 
     */
    public boolean isTest() {
        return test;
    }

    /**
     * Legt den Wert der test-Eigenschaft fest.
     * 
     */
    public void setTest(boolean value) {
        this.test = value;
    }

    /**
     * Ruft den Wert der prepared-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrepared() {
        return prepared;
    }

    /**
     * Legt den Wert der prepared-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrepared(String value) {
        this.prepared = value;
    }

    /**
     * Ruft den Wert der sender-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SenderType }
     *     
     */
    public SenderType getSender() {
        return sender;
    }

    /**
     * Legt den Wert der sender-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SenderType }
     *     
     */
    public void setSender(SenderType value) {
        this.sender = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receiver property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceiver().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyType }
     * 
     * 
     */
    public List<PartyType> getReceiver() {
        if (receiver == null) {
            receiver = new ArrayList<PartyType>();
        }
        return this.receiver;
    }

    /**
     * Name provides a name for the transmission. Multiple instances allow for parallel language values.Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getName() {
        if (name == null) {
            name = new ArrayList<TextType>();
        }
        return this.name;
    }

    /**
     * Gets the value of the structure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the structure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PayloadStructureType }
     * 
     * 
     */
    public List<PayloadStructureType> getStructure() {
        if (structure == null) {
            structure = new ArrayList<PayloadStructureType>();
        }
        return this.structure;
    }

    /**
     * Ruft den Wert der dataProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public DataProviderReferenceType getDataProvider() {
        return dataProvider;
    }

    /**
     * Legt den Wert der dataProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public void setDataProvider(DataProviderReferenceType value) {
        this.dataProvider = value;
    }

    /**
     * Ruft den Wert der dataSetAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getDataSetAction() {
        return dataSetAction;
    }

    /**
     * Legt den Wert der dataSetAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setDataSetAction(ActionType value) {
        this.dataSetAction = value;
    }

    /**
     * Gets the value of the dataSetID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataSetID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataSetID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDataSetID() {
        if (dataSetID == null) {
            dataSetID = new ArrayList<String>();
        }
        return this.dataSetID;
    }

    /**
     * Ruft den Wert der extracted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExtracted() {
        return extracted;
    }

    /**
     * Legt den Wert der extracted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExtracted(XMLGregorianCalendar value) {
        this.extracted = value;
    }

    /**
     * Gets the value of the reportingBegin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingBegin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingBegin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportingBegin() {
        if (reportingBegin == null) {
            reportingBegin = new ArrayList<String>();
        }
        return this.reportingBegin;
    }

    /**
     * Gets the value of the reportingEnd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingEnd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingEnd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportingEnd() {
        if (reportingEnd == null) {
            reportingEnd = new ArrayList<String>();
        }
        return this.reportingEnd;
    }

    /**
     * Ruft den Wert der embargoDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEmbargoDate() {
        return embargoDate;
    }

    /**
     * Legt den Wert der embargoDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEmbargoDate(XMLGregorianCalendar value) {
        this.embargoDate = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the source property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getSource() {
        if (source == null) {
            source = new ArrayList<TextType>();
        }
        return this.source;
    }

}
