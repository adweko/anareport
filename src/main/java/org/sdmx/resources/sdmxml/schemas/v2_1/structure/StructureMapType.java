
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureOrUsageReferenceType;


/**
 * StructureMapType defines the structure for mapping components of one structure to components of another structure. A structure may be referenced directly meaning the map applies wherever the structure is used, or it may be a reference via a structure usage meaning the map only applies within the context of that usage. Using the related structures, one can make extrapolations between maps. For example, if key families, A, B, and C, are all grouped in a related structures container, then a map from key family A to C and a map from key family B to C could be used to infer a relation between key family A to C.
 * 
 * <p>Java-Klasse f�r StructureMapType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureMapType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}StructureMapBaseType">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureOrUsageReferenceType"/>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureOrUsageReferenceType"/>
 *         &lt;element name="ComponentMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ComponentMapType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="isExtension" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureMapType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "source",
    "target",
    "componentMap"
})
public class StructureMapType
    extends StructureMapBaseType
{

    @XmlElement(name = "Source", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected StructureOrUsageReferenceType source;
    @XmlElement(name = "Target", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected StructureOrUsageReferenceType target;
    @XmlElement(name = "ComponentMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected ComponentMapType componentMap;
    @XmlAttribute(name = "isExtension")
    protected Boolean isExtension;

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureOrUsageReferenceType }
     *     
     */
    public StructureOrUsageReferenceType getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureOrUsageReferenceType }
     *     
     */
    public void setSource(StructureOrUsageReferenceType value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der target-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureOrUsageReferenceType }
     *     
     */
    public StructureOrUsageReferenceType getTarget() {
        return target;
    }

    /**
     * Legt den Wert der target-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureOrUsageReferenceType }
     *     
     */
    public void setTarget(StructureOrUsageReferenceType value) {
        this.target = value;
    }

    /**
     * Ruft den Wert der componentMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComponentMapType }
     *     
     */
    public ComponentMapType getComponentMap() {
        return componentMap;
    }

    /**
     * Legt den Wert der componentMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentMapType }
     *     
     */
    public void setComponentMap(ComponentMapType value) {
        this.componentMap = value;
    }

    /**
     * Ruft den Wert der isExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsExtension() {
        if (isExtension == null) {
            return false;
        } else {
            return isExtension;
        }
    }

    /**
     * Legt den Wert der isExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsExtension(Boolean value) {
        this.isExtension = value;
    }

}
