
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ConceptReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DimensionTypeType;


/**
 * BaseDimensionType is an abstract base type which defines the basic structure of all dimensions.
 * 
 * <p>Java-Klasse f�r BaseDimensionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BaseDimensionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}BaseDimensionBaseType">
 *       &lt;sequence>
 *         &lt;element name="ConceptRole" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="position" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DimensionTypeType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseDimensionType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "conceptRole"
})
@XmlSeeAlso({
    MeasureDimensionType.class,
    TimeDimensionType.class,
    DimensionType.class
})
public abstract class BaseDimensionType
    extends BaseDimensionBaseType
{

    @XmlElement(name = "ConceptRole", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<ConceptReferenceType> conceptRole;
    @XmlAttribute(name = "position")
    protected Integer position;
    @XmlAttribute(name = "type")
    protected DimensionTypeType type;

    /**
     * Gets the value of the conceptRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conceptRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConceptRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConceptReferenceType }
     * 
     * 
     */
    public List<ConceptReferenceType> getConceptRole() {
        if (conceptRole == null) {
            conceptRole = new ArrayList<ConceptReferenceType>();
        }
        return this.conceptRole;
    }

    /**
     * Ruft den Wert der position-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPosition() {
        return position;
    }

    /**
     * Legt den Wert der position-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPosition(Integer value) {
        this.position = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DimensionTypeType }
     *     
     */
    public DimensionTypeType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DimensionTypeType }
     *     
     */
    public void setType(DimensionTypeType value) {
        this.type = value;
    }

}
