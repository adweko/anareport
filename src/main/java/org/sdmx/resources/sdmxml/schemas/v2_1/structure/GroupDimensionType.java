
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalDimensionReferenceType;


/**
 * GroupDimensionType defines a dimension component with a group key descriptor component list. Although technically a component, this is essentially a reference to a dimension defined in the key descriptor. Therefore, the identification, name, and description, concept identity and representation properties that are typically available for a component are not allowed here, as they are all inherited from the referenced dimension.
 * 
 * <p>Java-Klasse f�r GroupDimensionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GroupDimensionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}GroupDimensionBaseType">
 *       &lt;sequence>
 *         &lt;element name="DimensionReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalDimensionReferenceType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDimensionType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "dimensionReference"
})
public class GroupDimensionType
    extends GroupDimensionBaseType
{

    @XmlElement(name = "DimensionReference", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected LocalDimensionReferenceType dimensionReference;

    /**
     * Ruft den Wert der dimensionReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalDimensionReferenceType }
     *     
     */
    public LocalDimensionReferenceType getDimensionReference() {
        return dimensionReference;
    }

    /**
     * Legt den Wert der dimensionReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDimensionReferenceType }
     *     
     */
    public void setDimensionReference(LocalDimensionReferenceType value) {
        this.dimensionReference = value;
    }

}
