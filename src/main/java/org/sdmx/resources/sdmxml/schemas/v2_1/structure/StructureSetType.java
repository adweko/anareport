
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureOrUsageReferenceType;


/**
 * StructureSetType describes the structure of a structure set. It allows components in one structure, structure usage, or item scheme to be mapped to components in another structural component of the same type.
 * 
 * <p>Java-Klasse f�r StructureSetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureSetType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}StructureSetBaseType">
 *       &lt;sequence>
 *         &lt;element name="RelatedStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureOrUsageReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="OrganisationSchemeMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}OrganisationSchemeMapType"/>
 *           &lt;element name="CategorySchemeMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CategorySchemeMapType"/>
 *           &lt;element name="CodelistMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodelistMapType"/>
 *           &lt;element name="ConceptSchemeMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConceptSchemeMapType"/>
 *           &lt;element name="ReportingTaxonomyMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReportingTaxonomyMapType"/>
 *           &lt;element name="HybridCodelistMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HybridCodelistMapType"/>
 *           &lt;element name="StructureMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}StructureMapType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureSetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "relatedStructure",
    "organisationSchemeMapOrCategorySchemeMapOrCodelistMap"
})
public class StructureSetType
    extends StructureSetBaseType
{

    @XmlElement(name = "RelatedStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<StructureOrUsageReferenceType> relatedStructure;
    @XmlElements({
        @XmlElement(name = "OrganisationSchemeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = OrganisationSchemeMapType.class),
        @XmlElement(name = "CategorySchemeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = CategorySchemeMapType.class),
        @XmlElement(name = "CodelistMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = CodelistMapType.class),
        @XmlElement(name = "ConceptSchemeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = ConceptSchemeMapType.class),
        @XmlElement(name = "ReportingTaxonomyMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = ReportingTaxonomyMapType.class),
        @XmlElement(name = "HybridCodelistMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = HybridCodelistMapType.class),
        @XmlElement(name = "StructureMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = StructureMapType.class)
    })
    protected List<NameableType> organisationSchemeMapOrCategorySchemeMapOrCodelistMap;

    /**
     * Gets the value of the relatedStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StructureOrUsageReferenceType }
     * 
     * 
     */
    public List<StructureOrUsageReferenceType> getRelatedStructure() {
        if (relatedStructure == null) {
            relatedStructure = new ArrayList<StructureOrUsageReferenceType>();
        }
        return this.relatedStructure;
    }

    /**
     * Gets the value of the organisationSchemeMapOrCategorySchemeMapOrCodelistMap property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the organisationSchemeMapOrCategorySchemeMapOrCodelistMap property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrganisationSchemeMapOrCategorySchemeMapOrCodelistMap().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrganisationSchemeMapType }
     * {@link CategorySchemeMapType }
     * {@link CodelistMapType }
     * {@link ConceptSchemeMapType }
     * {@link ReportingTaxonomyMapType }
     * {@link HybridCodelistMapType }
     * {@link StructureMapType }
     * 
     * 
     */
    public List<NameableType> getOrganisationSchemeMapOrCategorySchemeMapOrCodelistMap() {
        if (organisationSchemeMapOrCategorySchemeMapOrCodelistMap == null) {
            organisationSchemeMapOrCategorySchemeMapOrCodelistMap = new ArrayList<NameableType>();
        }
        return this.organisationSchemeMapOrCategorySchemeMapOrCodelistMap;
    }

}
