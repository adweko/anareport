
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * TargetObjectWhereBaseType is an abstract base type that forms the basis for the TargetObjectWhereType.
 * 
 * <p>Java-Klasse f�r TargetObjectWhereBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TargetObjectWhereBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ComponentWhereType">
 *       &lt;sequence>
 *         &lt;element name="Annotation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AnnotationWhereType" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" minOccurs="0"/>
 *         &lt;element name="Enumeration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetObjectWhereBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlSeeAlso({
    TargetObjectWhereType.class
})
public abstract class TargetObjectWhereBaseType
    extends ComponentWhereType
{


}
