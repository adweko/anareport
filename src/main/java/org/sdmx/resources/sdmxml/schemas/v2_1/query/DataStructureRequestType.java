
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * DataStructureRequestType extends the base DataStructureRequestType to add additional parameters that are necessary when querying for a schema.
 * 
 * <p>Java-Klasse f�r DataStructureRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataStructureRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureRequestType">
 *       &lt;attribute name="timeSeries" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="processConstraints" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataStructureRequestType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class DataStructureRequestType
    extends org.sdmx.resources.sdmxml.schemas.v2_1.common.DataStructureRequestType
{

    @XmlAttribute(name = "timeSeries")
    protected Boolean timeSeries;
    @XmlAttribute(name = "processConstraints")
    protected Boolean processConstraints;

    /**
     * Ruft den Wert der timeSeries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isTimeSeries() {
        if (timeSeries == null) {
            return false;
        } else {
            return timeSeries;
        }
    }

    /**
     * Legt den Wert der timeSeries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTimeSeries(Boolean value) {
        this.timeSeries = value;
    }

    /**
     * Ruft den Wert der processConstraints-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProcessConstraints() {
        if (processConstraints == null) {
            return false;
        } else {
            return processConstraints;
        }
    }

    /**
     * Legt den Wert der processConstraints-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessConstraints(Boolean value) {
        this.processConstraints = value;
    }

}
