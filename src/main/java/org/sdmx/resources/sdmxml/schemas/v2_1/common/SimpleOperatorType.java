
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r SimpleOperatorType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SimpleOperatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="notEqual"/>
 *     &lt;enumeration value="equal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SimpleOperatorType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum SimpleOperatorType {


    /**
     * (!=) - value must not be equal to the value supplied.
     * 
     */
    @XmlEnumValue("notEqual")
    NOT_EQUAL("notEqual"),

    /**
     * (=) - value must be exactly equal to the value supplied.
     * 
     */
    @XmlEnumValue("equal")
    EQUAL("equal");
    private final String value;

    SimpleOperatorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SimpleOperatorType fromValue(String v) {
        for (SimpleOperatorType c: SimpleOperatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
