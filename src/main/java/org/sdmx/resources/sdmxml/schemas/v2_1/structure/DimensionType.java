
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DimensionType describes the structure of an ordinary dimension, which is defined as a statistical concept used (most probably together with other statistical concepts) to identify a statistical series, such as a time series, e.g. a statistical concept indicating certain economic activity or a geographical reference area. The dimension takes its semantic, and in some cases it representation, from its concept identity. A dimension can be coded by referencing a code list from its coded local representation. It can also specify its text format, which is used as the representation of the dimension if a coded representation is not defined. Neither the coded or uncoded representation are necessary, since the dimension may take these from the referenced concept.
 * 
 * <p>Java-Klasse f�r DimensionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DimensionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}BaseDimensionType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element name="ConceptIdentity" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType"/>
 *         &lt;element name="LocalRepresentation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}SimpleDataStructureRepresentationType" minOccurs="0"/>
 *         &lt;element name="ConceptRole" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DimensionTypeType" fixed="Dimension" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DimensionType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class DimensionType
    extends BaseDimensionType
{


}
