
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnyCodelistReferenceType;


/**
 * HybridCodelistMapType defines the structure of a map which relates codes (possibly hierarchical) from different code lists.
 * 
 * <p>Java-Klasse f�r HybridCodelistMapType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HybridCodelistMapType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HybridCodelistMapBaseType">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnyCodelistReferenceType"/>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnyCodelistReferenceType"/>
 *         &lt;element name="HybridCodeMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HybridCodeMapType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HybridCodelistMapType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "source",
    "target",
    "hybridCodeMap"
})
public class HybridCodelistMapType
    extends HybridCodelistMapBaseType
{

    @XmlElement(name = "Source", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected AnyCodelistReferenceType source;
    @XmlElement(name = "Target", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected AnyCodelistReferenceType target;
    @XmlElement(name = "HybridCodeMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected List<HybridCodeMapType> hybridCodeMap;

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnyCodelistReferenceType }
     *     
     */
    public AnyCodelistReferenceType getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnyCodelistReferenceType }
     *     
     */
    public void setSource(AnyCodelistReferenceType value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der target-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnyCodelistReferenceType }
     *     
     */
    public AnyCodelistReferenceType getTarget() {
        return target;
    }

    /**
     * Legt den Wert der target-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnyCodelistReferenceType }
     *     
     */
    public void setTarget(AnyCodelistReferenceType value) {
        this.target = value;
    }

    /**
     * Gets the value of the hybridCodeMap property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hybridCodeMap property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHybridCodeMap().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HybridCodeMapType }
     * 
     * 
     */
    public List<HybridCodeMapType> getHybridCodeMap() {
        if (hybridCodeMap == null) {
            hybridCodeMap = new ArrayList<HybridCodeMapType>();
        }
        return this.hybridCodeMap;
    }

}
