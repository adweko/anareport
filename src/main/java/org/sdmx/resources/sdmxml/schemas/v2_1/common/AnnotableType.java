
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.generic.ObsOnlyType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.ComponentMapType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.ComputationType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.HybridCodeMapType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.IdentifiableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.InputOutputType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.ItemAssociationType;


/**
 * AnnotableType is an abstract base type used for all annotable artefacts. Any type that provides for annotations should extend this type.
 * 
 * <p>Java-Klasse f�r AnnotableType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AnnotableType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnnotableType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", propOrder = {
    "annotations"
})
@XmlSeeAlso({
    ItemAssociationType.class,
    ComputationType.class,
    HybridCodeMapType.class,
    InputOutputType.class,
    ComponentMapType.class,
    IdentifiableType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.data.generic.SeriesType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.data.generic.DataSetType.class,
    ObsOnlyType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.data.generic.GroupType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.data.generic.ObsType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.GroupType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.SeriesType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.metadata.generic.ReportedAttributeType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.metadata.generic.MetadataSetType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.metadata.generic.ReportType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.metadata.structurespecific.ReportedAttributeType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.metadata.structurespecific.MetadataSetType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.metadata.structurespecific.ReportType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.DataSetType.class,
    org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType.class
})
public abstract class AnnotableType {

    @XmlElement(name = "Annotations", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected AnnotationsType annotations;

    /**
     * Ruft den Wert der annotations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnotationsType }
     *     
     */
    public AnnotationsType getAnnotations() {
        return annotations;
    }

    /**
     * Legt den Wert der annotations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnotationsType }
     *     
     */
    public void setAnnotations(AnnotationsType value) {
        this.annotations = value;
    }

}
