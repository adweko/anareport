
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r MetadataStructureComponentTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="MetadataStructureComponentTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentTypeCodelistType">
 *     &lt;enumeration value="ConstraintTarget"/>
 *     &lt;enumeration value="DataSetTarget"/>
 *     &lt;enumeration value="IdentifiableObjectTarget"/>
 *     &lt;enumeration value="DimensionDescriptorValuesTarget"/>
 *     &lt;enumeration value="MetadataAttribute"/>
 *     &lt;enumeration value="ReportPeriodTarget"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MetadataStructureComponentTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ComponentTypeCodelistType.class)
public enum MetadataStructureComponentTypeCodelistType {

    @XmlEnumValue("ConstraintTarget")
    CONSTRAINT_TARGET(ComponentTypeCodelistType.CONSTRAINT_TARGET),
    @XmlEnumValue("DataSetTarget")
    DATA_SET_TARGET(ComponentTypeCodelistType.DATA_SET_TARGET),
    @XmlEnumValue("IdentifiableObjectTarget")
    IDENTIFIABLE_OBJECT_TARGET(ComponentTypeCodelistType.IDENTIFIABLE_OBJECT_TARGET),
    @XmlEnumValue("DimensionDescriptorValuesTarget")
    DIMENSION_DESCRIPTOR_VALUES_TARGET(ComponentTypeCodelistType.DIMENSION_DESCRIPTOR_VALUES_TARGET),
    @XmlEnumValue("MetadataAttribute")
    METADATA_ATTRIBUTE(ComponentTypeCodelistType.METADATA_ATTRIBUTE),
    @XmlEnumValue("ReportPeriodTarget")
    REPORT_PERIOD_TARGET(ComponentTypeCodelistType.REPORT_PERIOD_TARGET);
    private final ComponentTypeCodelistType value;

    MetadataStructureComponentTypeCodelistType(ComponentTypeCodelistType v) {
        value = v;
    }

    public ComponentTypeCodelistType value() {
        return value;
    }

    public static MetadataStructureComponentTypeCodelistType fromValue(ComponentTypeCodelistType v) {
        for (MetadataStructureComponentTypeCodelistType c: MetadataStructureComponentTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
