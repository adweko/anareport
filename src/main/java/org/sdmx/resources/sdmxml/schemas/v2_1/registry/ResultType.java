
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ContentConstraintReferenceType;


/**
 * ResultType contains the details about a data or metadata source, through the complete registration information. In addition, a reference to the content constraints for the data source may be provided, if the query requested this information.
 * 
 * <p>Java-Klasse f�r ResultType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}RegistrationType"/>
 *         &lt;element name="ContentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ContentConstraintReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "registration",
    "contentConstraint"
})
public class ResultType {

    @XmlElement(name = "Registration", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected RegistrationType registration;
    @XmlElement(name = "ContentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected List<ContentConstraintReferenceType> contentConstraint;

    /**
     * Ruft den Wert der registration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationType }
     *     
     */
    public RegistrationType getRegistration() {
        return registration;
    }

    /**
     * Legt den Wert der registration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationType }
     *     
     */
    public void setRegistration(RegistrationType value) {
        this.registration = value;
    }

    /**
     * Gets the value of the contentConstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contentConstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContentConstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContentConstraintReferenceType }
     * 
     * 
     */
    public List<ContentConstraintReferenceType> getContentConstraint() {
        if (contentConstraint == null) {
            contentConstraint = new ArrayList<ContentConstraintReferenceType>();
        }
        return this.contentConstraint;
    }

}
