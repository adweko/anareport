
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r DataReturnDetailType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="DataReturnDetailType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Full"/>
 *     &lt;enumeration value="DataOnly"/>
 *     &lt;enumeration value="SeriesKeyOnly"/>
 *     &lt;enumeration value="NoData"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DataReturnDetailType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum
public enum DataReturnDetailType {


    /**
     * The entire data set (including all data, documentation, and annotations) will be returned.
     * 
     */
    @XmlEnumValue("Full")
    FULL("Full"),

    /**
     * Only the observed values and their keys will be returned. Annotations and documentation (i.e. Attributes) and therefore Groups, will be excluded.
     * 
     */
    @XmlEnumValue("DataOnly")
    DATA_ONLY("DataOnly"),

    /**
     * Only the series elements and the values for the dimensions will be returned. Annotations, documentation, and observations will be excluded.
     * 
     */
    @XmlEnumValue("SeriesKeyOnly")
    SERIES_KEY_ONLY("SeriesKeyOnly"),

    /**
     * Returns all documentation at the DataSet, Group, and Series level without any Observations (therefore, Observation level documentation is not returned). Annotations are not returned.
     * 
     */
    @XmlEnumValue("NoData")
    NO_DATA("NoData");
    private final String value;

    DataReturnDetailType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DataReturnDetailType fromValue(String v) {
        for (DataReturnDetailType c: DataReturnDetailType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
