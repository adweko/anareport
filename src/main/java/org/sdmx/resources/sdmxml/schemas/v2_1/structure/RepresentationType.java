
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ItemSchemeReferenceBaseType;


/**
 * RepresentationType is an abstract type that defines a representation. Because the type of item schemes that are allowed as the an enumeration vary based on the object in which this is defined, this type is abstract to force that the enumeration reference be restricted to the proper type of item scheme reference.
 * 
 * <p>Java-Klasse f�r RepresentationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RepresentationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="TextFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}TextFormatType"/>
 *         &lt;sequence>
 *           &lt;element name="Enumeration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeReferenceBaseType"/>
 *           &lt;element name="EnumerationFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodededTextFormatType" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepresentationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "textFormat",
    "enumeration",
    "enumerationFormat"
})
@XmlSeeAlso({
    KeyDescriptorValuesRepresentationType.class,
    ConceptRepresentation.class,
    DataSetRepresentationType.class,
    ConstraintRepresentationType.class,
    IdentifiableObjectRepresentationType.class,
    ReportPeriodRepresentationType.class,
    DataStructureRepresentationType.class,
    MetadataAttributeRepresentationType.class
})
public abstract class RepresentationType {

    @XmlElement(name = "TextFormat", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected TextFormatType textFormat;
    @XmlElement(name = "Enumeration", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ItemSchemeReferenceBaseType enumeration;
    @XmlElement(name = "EnumerationFormat", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected CodededTextFormatType enumerationFormat;

    /**
     * Ruft den Wert der textFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextFormatType }
     *     
     */
    public TextFormatType getTextFormat() {
        return textFormat;
    }

    /**
     * Legt den Wert der textFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextFormatType }
     *     
     */
    public void setTextFormat(TextFormatType value) {
        this.textFormat = value;
    }

    /**
     * Ruft den Wert der enumeration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ItemSchemeReferenceBaseType }
     *     
     */
    public ItemSchemeReferenceBaseType getEnumeration() {
        return enumeration;
    }

    /**
     * Legt den Wert der enumeration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemSchemeReferenceBaseType }
     *     
     */
    public void setEnumeration(ItemSchemeReferenceBaseType value) {
        this.enumeration = value;
    }

    /**
     * Ruft den Wert der enumerationFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodededTextFormatType }
     *     
     */
    public CodededTextFormatType getEnumerationFormat() {
        return enumerationFormat;
    }

    /**
     * Legt den Wert der enumerationFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodededTextFormatType }
     *     
     */
    public void setEnumerationFormat(CodededTextFormatType value) {
        this.enumerationFormat = value;
    }

}
