
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataKeyType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ObjectReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SetReferenceType;


/**
 * IdentifierComponentValueType describes the structure that is used to match reference metadata where a given identifier component has a particular value.
 * 
 * <p>Java-Klasse f�r TargetObjectValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TargetObjectValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NCNameIDType"/>
 *         &lt;choice>
 *           &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType"/>
 *           &lt;element name="DataKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataKeyType"/>
 *           &lt;element name="Object" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeValue" maxOccurs="2"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetObjectValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "id",
    "dataSet",
    "dataKey",
    "object",
    "timeValue"
})
public class TargetObjectValueType {

    @XmlElement(name = "ID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected String id;
    @XmlElement(name = "DataSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected SetReferenceType dataSet;
    @XmlElement(name = "DataKey", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected DataKeyType dataKey;
    @XmlElement(name = "Object", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected ObjectReferenceType object;
    @XmlElement(name = "TimeValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<TimePeriodValueType> timeValue;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der dataSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SetReferenceType }
     *     
     */
    public SetReferenceType getDataSet() {
        return dataSet;
    }

    /**
     * Legt den Wert der dataSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SetReferenceType }
     *     
     */
    public void setDataSet(SetReferenceType value) {
        this.dataSet = value;
    }

    /**
     * Ruft den Wert der dataKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataKeyType }
     *     
     */
    public DataKeyType getDataKey() {
        return dataKey;
    }

    /**
     * Legt den Wert der dataKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataKeyType }
     *     
     */
    public void setDataKey(DataKeyType value) {
        this.dataKey = value;
    }

    /**
     * Ruft den Wert der object-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceType }
     *     
     */
    public ObjectReferenceType getObject() {
        return object;
    }

    /**
     * Legt den Wert der object-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceType }
     *     
     */
    public void setObject(ObjectReferenceType value) {
        this.object = value;
    }

    /**
     * TimeValue is used to provide a time value or range for matching a reporting period which the target object should reference to result in a match.Gets the value of the timeValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timeValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimeValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TimePeriodValueType }
     * 
     * 
     */
    public List<TimePeriodValueType> getTimeValue() {
        if (timeValue == null) {
            timeValue = new ArrayList<TimePeriodValueType>();
        }
        return this.timeValue;
    }

}
