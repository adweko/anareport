
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ProcessesType describes the structure of the processes container. It contains one or more process, which can be explicitly detailed or referenced from an external structure document or registry service.
 * 
 * <p>Java-Klasse f�r ProcessesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProcessesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Process" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ProcessType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessesType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "process"
})
public class ProcessesType {

    @XmlElement(name = "Process", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected List<ProcessType> process;

    /**
     * Gets the value of the process property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the process property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcess().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessType }
     * 
     * 
     */
    public List<ProcessType> getProcess() {
        if (process == null) {
            process = new ArrayList<ProcessType>();
        }
        return this.process;
    }

}
