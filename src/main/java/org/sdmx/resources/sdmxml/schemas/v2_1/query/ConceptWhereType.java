
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CodelistReferenceType;


/**
 * ConceptWhereType defines a set of parameters for matching a category. All supplied parameters must be matched in order for an object to satisfy the query. In addition to the base parameters for an item, a concept can be queried based on the item scheme that is used as a core representation.
 * 
 * <p>Java-Klasse f�r ConceptWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConceptWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ConceptWhereBaseType">
 *       &lt;sequence>
 *         &lt;element name="Enumeration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConceptWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "enumeration"
})
public class ConceptWhereType
    extends ConceptWhereBaseType
{

    @XmlElement(name = "Enumeration", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected CodelistReferenceType enumeration;

    /**
     * Ruft den Wert der enumeration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodelistReferenceType }
     *     
     */
    public CodelistReferenceType getEnumeration() {
        return enumeration;
    }

    /**
     * Legt den Wert der enumeration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodelistReferenceType }
     *     
     */
    public void setEnumeration(CodelistReferenceType value) {
        this.enumeration = value;
    }

}
