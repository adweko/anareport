
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r MaintainableTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="MaintainableTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType">
 *     &lt;enumeration value="Any"/>
 *     &lt;enumeration value="AgencyScheme"/>
 *     &lt;enumeration value="AttachmentConstraint"/>
 *     &lt;enumeration value="Categorisation"/>
 *     &lt;enumeration value="CategoryScheme"/>
 *     &lt;enumeration value="Codelist"/>
 *     &lt;enumeration value="ConceptScheme"/>
 *     &lt;enumeration value="Constraint"/>
 *     &lt;enumeration value="ContentConstraint"/>
 *     &lt;enumeration value="Dataflow"/>
 *     &lt;enumeration value="DataConsumerScheme"/>
 *     &lt;enumeration value="DataProviderScheme"/>
 *     &lt;enumeration value="DataStructure"/>
 *     &lt;enumeration value="HierarchicalCodelist"/>
 *     &lt;enumeration value="Metadataflow"/>
 *     &lt;enumeration value="MetadataStructure"/>
 *     &lt;enumeration value="OrganisationScheme"/>
 *     &lt;enumeration value="OrganisationUnitScheme"/>
 *     &lt;enumeration value="Process"/>
 *     &lt;enumeration value="ProvisionAgreement"/>
 *     &lt;enumeration value="ReportingTaxonomy"/>
 *     &lt;enumeration value="StructureSet"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MaintainableTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ObjectTypeCodelistType.class)
public enum MaintainableTypeCodelistType {

    @XmlEnumValue("Any")
    ANY(ObjectTypeCodelistType.ANY),
    @XmlEnumValue("AgencyScheme")
    AGENCY_SCHEME(ObjectTypeCodelistType.AGENCY_SCHEME),
    @XmlEnumValue("AttachmentConstraint")
    ATTACHMENT_CONSTRAINT(ObjectTypeCodelistType.ATTACHMENT_CONSTRAINT),
    @XmlEnumValue("Categorisation")
    CATEGORISATION(ObjectTypeCodelistType.CATEGORISATION),
    @XmlEnumValue("CategoryScheme")
    CATEGORY_SCHEME(ObjectTypeCodelistType.CATEGORY_SCHEME),
    @XmlEnumValue("Codelist")
    CODELIST(ObjectTypeCodelistType.CODELIST),
    @XmlEnumValue("ConceptScheme")
    CONCEPT_SCHEME(ObjectTypeCodelistType.CONCEPT_SCHEME),
    @XmlEnumValue("Constraint")
    CONSTRAINT(ObjectTypeCodelistType.CONSTRAINT),
    @XmlEnumValue("ContentConstraint")
    CONTENT_CONSTRAINT(ObjectTypeCodelistType.CONTENT_CONSTRAINT),
    @XmlEnumValue("Dataflow")
    DATAFLOW(ObjectTypeCodelistType.DATAFLOW),
    @XmlEnumValue("DataConsumerScheme")
    DATA_CONSUMER_SCHEME(ObjectTypeCodelistType.DATA_CONSUMER_SCHEME),
    @XmlEnumValue("DataProviderScheme")
    DATA_PROVIDER_SCHEME(ObjectTypeCodelistType.DATA_PROVIDER_SCHEME),
    @XmlEnumValue("DataStructure")
    DATA_STRUCTURE(ObjectTypeCodelistType.DATA_STRUCTURE),
    @XmlEnumValue("HierarchicalCodelist")
    HIERARCHICAL_CODELIST(ObjectTypeCodelistType.HIERARCHICAL_CODELIST),
    @XmlEnumValue("Metadataflow")
    METADATAFLOW(ObjectTypeCodelistType.METADATAFLOW),
    @XmlEnumValue("MetadataStructure")
    METADATA_STRUCTURE(ObjectTypeCodelistType.METADATA_STRUCTURE),
    @XmlEnumValue("OrganisationScheme")
    ORGANISATION_SCHEME(ObjectTypeCodelistType.ORGANISATION_SCHEME),
    @XmlEnumValue("OrganisationUnitScheme")
    ORGANISATION_UNIT_SCHEME(ObjectTypeCodelistType.ORGANISATION_UNIT_SCHEME),
    @XmlEnumValue("Process")
    PROCESS(ObjectTypeCodelistType.PROCESS),
    @XmlEnumValue("ProvisionAgreement")
    PROVISION_AGREEMENT(ObjectTypeCodelistType.PROVISION_AGREEMENT),
    @XmlEnumValue("ReportingTaxonomy")
    REPORTING_TAXONOMY(ObjectTypeCodelistType.REPORTING_TAXONOMY),
    @XmlEnumValue("StructureSet")
    STRUCTURE_SET(ObjectTypeCodelistType.STRUCTURE_SET);
    private final ObjectTypeCodelistType value;

    MaintainableTypeCodelistType(ObjectTypeCodelistType v) {
        value = v;
    }

    public ObjectTypeCodelistType value() {
        return value;
    }

    public static MaintainableTypeCodelistType fromValue(ObjectTypeCodelistType v) {
        for (MaintainableTypeCodelistType c: MaintainableTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
