
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ConceptReferenceType;


/**
 * ConceptValueType describes the structure of a query for the value of the underlying concept of a component. It provides a reference to a concept in a concept scheme via a URN and/or a complete set of reference fields, as well as a numeric, text, or un-typed value.
 * 
 * <p>Java-Klasse f�r ConceptValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConceptValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Concept" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType"/>
 *         &lt;choice>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}NumericValue" maxOccurs="2"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TextValue" maxOccurs="unbounded"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeValue" maxOccurs="2"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}Value"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConceptValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "concept",
    "numericValue",
    "textValue",
    "timeValue",
    "value"
})
public class ConceptValueType {

    @XmlElement(name = "Concept", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected ConceptReferenceType concept;
    @XmlElement(name = "NumericValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<NumericValueType> numericValue;
    @XmlElement(name = "TextValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<QueryTextType> textValue;
    @XmlElement(name = "TimeValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<TimePeriodValueType> timeValue;
    @XmlElement(name = "Value", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected SimpleValueType value;

    /**
     * Ruft den Wert der concept-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConceptReferenceType }
     *     
     */
    public ConceptReferenceType getConcept() {
        return concept;
    }

    /**
     * Legt den Wert der concept-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptReferenceType }
     *     
     */
    public void setConcept(ConceptReferenceType value) {
        this.concept = value;
    }

    /**
     * Gets the value of the numericValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the numericValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNumericValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NumericValueType }
     * 
     * 
     */
    public List<NumericValueType> getNumericValue() {
        if (numericValue == null) {
            numericValue = new ArrayList<NumericValueType>();
        }
        return this.numericValue;
    }

    /**
     * Gets the value of the textValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the textValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryTextType }
     * 
     * 
     */
    public List<QueryTextType> getTextValue() {
        if (textValue == null) {
            textValue = new ArrayList<QueryTextType>();
        }
        return this.textValue;
    }

    /**
     * Gets the value of the timeValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timeValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimeValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TimePeriodValueType }
     * 
     * 
     */
    public List<TimePeriodValueType> getTimeValue() {
        if (timeValue == null) {
            timeValue = new ArrayList<TimePeriodValueType>();
        }
        return this.timeValue;
    }

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SimpleValueType }
     *     
     */
    public SimpleValueType getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleValueType }
     *     
     */
    public void setValue(SimpleValueType value) {
        this.value = value;
    }

}
