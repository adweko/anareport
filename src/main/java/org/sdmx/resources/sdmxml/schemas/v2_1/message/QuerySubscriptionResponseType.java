
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * QuerySubscriptionResponseType defines the structure of a registry query subscription response document.
 * 
 * <p>Java-Klasse f�r QuerySubscriptionResponseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="QuerySubscriptionResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}RegistryInterfaceType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}BasicHeaderType"/>
 *         &lt;element name="QuerySubscriptionResponse" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}QuerySubscriptionResponseType"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer}Footer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubscriptionResponseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
public class QuerySubscriptionResponseType
    extends RegistryInterfaceType
{


}
