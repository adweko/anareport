
package org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import de.bundesbank.statistik.anacredit.ack.v1.BBKANCRDTACKHDRC;
import de.bundesbank.statistik.anacredit.ack.v1.BBKANCRDTACKMSSGIDC;
import de.bundesbank.statistik.anacredit.ack.v1.BBKANCRDTVLDACKC;
import de.bundesbank.statistik.anacredit.ack.v1.BBKANCRDTVLDACKXMLC;
import de.bundesbank.statistik.anacredit.rmndr.v1.BBKANCRDTRMNDHDRC;
import de.bundesbank.statistik.anacredit.rmndr.v1.BBKANCRDTRMNDRC;
import de.bundesbank.statistik.anacredit.t1m.v2.BBKANCRDTENTTYINSTRMNTC;
import de.bundesbank.statistik.anacredit.t1m.v2.BBKANCRDTFNNCLC;
import de.bundesbank.statistik.anacredit.t1m.v2.BBKANCRDTINSTRMNTC;
import de.bundesbank.statistik.anacredit.t1m.v2.BBKANCRDTJNTLBLTSC;
import de.bundesbank.statistik.anacredit.t2m.v2.BBKANCRDTENTTYDFLTC;
import de.bundesbank.statistik.anacredit.t2m.v2.BBKANCRDTENTTYRSKC;
import de.bundesbank.statistik.anacredit.t2m.v2.BBKANCRDTINSTRMNTPRTCTNRCVDC;
import de.bundesbank.statistik.anacredit.t2m.v2.BBKANCRDTPRTCTNPRVDRC;
import de.bundesbank.statistik.anacredit.t2m.v2.BBKANCRDTPRTCTNRCVDC;
import de.bundesbank.statistik.anacredit.t2q.v2.BBKANCRDTACCNTNGC;
import de.bundesbank.statistik.riad.v2.BBKANCRDTENTTYCHNGECDC;
import de.bundesbank.statistik.riad.v2.BBKANCRDTENTTYRFRNCC;
import de.bundesbank.statistik.riad.v2.BBKRIADHDRC;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ActionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;


/**
 * 
 *    		
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:xs="http://www.w3.org/2001/XMLSchema"&gt;DataSetType is the abstract type which defines the base structure for any data structure definition specific data set. A derived data set type will be created that is specific to a data structure definition and the details of the organisation of the data (i.e. which dimension is the observation dimension and whether explicit measures should be used). Data is organised into either a collection of series (grouped observations) or a collection of un-grouped observations. The derived data set type will restrict this choice to be either grouped or un-grouped observations. If this dimension is "AllDimensions" then the derived data set type must consist of a collection of un-grouped observations; otherwise the data set will contain a collection of series with the observations in the series disambiguated by the specified dimension at the observation level. This data set is capable of containing data (observed values) and/or documentation (attribute values) and can be used for incremental updates and deletions (i.e. only the relevant updates or deletes are exchanged). It is assumed that each series or un-grouped observation will be distinct in its purpose. For example, if series contains both data and documentation, it assumed that each series will have a unique key. If the series contains only data or only documentation, then it is possible that another series with the same key might exist, but with not with the same purpose (i.e. to provide data or documentation) as the first series.&lt;/p&gt;
 * </pre>
 * 
 *    		
 *    		
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:xs="http://www.w3.org/2001/XMLSchema"&gt;This base type is designed such that derived types can be processed in a generic manner; it assures that data structure definition specific data will have a consistent structure. The group, series, and observation elements are unqualified, meaning that they are not qualified with a namespace in an instance. This means that in the derived data set types, the elements will always be the same, regardless of the target namespace of the schemas which defines these derived types. This allows for consistent processing of the structure without regard to what the namespace might be for the data structure definition specific schema.&lt;/p&gt;
 * </pre>
 * 
 * 			
 * 			
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:xs="http://www.w3.org/2001/XMLSchema"&gt;The data set can contain values for attributes which do not have an attribute relationship with any data structure definition components. These attribute values will exist in XML attributes in this element based on this type (DataSet). This is specified in the content model with the declaration of anyAttributes in the "local" namespace. The derived data set type will refine this structure so that the attributes are explicit. The XML attributes will be given a name based on the attribute's identifier. These XML attributes will be unqualified (meaning they do not have a namespace associated with them). To allow for generic processing, it is required that the only unqualified XML attributes in the derived data set type (outside of the standard data set attributes) be for attributes declared in the data structure definition. If additional attributes are required, these should be qualified with a namespace so that a generic application can easily distinguish them as not being meant to represent a data structure definition attribute.&lt;/p&gt;
 * </pre>
 * 
 *    		
 * 
 * <p>Java-Klasse f�r DataSetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataSetType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0" form="unqualified"/>
 *         &lt;element name="Group" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}GroupType" maxOccurs="unbounded" minOccurs="0" form="unqualified"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="Series" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}SeriesType" maxOccurs="unbounded" form="unqualified"/>
 *           &lt;element name="Obs" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType" maxOccurs="unbounded" form="unqualified"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}SetAttributeGroup"/>
 *       &lt;attribute name="REPORTING_YEAR_START_DAY" type="{http://www.w3.org/2001/XMLSchema}gMonthDay" />
 *       &lt;anyAttribute namespace=''/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific", propOrder = {
    "dataProvider",
    "group",
    "series",
    "obs"
})
@XmlSeeAlso({
    BBKANCRDTACKHDRC.class,
    BBKANCRDTVLDACKC.class,
    BBKANCRDTVLDACKXMLC.class,
    BBKANCRDTACKMSSGIDC.class,
    TimeSeriesDataSetType.class,
    BBKANCRDTRMNDHDRC.class,
    BBKANCRDTRMNDRC.class,
    BBKANCRDTINSTRMNTC.class,
    BBKANCRDTENTTYINSTRMNTC.class,
    de.bundesbank.statistik.anacredit.t1m.v2.BBKANCRDTHDRC.class,
    BBKANCRDTJNTLBLTSC.class,
    BBKANCRDTFNNCLC.class,
    de.bundesbank.statistik.anacredit.t2m.v2.BBKANCRDTHDRC.class,
    BBKANCRDTPRTCTNPRVDRC.class,
    BBKANCRDTENTTYDFLTC.class,
    BBKANCRDTENTTYRSKC.class,
    BBKANCRDTINSTRMNTPRTCTNRCVDC.class,
    BBKANCRDTPRTCTNRCVDC.class,
    de.bundesbank.statistik.anacredit.t2q.v2.BBKANCRDTHDRC.class,
    BBKANCRDTACCNTNGC.class,
    BBKANCRDTENTTYCHNGECDC.class,
    BBKANCRDTENTTYRFRNCC.class,
    BBKRIADHDRC.class
})
public class DataSetType
    extends AnnotableType
{

    @XmlElement(name = "DataProvider")
    protected DataProviderReferenceType dataProvider;
    @XmlElement(name = "Group")
    protected List<GroupType> group;
    @XmlElement(name = "Series")
    protected List<SeriesType> series;
    @XmlElement(name = "Obs")
    protected List<ObsType> obs;
    @XmlAttribute(name = "REPORTING_YEAR_START_DAY")
    @XmlSchemaType(name = "gMonthDay")
    protected XMLGregorianCalendar reportingyearstartday;
    @XmlAttribute(name = "structureRef", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific", required = true)
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object structureRef;
    @XmlAttribute(name = "setID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
    protected String setID;
    @XmlAttribute(name = "action", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
    protected ActionType action;
    @XmlAttribute(name = "reportingBeginDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
    protected List<String> reportingBeginDate;
    @XmlAttribute(name = "reportingEndDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
    protected List<String> reportingEndDate;
    @XmlAttribute(name = "validFromDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validFromDate;
    @XmlAttribute(name = "validToDate", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validToDate;
    @XmlAttribute(name = "publicationYear", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
    @XmlSchemaType(name = "gYear")
    protected XMLGregorianCalendar publicationYear;
    @XmlAttribute(name = "publicationPeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
    protected List<String> publicationPeriod;
    @XmlAttribute(name = "dataScope", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific", required = true)
    protected DataScopeType dataScope;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Ruft den Wert der dataProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public DataProviderReferenceType getDataProvider() {
        return dataProvider;
    }

    /**
     * Legt den Wert der dataProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public void setDataProvider(DataProviderReferenceType value) {
        this.dataProvider = value;
    }

    /**
     * Gets the value of the group property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the group property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupType }
     * 
     * 
     */
    public List<GroupType> getGroup() {
        if (group == null) {
            group = new ArrayList<GroupType>();
        }
        return this.group;
    }

    /**
     * Gets the value of the series property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the series property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SeriesType }
     * 
     * 
     */
    public List<SeriesType> getSeries() {
        if (series == null) {
            series = new ArrayList<SeriesType>();
        }
        return this.series;
    }

    /**
     * Gets the value of the obs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the obs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObsType }
     * 
     * 
     */
    public List<ObsType> getObs() {
        if (obs == null) {
            obs = new ArrayList<ObsType>();
        }
        return this.obs;
    }

    /**
     * Ruft den Wert der reportingyearstartday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getREPORTINGYEARSTARTDAY() {
        return reportingyearstartday;
    }

    /**
     * Legt den Wert der reportingyearstartday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setREPORTINGYEARSTARTDAY(XMLGregorianCalendar value) {
        this.reportingyearstartday = value;
    }

    /**
     * Ruft den Wert der structureRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getStructureRef() {
        return structureRef;
    }

    /**
     * Legt den Wert der structureRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setStructureRef(Object value) {
        this.structureRef = value;
    }

    /**
     * Ruft den Wert der setID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetID() {
        return setID;
    }

    /**
     * Legt den Wert der setID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetID(String value) {
        this.setID = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setAction(ActionType value) {
        this.action = value;
    }

    /**
     * Gets the value of the reportingBeginDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingBeginDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingBeginDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportingBeginDate() {
        if (reportingBeginDate == null) {
            reportingBeginDate = new ArrayList<String>();
        }
        return this.reportingBeginDate;
    }

    /**
     * Gets the value of the reportingEndDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingEndDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingEndDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReportingEndDate() {
        if (reportingEndDate == null) {
            reportingEndDate = new ArrayList<String>();
        }
        return this.reportingEndDate;
    }

    /**
     * Ruft den Wert der validFromDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidFromDate() {
        return validFromDate;
    }

    /**
     * Legt den Wert der validFromDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFromDate(XMLGregorianCalendar value) {
        this.validFromDate = value;
    }

    /**
     * Ruft den Wert der validToDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidToDate() {
        return validToDate;
    }

    /**
     * Legt den Wert der validToDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidToDate(XMLGregorianCalendar value) {
        this.validToDate = value;
    }

    /**
     * Ruft den Wert der publicationYear-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPublicationYear() {
        return publicationYear;
    }

    /**
     * Legt den Wert der publicationYear-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPublicationYear(XMLGregorianCalendar value) {
        this.publicationYear = value;
    }

    /**
     * Gets the value of the publicationPeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the publicationPeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPublicationPeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPublicationPeriod() {
        if (publicationPeriod == null) {
            publicationPeriod = new ArrayList<String>();
        }
        return this.publicationPeriod;
    }

    /**
     * Ruft den Wert der dataScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataScopeType }
     *     
     */
    public DataScopeType getDataScope() {
        return dataScope;
    }

    /**
     * Legt den Wert der dataScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataScopeType }
     *     
     */
    public void setDataScope(DataScopeType value) {
        this.dataScope = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
