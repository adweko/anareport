
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.OrganisationReferenceType;


/**
 * SubscriptionType describes the details of a subscription to a registration or change event for registry resources. When it occurs as the content of a response message, the registry URN must be provide, unless the response is a failure notification for the creation of a new subscription.
 * 
 * <p>Java-Klasse f�r SubscriptionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubscriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Organisation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationReferenceType"/>
 *         &lt;element name="RegistryURN" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *         &lt;element name="NotificationMailTo" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}NotificationURLType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="NotificationHTTP" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}NotificationURLType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SubscriberAssignedID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" minOccurs="0"/>
 *         &lt;element name="ValidityPeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}ValidityPeriodType"/>
 *         &lt;element name="EventSelector" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}EventSelectorType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriptionType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "organisation",
    "registryURN",
    "notificationMailTo",
    "notificationHTTP",
    "subscriberAssignedID",
    "validityPeriod",
    "eventSelector"
})
public class SubscriptionType {

    @XmlElement(name = "Organisation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected OrganisationReferenceType organisation;
    @XmlElement(name = "RegistryURN", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    @XmlSchemaType(name = "anyURI")
    protected String registryURN;
    @XmlElement(name = "NotificationMailTo", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected List<NotificationURLType> notificationMailTo;
    @XmlElement(name = "NotificationHTTP", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected List<NotificationURLType> notificationHTTP;
    @XmlElement(name = "SubscriberAssignedID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected String subscriberAssignedID;
    @XmlElement(name = "ValidityPeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected ValidityPeriodType validityPeriod;
    @XmlElement(name = "EventSelector", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected EventSelectorType eventSelector;

    /**
     * Ruft den Wert der organisation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationReferenceType }
     *     
     */
    public OrganisationReferenceType getOrganisation() {
        return organisation;
    }

    /**
     * Legt den Wert der organisation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationReferenceType }
     *     
     */
    public void setOrganisation(OrganisationReferenceType value) {
        this.organisation = value;
    }

    /**
     * Ruft den Wert der registryURN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistryURN() {
        return registryURN;
    }

    /**
     * Legt den Wert der registryURN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistryURN(String value) {
        this.registryURN = value;
    }

    /**
     * Gets the value of the notificationMailTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notificationMailTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotificationMailTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NotificationURLType }
     * 
     * 
     */
    public List<NotificationURLType> getNotificationMailTo() {
        if (notificationMailTo == null) {
            notificationMailTo = new ArrayList<NotificationURLType>();
        }
        return this.notificationMailTo;
    }

    /**
     * Gets the value of the notificationHTTP property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notificationHTTP property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotificationHTTP().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NotificationURLType }
     * 
     * 
     */
    public List<NotificationURLType> getNotificationHTTP() {
        if (notificationHTTP == null) {
            notificationHTTP = new ArrayList<NotificationURLType>();
        }
        return this.notificationHTTP;
    }

    /**
     * Ruft den Wert der subscriberAssignedID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberAssignedID() {
        return subscriberAssignedID;
    }

    /**
     * Legt den Wert der subscriberAssignedID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberAssignedID(String value) {
        this.subscriberAssignedID = value;
    }

    /**
     * Ruft den Wert der validityPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValidityPeriodType }
     *     
     */
    public ValidityPeriodType getValidityPeriod() {
        return validityPeriod;
    }

    /**
     * Legt den Wert der validityPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidityPeriodType }
     *     
     */
    public void setValidityPeriod(ValidityPeriodType value) {
        this.validityPeriod = value;
    }

    /**
     * Ruft den Wert der eventSelector-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EventSelectorType }
     *     
     */
    public EventSelectorType getEventSelector() {
        return eventSelector;
    }

    /**
     * Legt den Wert der eventSelector-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EventSelectorType }
     *     
     */
    public void setEventSelector(EventSelectorType value) {
        this.eventSelector = value;
    }

}
