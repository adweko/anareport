
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * HierarchicalCodelistType describes the structure of a hierarchical codelist. A hierarchical code list is defined as an organised collection of codes that may participate in many parent/child relationships with other codes in the list, as defined by one or more hierarchy of the list.
 * 
 * <p>Java-Klasse f�r HierarchicalCodelistType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HierarchicalCodelistType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HierarchicalCodelistBaseType">
 *       &lt;sequence>
 *         &lt;element name="IncludedCodelist" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}IncludedCodelistReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Hierarchy" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HierarchyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HierarchicalCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "includedCodelist",
    "hierarchy"
})
public class HierarchicalCodelistType
    extends HierarchicalCodelistBaseType
{

    @XmlElement(name = "IncludedCodelist", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<IncludedCodelistReferenceType> includedCodelist;
    @XmlElement(name = "Hierarchy", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<HierarchyType> hierarchy;

    /**
     * Gets the value of the includedCodelist property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedCodelist property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludedCodelist().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IncludedCodelistReferenceType }
     * 
     * 
     */
    public List<IncludedCodelistReferenceType> getIncludedCodelist() {
        if (includedCodelist == null) {
            includedCodelist = new ArrayList<IncludedCodelistReferenceType>();
        }
        return this.includedCodelist;
    }

    /**
     * Gets the value of the hierarchy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hierarchy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHierarchy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HierarchyType }
     * 
     * 
     */
    public List<HierarchyType> getHierarchy() {
        if (hierarchy == null) {
            hierarchy = new ArrayList<HierarchyType>();
        }
        return this.hierarchy;
    }

}
