
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ObsDimensionsCodeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ObsDimensionsCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AllDimensions"/>
 *     &lt;enumeration value="TIME_PERIOD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ObsDimensionsCodeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum ObsDimensionsCodeType {


    /**
     * AllDimensions notes that the cross sectional format shall be flat; that is all dimensions should be contained at the observation level.
     * 
     */
    @XmlEnumValue("AllDimensions")
    ALL_DIMENSIONS("AllDimensions"),

    /**
     * TIME_PERIOD refers to the fixed identifier for the time dimension.
     * 
     */
    TIME_PERIOD("TIME_PERIOD");
    private final String value;

    ObsDimensionsCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ObsDimensionsCodeType fromValue(String v) {
        for (ObsDimensionsCodeType c: ObsDimensionsCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
