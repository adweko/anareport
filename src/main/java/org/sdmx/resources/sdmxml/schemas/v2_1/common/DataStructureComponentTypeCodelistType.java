
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r DataStructureComponentTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="DataStructureComponentTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentTypeCodelistType">
 *     &lt;enumeration value="Attribute"/>
 *     &lt;enumeration value="Dimension"/>
 *     &lt;enumeration value="MeasureDimension"/>
 *     &lt;enumeration value="PrimaryMeasure"/>
 *     &lt;enumeration value="ReportingYearStartDay"/>
 *     &lt;enumeration value="TimeDimension"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DataStructureComponentTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ComponentTypeCodelistType.class)
public enum DataStructureComponentTypeCodelistType {

    @XmlEnumValue("Attribute")
    ATTRIBUTE(ComponentTypeCodelistType.ATTRIBUTE),
    @XmlEnumValue("Dimension")
    DIMENSION(ComponentTypeCodelistType.DIMENSION),
    @XmlEnumValue("MeasureDimension")
    MEASURE_DIMENSION(ComponentTypeCodelistType.MEASURE_DIMENSION),
    @XmlEnumValue("PrimaryMeasure")
    PRIMARY_MEASURE(ComponentTypeCodelistType.PRIMARY_MEASURE),
    @XmlEnumValue("ReportingYearStartDay")
    REPORTING_YEAR_START_DAY(ComponentTypeCodelistType.REPORTING_YEAR_START_DAY),
    @XmlEnumValue("TimeDimension")
    TIME_DIMENSION(ComponentTypeCodelistType.TIME_DIMENSION);
    private final ComponentTypeCodelistType value;

    DataStructureComponentTypeCodelistType(ComponentTypeCodelistType v) {
        value = v;
    }

    public ComponentTypeCodelistType value() {
        return value;
    }

    public static DataStructureComponentTypeCodelistType fromValue(ComponentTypeCodelistType v) {
        for (DataStructureComponentTypeCodelistType c: DataStructureComponentTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
