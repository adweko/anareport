
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * OrganisationSchemesType describes the structure of the organisation schemes container. It contains one or more organisation scheme, which can be explicitly detailed or referenced from an external structure document or registry service.
 * 
 * <p>Java-Klasse f�r OrganisationSchemesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrganisationSchemesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="AgencyScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}AgencySchemeType"/>
 *         &lt;element name="DataConsumerScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataConsumerSchemeType"/>
 *         &lt;element name="DataProviderScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataProviderSchemeType"/>
 *         &lt;element name="OrganisationUnitScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}OrganisationUnitSchemeType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationSchemesType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "agencySchemeOrDataConsumerSchemeOrDataProviderScheme"
})
public class OrganisationSchemesType {

    @XmlElements({
        @XmlElement(name = "AgencyScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = AgencySchemeType.class),
        @XmlElement(name = "DataConsumerScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = DataConsumerSchemeType.class),
        @XmlElement(name = "DataProviderScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = DataProviderSchemeType.class),
        @XmlElement(name = "OrganisationUnitScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = OrganisationUnitSchemeType.class)
    })
    protected List<OrganisationSchemeType> agencySchemeOrDataConsumerSchemeOrDataProviderScheme;

    /**
     * Gets the value of the agencySchemeOrDataConsumerSchemeOrDataProviderScheme property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agencySchemeOrDataConsumerSchemeOrDataProviderScheme property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgencySchemeOrDataConsumerSchemeOrDataProviderScheme().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgencySchemeType }
     * {@link DataConsumerSchemeType }
     * {@link DataProviderSchemeType }
     * {@link OrganisationUnitSchemeType }
     * 
     * 
     */
    public List<OrganisationSchemeType> getAgencySchemeOrDataConsumerSchemeOrDataProviderScheme() {
        if (agencySchemeOrDataConsumerSchemeOrDataProviderScheme == null) {
            agencySchemeOrDataConsumerSchemeOrDataProviderScheme = new ArrayList<OrganisationSchemeType>();
        }
        return this.agencySchemeOrDataConsumerSchemeOrDataProviderScheme;
    }

}
