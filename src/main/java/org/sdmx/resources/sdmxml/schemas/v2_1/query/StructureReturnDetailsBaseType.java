
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * StructureReturnDetailsBaseType is an abstract base type which forms the basis of StructureReturnDetailsType.
 * 
 * <p>Java-Klasse f�r StructureReturnDetailsBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureReturnDetailsBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReturnDetailsBaseType">
 *       &lt;attribute name="detail" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructureReturnDetailType" default="Full" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureReturnDetailsBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlSeeAlso({
    StructureReturnDetailsType.class
})
public abstract class StructureReturnDetailsBaseType
    extends ReturnDetailsBaseType
{


}
