
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * GroupDimensionBaseType is an abstract base type which refines the base ComponentType in order to form the basis for the GroupDimensionType.
 * 
 * <p>Java-Klasse f�r GroupDimensionBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GroupDimensionBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ComponentType">
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDimensionBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlSeeAlso({
    GroupDimensionType.class
})
public abstract class GroupDimensionBaseType
    extends ComponentType
{


}
