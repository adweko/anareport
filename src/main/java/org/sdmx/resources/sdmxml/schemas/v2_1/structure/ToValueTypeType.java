
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ToValueTypeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ToValueTypeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Value"/>
 *     &lt;enumeration value="Name"/>
 *     &lt;enumeration value="Description"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ToValueTypeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlEnum
public enum ToValueTypeType {


    /**
     * Code or other tokenized value, as provided in the representation scheme.
     * 
     */
    @XmlEnumValue("Value")
    VALUE("Value"),

    /**
     * The human-readable name of the Value, as provided in the representation scheme.
     * 
     */
    @XmlEnumValue("Name")
    NAME("Name"),

    /**
     * The human-readable description of the Value, as provided in the representation scheme.
     * 
     */
    @XmlEnumValue("Description")
    DESCRIPTION("Description");
    private final String value;

    ToValueTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ToValueTypeType fromValue(String v) {
        for (ToValueTypeType c: ToValueTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
