
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DataSetTextFormatType is a restricted version of the NonFacetedTextFormatType that specifies a fixed DataSetReference representation.
 * 
 * <p>Java-Klasse f�r DataSetTextFormatType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataSetTextFormatType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}TargetObjectTextFormatType">
 *       &lt;attribute name="textType" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}TargetObjectDataType" fixed="DataSetReference" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSetTextFormatType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class DataSetTextFormatType
    extends TargetObjectTextFormatType
{


}
