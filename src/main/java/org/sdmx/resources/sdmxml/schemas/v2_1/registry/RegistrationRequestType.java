
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ActionType;


/**
 * RegistrationRequestType describes the structure of a single registration request. It contains the details of a registation and an action field to indicate the action to be taken on the contained registration.
 * 
 * <p>Java-Klasse f�r RegistrationRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RegistrationRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}RegistrationType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="action" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ActionType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationRequestType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "registration"
})
public class RegistrationRequestType {

    @XmlElement(name = "Registration", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected RegistrationType registration;
    @XmlAttribute(name = "action", required = true)
    protected ActionType action;

    /**
     * Ruft den Wert der registration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationType }
     *     
     */
    public RegistrationType getRegistration() {
        return registration;
    }

    /**
     * Legt den Wert der registration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationType }
     *     
     */
    public void setRegistration(RegistrationType value) {
        this.registration = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setAction(ActionType value) {
        this.action = value;
    }

}
