
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureUsageReferenceType;


/**
 * ReportingCategoryType describes the structure of a reporting category, which groups structure usages into useful sub-packages. Sub ordinate reporting categories can be nested within the category definition.
 * 
 * <p>Java-Klasse f�r ReportingCategoryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReportingCategoryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReportingCategoryBaseType">
 *       &lt;choice>
 *         &lt;element name="StructuralMetadata" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProvisioningMetadata" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureUsageReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingCategoryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "structuralMetadata",
    "provisioningMetadata"
})
public class ReportingCategoryType
    extends ReportingCategoryBaseType
{

    @XmlElement(name = "StructuralMetadata", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<StructureReferenceType> structuralMetadata;
    @XmlElement(name = "ProvisioningMetadata", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<StructureUsageReferenceType> provisioningMetadata;

    /**
     * Gets the value of the structuralMetadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the structuralMetadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStructuralMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StructureReferenceType }
     * 
     * 
     */
    public List<StructureReferenceType> getStructuralMetadata() {
        if (structuralMetadata == null) {
            structuralMetadata = new ArrayList<StructureReferenceType>();
        }
        return this.structuralMetadata;
    }

    /**
     * Gets the value of the provisioningMetadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the provisioningMetadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProvisioningMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StructureUsageReferenceType }
     * 
     * 
     */
    public List<StructureUsageReferenceType> getProvisioningMetadata() {
        if (provisioningMetadata == null) {
            provisioningMetadata = new ArrayList<StructureUsageReferenceType>();
        }
        return this.provisioningMetadata;
    }

}
