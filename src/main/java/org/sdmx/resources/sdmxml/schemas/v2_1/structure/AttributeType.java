
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ConceptReferenceType;


/**
 * AttributeType describes the structure of a data attribute, which is defined as a characteristic of an object or entity. The attribute takes its semantic, and in some cases it representation, from its concept identity. An attribute can be coded by referencing a code list from its coded local representation. It can also specify its text format, which is used as the representation of the attribute if a coded representation is not defined. Neither the coded or uncoded representation are necessary, since the attribute may take these from the referenced concept. An attribute specifies its relationship with other data structure components and is given an assignment status. These two properties dictate where in a data message the attribute will be attached, and whether or not the attribute will be required to be given a value. A set of roles defined in concept scheme can be assigned to the attribute.
 * 
 * <p>Java-Klasse f�r AttributeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}AttributeBaseType">
 *       &lt;sequence>
 *         &lt;element name="ConceptRole" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttributeRelationship" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}AttributeRelationshipType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="assignmentStatus" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}UsageStatusType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "conceptRole",
    "attributeRelationship"
})
@XmlSeeAlso({
    ReportingYearStartDayType.class
})
public class AttributeType
    extends AttributeBaseType
{

    @XmlElement(name = "ConceptRole", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<ConceptReferenceType> conceptRole;
    @XmlElement(name = "AttributeRelationship", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected AttributeRelationshipType attributeRelationship;
    @XmlAttribute(name = "assignmentStatus", required = true)
    protected UsageStatusType assignmentStatus;

    /**
     * Gets the value of the conceptRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conceptRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConceptRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConceptReferenceType }
     * 
     * 
     */
    public List<ConceptReferenceType> getConceptRole() {
        if (conceptRole == null) {
            conceptRole = new ArrayList<ConceptReferenceType>();
        }
        return this.conceptRole;
    }

    /**
     * Ruft den Wert der attributeRelationship-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributeRelationshipType }
     *     
     */
    public AttributeRelationshipType getAttributeRelationship() {
        return attributeRelationship;
    }

    /**
     * Legt den Wert der attributeRelationship-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeRelationshipType }
     *     
     */
    public void setAttributeRelationship(AttributeRelationshipType value) {
        this.attributeRelationship = value;
    }

    /**
     * Ruft den Wert der assignmentStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UsageStatusType }
     *     
     */
    public UsageStatusType getAssignmentStatus() {
        return assignmentStatus;
    }

    /**
     * Legt den Wert der assignmentStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UsageStatusType }
     *     
     */
    public void setAssignmentStatus(UsageStatusType value) {
        this.assignmentStatus = value;
    }

}
