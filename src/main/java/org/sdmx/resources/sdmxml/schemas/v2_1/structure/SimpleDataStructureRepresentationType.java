
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * SimpleDataStructureRepresentationType defines the representation for any non-measure and non-time dimension data structure definition component.
 * 
 * <p>Java-Klasse f�r SimpleDataStructureRepresentationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SimpleDataStructureRepresentationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataStructureRepresentationType">
 *       &lt;choice>
 *         &lt;element name="TextFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}SimpleComponentTextFormatType"/>
 *         &lt;sequence>
 *           &lt;element name="Enumeration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistReferenceType"/>
 *           &lt;element name="EnumerationFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodededTextFormatType" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleDataStructureRepresentationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlSeeAlso({
    ReportingYearStartDayRepresentationType.class,
    TimeDimensionRepresentationType.class
})
public class SimpleDataStructureRepresentationType
    extends DataStructureRepresentationType
{


}
