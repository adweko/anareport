
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeSeriesDataQueryType defines the structure of a query for data. This specifically applies to requesting time series only structured data.
 * 
 * <p>Java-Klasse f�r TimeSeriesDataQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeSeriesDataQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataQueryType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeSeriesDataReturnDetailsType"/>
 *         &lt;element name="DataWhere" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersAndType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeSeriesDataQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class TimeSeriesDataQueryType
    extends DataQueryType
{


}
