
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ConceptReferenceType;


/**
 * ComponentType is an abstract base type for all components. It contains information pertaining to a component, including an optional reference to a concept, an optional role played by the concept, an optional text format description, and an optional local representation.
 * 
 * <p>Java-Klasse f�r ComponentType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ComponentBaseType">
 *       &lt;sequence>
 *         &lt;element name="ConceptIdentity" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" minOccurs="0"/>
 *         &lt;element name="LocalRepresentation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}RepresentationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "conceptIdentity",
    "localRepresentation"
})
@XmlSeeAlso({
    PrimaryMeasureType.class,
    AttributeBaseType.class,
    BaseDimensionBaseType.class,
    GroupDimensionBaseType.class,
    MetadataAttributeBaseType.class,
    TargetObject.class
})
public abstract class ComponentType
    extends ComponentBaseType
{

    @XmlElement(name = "ConceptIdentity", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ConceptReferenceType conceptIdentity;
    @XmlElement(name = "LocalRepresentation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected RepresentationType localRepresentation;

    /**
     * Ruft den Wert der conceptIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConceptReferenceType }
     *     
     */
    public ConceptReferenceType getConceptIdentity() {
        return conceptIdentity;
    }

    /**
     * Legt den Wert der conceptIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptReferenceType }
     *     
     */
    public void setConceptIdentity(ConceptReferenceType value) {
        this.conceptIdentity = value;
    }

    /**
     * Ruft den Wert der localRepresentation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RepresentationType }
     *     
     */
    public RepresentationType getLocalRepresentation() {
        return localRepresentation;
    }

    /**
     * Legt den Wert der localRepresentation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RepresentationType }
     *     
     */
    public void setLocalRepresentation(RepresentationType value) {
        this.localRepresentation = value;
    }

}
