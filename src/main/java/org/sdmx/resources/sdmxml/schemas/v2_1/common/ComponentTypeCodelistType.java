
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ComponentTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ComponentTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType">
 *     &lt;enumeration value="Attribute"/>
 *     &lt;enumeration value="ConstraintTarget"/>
 *     &lt;enumeration value="DataSetTarget"/>
 *     &lt;enumeration value="Dimension"/>
 *     &lt;enumeration value="IdentifiableObjectTarget"/>
 *     &lt;enumeration value="DimensionDescriptorValuesTarget"/>
 *     &lt;enumeration value="MeasureDimension"/>
 *     &lt;enumeration value="MetadataAttribute"/>
 *     &lt;enumeration value="PrimaryMeasure"/>
 *     &lt;enumeration value="ReportingYearStartDay"/>
 *     &lt;enumeration value="ReportPeriodTarget"/>
 *     &lt;enumeration value="TimeDimension"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ComponentTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ObjectTypeCodelistType.class)
public enum ComponentTypeCodelistType {

    @XmlEnumValue("Attribute")
    ATTRIBUTE(ObjectTypeCodelistType.ATTRIBUTE),
    @XmlEnumValue("ConstraintTarget")
    CONSTRAINT_TARGET(ObjectTypeCodelistType.CONSTRAINT_TARGET),
    @XmlEnumValue("DataSetTarget")
    DATA_SET_TARGET(ObjectTypeCodelistType.DATA_SET_TARGET),
    @XmlEnumValue("Dimension")
    DIMENSION(ObjectTypeCodelistType.DIMENSION),
    @XmlEnumValue("IdentifiableObjectTarget")
    IDENTIFIABLE_OBJECT_TARGET(ObjectTypeCodelistType.IDENTIFIABLE_OBJECT_TARGET),
    @XmlEnumValue("DimensionDescriptorValuesTarget")
    DIMENSION_DESCRIPTOR_VALUES_TARGET(ObjectTypeCodelistType.DIMENSION_DESCRIPTOR_VALUES_TARGET),
    @XmlEnumValue("MeasureDimension")
    MEASURE_DIMENSION(ObjectTypeCodelistType.MEASURE_DIMENSION),
    @XmlEnumValue("MetadataAttribute")
    METADATA_ATTRIBUTE(ObjectTypeCodelistType.METADATA_ATTRIBUTE),
    @XmlEnumValue("PrimaryMeasure")
    PRIMARY_MEASURE(ObjectTypeCodelistType.PRIMARY_MEASURE),
    @XmlEnumValue("ReportingYearStartDay")
    REPORTING_YEAR_START_DAY(ObjectTypeCodelistType.REPORTING_YEAR_START_DAY),
    @XmlEnumValue("ReportPeriodTarget")
    REPORT_PERIOD_TARGET(ObjectTypeCodelistType.REPORT_PERIOD_TARGET),
    @XmlEnumValue("TimeDimension")
    TIME_DIMENSION(ObjectTypeCodelistType.TIME_DIMENSION);
    private final ObjectTypeCodelistType value;

    ComponentTypeCodelistType(ObjectTypeCodelistType v) {
        value = v;
    }

    public ObjectTypeCodelistType value() {
        return value;
    }

    public static ComponentTypeCodelistType fromValue(ObjectTypeCodelistType v) {
        for (ComponentTypeCodelistType c: ComponentTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
