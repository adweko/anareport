
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.sdmx.resources.sdmxml.schemas.v2_1.message package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StructureSetQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "StructureSetQuery");
    private final static QName _Error_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "Error");
    private final static QName _DataflowQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "DataflowQuery");
    private final static QName _SubmitRegistrationsResponse_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "SubmitRegistrationsResponse");
    private final static QName _QueryRegistrationResponse_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "QueryRegistrationResponse");
    private final static QName _CodelistQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "CodelistQuery");
    private final static QName _SubmitStructureResponse_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "SubmitStructureResponse");
    private final static QName _StructureSpecificTimeSeriesData_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "StructureSpecificTimeSeriesData");
    private final static QName _MetadataStructureQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "MetadataStructureQuery");
    private final static QName _CategorisationQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "CategorisationQuery");
    private final static QName _SubmitSubscriptionsRequest_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "SubmitSubscriptionsRequest");
    private final static QName _ProvisionAgreementQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "ProvisionAgreementQuery");
    private final static QName _QueryRegistrationRequest_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "QueryRegistrationRequest");
    private final static QName _HierarchicalCodelistQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "HierarchicalCodelistQuery");
    private final static QName _ConceptSchemeQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "ConceptSchemeQuery");
    private final static QName _Structure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "Structure");
    private final static QName _DataStructureQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "DataStructureQuery");
    private final static QName _CategorySchemeQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "CategorySchemeQuery");
    private final static QName _GenericMetadata_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "GenericMetadata");
    private final static QName _ReportingTaxonomyQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "ReportingTaxonomyQuery");
    private final static QName _MetadataSchemaQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "MetadataSchemaQuery");
    private final static QName _StructureSpecificTimeSeriesDataQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "StructureSpecificTimeSeriesDataQuery");
    private final static QName _MetadataflowQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "MetadataflowQuery");
    private final static QName _GenericTimeSeriesData_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "GenericTimeSeriesData");
    private final static QName _GenericDataQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "GenericDataQuery");
    private final static QName _ConstraintQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "ConstraintQuery");
    private final static QName _StructuresQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "StructuresQuery");
    private final static QName _StructureSpecificMetadata_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "StructureSpecificMetadata");
    private final static QName _GenericTimeSeriesDataQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "GenericTimeSeriesDataQuery");
    private final static QName _GenericData_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "GenericData");
    private final static QName _OrganisationSchemeQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "OrganisationSchemeQuery");
    private final static QName _StructureSpecificMetadataQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "StructureSpecificMetadataQuery");
    private final static QName _RegistryInterface_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "RegistryInterface");
    private final static QName _SubmitRegistrationsRequest_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "SubmitRegistrationsRequest");
    private final static QName _GenericMetadataQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "GenericMetadataQuery");
    private final static QName _ProcessQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "ProcessQuery");
    private final static QName _SubmitStructureRequest_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "SubmitStructureRequest");
    private final static QName _NotifyRegistryEvent_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "NotifyRegistryEvent");
    private final static QName _QuerySubscriptionRequest_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "QuerySubscriptionRequest");
    private final static QName _QuerySubscriptionResponse_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "QuerySubscriptionResponse");
    private final static QName _DataSchemaQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "DataSchemaQuery");
    private final static QName _StructureSpecificDataQuery_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "StructureSpecificDataQuery");
    private final static QName _StructureSpecificData_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "StructureSpecificData");
    private final static QName _SubmitSubscriptionsResponse_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "SubmitSubscriptionsResponse");
    private final static QName _ContactTypeTelephone_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "Telephone");
    private final static QName _ContactTypeX400_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "X400");
    private final static QName _ContactTypeEmail_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "Email");
    private final static QName _ContactTypeURI_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "URI");
    private final static QName _ContactTypeFax_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", "Fax");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.sdmx.resources.sdmxml.schemas.v2_1.message
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NotifyRegistryEventType }
     * 
     */
    public NotifyRegistryEventType createNotifyRegistryEventType() {
        return new NotifyRegistryEventType();
    }

    /**
     * Create an instance of {@link ProvisionAgreementQueryType }
     * 
     */
    public ProvisionAgreementQueryType createProvisionAgreementQueryType() {
        return new ProvisionAgreementQueryType();
    }

    /**
     * Create an instance of {@link SubmitRegistrationsRequestType }
     * 
     */
    public SubmitRegistrationsRequestType createSubmitRegistrationsRequestType() {
        return new SubmitRegistrationsRequestType();
    }

    /**
     * Create an instance of {@link DataQueryType }
     * 
     */
    public DataQueryType createDataQueryType() {
        return new DataQueryType();
    }

    /**
     * Create an instance of {@link StructureSpecificDataType }
     * 
     */
    public StructureSpecificDataType createStructureSpecificDataType() {
        return new StructureSpecificDataType();
    }

    /**
     * Create an instance of {@link CategorySchemeQueryType }
     * 
     */
    public CategorySchemeQueryType createCategorySchemeQueryType() {
        return new CategorySchemeQueryType();
    }

    /**
     * Create an instance of {@link StructureSpecificMetadataType }
     * 
     */
    public StructureSpecificMetadataType createStructureSpecificMetadataType() {
        return new StructureSpecificMetadataType();
    }

    /**
     * Create an instance of {@link SubmitSubscriptionsResponseType }
     * 
     */
    public SubmitSubscriptionsResponseType createSubmitSubscriptionsResponseType() {
        return new SubmitSubscriptionsResponseType();
    }

    /**
     * Create an instance of {@link QueryRegistrationRequestType }
     * 
     */
    public QueryRegistrationRequestType createQueryRegistrationRequestType() {
        return new QueryRegistrationRequestType();
    }

    /**
     * Create an instance of {@link GenericDataQueryType }
     * 
     */
    public GenericDataQueryType createGenericDataQueryType() {
        return new GenericDataQueryType();
    }

    /**
     * Create an instance of {@link ConstraintQueryType }
     * 
     */
    public ConstraintQueryType createConstraintQueryType() {
        return new ConstraintQueryType();
    }

    /**
     * Create an instance of {@link GenericTimeSeriesDataType }
     * 
     */
    public GenericTimeSeriesDataType createGenericTimeSeriesDataType() {
        return new GenericTimeSeriesDataType();
    }

    /**
     * Create an instance of {@link StructureSpecificTimeSeriesDataType }
     * 
     */
    public StructureSpecificTimeSeriesDataType createStructureSpecificTimeSeriesDataType() {
        return new StructureSpecificTimeSeriesDataType();
    }

    /**
     * Create an instance of {@link DataflowQueryType }
     * 
     */
    public DataflowQueryType createDataflowQueryType() {
        return new DataflowQueryType();
    }

    /**
     * Create an instance of {@link MetadataStructureQueryType }
     * 
     */
    public MetadataStructureQueryType createMetadataStructureQueryType() {
        return new MetadataStructureQueryType();
    }

    /**
     * Create an instance of {@link CategorisationQueryType }
     * 
     */
    public CategorisationQueryType createCategorisationQueryType() {
        return new CategorisationQueryType();
    }

    /**
     * Create an instance of {@link DataSchemaQueryType }
     * 
     */
    public DataSchemaQueryType createDataSchemaQueryType() {
        return new DataSchemaQueryType();
    }

    /**
     * Create an instance of {@link ConceptSchemeQueryType }
     * 
     */
    public ConceptSchemeQueryType createConceptSchemeQueryType() {
        return new ConceptSchemeQueryType();
    }

    /**
     * Create an instance of {@link OrganisationSchemeQueryType }
     * 
     */
    public OrganisationSchemeQueryType createOrganisationSchemeQueryType() {
        return new OrganisationSchemeQueryType();
    }

    /**
     * Create an instance of {@link StructuresQueryType }
     * 
     */
    public StructuresQueryType createStructuresQueryType() {
        return new StructuresQueryType();
    }

    /**
     * Create an instance of {@link RegistryInterfaceType }
     * 
     */
    public RegistryInterfaceType createRegistryInterfaceType() {
        return new RegistryInterfaceType();
    }

    /**
     * Create an instance of {@link SubmitSubscriptionsRequestType }
     * 
     */
    public SubmitSubscriptionsRequestType createSubmitSubscriptionsRequestType() {
        return new SubmitSubscriptionsRequestType();
    }

    /**
     * Create an instance of {@link StructureType }
     * 
     */
    public StructureType createStructureType() {
        return new StructureType();
    }

    /**
     * Create an instance of {@link SubmitStructureRequestType }
     * 
     */
    public SubmitStructureRequestType createSubmitStructureRequestType() {
        return new SubmitStructureRequestType();
    }

    /**
     * Create an instance of {@link MetadataQueryType }
     * 
     */
    public MetadataQueryType createMetadataQueryType() {
        return new MetadataQueryType();
    }

    /**
     * Create an instance of {@link DataStructureQueryType }
     * 
     */
    public DataStructureQueryType createDataStructureQueryType() {
        return new DataStructureQueryType();
    }

    /**
     * Create an instance of {@link HierarchicalCodelistQueryType }
     * 
     */
    public HierarchicalCodelistQueryType createHierarchicalCodelistQueryType() {
        return new HierarchicalCodelistQueryType();
    }

    /**
     * Create an instance of {@link QuerySubscriptionRequestType }
     * 
     */
    public QuerySubscriptionRequestType createQuerySubscriptionRequestType() {
        return new QuerySubscriptionRequestType();
    }

    /**
     * Create an instance of {@link SubmitStructureResponseType }
     * 
     */
    public SubmitStructureResponseType createSubmitStructureResponseType() {
        return new SubmitStructureResponseType();
    }

    /**
     * Create an instance of {@link GenericTimeSeriesDataQueryType }
     * 
     */
    public GenericTimeSeriesDataQueryType createGenericTimeSeriesDataQueryType() {
        return new GenericTimeSeriesDataQueryType();
    }

    /**
     * Create an instance of {@link ErrorType }
     * 
     */
    public ErrorType createErrorType() {
        return new ErrorType();
    }

    /**
     * Create an instance of {@link QuerySubscriptionResponseType }
     * 
     */
    public QuerySubscriptionResponseType createQuerySubscriptionResponseType() {
        return new QuerySubscriptionResponseType();
    }

    /**
     * Create an instance of {@link MetadataflowQueryType }
     * 
     */
    public MetadataflowQueryType createMetadataflowQueryType() {
        return new MetadataflowQueryType();
    }

    /**
     * Create an instance of {@link GenericDataType }
     * 
     */
    public GenericDataType createGenericDataType() {
        return new GenericDataType();
    }

    /**
     * Create an instance of {@link StructureSetQueryType }
     * 
     */
    public StructureSetQueryType createStructureSetQueryType() {
        return new StructureSetQueryType();
    }

    /**
     * Create an instance of {@link ProcessQueryType }
     * 
     */
    public ProcessQueryType createProcessQueryType() {
        return new ProcessQueryType();
    }

    /**
     * Create an instance of {@link StructureSpecificTimeSeriesDataQueryType }
     * 
     */
    public StructureSpecificTimeSeriesDataQueryType createStructureSpecificTimeSeriesDataQueryType() {
        return new StructureSpecificTimeSeriesDataQueryType();
    }

    /**
     * Create an instance of {@link QueryRegistrationResponseType }
     * 
     */
    public QueryRegistrationResponseType createQueryRegistrationResponseType() {
        return new QueryRegistrationResponseType();
    }

    /**
     * Create an instance of {@link SubmitRegistrationsResponseType }
     * 
     */
    public SubmitRegistrationsResponseType createSubmitRegistrationsResponseType() {
        return new SubmitRegistrationsResponseType();
    }

    /**
     * Create an instance of {@link GenericMetadataType }
     * 
     */
    public GenericMetadataType createGenericMetadataType() {
        return new GenericMetadataType();
    }

    /**
     * Create an instance of {@link MetadataSchemaQueryType }
     * 
     */
    public MetadataSchemaQueryType createMetadataSchemaQueryType() {
        return new MetadataSchemaQueryType();
    }

    /**
     * Create an instance of {@link CodelistQueryType }
     * 
     */
    public CodelistQueryType createCodelistQueryType() {
        return new CodelistQueryType();
    }

    /**
     * Create an instance of {@link ReportingTaxonomyQueryType }
     * 
     */
    public ReportingTaxonomyQueryType createReportingTaxonomyQueryType() {
        return new ReportingTaxonomyQueryType();
    }

    /**
     * Create an instance of {@link ContactType }
     * 
     */
    public ContactType createContactType() {
        return new ContactType();
    }

    /**
     * Create an instance of {@link GenericDataHeaderType }
     * 
     */
    public GenericDataHeaderType createGenericDataHeaderType() {
        return new GenericDataHeaderType();
    }

    /**
     * Create an instance of {@link GenericTimeSeriesDataHeaderType }
     * 
     */
    public GenericTimeSeriesDataHeaderType createGenericTimeSeriesDataHeaderType() {
        return new GenericTimeSeriesDataHeaderType();
    }

    /**
     * Create an instance of {@link StructureSpecificMetadataHeaderType }
     * 
     */
    public StructureSpecificMetadataHeaderType createStructureSpecificMetadataHeaderType() {
        return new StructureSpecificMetadataHeaderType();
    }

    /**
     * Create an instance of {@link StructureSpecificDataHeaderType }
     * 
     */
    public StructureSpecificDataHeaderType createStructureSpecificDataHeaderType() {
        return new StructureSpecificDataHeaderType();
    }

    /**
     * Create an instance of {@link GenericMetadataHeaderType }
     * 
     */
    public GenericMetadataHeaderType createGenericMetadataHeaderType() {
        return new GenericMetadataHeaderType();
    }

    /**
     * Create an instance of {@link StructureSpecificTimeSeriesDataHeaderType }
     * 
     */
    public StructureSpecificTimeSeriesDataHeaderType createStructureSpecificTimeSeriesDataHeaderType() {
        return new StructureSpecificTimeSeriesDataHeaderType();
    }

    /**
     * Create an instance of {@link PartyType }
     * 
     */
    public PartyType createPartyType() {
        return new PartyType();
    }

    /**
     * Create an instance of {@link BasicHeaderType }
     * 
     */
    public BasicHeaderType createBasicHeaderType() {
        return new BasicHeaderType();
    }

    /**
     * Create an instance of {@link SenderType }
     * 
     */
    public SenderType createSenderType() {
        return new SenderType();
    }

    /**
     * Create an instance of {@link StructureHeaderType }
     * 
     */
    public StructureHeaderType createStructureHeaderType() {
        return new StructureHeaderType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureSetQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "StructureSetQuery")
    public JAXBElement<StructureSetQueryType> createStructureSetQuery(StructureSetQueryType value) {
        return new JAXBElement<StructureSetQueryType>(_StructureSetQuery_QNAME, StructureSetQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErrorType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "Error")
    public JAXBElement<ErrorType> createError(ErrorType value) {
        return new JAXBElement<ErrorType>(_Error_QNAME, ErrorType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataflowQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "DataflowQuery")
    public JAXBElement<DataflowQueryType> createDataflowQuery(DataflowQueryType value) {
        return new JAXBElement<DataflowQueryType>(_DataflowQuery_QNAME, DataflowQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitRegistrationsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "SubmitRegistrationsResponse")
    public JAXBElement<SubmitRegistrationsResponseType> createSubmitRegistrationsResponse(SubmitRegistrationsResponseType value) {
        return new JAXBElement<SubmitRegistrationsResponseType>(_SubmitRegistrationsResponse_QNAME, SubmitRegistrationsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryRegistrationResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "QueryRegistrationResponse")
    public JAXBElement<QueryRegistrationResponseType> createQueryRegistrationResponse(QueryRegistrationResponseType value) {
        return new JAXBElement<QueryRegistrationResponseType>(_QueryRegistrationResponse_QNAME, QueryRegistrationResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodelistQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "CodelistQuery")
    public JAXBElement<CodelistQueryType> createCodelistQuery(CodelistQueryType value) {
        return new JAXBElement<CodelistQueryType>(_CodelistQuery_QNAME, CodelistQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitStructureResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "SubmitStructureResponse")
    public JAXBElement<SubmitStructureResponseType> createSubmitStructureResponse(SubmitStructureResponseType value) {
        return new JAXBElement<SubmitStructureResponseType>(_SubmitStructureResponse_QNAME, SubmitStructureResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureSpecificTimeSeriesDataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "StructureSpecificTimeSeriesData")
    public JAXBElement<StructureSpecificTimeSeriesDataType> createStructureSpecificTimeSeriesData(StructureSpecificTimeSeriesDataType value) {
        return new JAXBElement<StructureSpecificTimeSeriesDataType>(_StructureSpecificTimeSeriesData_QNAME, StructureSpecificTimeSeriesDataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataStructureQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "MetadataStructureQuery")
    public JAXBElement<MetadataStructureQueryType> createMetadataStructureQuery(MetadataStructureQueryType value) {
        return new JAXBElement<MetadataStructureQueryType>(_MetadataStructureQuery_QNAME, MetadataStructureQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategorisationQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "CategorisationQuery")
    public JAXBElement<CategorisationQueryType> createCategorisationQuery(CategorisationQueryType value) {
        return new JAXBElement<CategorisationQueryType>(_CategorisationQuery_QNAME, CategorisationQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitSubscriptionsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "SubmitSubscriptionsRequest")
    public JAXBElement<SubmitSubscriptionsRequestType> createSubmitSubscriptionsRequest(SubmitSubscriptionsRequestType value) {
        return new JAXBElement<SubmitSubscriptionsRequestType>(_SubmitSubscriptionsRequest_QNAME, SubmitSubscriptionsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisionAgreementQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "ProvisionAgreementQuery")
    public JAXBElement<ProvisionAgreementQueryType> createProvisionAgreementQuery(ProvisionAgreementQueryType value) {
        return new JAXBElement<ProvisionAgreementQueryType>(_ProvisionAgreementQuery_QNAME, ProvisionAgreementQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryRegistrationRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "QueryRegistrationRequest")
    public JAXBElement<QueryRegistrationRequestType> createQueryRegistrationRequest(QueryRegistrationRequestType value) {
        return new JAXBElement<QueryRegistrationRequestType>(_QueryRegistrationRequest_QNAME, QueryRegistrationRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HierarchicalCodelistQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "HierarchicalCodelistQuery")
    public JAXBElement<HierarchicalCodelistQueryType> createHierarchicalCodelistQuery(HierarchicalCodelistQueryType value) {
        return new JAXBElement<HierarchicalCodelistQueryType>(_HierarchicalCodelistQuery_QNAME, HierarchicalCodelistQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConceptSchemeQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "ConceptSchemeQuery")
    public JAXBElement<ConceptSchemeQueryType> createConceptSchemeQuery(ConceptSchemeQueryType value) {
        return new JAXBElement<ConceptSchemeQueryType>(_ConceptSchemeQuery_QNAME, ConceptSchemeQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "Structure")
    public JAXBElement<StructureType> createStructure(StructureType value) {
        return new JAXBElement<StructureType>(_Structure_QNAME, StructureType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataStructureQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "DataStructureQuery")
    public JAXBElement<DataStructureQueryType> createDataStructureQuery(DataStructureQueryType value) {
        return new JAXBElement<DataStructureQueryType>(_DataStructureQuery_QNAME, DataStructureQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategorySchemeQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "CategorySchemeQuery")
    public JAXBElement<CategorySchemeQueryType> createCategorySchemeQuery(CategorySchemeQueryType value) {
        return new JAXBElement<CategorySchemeQueryType>(_CategorySchemeQuery_QNAME, CategorySchemeQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenericMetadataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "GenericMetadata")
    public JAXBElement<GenericMetadataType> createGenericMetadata(GenericMetadataType value) {
        return new JAXBElement<GenericMetadataType>(_GenericMetadata_QNAME, GenericMetadataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportingTaxonomyQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "ReportingTaxonomyQuery")
    public JAXBElement<ReportingTaxonomyQueryType> createReportingTaxonomyQuery(ReportingTaxonomyQueryType value) {
        return new JAXBElement<ReportingTaxonomyQueryType>(_ReportingTaxonomyQuery_QNAME, ReportingTaxonomyQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataSchemaQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "MetadataSchemaQuery")
    public JAXBElement<MetadataSchemaQueryType> createMetadataSchemaQuery(MetadataSchemaQueryType value) {
        return new JAXBElement<MetadataSchemaQueryType>(_MetadataSchemaQuery_QNAME, MetadataSchemaQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureSpecificTimeSeriesDataQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "StructureSpecificTimeSeriesDataQuery")
    public JAXBElement<StructureSpecificTimeSeriesDataQueryType> createStructureSpecificTimeSeriesDataQuery(StructureSpecificTimeSeriesDataQueryType value) {
        return new JAXBElement<StructureSpecificTimeSeriesDataQueryType>(_StructureSpecificTimeSeriesDataQuery_QNAME, StructureSpecificTimeSeriesDataQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataflowQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "MetadataflowQuery")
    public JAXBElement<MetadataflowQueryType> createMetadataflowQuery(MetadataflowQueryType value) {
        return new JAXBElement<MetadataflowQueryType>(_MetadataflowQuery_QNAME, MetadataflowQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenericTimeSeriesDataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "GenericTimeSeriesData")
    public JAXBElement<GenericTimeSeriesDataType> createGenericTimeSeriesData(GenericTimeSeriesDataType value) {
        return new JAXBElement<GenericTimeSeriesDataType>(_GenericTimeSeriesData_QNAME, GenericTimeSeriesDataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenericDataQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "GenericDataQuery")
    public JAXBElement<GenericDataQueryType> createGenericDataQuery(GenericDataQueryType value) {
        return new JAXBElement<GenericDataQueryType>(_GenericDataQuery_QNAME, GenericDataQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConstraintQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "ConstraintQuery")
    public JAXBElement<ConstraintQueryType> createConstraintQuery(ConstraintQueryType value) {
        return new JAXBElement<ConstraintQueryType>(_ConstraintQuery_QNAME, ConstraintQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructuresQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "StructuresQuery")
    public JAXBElement<StructuresQueryType> createStructuresQuery(StructuresQueryType value) {
        return new JAXBElement<StructuresQueryType>(_StructuresQuery_QNAME, StructuresQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureSpecificMetadataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "StructureSpecificMetadata")
    public JAXBElement<StructureSpecificMetadataType> createStructureSpecificMetadata(StructureSpecificMetadataType value) {
        return new JAXBElement<StructureSpecificMetadataType>(_StructureSpecificMetadata_QNAME, StructureSpecificMetadataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenericTimeSeriesDataQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "GenericTimeSeriesDataQuery")
    public JAXBElement<GenericTimeSeriesDataQueryType> createGenericTimeSeriesDataQuery(GenericTimeSeriesDataQueryType value) {
        return new JAXBElement<GenericTimeSeriesDataQueryType>(_GenericTimeSeriesDataQuery_QNAME, GenericTimeSeriesDataQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenericDataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "GenericData")
    public JAXBElement<GenericDataType> createGenericData(GenericDataType value) {
        return new JAXBElement<GenericDataType>(_GenericData_QNAME, GenericDataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrganisationSchemeQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "OrganisationSchemeQuery")
    public JAXBElement<OrganisationSchemeQueryType> createOrganisationSchemeQuery(OrganisationSchemeQueryType value) {
        return new JAXBElement<OrganisationSchemeQueryType>(_OrganisationSchemeQuery_QNAME, OrganisationSchemeQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "StructureSpecificMetadataQuery")
    public JAXBElement<MetadataQueryType> createStructureSpecificMetadataQuery(MetadataQueryType value) {
        return new JAXBElement<MetadataQueryType>(_StructureSpecificMetadataQuery_QNAME, MetadataQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryInterfaceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "RegistryInterface")
    public JAXBElement<RegistryInterfaceType> createRegistryInterface(RegistryInterfaceType value) {
        return new JAXBElement<RegistryInterfaceType>(_RegistryInterface_QNAME, RegistryInterfaceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitRegistrationsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "SubmitRegistrationsRequest")
    public JAXBElement<SubmitRegistrationsRequestType> createSubmitRegistrationsRequest(SubmitRegistrationsRequestType value) {
        return new JAXBElement<SubmitRegistrationsRequestType>(_SubmitRegistrationsRequest_QNAME, SubmitRegistrationsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "GenericMetadataQuery")
    public JAXBElement<MetadataQueryType> createGenericMetadataQuery(MetadataQueryType value) {
        return new JAXBElement<MetadataQueryType>(_GenericMetadataQuery_QNAME, MetadataQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "ProcessQuery")
    public JAXBElement<ProcessQueryType> createProcessQuery(ProcessQueryType value) {
        return new JAXBElement<ProcessQueryType>(_ProcessQuery_QNAME, ProcessQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitStructureRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "SubmitStructureRequest")
    public JAXBElement<SubmitStructureRequestType> createSubmitStructureRequest(SubmitStructureRequestType value) {
        return new JAXBElement<SubmitStructureRequestType>(_SubmitStructureRequest_QNAME, SubmitStructureRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotifyRegistryEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "NotifyRegistryEvent")
    public JAXBElement<NotifyRegistryEventType> createNotifyRegistryEvent(NotifyRegistryEventType value) {
        return new JAXBElement<NotifyRegistryEventType>(_NotifyRegistryEvent_QNAME, NotifyRegistryEventType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuerySubscriptionRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "QuerySubscriptionRequest")
    public JAXBElement<QuerySubscriptionRequestType> createQuerySubscriptionRequest(QuerySubscriptionRequestType value) {
        return new JAXBElement<QuerySubscriptionRequestType>(_QuerySubscriptionRequest_QNAME, QuerySubscriptionRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuerySubscriptionResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "QuerySubscriptionResponse")
    public JAXBElement<QuerySubscriptionResponseType> createQuerySubscriptionResponse(QuerySubscriptionResponseType value) {
        return new JAXBElement<QuerySubscriptionResponseType>(_QuerySubscriptionResponse_QNAME, QuerySubscriptionResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSchemaQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "DataSchemaQuery")
    public JAXBElement<DataSchemaQueryType> createDataSchemaQuery(DataSchemaQueryType value) {
        return new JAXBElement<DataSchemaQueryType>(_DataSchemaQuery_QNAME, DataSchemaQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "StructureSpecificDataQuery")
    public JAXBElement<DataQueryType> createStructureSpecificDataQuery(DataQueryType value) {
        return new JAXBElement<DataQueryType>(_StructureSpecificDataQuery_QNAME, DataQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureSpecificDataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "StructureSpecificData")
    public JAXBElement<StructureSpecificDataType> createStructureSpecificData(StructureSpecificDataType value) {
        return new JAXBElement<StructureSpecificDataType>(_StructureSpecificData_QNAME, StructureSpecificDataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitSubscriptionsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "SubmitSubscriptionsResponse")
    public JAXBElement<SubmitSubscriptionsResponseType> createSubmitSubscriptionsResponse(SubmitSubscriptionsResponseType value) {
        return new JAXBElement<SubmitSubscriptionsResponseType>(_SubmitSubscriptionsResponse_QNAME, SubmitSubscriptionsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "Telephone", scope = ContactType.class)
    public JAXBElement<String> createContactTypeTelephone(String value) {
        return new JAXBElement<String>(_ContactTypeTelephone_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "X400", scope = ContactType.class)
    public JAXBElement<String> createContactTypeX400(String value) {
        return new JAXBElement<String>(_ContactTypeX400_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "Email", scope = ContactType.class)
    public JAXBElement<String> createContactTypeEmail(String value) {
        return new JAXBElement<String>(_ContactTypeEmail_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "URI", scope = ContactType.class)
    public JAXBElement<String> createContactTypeURI(String value) {
        return new JAXBElement<String>(_ContactTypeURI_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", name = "Fax", scope = ContactType.class)
    public JAXBElement<String> createContactTypeFax(String value) {
        return new JAXBElement<String>(_ContactTypeFax_QNAME, String.class, ContactType.class, value);
    }

}
