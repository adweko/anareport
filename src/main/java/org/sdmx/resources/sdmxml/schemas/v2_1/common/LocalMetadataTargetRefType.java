
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * LocalMetadataTargetRefType contains a local reference to a metadata target object.
 * 
 * <p>Java-Klasse f�r LocalMetadataTargetRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LocalMetadataTargetRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalComponentListRefBaseType">
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentListTypeCodelistType" fixed="MetadataTarget" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructurePackageTypeCodelistType" fixed="metadatastructure" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocalMetadataTargetRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class LocalMetadataTargetRefType
    extends LocalComponentListRefBaseType
{


}
