
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ItemSchemeType is an abstract base type for all item scheme objects. It contains a collection of items. Concrete instances of this type should restrict the actual types of items allowed within the scheme.
 * 
 * <p>Java-Klasse f�r ItemSchemeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ItemSchemeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MaintainableType">
 *       &lt;sequence>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Item"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="isPartial" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemSchemeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "item"
})
@XmlSeeAlso({
    CodelistType.class,
    ReportingTaxonomyType.class,
    OrganisationSchemeBaseType.class,
    ConceptSchemeType.class,
    CategorySchemeType.class
})
public abstract class ItemSchemeType
    extends MaintainableType
{

    @XmlElementRef(name = "Item", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ItemType>> item;
    @XmlAttribute(name = "isPartial")
    protected Boolean isPartial;

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CategoryType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationUnitType }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportingCategoryType }{@code >}
     * {@link JAXBElement }{@code <}{@link ConceptType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataProviderType }{@code >}
     * {@link JAXBElement }{@code <}{@link AgencyType }{@code >}
     * {@link JAXBElement }{@code <}{@link ItemType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataConsumerType }{@code >}
     * {@link JAXBElement }{@code <}{@link CodeType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ItemType>> getItem() {
        if (item == null) {
            item = new ArrayList<JAXBElement<? extends ItemType>>();
        }
        return this.item;
    }

    /**
     * Ruft den Wert der isPartial-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsPartial() {
        if (isPartial == null) {
            return false;
        } else {
            return isPartial;
        }
    }

    /**
     * Legt den Wert der isPartial-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPartial(Boolean value) {
        this.isPartial = value;
    }

}
