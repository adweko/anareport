
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * StructureType is an abstract base type for all structure objects. Concrete instances of this should restrict to a concrete grouping.
 * 
 * <p>Java-Klasse f�r StructureType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MaintainableType">
 *       &lt;sequence>
 *         &lt;sequence minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Grouping"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "grouping"
})
@XmlSeeAlso({
    DataStructureType.class,
    MetadataStructureType.class
})
public abstract class StructureType
    extends MaintainableType
{

    @XmlElementRef(name = "Grouping", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false)
    protected JAXBElement<? extends GroupingType> grouping;

    /**
     * Ruft den Wert der grouping-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DataStructureComponentsType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MetadataStructureComponentsType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GroupingType }{@code >}
     *     
     */
    public JAXBElement<? extends GroupingType> getGrouping() {
        return grouping;
    }

    /**
     * Legt den Wert der grouping-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DataStructureComponentsType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MetadataStructureComponentsType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GroupingType }{@code >}
     *     
     */
    public void setGrouping(JAXBElement<? extends GroupingType> value) {
        this.grouping = value;
    }

}
