
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * OrganisationWhereType defines a set of parameters for matching an organisation. In addition to the base parameters for any item, there is an additional parameter for matching an organisation based on the roles it serves. All supplied parameters must be matched in order for an object to satisfy the query.
 * 
 * <p>Java-Klasse f�r OrganisationWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OrganisationWhereType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ItemWhereType">
 *       &lt;sequence>
 *         &lt;element name="Annotation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AnnotationWhereType" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="Parent" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalOrganisationUnitReferenceType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class OrganisationWhereType
    extends ItemWhereType
{


}
