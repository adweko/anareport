
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * GroupWhereBaseType is an abstract base type that forms the basis for the GroupWhereType.
 * 
 * <p>Java-Klasse f�r GroupWhereBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GroupWhereBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ComponentListWhereType">
 *       &lt;sequence>
 *         &lt;element name="Annotation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AnnotationWhereType" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" minOccurs="0"/>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}GroupDimensionWhere"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupWhereBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlSeeAlso({
    GroupWhereType.class
})
public abstract class GroupWhereBaseType
    extends ComponentListWhereType
{


}
