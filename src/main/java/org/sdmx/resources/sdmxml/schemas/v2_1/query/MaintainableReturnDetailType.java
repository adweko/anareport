
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r MaintainableReturnDetailType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="MaintainableReturnDetailType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructureReturnDetailType">
 *     &lt;enumeration value="Stub"/>
 *     &lt;enumeration value="CompleteStub"/>
 *     &lt;enumeration value="Full"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MaintainableReturnDetailType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum(StructureReturnDetailType.class)
public enum MaintainableReturnDetailType {

    @XmlEnumValue("Stub")
    STUB(StructureReturnDetailType.STUB),
    @XmlEnumValue("CompleteStub")
    COMPLETE_STUB(StructureReturnDetailType.COMPLETE_STUB),
    @XmlEnumValue("Full")
    FULL(StructureReturnDetailType.FULL);
    private final StructureReturnDetailType value;

    MaintainableReturnDetailType(StructureReturnDetailType v) {
        value = v;
    }

    public StructureReturnDetailType value() {
        return value;
    }

    public static MaintainableReturnDetailType fromValue(StructureReturnDetailType v) {
        for (MaintainableReturnDetailType c: MaintainableReturnDetailType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
