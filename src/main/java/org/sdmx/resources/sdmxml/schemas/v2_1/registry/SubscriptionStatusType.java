
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * SubscriptionStatusType describes the structure a status for a single subscription request.
 * 
 * <p>Java-Klasse f�r SubscriptionStatusType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubscriptionStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubscriptionURN" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *         &lt;element name="SubscriberAssignedID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" minOccurs="0"/>
 *         &lt;element name="StatusMessage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}StatusMessageType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriptionStatusType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "subscriptionURN",
    "subscriberAssignedID",
    "statusMessage"
})
public class SubscriptionStatusType {

    @XmlElement(name = "SubscriptionURN", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    @XmlSchemaType(name = "anyURI")
    protected String subscriptionURN;
    @XmlElement(name = "SubscriberAssignedID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected String subscriberAssignedID;
    @XmlElement(name = "StatusMessage", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected StatusMessageType statusMessage;

    /**
     * Ruft den Wert der subscriptionURN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriptionURN() {
        return subscriptionURN;
    }

    /**
     * Legt den Wert der subscriptionURN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriptionURN(String value) {
        this.subscriptionURN = value;
    }

    /**
     * Ruft den Wert der subscriberAssignedID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberAssignedID() {
        return subscriberAssignedID;
    }

    /**
     * Legt den Wert der subscriberAssignedID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberAssignedID(String value) {
        this.subscriberAssignedID = value;
    }

    /**
     * Ruft den Wert der statusMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StatusMessageType }
     *     
     */
    public StatusMessageType getStatusMessage() {
        return statusMessage;
    }

    /**
     * Legt den Wert der statusMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusMessageType }
     *     
     */
    public void setStatusMessage(StatusMessageType value) {
        this.statusMessage = value;
    }

}
