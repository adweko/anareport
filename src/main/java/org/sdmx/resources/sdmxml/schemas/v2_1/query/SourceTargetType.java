
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r SourceTargetType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SourceTargetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Any"/>
 *     &lt;enumeration value="Source"/>
 *     &lt;enumeration value="Target"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SourceTargetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum
public enum SourceTargetType {

    @XmlEnumValue("Any")
    ANY("Any"),
    @XmlEnumValue("Source")
    SOURCE("Source"),
    @XmlEnumValue("Target")
    TARGET("Target");
    private final String value;

    SourceTargetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SourceTargetType fromValue(String v) {
        for (SourceTargetType c: SourceTargetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
