
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ItemTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ItemTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType">
 *     &lt;enumeration value="Agency"/>
 *     &lt;enumeration value="Category"/>
 *     &lt;enumeration value="Code"/>
 *     &lt;enumeration value="Concept"/>
 *     &lt;enumeration value="DataConsumer"/>
 *     &lt;enumeration value="DataProvider"/>
 *     &lt;enumeration value="OrganisationUnit"/>
 *     &lt;enumeration value="ReportingCategory"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ItemTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ObjectTypeCodelistType.class)
public enum ItemTypeCodelistType {

    @XmlEnumValue("Agency")
    AGENCY(ObjectTypeCodelistType.AGENCY),
    @XmlEnumValue("Category")
    CATEGORY(ObjectTypeCodelistType.CATEGORY),
    @XmlEnumValue("Code")
    CODE(ObjectTypeCodelistType.CODE),
    @XmlEnumValue("Concept")
    CONCEPT(ObjectTypeCodelistType.CONCEPT),
    @XmlEnumValue("DataConsumer")
    DATA_CONSUMER(ObjectTypeCodelistType.DATA_CONSUMER),
    @XmlEnumValue("DataProvider")
    DATA_PROVIDER(ObjectTypeCodelistType.DATA_PROVIDER),
    @XmlEnumValue("OrganisationUnit")
    ORGANISATION_UNIT(ObjectTypeCodelistType.ORGANISATION_UNIT),
    @XmlEnumValue("ReportingCategory")
    REPORTING_CATEGORY(ObjectTypeCodelistType.REPORTING_CATEGORY);
    private final ObjectTypeCodelistType value;

    ItemTypeCodelistType(ObjectTypeCodelistType v) {
        value = v;
    }

    public ObjectTypeCodelistType value() {
        return value;
    }

    public static ItemTypeCodelistType fromValue(ObjectTypeCodelistType v) {
        for (ItemTypeCodelistType c: ItemTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
