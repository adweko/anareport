
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * GenericDataStructureType defines the structural information for a generic data set. A reference to the structure, either explicitly or through a dataflow or provision agreement is required as well as the dimension which occurs at the observation level.
 * 
 * <p>Java-Klasse f�r GenericDataStructureType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GenericDataStructureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="ProvisionAgrement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType"/>
 *           &lt;element name="StructureUsage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType"/>
 *           &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureReferenceType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="dimensionAtObservation" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationDimensionType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericDataStructureType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    GenericTimeSeriesDataStructureType.class
})
public class GenericDataStructureType
    extends DataStructureType
{


}
