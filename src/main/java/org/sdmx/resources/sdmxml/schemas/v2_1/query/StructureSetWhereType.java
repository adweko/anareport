
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureOrUsageReferenceType;


/**
 * StructureSetWhereType defines the parameters of a structure set query. All supplied parameters must be matched in order for an object to satisfy the query. In addition to querying based on the base maintainable parameters, it is also possible to search based on the structures that are related by the set or the objects which are mapped by the set's maps.
 * 
 * <p>Java-Klasse f�r StructureSetWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureSetWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructureSetWhereBaseType">
 *       &lt;sequence>
 *         &lt;element name="RelatedStructures" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureOrUsageReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MappedObject" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MappedObjectType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureSetWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "relatedStructures",
    "mappedObject"
})
public class StructureSetWhereType
    extends StructureSetWhereBaseType
{

    @XmlElement(name = "RelatedStructures", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<StructureOrUsageReferenceType> relatedStructures;
    @XmlElement(name = "MappedObject", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<MappedObjectType> mappedObject;

    /**
     * Gets the value of the relatedStructures property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedStructures property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedStructures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StructureOrUsageReferenceType }
     * 
     * 
     */
    public List<StructureOrUsageReferenceType> getRelatedStructures() {
        if (relatedStructures == null) {
            relatedStructures = new ArrayList<StructureOrUsageReferenceType>();
        }
        return this.relatedStructures;
    }

    /**
     * Gets the value of the mappedObject property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mappedObject property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMappedObject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MappedObjectType }
     * 
     * 
     */
    public List<MappedObjectType> getMappedObject() {
        if (mappedObject == null) {
            mappedObject = new ArrayList<MappedObjectType>();
        }
        return this.mappedObject;
    }

}
