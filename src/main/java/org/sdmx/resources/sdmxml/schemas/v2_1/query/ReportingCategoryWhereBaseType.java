
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ReportingCategoryWhereBaseType is an abstract base type that forms the basis for the ReportingCategoryQueryType.
 * 
 * <p>Java-Klasse f�r ReportingCategoryWhereBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReportingCategoryWhereBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ItemWhereType">
 *       &lt;sequence>
 *         &lt;element name="Annotation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AnnotationWhereType" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;sequence maxOccurs="unbounded">
 *             &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReportingCategoryWhere"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingCategoryWhereBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlSeeAlso({
    ReportingCategoryWhereType.class
})
public abstract class ReportingCategoryWhereBaseType
    extends ItemWhereType
{


}
