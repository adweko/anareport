
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * CategorySchemeMapReferenceType is a type for referencing a category scheme map object. It consists of a URN and/or a complete set of reference fields.
 * 
 * <p>Java-Klasse f�r CategorySchemeMapReferenceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CategorySchemeMapReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ChildObjectReferenceType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="Ref" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategorySchemeMapRefType" form="unqualified"/>
 *           &lt;element name="URN" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0" form="unqualified"/>
 *         &lt;/sequence>
 *         &lt;element name="URN" type="{http://www.w3.org/2001/XMLSchema}anyURI" form="unqualified"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategorySchemeMapReferenceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class CategorySchemeMapReferenceType
    extends ChildObjectReferenceType
{


}
