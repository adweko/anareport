
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * StructureWhereQueryType is an abstract base type that serves as the basis for any structural metadata query. Concrete instances of this type are implied to be an and-query. A structural object will be returned for any object where all of the conditions are met.
 * 
 * <p>Java-Klasse f�r StructuralMetadataQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructuralMetadataQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructureReturnDetailsType"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructuralMetadataWhere"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuralMetadataQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "returnDetails",
    "structuralMetadataWhere"
})
@XmlSeeAlso({
    DataflowQueryType.class,
    StructureSetQueryType.class,
    OrganisationSchemeQueryType.class,
    HierarchicalCodelistQueryType.class,
    ProcessQueryType.class,
    ProvisionAgreementQueryType.class,
    MetadataflowQueryType.class,
    ReportingTaxonomyQueryType.class,
    StructuresQueryType.class,
    CategorisationQueryType.class,
    CategorySchemeQueryType.class,
    DataStructureQueryType.class,
    ConceptSchemeQueryType.class,
    CodelistQueryType.class,
    MetadataStructureQueryType.class,
    ConstraintQueryType.class
})
public abstract class StructuralMetadataQueryType {

    @XmlElement(name = "ReturnDetails", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected StructureReturnDetailsType returnDetails;
    @XmlElementRef(name = "StructuralMetadataWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class)
    protected JAXBElement<? extends MaintainableWhereType> structuralMetadataWhere;

    /**
     * Ruft den Wert der returnDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureReturnDetailsType }
     *     
     */
    public StructureReturnDetailsType getReturnDetails() {
        return returnDetails;
    }

    /**
     * Legt den Wert der returnDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureReturnDetailsType }
     *     
     */
    public void setReturnDetails(StructureReturnDetailsType value) {
        this.returnDetails = value;
    }

    /**
     * Ruft den Wert der structuralMetadataWhere-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DataflowWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MaintainableWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ConceptSchemeWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ProvisionAgreementWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CodelistWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReportingTaxonomyWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OrganisationSchemeWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HierarchicalCodelistWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MetadataflowWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MetadataStructureWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link StructureSetWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ConstraintWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DataStructureWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ProcessWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CategorySchemeWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CategorisationWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link StructuresWhereType }{@code >}
     *     
     */
    public JAXBElement<? extends MaintainableWhereType> getStructuralMetadataWhere() {
        return structuralMetadataWhere;
    }

    /**
     * Legt den Wert der structuralMetadataWhere-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DataflowWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MaintainableWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ConceptSchemeWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ProvisionAgreementWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CodelistWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReportingTaxonomyWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OrganisationSchemeWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HierarchicalCodelistWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MetadataflowWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MetadataStructureWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link StructureSetWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ConstraintWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DataStructureWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ProcessWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CategorySchemeWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CategorisationWhereType }{@code >}
     *     {@link JAXBElement }{@code <}{@link StructuresWhereType }{@code >}
     *     
     */
    public void setStructuralMetadataWhere(JAXBElement<? extends MaintainableWhereType> value) {
        this.structuralMetadataWhere = value;
    }

}
