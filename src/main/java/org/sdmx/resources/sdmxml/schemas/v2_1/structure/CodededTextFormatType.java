
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * CodededTextFormatType is a restricted version of the SimpleComponentTextFormatType that only allows factets and text types applicable to codes. Although the time facets permit any value, an actual code identifier does not support the necessary characters for time. Therefore these facets should not contain time in their values.
 * 
 * <p>Java-Klasse f�r CodededTextFormatType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CodededTextFormatType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}SimpleComponentTextFormatType">
 *       &lt;attribute name="textType" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodeDataType" />
 *       &lt;attribute name="isSequence" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="interval" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="startValue" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="endValue" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="timeInterval" type="{http://www.w3.org/2001/XMLSchema}duration" />
 *       &lt;attribute name="startTime" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StandardTimePeriodType" />
 *       &lt;attribute name="endTime" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StandardTimePeriodType" />
 *       &lt;attribute name="minLength" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" />
 *       &lt;attribute name="maxLength" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" />
 *       &lt;attribute name="minValue" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="maxValue" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="pattern" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodededTextFormatType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class CodededTextFormatType
    extends SimpleComponentTextFormatType
{


}
