
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CubeRegionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.EmptyType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataTargetRegionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ReferencePeriodType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.DataKeySetType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.MetadataKeySetType;


/**
 * QueryRegistrationRequestType describes the structure of a registration query request. The type of query (data, reference metadata, or both) must be specified. It is possible to query for registrations for a particular provision agreement, data provider, or structure usage, or to query for all registrations in the registry. In addition the search can be refined by providing constraints in the form of explicit time periods, constraint regions, and key sets. When constraint regions and key sets are provided they will be effectively processed by first matching all data for the included keys and regions (all available data if there are none) and then removing any data matching the excluded keys or regions.
 * 
 * <p>Java-Klasse f�r QueryRegistrationRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="QueryRegistrationRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryType" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}QueryTypeType"/>
 *         &lt;choice>
 *           &lt;element name="All" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *           &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType"/>
 *           &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType"/>
 *           &lt;element name="Dataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType"/>
 *           &lt;element name="Metadataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataflowReferenceType"/>
 *         &lt;/choice>
 *         &lt;element name="ReferencePeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ReferencePeriodType" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="DataKeySet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataKeySetType"/>
 *           &lt;element name="MetadataKeySet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataKeySetType"/>
 *           &lt;element name="CubeRegion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CubeRegionType"/>
 *           &lt;element name="MetadataTargetRegion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataTargetRegionType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="returnConstraints" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryRegistrationRequestType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "queryType",
    "all",
    "provisionAgreement",
    "dataProvider",
    "dataflow",
    "metadataflow",
    "referencePeriod",
    "dataKeySetOrMetadataKeySetOrCubeRegion"
})
public class QueryRegistrationRequestType {

    @XmlElement(name = "QueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected QueryTypeType queryType;
    @XmlElement(name = "All", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected EmptyType all;
    @XmlElement(name = "ProvisionAgreement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected ProvisionAgreementReferenceType provisionAgreement;
    @XmlElement(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected DataProviderReferenceType dataProvider;
    @XmlElement(name = "Dataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected DataflowReferenceType dataflow;
    @XmlElement(name = "Metadataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected MetadataflowReferenceType metadataflow;
    @XmlElement(name = "ReferencePeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected ReferencePeriodType referencePeriod;
    @XmlElements({
        @XmlElement(name = "DataKeySet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = DataKeySetType.class),
        @XmlElement(name = "MetadataKeySet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = MetadataKeySetType.class),
        @XmlElement(name = "CubeRegion", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = CubeRegionType.class),
        @XmlElement(name = "MetadataTargetRegion", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = MetadataTargetRegionType.class)
    })
    protected List<Object> dataKeySetOrMetadataKeySetOrCubeRegion;
    @XmlAttribute(name = "returnConstraints")
    protected Boolean returnConstraints;

    /**
     * Ruft den Wert der queryType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QueryTypeType }
     *     
     */
    public QueryTypeType getQueryType() {
        return queryType;
    }

    /**
     * Legt den Wert der queryType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryTypeType }
     *     
     */
    public void setQueryType(QueryTypeType value) {
        this.queryType = value;
    }

    /**
     * Ruft den Wert der all-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAll() {
        return all;
    }

    /**
     * Legt den Wert der all-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAll(EmptyType value) {
        this.all = value;
    }

    /**
     * Ruft den Wert der provisionAgreement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProvisionAgreementReferenceType }
     *     
     */
    public ProvisionAgreementReferenceType getProvisionAgreement() {
        return provisionAgreement;
    }

    /**
     * Legt den Wert der provisionAgreement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvisionAgreementReferenceType }
     *     
     */
    public void setProvisionAgreement(ProvisionAgreementReferenceType value) {
        this.provisionAgreement = value;
    }

    /**
     * Ruft den Wert der dataProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public DataProviderReferenceType getDataProvider() {
        return dataProvider;
    }

    /**
     * Legt den Wert der dataProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public void setDataProvider(DataProviderReferenceType value) {
        this.dataProvider = value;
    }

    /**
     * Ruft den Wert der dataflow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataflowReferenceType }
     *     
     */
    public DataflowReferenceType getDataflow() {
        return dataflow;
    }

    /**
     * Legt den Wert der dataflow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataflowReferenceType }
     *     
     */
    public void setDataflow(DataflowReferenceType value) {
        this.dataflow = value;
    }

    /**
     * Ruft den Wert der metadataflow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MetadataflowReferenceType }
     *     
     */
    public MetadataflowReferenceType getMetadataflow() {
        return metadataflow;
    }

    /**
     * Legt den Wert der metadataflow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MetadataflowReferenceType }
     *     
     */
    public void setMetadataflow(MetadataflowReferenceType value) {
        this.metadataflow = value;
    }

    /**
     * Ruft den Wert der referencePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencePeriodType }
     *     
     */
    public ReferencePeriodType getReferencePeriod() {
        return referencePeriod;
    }

    /**
     * Legt den Wert der referencePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencePeriodType }
     *     
     */
    public void setReferencePeriod(ReferencePeriodType value) {
        this.referencePeriod = value;
    }

    /**
     * Gets the value of the dataKeySetOrMetadataKeySetOrCubeRegion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataKeySetOrMetadataKeySetOrCubeRegion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataKeySetOrMetadataKeySetOrCubeRegion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataKeySetType }
     * {@link MetadataKeySetType }
     * {@link CubeRegionType }
     * {@link MetadataTargetRegionType }
     * 
     * 
     */
    public List<Object> getDataKeySetOrMetadataKeySetOrCubeRegion() {
        if (dataKeySetOrMetadataKeySetOrCubeRegion == null) {
            dataKeySetOrMetadataKeySetOrCubeRegion = new ArrayList<Object>();
        }
        return this.dataKeySetOrMetadataKeySetOrCubeRegion;
    }

    /**
     * Ruft den Wert der returnConstraints-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnConstraints() {
        if (returnConstraints == null) {
            return false;
        } else {
            return returnConstraints;
        }
    }

    /**
     * Legt den Wert der returnConstraints-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnConstraints(Boolean value) {
        this.returnConstraints = value;
    }

}
