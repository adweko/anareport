
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DistinctKeyType;


/**
 * KeySetType is an abstract base type for defining a collection of keys.
 * 
 * <p>Java-Klasse f�r KeySetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="KeySetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DistinctKeyType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="isIncluded" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeySetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "key"
})
@XmlSeeAlso({
    MetadataKeySetType.class,
    DataKeySetType.class
})
public abstract class KeySetType {

    @XmlElement(name = "Key", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected List<DistinctKeyType> key;
    @XmlAttribute(name = "isIncluded", required = true)
    protected boolean isIncluded;

    /**
     * Gets the value of the key property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the key property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DistinctKeyType }
     * 
     * 
     */
    public List<DistinctKeyType> getKey() {
        if (key == null) {
            key = new ArrayList<DistinctKeyType>();
        }
        return this.key;
    }

    /**
     * Ruft den Wert der isIncluded-Eigenschaft ab.
     * 
     */
    public boolean isIsIncluded() {
        return isIncluded;
    }

    /**
     * Legt den Wert der isIncluded-Eigenschaft fest.
     * 
     */
    public void setIsIncluded(boolean value) {
        this.isIncluded = value;
    }

}
