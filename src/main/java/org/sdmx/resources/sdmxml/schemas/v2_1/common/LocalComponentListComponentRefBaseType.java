
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * LocalComponentRefBaseType is an abstract base type which provides a local reference to a component object.
 * 
 * <p>Java-Klasse f�r LocalComponentListComponentRefBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LocalComponentListComponentRefBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalIdentifiableRefBaseType">
 *       &lt;attribute name="containerID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocalComponentListComponentRefBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    LocalComponentListComponentRefType.class,
    LocalComponentRefBaseType.class,
    LocalDataStructureComponentRefType.class,
    LocalMetadataStructureComponentRefType.class
})
public abstract class LocalComponentListComponentRefBaseType
    extends LocalIdentifiableRefBaseType
{


}
