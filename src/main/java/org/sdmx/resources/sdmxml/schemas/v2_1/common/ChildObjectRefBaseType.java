
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ChildObjectRefBaseType is an abstract base type for referencing any child object defined directly within a maintainable object.
 * 
 * <p>Java-Klasse f�r ChildObjectRefBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChildObjectRefBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}RefBaseType">
 *       &lt;attribute name="agencyID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedNCNameIDType" />
 *       &lt;attribute name="maintainableParentID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="maintainableParentVersion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionType" default="1.0" />
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedIDType" />
 *       &lt;attribute name="local" type="{http://www.w3.org/2001/XMLSchema}boolean" fixed="false" />
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PackageTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChildObjectRefBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    HierarchyRefType.class,
    OrganisationSchemeMapRefType.class,
    ProcessStepRefType.class,
    StructureMapRefType.class,
    LevelRefType.class,
    ConceptSchemeMapRefType.class,
    CodelistMapRefType.class,
    CategorySchemeMapRefType.class,
    ComponentListRefBaseType.class,
    ItemRefBaseType.class
})
public abstract class ChildObjectRefBaseType
    extends RefBaseType
{


}
