
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ContentConstraintTypeCodeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ContentConstraintTypeCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Allowed"/>
 *     &lt;enumeration value="Actual"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContentConstraintTypeCodeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum ContentConstraintTypeCodeType {


    /**
     * The constraint contains the allowed values for attachable object.
     * 
     */
    @XmlEnumValue("Allowed")
    ALLOWED("Allowed"),

    /**
     * The constraints contains the actual data present for the attachable object.
     * 
     */
    @XmlEnumValue("Actual")
    ACTUAL("Actual");
    private final String value;

    ContentConstraintTypeCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContentConstraintTypeCodeType fromValue(String v) {
        for (ContentConstraintTypeCodeType c: ContentConstraintTypeCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
