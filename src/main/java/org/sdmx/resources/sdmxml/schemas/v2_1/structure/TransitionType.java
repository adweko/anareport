
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalProcessStepReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TextType;


/**
 * TransitionType describes the details of a transition, which is an expression in a textual or formalised way of the transformation of data between two specific operations performed on the data.
 * 
 * <p>Java-Klasse f�r TransitionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TransitionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}IdentifiableType">
 *       &lt;sequence>
 *         &lt;element name="TargetStep" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalProcessStepReferenceType"/>
 *         &lt;element name="Condition" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TextType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="localID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransitionType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "targetStep",
    "condition"
})
public class TransitionType
    extends IdentifiableType
{

    @XmlElement(name = "TargetStep", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected LocalProcessStepReferenceType targetStep;
    @XmlElement(name = "Condition", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected List<TextType> condition;
    @XmlAttribute(name = "localID")
    protected String localID;

    /**
     * Ruft den Wert der targetStep-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalProcessStepReferenceType }
     *     
     */
    public LocalProcessStepReferenceType getTargetStep() {
        return targetStep;
    }

    /**
     * Legt den Wert der targetStep-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalProcessStepReferenceType }
     *     
     */
    public void setTargetStep(LocalProcessStepReferenceType value) {
        this.targetStep = value;
    }

    /**
     * Gets the value of the condition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the condition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getCondition() {
        if (condition == null) {
            condition = new ArrayList<TextType>();
        }
        return this.condition;
    }

    /**
     * Ruft den Wert der localID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalID() {
        return localID;
    }

    /**
     * Legt den Wert der localID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalID(String value) {
        this.localID = value;
    }

}
