
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CodeReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalCodeReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalLevelReferenceType;


/**
 * HierarchicalCodeType describes the structure of a hierarchical code. A hierarchical code provides for a reference to a code that is referenced within the hierarchical code list via either a complete reference to a code through either a URN or full set of reference fields, or a local reference which utilizes the included codelist reference alias and the identification of a code from the list. Codes are arranged in a hierarchy by this reference. Note that it is possible to reference a single code such that it has multiple parents within the hierarchy. Further, the hierarchy may or may not be a leveled one.
 * 
 * <p>Java-Klasse f�r HierarchicalCodeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HierarchicalCodeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HierarchicalCodeBaseType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Code" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodeReferenceType"/>
 *           &lt;sequence>
 *             &lt;element name="CodelistAliasRef" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType"/>
 *             &lt;element name="CodeID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalCodeReferenceType"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *         &lt;element name="HierarchicalCode" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}HierarchicalCodeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Level" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalLevelReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionType" />
 *       &lt;attribute name="validFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="validTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HierarchicalCodeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "code",
    "codelistAliasRef",
    "codeID",
    "hierarchicalCode",
    "level"
})
public class HierarchicalCodeType
    extends HierarchicalCodeBaseType
{

    @XmlElement(name = "Code", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected CodeReferenceType code;
    @XmlElement(name = "CodelistAliasRef", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected String codelistAliasRef;
    @XmlElement(name = "CodeID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected LocalCodeReferenceType codeID;
    @XmlElement(name = "HierarchicalCode", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<HierarchicalCodeType> hierarchicalCode;
    @XmlElement(name = "Level", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected LocalLevelReferenceType level;
    @XmlAttribute(name = "version")
    protected String version;
    @XmlAttribute(name = "validFrom")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validFrom;
    @XmlAttribute(name = "validTo")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar validTo;

    /**
     * Ruft den Wert der code-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeReferenceType }
     *     
     */
    public CodeReferenceType getCode() {
        return code;
    }

    /**
     * Legt den Wert der code-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeReferenceType }
     *     
     */
    public void setCode(CodeReferenceType value) {
        this.code = value;
    }

    /**
     * Ruft den Wert der codelistAliasRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodelistAliasRef() {
        return codelistAliasRef;
    }

    /**
     * Legt den Wert der codelistAliasRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodelistAliasRef(String value) {
        this.codelistAliasRef = value;
    }

    /**
     * Ruft den Wert der codeID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalCodeReferenceType }
     *     
     */
    public LocalCodeReferenceType getCodeID() {
        return codeID;
    }

    /**
     * Legt den Wert der codeID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalCodeReferenceType }
     *     
     */
    public void setCodeID(LocalCodeReferenceType value) {
        this.codeID = value;
    }

    /**
     * Gets the value of the hierarchicalCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hierarchicalCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHierarchicalCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HierarchicalCodeType }
     * 
     * 
     */
    public List<HierarchicalCodeType> getHierarchicalCode() {
        if (hierarchicalCode == null) {
            hierarchicalCode = new ArrayList<HierarchicalCodeType>();
        }
        return this.hierarchicalCode;
    }

    /**
     * Ruft den Wert der level-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalLevelReferenceType }
     *     
     */
    public LocalLevelReferenceType getLevel() {
        return level;
    }

    /**
     * Legt den Wert der level-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalLevelReferenceType }
     *     
     */
    public void setLevel(LocalLevelReferenceType value) {
        this.level = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der validFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidFrom() {
        return validFrom;
    }

    /**
     * Legt den Wert der validFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidFrom(XMLGregorianCalendar value) {
        this.validFrom = value;
    }

    /**
     * Ruft den Wert der validTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValidTo() {
        return validTo;
    }

    /**
     * Legt den Wert der validTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValidTo(XMLGregorianCalendar value) {
        this.validTo = value;
    }

}
