
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * DinstinctKeyValueType is an abstract base type which defines a singular, required value for a key component.
 * 
 * <p>Java-Klasse f�r DinstinctKeyValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DinstinctKeyValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentValueSetType">
 *       &lt;choice>
 *         &lt;element name="Value" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleKeyValueType"/>
 *         &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType"/>
 *         &lt;element name="DataKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataKeyType"/>
 *         &lt;element name="Object" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SingleNCNameIDType" />
 *       &lt;attribute name="include" type="{http://www.w3.org/2001/XMLSchema}boolean" fixed="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DinstinctKeyValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    MetadataKeyValueType.class,
    DataKeyValueType.class
})
public abstract class DinstinctKeyValueType
    extends ComponentValueSetType
{


}
