
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.StructuresType;


/**
 * StructuralEventType provides the details of a structure event, specifically the object that changed.
 * 
 * <p>Java-Klasse f�r StructuralEventType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructuralEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Structures"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuralEventType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "structures"
})
public class StructuralEventType {

    @XmlElement(name = "Structures", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected StructuresType structures;

    /**
     * Structures contains the details of the structural object that has triggered the event. Although this container allows for multiple structural object, it should only contain the one changed object.
     * 
     * @return
     *     possible object is
     *     {@link StructuresType }
     *     
     */
    public StructuresType getStructures() {
        return structures;
    }

    /**
     * Legt den Wert der structures-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructuresType }
     *     
     */
    public void setStructures(StructuresType value) {
        this.structures = value;
    }

}
