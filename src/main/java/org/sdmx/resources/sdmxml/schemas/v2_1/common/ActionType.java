
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ActionType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Append"/>
 *     &lt;enumeration value="Replace"/>
 *     &lt;enumeration value="Delete"/>
 *     &lt;enumeration value="Information"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActionType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum ActionType {


    /**
     * Append - this is an incremental update for an existing data/metadata set or the provision of new data or documentation (attribute values) formerly absent. If any of the supplied data or metadata is already present, it will not replace that data or metadata. This corresponds to the "Update" value found in version 1.0 of the SDMX Technical Standards.
     * 
     */
    @XmlEnumValue("Append")
    APPEND("Append"),

    /**
     * Replace - data/metadata is to be replaced, and may also include additional data/metadata to be appended. The replacement occurs at the level of the observation - that is, it is not possible to replace an entire series.
     * 
     */
    @XmlEnumValue("Replace")
    REPLACE("Replace"),

    /**
     * Delete - data/metadata is to be deleted. Deletion occurs at the lowest level object. For instance, if a delete data message contains a series with no observations, then the entire series will be deleted. If the series contains observations, then only those observations specified will be deleted. The same basic concept applies for attributes. If a series or observation in a delete message contains attributes, then only those attributes will be deleted.
     * 
     */
    @XmlEnumValue("Delete")
    DELETE("Delete"),

    /**
     * Informational - data/metadata is being exchanged for informational purposes only, and not meant to update a system.
     * 
     */
    @XmlEnumValue("Information")
    INFORMATION("Information");
    private final String value;

    ActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionType fromValue(String v) {
        for (ActionType c: ActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
