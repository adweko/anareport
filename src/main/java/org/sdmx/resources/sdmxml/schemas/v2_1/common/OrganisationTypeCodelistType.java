
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r OrganisationTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganisationTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemTypeCodelistType">
 *     &lt;enumeration value="Agency"/>
 *     &lt;enumeration value="DataConsumer"/>
 *     &lt;enumeration value="DataProvider"/>
 *     &lt;enumeration value="OrganisationUnit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrganisationTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ItemTypeCodelistType.class)
public enum OrganisationTypeCodelistType {

    @XmlEnumValue("Agency")
    AGENCY(ItemTypeCodelistType.AGENCY),
    @XmlEnumValue("DataConsumer")
    DATA_CONSUMER(ItemTypeCodelistType.DATA_CONSUMER),
    @XmlEnumValue("DataProvider")
    DATA_PROVIDER(ItemTypeCodelistType.DATA_PROVIDER),
    @XmlEnumValue("OrganisationUnit")
    ORGANISATION_UNIT(ItemTypeCodelistType.ORGANISATION_UNIT);
    private final ItemTypeCodelistType value;

    OrganisationTypeCodelistType(ItemTypeCodelistType v) {
        value = v;
    }

    public ItemTypeCodelistType value() {
        return value;
    }

    public static OrganisationTypeCodelistType fromValue(ItemTypeCodelistType v) {
        for (OrganisationTypeCodelistType c: OrganisationTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
