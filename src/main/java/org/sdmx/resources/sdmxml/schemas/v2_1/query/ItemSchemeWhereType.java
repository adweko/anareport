
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ItemSchemeQueryType is an abstract base type that serves as the basis for any query for an item scheme.
 * 
 * <p>Java-Klasse f�r ItemSchemeWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ItemSchemeWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableWhereType">
 *       &lt;sequence>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ItemWhere"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemSchemeWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "itemWhere"
})
@XmlSeeAlso({
    OrganisationSchemeWhereType.class,
    CodelistWhereType.class,
    ConceptSchemeWhereType.class,
    ReportingTaxonomyWhereType.class,
    CategorySchemeWhereType.class
})
public abstract class ItemSchemeWhereType
    extends MaintainableWhereType
{

    @XmlElementRef(name = "ItemWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ItemWhereType>> itemWhere;

    /**
     * Gets the value of the itemWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CategoryWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link CodeWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ConceptWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ItemWhereType }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportingCategoryWhereType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ItemWhereType>> getItemWhere() {
        if (itemWhere == null) {
            itemWhere = new ArrayList<JAXBElement<? extends ItemWhereType>>();
        }
        return this.itemWhere;
    }

}
