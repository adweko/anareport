
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureReferenceBaseType;


/**
 * StructureUsageWhereType is an abstract base type that serves as the basis for a query for a structure usage object.
 * 
 * <p>Java-Klasse f�r StructureUsageWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureUsageWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableWhereType">
 *       &lt;sequence>
 *         &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureReferenceBaseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureUsageWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "structure"
})
@XmlSeeAlso({
    MetadataflowWhereType.class,
    DataflowWhereType.class
})
public abstract class StructureUsageWhereType
    extends MaintainableWhereType
{

    @XmlElement(name = "Structure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected StructureReferenceBaseType structure;

    /**
     * Ruft den Wert der structure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureReferenceBaseType }
     *     
     */
    public StructureReferenceBaseType getStructure() {
        return structure;
    }

    /**
     * Legt den Wert der structure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureReferenceBaseType }
     *     
     */
    public void setStructure(StructureReferenceBaseType value) {
        this.structure = value;
    }

}
