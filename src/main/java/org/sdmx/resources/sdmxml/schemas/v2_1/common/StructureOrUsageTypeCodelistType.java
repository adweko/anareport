
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r StructureOrUsageTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="StructureOrUsageTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableTypeCodelistType">
 *     &lt;enumeration value="Dataflow"/>
 *     &lt;enumeration value="DataStructure"/>
 *     &lt;enumeration value="Metadataflow"/>
 *     &lt;enumeration value="MetadataStructure"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StructureOrUsageTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(MaintainableTypeCodelistType.class)
public enum StructureOrUsageTypeCodelistType {

    @XmlEnumValue("Dataflow")
    DATAFLOW(MaintainableTypeCodelistType.DATAFLOW),
    @XmlEnumValue("DataStructure")
    DATA_STRUCTURE(MaintainableTypeCodelistType.DATA_STRUCTURE),
    @XmlEnumValue("Metadataflow")
    METADATAFLOW(MaintainableTypeCodelistType.METADATAFLOW),
    @XmlEnumValue("MetadataStructure")
    METADATA_STRUCTURE(MaintainableTypeCodelistType.METADATA_STRUCTURE);
    private final MaintainableTypeCodelistType value;

    StructureOrUsageTypeCodelistType(MaintainableTypeCodelistType v) {
        value = v;
    }

    public MaintainableTypeCodelistType value() {
        return value;
    }

    public static StructureOrUsageTypeCodelistType fromValue(MaintainableTypeCodelistType v) {
        for (StructureOrUsageTypeCodelistType c: StructureOrUsageTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
