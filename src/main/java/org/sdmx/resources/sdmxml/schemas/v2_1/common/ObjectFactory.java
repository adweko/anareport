
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.sdmx.resources.sdmxml.schemas.v2_1.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PrimaryMeasure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "PrimaryMeasure");
    private final static QName _ProcessStep_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ProcessStep");
    private final static QName _GroupDimensionDescriptor_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "GroupDimensionDescriptor");
    private final static QName _DataConsumer_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "DataConsumer");
    private final static QName _Agency_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Agency");
    private final static QName _ConceptMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ConceptMap");
    private final static QName _MeasureDescriptor_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "MeasureDescriptor");
    private final static QName _MetadataTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "MetadataTarget");
    private final static QName _Name_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Name");
    private final static QName _Process_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Process");
    private final static QName _AgencyScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "AgencyScheme");
    private final static QName _StructureSet_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "StructureSet");
    private final static QName _DimensionDescriptorValuesTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "DimensionDescriptorValuesTarget");
    private final static QName _IdentifiableObjectTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "IdentifiableObjectTarget");
    private final static QName _MeasureDimension_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "MeasureDimension");
    private final static QName _Attribute_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Attribute");
    private final static QName _ConstraintTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ConstraintTarget");
    private final static QName _Metadataflow_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Metadataflow");
    private final static QName _DimensionDescriptor_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "DimensionDescriptor");
    private final static QName _DataStructure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "DataStructure");
    private final static QName _HierarchicalCodelist_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "HierarchicalCodelist");
    private final static QName _ConceptScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ConceptScheme");
    private final static QName _AttributeDescriptor_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "AttributeDescriptor");
    private final static QName _ConceptSchemeMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ConceptSchemeMap");
    private final static QName _Codelist_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Codelist");
    private final static QName _HierarchicalCode_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "HierarchicalCode");
    private final static QName _CodelistMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "CodelistMap");
    private final static QName _Code_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Code");
    private final static QName _DataSetTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "DataSetTarget");
    private final static QName _Categorisation_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Categorisation");
    private final static QName _DataProvider_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "DataProvider");
    private final static QName _Any_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Any");
    private final static QName _Category_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Category");
    private final static QName _OrganisationMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "OrganisationMap");
    private final static QName _StructuredText_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "StructuredText");
    private final static QName _MetadataStructure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "MetadataStructure");
    private final static QName _ReportPeriodTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ReportPeriodTarget");
    private final static QName _TimeDimension_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "TimeDimension");
    private final static QName _ReportingCategory_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ReportingCategory");
    private final static QName _Hierarchy_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Hierarchy");
    private final static QName _ReportingTaxonomyMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ReportingTaxonomyMap");
    private final static QName _DataProviderScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "DataProviderScheme");
    private final static QName _MetadataSet_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "MetadataSet");
    private final static QName _ContentConstraint_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ContentConstraint");
    private final static QName _OrganisationUnitScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "OrganisationUnitScheme");
    private final static QName _CategorySchemeMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "CategorySchemeMap");
    private final static QName _StructureMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "StructureMap");
    private final static QName _Concept_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Concept");
    private final static QName _AttachmentConstraint_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "AttachmentConstraint");
    private final static QName _CodeMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "CodeMap");
    private final static QName _OrganisationSchemeMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "OrganisationSchemeMap");
    private final static QName _MetadataAttribute_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "MetadataAttribute");
    private final static QName _Description_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Description");
    private final static QName _Dataflow_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Dataflow");
    private final static QName _Transition_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Transition");
    private final static QName _ProvisionAgreement_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ProvisionAgreement");
    private final static QName _ReportingTaxonomy_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ReportingTaxonomy");
    private final static QName _Text_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Text");
    private final static QName _Dimension_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Dimension");
    private final static QName _CategoryScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "CategoryScheme");
    private final static QName _Annotations_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Annotations");
    private final static QName _HybridCodeMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "HybridCodeMap");
    private final static QName _Level_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "Level");
    private final static QName _DataConsumerScheme_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "DataConsumerScheme");
    private final static QName _ComponentMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ComponentMap");
    private final static QName _HybridCodelistMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "HybridCodelistMap");
    private final static QName _ReportStructure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ReportStructure");
    private final static QName _ReportingCategoryMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "ReportingCategoryMap");
    private final static QName _OrganisationUnit_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", "OrganisationUnit");
    private final static QName _ReferenceTypeURN_QNAME = new QName("", "URN");
    private final static QName _ReferenceTypeRef_QNAME = new QName("", "Ref");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.sdmx.resources.sdmxml.schemas.v2_1.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EmptyType }
     * 
     */
    public EmptyType createEmptyType() {
        return new EmptyType();
    }

    /**
     * Create an instance of {@link TextType }
     * 
     */
    public TextType createTextType() {
        return new TextType();
    }

    /**
     * Create an instance of {@link AnnotationsType }
     * 
     */
    public AnnotationsType createAnnotationsType() {
        return new AnnotationsType();
    }

    /**
     * Create an instance of {@link XHTMLType }
     * 
     */
    public XHTMLType createXHTMLType() {
        return new XHTMLType();
    }

    /**
     * Create an instance of {@link ConceptSchemeMapReferenceType }
     * 
     */
    public ConceptSchemeMapReferenceType createConceptSchemeMapReferenceType() {
        return new ConceptSchemeMapReferenceType();
    }

    /**
     * Create an instance of {@link ProvisionAgreementReferenceType }
     * 
     */
    public ProvisionAgreementReferenceType createProvisionAgreementReferenceType() {
        return new ProvisionAgreementReferenceType();
    }

    /**
     * Create an instance of {@link HierarchicalCodelistReferenceType }
     * 
     */
    public HierarchicalCodelistReferenceType createHierarchicalCodelistReferenceType() {
        return new HierarchicalCodelistReferenceType();
    }

    /**
     * Create an instance of {@link GenericMetadataStructureType }
     * 
     */
    public GenericMetadataStructureType createGenericMetadataStructureType() {
        return new GenericMetadataStructureType();
    }

    /**
     * Create an instance of {@link LocalCategoryRefType }
     * 
     */
    public LocalCategoryRefType createLocalCategoryRefType() {
        return new LocalCategoryRefType();
    }

    /**
     * Create an instance of {@link LocalDataConsumerRefType }
     * 
     */
    public LocalDataConsumerRefType createLocalDataConsumerRefType() {
        return new LocalDataConsumerRefType();
    }

    /**
     * Create an instance of {@link LocalCodelistMapReferenceType }
     * 
     */
    public LocalCodelistMapReferenceType createLocalCodelistMapReferenceType() {
        return new LocalCodelistMapReferenceType();
    }

    /**
     * Create an instance of {@link LocalCodelistMapRefType }
     * 
     */
    public LocalCodelistMapRefType createLocalCodelistMapRefType() {
        return new LocalCodelistMapRefType();
    }

    /**
     * Create an instance of {@link MetadataTargetRegionType }
     * 
     */
    public MetadataTargetRegionType createMetadataTargetRegionType() {
        return new MetadataTargetRegionType();
    }

    /**
     * Create an instance of {@link HierarchyRefType }
     * 
     */
    public HierarchyRefType createHierarchyRefType() {
        return new HierarchyRefType();
    }

    /**
     * Create an instance of {@link DimensionRefType }
     * 
     */
    public DimensionRefType createDimensionRefType() {
        return new DimensionRefType();
    }

    /**
     * Create an instance of {@link OrganisationSchemeMapRefType }
     * 
     */
    public OrganisationSchemeMapRefType createOrganisationSchemeMapRefType() {
        return new OrganisationSchemeMapRefType();
    }

    /**
     * Create an instance of {@link LocalPrimaryMeasureReferenceType }
     * 
     */
    public LocalPrimaryMeasureReferenceType createLocalPrimaryMeasureReferenceType() {
        return new LocalPrimaryMeasureReferenceType();
    }

    /**
     * Create an instance of {@link DataStructureEnumerationSchemeRefType }
     * 
     */
    public DataStructureEnumerationSchemeRefType createDataStructureEnumerationSchemeRefType() {
        return new DataStructureEnumerationSchemeRefType();
    }

    /**
     * Create an instance of {@link LocalTargetObjectRefType }
     * 
     */
    public LocalTargetObjectRefType createLocalTargetObjectRefType() {
        return new LocalTargetObjectRefType();
    }

    /**
     * Create an instance of {@link OrganisationReferenceType }
     * 
     */
    public OrganisationReferenceType createOrganisationReferenceType() {
        return new OrganisationReferenceType();
    }

    /**
     * Create an instance of {@link StructureSpecificMetadataStructureType }
     * 
     */
    public StructureSpecificMetadataStructureType createStructureSpecificMetadataStructureType() {
        return new StructureSpecificMetadataStructureType();
    }

    /**
     * Create an instance of {@link CodelistMapReferenceType }
     * 
     */
    public CodelistMapReferenceType createCodelistMapReferenceType() {
        return new CodelistMapReferenceType();
    }

    /**
     * Create an instance of {@link ReportPeriodTargetReferenceType }
     * 
     */
    public ReportPeriodTargetReferenceType createReportPeriodTargetReferenceType() {
        return new ReportPeriodTargetReferenceType();
    }

    /**
     * Create an instance of {@link DataProviderSchemeReferenceType }
     * 
     */
    public DataProviderSchemeReferenceType createDataProviderSchemeReferenceType() {
        return new DataProviderSchemeReferenceType();
    }

    /**
     * Create an instance of {@link ProcessReferenceType }
     * 
     */
    public ProcessReferenceType createProcessReferenceType() {
        return new ProcessReferenceType();
    }

    /**
     * Create an instance of {@link StructureSpecificDataStructureType }
     * 
     */
    public StructureSpecificDataStructureType createStructureSpecificDataStructureType() {
        return new StructureSpecificDataStructureType();
    }

    /**
     * Create an instance of {@link LocalAgencyRefType }
     * 
     */
    public LocalAgencyRefType createLocalAgencyRefType() {
        return new LocalAgencyRefType();
    }

    /**
     * Create an instance of {@link LocalReportStructureRefType }
     * 
     */
    public LocalReportStructureRefType createLocalReportStructureRefType() {
        return new LocalReportStructureRefType();
    }

    /**
     * Create an instance of {@link DataStructureRequestType }
     * 
     */
    public DataStructureRequestType createDataStructureRequestType() {
        return new DataStructureRequestType();
    }

    /**
     * Create an instance of {@link DataConsumerSchemeReferenceType }
     * 
     */
    public DataConsumerSchemeReferenceType createDataConsumerSchemeReferenceType() {
        return new DataConsumerSchemeReferenceType();
    }

    /**
     * Create an instance of {@link CategorySchemeReferenceType }
     * 
     */
    public CategorySchemeReferenceType createCategorySchemeReferenceType() {
        return new CategorySchemeReferenceType();
    }

    /**
     * Create an instance of {@link TimeDimensionRefType }
     * 
     */
    public TimeDimensionRefType createTimeDimensionRefType() {
        return new TimeDimensionRefType();
    }

    /**
     * Create an instance of {@link CodelistReferenceType }
     * 
     */
    public CodelistReferenceType createCodelistReferenceType() {
        return new CodelistReferenceType();
    }

    /**
     * Create an instance of {@link HierarchicalCodeReferenceType }
     * 
     */
    public HierarchicalCodeReferenceType createHierarchicalCodeReferenceType() {
        return new HierarchicalCodeReferenceType();
    }

    /**
     * Create an instance of {@link LocalGroupKeyDescriptorReferenceType }
     * 
     */
    public LocalGroupKeyDescriptorReferenceType createLocalGroupKeyDescriptorReferenceType() {
        return new LocalGroupKeyDescriptorReferenceType();
    }

    /**
     * Create an instance of {@link LocalComponentListComponentRefType }
     * 
     */
    public LocalComponentListComponentRefType createLocalComponentListComponentRefType() {
        return new LocalComponentListComponentRefType();
    }

    /**
     * Create an instance of {@link ReportingTaxonomyRefType }
     * 
     */
    public ReportingTaxonomyRefType createReportingTaxonomyRefType() {
        return new ReportingTaxonomyRefType();
    }

    /**
     * Create an instance of {@link ItemSchemeReferenceType }
     * 
     */
    public ItemSchemeReferenceType createItemSchemeReferenceType() {
        return new ItemSchemeReferenceType();
    }

    /**
     * Create an instance of {@link AgencyReferenceType }
     * 
     */
    public AgencyReferenceType createAgencyReferenceType() {
        return new AgencyReferenceType();
    }

    /**
     * Create an instance of {@link LocalOrganisationRefType }
     * 
     */
    public LocalOrganisationRefType createLocalOrganisationRefType() {
        return new LocalOrganisationRefType();
    }

    /**
     * Create an instance of {@link MaintainableObjectTypeListType }
     * 
     */
    public MaintainableObjectTypeListType createMaintainableObjectTypeListType() {
        return new MaintainableObjectTypeListType();
    }

    /**
     * Create an instance of {@link OrganisationSchemeRefType }
     * 
     */
    public OrganisationSchemeRefType createOrganisationSchemeRefType() {
        return new OrganisationSchemeRefType();
    }

    /**
     * Create an instance of {@link MetadataAttributeRefType }
     * 
     */
    public MetadataAttributeRefType createMetadataAttributeRefType() {
        return new MetadataAttributeRefType();
    }

    /**
     * Create an instance of {@link KeyDescriptorReferenceType }
     * 
     */
    public KeyDescriptorReferenceType createKeyDescriptorReferenceType() {
        return new KeyDescriptorReferenceType();
    }

    /**
     * Create an instance of {@link ObjectRefType }
     * 
     */
    public ObjectRefType createObjectRefType() {
        return new ObjectRefType();
    }

    /**
     * Create an instance of {@link ConceptRefType }
     * 
     */
    public ConceptRefType createConceptRefType() {
        return new ConceptRefType();
    }

    /**
     * Create an instance of {@link HierarchyReferenceType }
     * 
     */
    public HierarchyReferenceType createHierarchyReferenceType() {
        return new HierarchyReferenceType();
    }

    /**
     * Create an instance of {@link MetadataflowRefType }
     * 
     */
    public MetadataflowRefType createMetadataflowRefType() {
        return new MetadataflowRefType();
    }

    /**
     * Create an instance of {@link LocalMetadataTargetReferenceType }
     * 
     */
    public LocalMetadataTargetReferenceType createLocalMetadataTargetReferenceType() {
        return new LocalMetadataTargetReferenceType();
    }

    /**
     * Create an instance of {@link SimpleValueType }
     * 
     */
    public SimpleValueType createSimpleValueType() {
        return new SimpleValueType();
    }

    /**
     * Create an instance of {@link GenericTimeSeriesDataStructureType }
     * 
     */
    public GenericTimeSeriesDataStructureType createGenericTimeSeriesDataStructureType() {
        return new GenericTimeSeriesDataStructureType();
    }

    /**
     * Create an instance of {@link StructureUsageReferenceType }
     * 
     */
    public StructureUsageReferenceType createStructureUsageReferenceType() {
        return new StructureUsageReferenceType();
    }

    /**
     * Create an instance of {@link MetadataflowReferenceType }
     * 
     */
    public MetadataflowReferenceType createMetadataflowReferenceType() {
        return new MetadataflowReferenceType();
    }

    /**
     * Create an instance of {@link ProcessStepRefType }
     * 
     */
    public ProcessStepRefType createProcessStepRefType() {
        return new ProcessStepRefType();
    }

    /**
     * Create an instance of {@link LocalCodeReferenceType }
     * 
     */
    public LocalCodeReferenceType createLocalCodeReferenceType() {
        return new LocalCodeReferenceType();
    }

    /**
     * Create an instance of {@link ConstraintTargetRefType }
     * 
     */
    public ConstraintTargetRefType createConstraintTargetRefType() {
        return new ConstraintTargetRefType();
    }

    /**
     * Create an instance of {@link SimpleKeyValueType }
     * 
     */
    public SimpleKeyValueType createSimpleKeyValueType() {
        return new SimpleKeyValueType();
    }

    /**
     * Create an instance of {@link CategorisationReferenceType }
     * 
     */
    public CategorisationReferenceType createCategorisationReferenceType() {
        return new CategorisationReferenceType();
    }

    /**
     * Create an instance of {@link ContentConstraintReferenceType }
     * 
     */
    public ContentConstraintReferenceType createContentConstraintReferenceType() {
        return new ContentConstraintReferenceType();
    }

    /**
     * Create an instance of {@link CubeRegionKeyType }
     * 
     */
    public CubeRegionKeyType createCubeRegionKeyType() {
        return new CubeRegionKeyType();
    }

    /**
     * Create an instance of {@link OrganisationRefType }
     * 
     */
    public OrganisationRefType createOrganisationRefType() {
        return new OrganisationRefType();
    }

    /**
     * Create an instance of {@link GroupKeyDescriptorReferenceType }
     * 
     */
    public GroupKeyDescriptorReferenceType createGroupKeyDescriptorReferenceType() {
        return new GroupKeyDescriptorReferenceType();
    }

    /**
     * Create an instance of {@link MetadataTargetRefType }
     * 
     */
    public MetadataTargetRefType createMetadataTargetRefType() {
        return new MetadataTargetRefType();
    }

    /**
     * Create an instance of {@link GenericDataStructureRequestType }
     * 
     */
    public GenericDataStructureRequestType createGenericDataStructureRequestType() {
        return new GenericDataStructureRequestType();
    }

    /**
     * Create an instance of {@link LocalReportStructureReferenceType }
     * 
     */
    public LocalReportStructureReferenceType createLocalReportStructureReferenceType() {
        return new LocalReportStructureReferenceType();
    }

    /**
     * Create an instance of {@link DataProviderSchemeRefType }
     * 
     */
    public DataProviderSchemeRefType createDataProviderSchemeRefType() {
        return new DataProviderSchemeRefType();
    }

    /**
     * Create an instance of {@link StructureOrUsageReferenceType }
     * 
     */
    public StructureOrUsageReferenceType createStructureOrUsageReferenceType() {
        return new StructureOrUsageReferenceType();
    }

    /**
     * Create an instance of {@link LocalAgencyReferenceType }
     * 
     */
    public LocalAgencyReferenceType createLocalAgencyReferenceType() {
        return new LocalAgencyReferenceType();
    }

    /**
     * Create an instance of {@link ReferencePeriodType }
     * 
     */
    public ReferencePeriodType createReferencePeriodType() {
        return new ReferencePeriodType();
    }

    /**
     * Create an instance of {@link MetadataTargetReferenceType }
     * 
     */
    public MetadataTargetReferenceType createMetadataTargetReferenceType() {
        return new MetadataTargetReferenceType();
    }

    /**
     * Create an instance of {@link LocalDimensionReferenceType }
     * 
     */
    public LocalDimensionReferenceType createLocalDimensionReferenceType() {
        return new LocalDimensionReferenceType();
    }

    /**
     * Create an instance of {@link TimeDimensionReferenceType }
     * 
     */
    public TimeDimensionReferenceType createTimeDimensionReferenceType() {
        return new TimeDimensionReferenceType();
    }

    /**
     * Create an instance of {@link PrimaryMeasureRefType }
     * 
     */
    public PrimaryMeasureRefType createPrimaryMeasureRefType() {
        return new PrimaryMeasureRefType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorReferenceType }
     * 
     */
    public AttributeDescriptorReferenceType createAttributeDescriptorReferenceType() {
        return new AttributeDescriptorReferenceType();
    }

    /**
     * Create an instance of {@link CategorySchemeRefType }
     * 
     */
    public CategorySchemeRefType createCategorySchemeRefType() {
        return new CategorySchemeRefType();
    }

    /**
     * Create an instance of {@link StructureSetRefType }
     * 
     */
    public StructureSetRefType createStructureSetRefType() {
        return new StructureSetRefType();
    }

    /**
     * Create an instance of {@link LocalLevelRefType }
     * 
     */
    public LocalLevelRefType createLocalLevelRefType() {
        return new LocalLevelRefType();
    }

    /**
     * Create an instance of {@link KeyDescriptorValuesTargetReferenceType }
     * 
     */
    public KeyDescriptorValuesTargetReferenceType createKeyDescriptorValuesTargetReferenceType() {
        return new KeyDescriptorValuesTargetReferenceType();
    }

    /**
     * Create an instance of {@link DataSetTargetRefType }
     * 
     */
    public DataSetTargetRefType createDataSetTargetRefType() {
        return new DataSetTargetRefType();
    }

    /**
     * Create an instance of {@link StructureUsageRefType }
     * 
     */
    public StructureUsageRefType createStructureUsageRefType() {
        return new StructureUsageRefType();
    }

    /**
     * Create an instance of {@link OrganisationUnitRefType }
     * 
     */
    public OrganisationUnitRefType createOrganisationUnitRefType() {
        return new OrganisationUnitRefType();
    }

    /**
     * Create an instance of {@link StructureMapRefType }
     * 
     */
    public StructureMapRefType createStructureMapRefType() {
        return new StructureMapRefType();
    }

    /**
     * Create an instance of {@link ProcessRefType }
     * 
     */
    public ProcessRefType createProcessRefType() {
        return new ProcessRefType();
    }

    /**
     * Create an instance of {@link CodedStatusMessageType }
     * 
     */
    public CodedStatusMessageType createCodedStatusMessageType() {
        return new CodedStatusMessageType();
    }

    /**
     * Create an instance of {@link LocalGroupKeyDescriptorRefType }
     * 
     */
    public LocalGroupKeyDescriptorRefType createLocalGroupKeyDescriptorRefType() {
        return new LocalGroupKeyDescriptorRefType();
    }

    /**
     * Create an instance of {@link HierarchicalCodelistRefType }
     * 
     */
    public HierarchicalCodelistRefType createHierarchicalCodelistRefType() {
        return new HierarchicalCodelistRefType();
    }

    /**
     * Create an instance of {@link LevelRefType }
     * 
     */
    public LevelRefType createLevelRefType() {
        return new LevelRefType();
    }

    /**
     * Create an instance of {@link PrimaryMeasureReferenceType }
     * 
     */
    public PrimaryMeasureReferenceType createPrimaryMeasureReferenceType() {
        return new PrimaryMeasureReferenceType();
    }

    /**
     * Create an instance of {@link GroupKeyDescriptorRefType }
     * 
     */
    public GroupKeyDescriptorRefType createGroupKeyDescriptorRefType() {
        return new GroupKeyDescriptorRefType();
    }

    /**
     * Create an instance of {@link TimePeriodRangeType }
     * 
     */
    public TimePeriodRangeType createTimePeriodRangeType() {
        return new TimePeriodRangeType();
    }

    /**
     * Create an instance of {@link IdentifiableObjectTargetRefType }
     * 
     */
    public IdentifiableObjectTargetRefType createIdentifiableObjectTargetRefType() {
        return new IdentifiableObjectTargetRefType();
    }

    /**
     * Create an instance of {@link ReportStructureRefType }
     * 
     */
    public ReportStructureRefType createReportStructureRefType() {
        return new ReportStructureRefType();
    }

    /**
     * Create an instance of {@link CodelistRefType }
     * 
     */
    public CodelistRefType createCodelistRefType() {
        return new CodelistRefType();
    }

    /**
     * Create an instance of {@link ProvisionAgreementRefType }
     * 
     */
    public ProvisionAgreementRefType createProvisionAgreementRefType() {
        return new ProvisionAgreementRefType();
    }

    /**
     * Create an instance of {@link LocalConceptRefType }
     * 
     */
    public LocalConceptRefType createLocalConceptRefType() {
        return new LocalConceptRefType();
    }

    /**
     * Create an instance of {@link LocalProcessStepRefType }
     * 
     */
    public LocalProcessStepRefType createLocalProcessStepRefType() {
        return new LocalProcessStepRefType();
    }

    /**
     * Create an instance of {@link CategoryRefType }
     * 
     */
    public CategoryRefType createCategoryRefType() {
        return new CategoryRefType();
    }

    /**
     * Create an instance of {@link MeasureDimensionReferenceType }
     * 
     */
    public MeasureDimensionReferenceType createMeasureDimensionReferenceType() {
        return new MeasureDimensionReferenceType();
    }

    /**
     * Create an instance of {@link ItemSchemeRefType }
     * 
     */
    public ItemSchemeRefType createItemSchemeRefType() {
        return new ItemSchemeRefType();
    }

    /**
     * Create an instance of {@link MaintainableRefType }
     * 
     */
    public MaintainableRefType createMaintainableRefType() {
        return new MaintainableRefType();
    }

    /**
     * Create an instance of {@link LocalComponentListComponentReferenceType }
     * 
     */
    public LocalComponentListComponentReferenceType createLocalComponentListComponentReferenceType() {
        return new LocalComponentListComponentReferenceType();
    }

    /**
     * Create an instance of {@link ObjectTypeListType }
     * 
     */
    public ObjectTypeListType createObjectTypeListType() {
        return new ObjectTypeListType();
    }

    /**
     * Create an instance of {@link CategoryReferenceType }
     * 
     */
    public CategoryReferenceType createCategoryReferenceType() {
        return new CategoryReferenceType();
    }

    /**
     * Create an instance of {@link LocalOrganisationReferenceType }
     * 
     */
    public LocalOrganisationReferenceType createLocalOrganisationReferenceType() {
        return new LocalOrganisationReferenceType();
    }

    /**
     * Create an instance of {@link MetadataKeyValueType }
     * 
     */
    public MetadataKeyValueType createMetadataKeyValueType() {
        return new MetadataKeyValueType();
    }

    /**
     * Create an instance of {@link StructureRefType }
     * 
     */
    public StructureRefType createStructureRefType() {
        return new StructureRefType();
    }

    /**
     * Create an instance of {@link URNReferenceType }
     * 
     */
    public URNReferenceType createURNReferenceType() {
        return new URNReferenceType();
    }

    /**
     * Create an instance of {@link StructureSpecificDataTimeSeriesStructureType }
     * 
     */
    public StructureSpecificDataTimeSeriesStructureType createStructureSpecificDataTimeSeriesStructureType() {
        return new StructureSpecificDataTimeSeriesStructureType();
    }

    /**
     * Create an instance of {@link DataflowRefType }
     * 
     */
    public DataflowRefType createDataflowRefType() {
        return new DataflowRefType();
    }

    /**
     * Create an instance of {@link MeasureDescriptorReferenceType }
     * 
     */
    public MeasureDescriptorReferenceType createMeasureDescriptorReferenceType() {
        return new MeasureDescriptorReferenceType();
    }

    /**
     * Create an instance of {@link DataConsumerRefType }
     * 
     */
    public DataConsumerRefType createDataConsumerRefType() {
        return new DataConsumerRefType();
    }

    /**
     * Create an instance of {@link ConceptReferenceType }
     * 
     */
    public ConceptReferenceType createConceptReferenceType() {
        return new ConceptReferenceType();
    }

    /**
     * Create an instance of {@link MeasureDimensionRefType }
     * 
     */
    public MeasureDimensionRefType createMeasureDimensionRefType() {
        return new MeasureDimensionRefType();
    }

    /**
     * Create an instance of {@link LocalTargetObjectReferenceType }
     * 
     */
    public LocalTargetObjectReferenceType createLocalTargetObjectReferenceType() {
        return new LocalTargetObjectReferenceType();
    }

    /**
     * Create an instance of {@link LocalComponentRefType }
     * 
     */
    public LocalComponentRefType createLocalComponentRefType() {
        return new LocalComponentRefType();
    }

    /**
     * Create an instance of {@link DataStructureRefType }
     * 
     */
    public DataStructureRefType createDataStructureRefType() {
        return new DataStructureRefType();
    }

    /**
     * Create an instance of {@link StructureSetReferenceType }
     * 
     */
    public StructureSetReferenceType createStructureSetReferenceType() {
        return new StructureSetReferenceType();
    }

    /**
     * Create an instance of {@link LocalPrimaryMeasureRefType }
     * 
     */
    public LocalPrimaryMeasureRefType createLocalPrimaryMeasureRefType() {
        return new LocalPrimaryMeasureRefType();
    }

    /**
     * Create an instance of {@link ConceptSchemeMapRefType }
     * 
     */
    public ConceptSchemeMapRefType createConceptSchemeMapRefType() {
        return new ConceptSchemeMapRefType();
    }

    /**
     * Create an instance of {@link AttachmentConstraintReferenceType }
     * 
     */
    public AttachmentConstraintReferenceType createAttachmentConstraintReferenceType() {
        return new AttachmentConstraintReferenceType();
    }

    /**
     * Create an instance of {@link LocalOrganisationUnitReferenceType }
     * 
     */
    public LocalOrganisationUnitReferenceType createLocalOrganisationUnitReferenceType() {
        return new LocalOrganisationUnitReferenceType();
    }

    /**
     * Create an instance of {@link ConceptSchemeRefType }
     * 
     */
    public ConceptSchemeRefType createConceptSchemeRefType() {
        return new ConceptSchemeRefType();
    }

    /**
     * Create an instance of {@link StructureOrUsageRefType }
     * 
     */
    public StructureOrUsageRefType createStructureOrUsageRefType() {
        return new StructureOrUsageRefType();
    }

    /**
     * Create an instance of {@link ConstraintTargetReferenceType }
     * 
     */
    public ConstraintTargetReferenceType createConstraintTargetReferenceType() {
        return new ConstraintTargetReferenceType();
    }

    /**
     * Create an instance of {@link DataKeyValueType }
     * 
     */
    public DataKeyValueType createDataKeyValueType() {
        return new DataKeyValueType();
    }

    /**
     * Create an instance of {@link LocalReportingCategoryReferenceType }
     * 
     */
    public LocalReportingCategoryReferenceType createLocalReportingCategoryReferenceType() {
        return new LocalReportingCategoryReferenceType();
    }

    /**
     * Create an instance of {@link TransitionReferenceType }
     * 
     */
    public TransitionReferenceType createTransitionReferenceType() {
        return new TransitionReferenceType();
    }

    /**
     * Create an instance of {@link LocalReportingCategoryRefType }
     * 
     */
    public LocalReportingCategoryRefType createLocalReportingCategoryRefType() {
        return new LocalReportingCategoryRefType();
    }

    /**
     * Create an instance of {@link MeasureDescriptorRefType }
     * 
     */
    public MeasureDescriptorRefType createMeasureDescriptorRefType() {
        return new MeasureDescriptorRefType();
    }

    /**
     * Create an instance of {@link AnnotationType }
     * 
     */
    public AnnotationType createAnnotationType() {
        return new AnnotationType();
    }

    /**
     * Create an instance of {@link AttachmentConstraintRefType }
     * 
     */
    public AttachmentConstraintRefType createAttachmentConstraintRefType() {
        return new AttachmentConstraintRefType();
    }

    /**
     * Create an instance of {@link LocalDimensionRefType }
     * 
     */
    public LocalDimensionRefType createLocalDimensionRefType() {
        return new LocalDimensionRefType();
    }

    /**
     * Create an instance of {@link ProcessStepReferenceType }
     * 
     */
    public ProcessStepReferenceType createProcessStepReferenceType() {
        return new ProcessStepReferenceType();
    }

    /**
     * Create an instance of {@link LocalLevelReferenceType }
     * 
     */
    public LocalLevelReferenceType createLocalLevelReferenceType() {
        return new LocalLevelReferenceType();
    }

    /**
     * Create an instance of {@link AnyCodelistRefType }
     * 
     */
    public AnyCodelistRefType createAnyCodelistRefType() {
        return new AnyCodelistRefType();
    }

    /**
     * Create an instance of {@link StructureMapReferenceType }
     * 
     */
    public StructureMapReferenceType createStructureMapReferenceType() {
        return new StructureMapReferenceType();
    }

    /**
     * Create an instance of {@link SetReferenceType }
     * 
     */
    public SetReferenceType createSetReferenceType() {
        return new SetReferenceType();
    }

    /**
     * Create an instance of {@link AnyLocalCodeRefType }
     * 
     */
    public AnyLocalCodeRefType createAnyLocalCodeRefType() {
        return new AnyLocalCodeRefType();
    }

    /**
     * Create an instance of {@link LocalMetadataTargetRefType }
     * 
     */
    public LocalMetadataTargetRefType createLocalMetadataTargetRefType() {
        return new LocalMetadataTargetRefType();
    }

    /**
     * Create an instance of {@link ContentConstraintRefType }
     * 
     */
    public ContentConstraintRefType createContentConstraintRefType() {
        return new ContentConstraintRefType();
    }

    /**
     * Create an instance of {@link ReportCategoryRefType }
     * 
     */
    public ReportCategoryRefType createReportCategoryRefType() {
        return new ReportCategoryRefType();
    }

    /**
     * Create an instance of {@link DataConsumerSchemeRefType }
     * 
     */
    public DataConsumerSchemeRefType createDataConsumerSchemeRefType() {
        return new DataConsumerSchemeRefType();
    }

    /**
     * Create an instance of {@link StructureReferenceType }
     * 
     */
    public StructureReferenceType createStructureReferenceType() {
        return new StructureReferenceType();
    }

    /**
     * Create an instance of {@link CodelistMapRefType }
     * 
     */
    public CodelistMapRefType createCodelistMapRefType() {
        return new CodelistMapRefType();
    }

    /**
     * Create an instance of {@link ReportingCategoryReferenceType }
     * 
     */
    public ReportingCategoryReferenceType createReportingCategoryReferenceType() {
        return new ReportingCategoryReferenceType();
    }

    /**
     * Create an instance of {@link HierarchicalCodeRefType }
     * 
     */
    public HierarchicalCodeRefType createHierarchicalCodeRefType() {
        return new HierarchicalCodeRefType();
    }

    /**
     * Create an instance of {@link DataProviderRefType }
     * 
     */
    public DataProviderRefType createDataProviderRefType() {
        return new DataProviderRefType();
    }

    /**
     * Create an instance of {@link OrganisationSchemeMapReferenceType }
     * 
     */
    public OrganisationSchemeMapReferenceType createOrganisationSchemeMapReferenceType() {
        return new OrganisationSchemeMapReferenceType();
    }

    /**
     * Create an instance of {@link CodeRefType }
     * 
     */
    public CodeRefType createCodeRefType() {
        return new CodeRefType();
    }

    /**
     * Create an instance of {@link LocalMetadataStructureComponentReferenceType }
     * 
     */
    public LocalMetadataStructureComponentReferenceType createLocalMetadataStructureComponentReferenceType() {
        return new LocalMetadataStructureComponentReferenceType();
    }

    /**
     * Create an instance of {@link DataConsumerReferenceType }
     * 
     */
    public DataConsumerReferenceType createDataConsumerReferenceType() {
        return new DataConsumerReferenceType();
    }

    /**
     * Create an instance of {@link IdentifiableObjectTargetReferenceType }
     * 
     */
    public IdentifiableObjectTargetReferenceType createIdentifiableObjectTargetReferenceType() {
        return new IdentifiableObjectTargetReferenceType();
    }

    /**
     * Create an instance of {@link DataKeyType }
     * 
     */
    public DataKeyType createDataKeyType() {
        return new DataKeyType();
    }

    /**
     * Create an instance of {@link DataSetTargetReferenceType }
     * 
     */
    public DataSetTargetReferenceType createDataSetTargetReferenceType() {
        return new DataSetTargetReferenceType();
    }

    /**
     * Create an instance of {@link KeyDescriptorRefType }
     * 
     */
    public KeyDescriptorRefType createKeyDescriptorRefType() {
        return new KeyDescriptorRefType();
    }

    /**
     * Create an instance of {@link OrganisationSchemeReferenceType }
     * 
     */
    public OrganisationSchemeReferenceType createOrganisationSchemeReferenceType() {
        return new OrganisationSchemeReferenceType();
    }

    /**
     * Create an instance of {@link QueryableDataSourceType }
     * 
     */
    public QueryableDataSourceType createQueryableDataSourceType() {
        return new QueryableDataSourceType();
    }

    /**
     * Create an instance of {@link MetadataAttributeReferenceType }
     * 
     */
    public MetadataAttributeReferenceType createMetadataAttributeReferenceType() {
        return new MetadataAttributeReferenceType();
    }

    /**
     * Create an instance of {@link LocalDataStructureComponentRefType }
     * 
     */
    public LocalDataStructureComponentRefType createLocalDataStructureComponentRefType() {
        return new LocalDataStructureComponentRefType();
    }

    /**
     * Create an instance of {@link TimeSeriesDataStructureRequestType }
     * 
     */
    public TimeSeriesDataStructureRequestType createTimeSeriesDataStructureRequestType() {
        return new TimeSeriesDataStructureRequestType();
    }

    /**
     * Create an instance of {@link KeyDescriptorValuesTargetRefType }
     * 
     */
    public KeyDescriptorValuesTargetRefType createKeyDescriptorValuesTargetRefType() {
        return new KeyDescriptorValuesTargetRefType();
    }

    /**
     * Create an instance of {@link LocalCodeRefType }
     * 
     */
    public LocalCodeRefType createLocalCodeRefType() {
        return new LocalCodeRefType();
    }

    /**
     * Create an instance of {@link LevelReferenceType }
     * 
     */
    public LevelReferenceType createLevelReferenceType() {
        return new LevelReferenceType();
    }

    /**
     * Create an instance of {@link DataProviderReferenceType }
     * 
     */
    public DataProviderReferenceType createDataProviderReferenceType() {
        return new DataProviderReferenceType();
    }

    /**
     * Create an instance of {@link LocalConceptReferenceType }
     * 
     */
    public LocalConceptReferenceType createLocalConceptReferenceType() {
        return new LocalConceptReferenceType();
    }

    /**
     * Create an instance of {@link MetadataKeyType }
     * 
     */
    public MetadataKeyType createMetadataKeyType() {
        return new MetadataKeyType();
    }

    /**
     * Create an instance of {@link LocalDataProviderReferenceType }
     * 
     */
    public LocalDataProviderReferenceType createLocalDataProviderReferenceType() {
        return new LocalDataProviderReferenceType();
    }

    /**
     * Create an instance of {@link DataStructureEnumerationSchemeReferenceType }
     * 
     */
    public DataStructureEnumerationSchemeReferenceType createDataStructureEnumerationSchemeReferenceType() {
        return new DataStructureEnumerationSchemeReferenceType();
    }

    /**
     * Create an instance of {@link LocalProcessStepReferenceType }
     * 
     */
    public LocalProcessStepReferenceType createLocalProcessStepReferenceType() {
        return new LocalProcessStepReferenceType();
    }

    /**
     * Create an instance of {@link LocalCategoryReferenceType }
     * 
     */
    public LocalCategoryReferenceType createLocalCategoryReferenceType() {
        return new LocalCategoryReferenceType();
    }

    /**
     * Create an instance of {@link CategorySchemeMapRefType }
     * 
     */
    public CategorySchemeMapRefType createCategorySchemeMapRefType() {
        return new CategorySchemeMapRefType();
    }

    /**
     * Create an instance of {@link DataStructureReferenceType }
     * 
     */
    public DataStructureReferenceType createDataStructureReferenceType() {
        return new DataStructureReferenceType();
    }

    /**
     * Create an instance of {@link MetadataStructureRefType }
     * 
     */
    public MetadataStructureRefType createMetadataStructureRefType() {
        return new MetadataStructureRefType();
    }

    /**
     * Create an instance of {@link GenericDataStructureType }
     * 
     */
    public GenericDataStructureType createGenericDataStructureType() {
        return new GenericDataStructureType();
    }

    /**
     * Create an instance of {@link AttributeRefType }
     * 
     */
    public AttributeRefType createAttributeRefType() {
        return new AttributeRefType();
    }

    /**
     * Create an instance of {@link LocalDataStructureComponentReferenceType }
     * 
     */
    public LocalDataStructureComponentReferenceType createLocalDataStructureComponentReferenceType() {
        return new LocalDataStructureComponentReferenceType();
    }

    /**
     * Create an instance of {@link ObjectReferenceType }
     * 
     */
    public ObjectReferenceType createObjectReferenceType() {
        return new ObjectReferenceType();
    }

    /**
     * Create an instance of {@link LocalComponentReferenceType }
     * 
     */
    public LocalComponentReferenceType createLocalComponentReferenceType() {
        return new LocalComponentReferenceType();
    }

    /**
     * Create an instance of {@link StatusMessageType }
     * 
     */
    public StatusMessageType createStatusMessageType() {
        return new StatusMessageType();
    }

    /**
     * Create an instance of {@link AnyCodelistReferenceType }
     * 
     */
    public AnyCodelistReferenceType createAnyCodelistReferenceType() {
        return new AnyCodelistReferenceType();
    }

    /**
     * Create an instance of {@link OrganisationUnitSchemeReferenceType }
     * 
     */
    public OrganisationUnitSchemeReferenceType createOrganisationUnitSchemeReferenceType() {
        return new OrganisationUnitSchemeReferenceType();
    }

    /**
     * Create an instance of {@link ConstraintReferenceType }
     * 
     */
    public ConstraintReferenceType createConstraintReferenceType() {
        return new ConstraintReferenceType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorRefType }
     * 
     */
    public AttributeDescriptorRefType createAttributeDescriptorRefType() {
        return new AttributeDescriptorRefType();
    }

    /**
     * Create an instance of {@link ReportPeriodTargetRefType }
     * 
     */
    public ReportPeriodTargetRefType createReportPeriodTargetRefType() {
        return new ReportPeriodTargetRefType();
    }

    /**
     * Create an instance of {@link LocalDataProviderRefType }
     * 
     */
    public LocalDataProviderRefType createLocalDataProviderRefType() {
        return new LocalDataProviderRefType();
    }

    /**
     * Create an instance of {@link AgencyRefType }
     * 
     */
    public AgencyRefType createAgencyRefType() {
        return new AgencyRefType();
    }

    /**
     * Create an instance of {@link CategorisationRefType }
     * 
     */
    public CategorisationRefType createCategorisationRefType() {
        return new CategorisationRefType();
    }

    /**
     * Create an instance of {@link ConceptSchemeReferenceType }
     * 
     */
    public ConceptSchemeReferenceType createConceptSchemeReferenceType() {
        return new ConceptSchemeReferenceType();
    }

    /**
     * Create an instance of {@link CategorySchemeMapReferenceType }
     * 
     */
    public CategorySchemeMapReferenceType createCategorySchemeMapReferenceType() {
        return new CategorySchemeMapReferenceType();
    }

    /**
     * Create an instance of {@link OrganisationUnitSchemeRefType }
     * 
     */
    public OrganisationUnitSchemeRefType createOrganisationUnitSchemeRefType() {
        return new OrganisationUnitSchemeRefType();
    }

    /**
     * Create an instance of {@link OrganisationUnitReferenceType }
     * 
     */
    public OrganisationUnitReferenceType createOrganisationUnitReferenceType() {
        return new OrganisationUnitReferenceType();
    }

    /**
     * Create an instance of {@link LocalOrganisationUnitRefType }
     * 
     */
    public LocalOrganisationUnitRefType createLocalOrganisationUnitRefType() {
        return new LocalOrganisationUnitRefType();
    }

    /**
     * Create an instance of {@link DimensionReferenceType }
     * 
     */
    public DimensionReferenceType createDimensionReferenceType() {
        return new DimensionReferenceType();
    }

    /**
     * Create an instance of {@link MetadataAttributeValueSetType }
     * 
     */
    public MetadataAttributeValueSetType createMetadataAttributeValueSetType() {
        return new MetadataAttributeValueSetType();
    }

    /**
     * Create an instance of {@link MaintainableReferenceType }
     * 
     */
    public MaintainableReferenceType createMaintainableReferenceType() {
        return new MaintainableReferenceType();
    }

    /**
     * Create an instance of {@link CubeRegionType }
     * 
     */
    public CubeRegionType createCubeRegionType() {
        return new CubeRegionType();
    }

    /**
     * Create an instance of {@link AgencySchemeReferenceType }
     * 
     */
    public AgencySchemeReferenceType createAgencySchemeReferenceType() {
        return new AgencySchemeReferenceType();
    }

    /**
     * Create an instance of {@link TimeRangeValueType }
     * 
     */
    public TimeRangeValueType createTimeRangeValueType() {
        return new TimeRangeValueType();
    }

    /**
     * Create an instance of {@link TransitionRefType }
     * 
     */
    public TransitionRefType createTransitionRefType() {
        return new TransitionRefType();
    }

    /**
     * Create an instance of {@link MetadataTargetRegionKeyType }
     * 
     */
    public MetadataTargetRegionKeyType createMetadataTargetRegionKeyType() {
        return new MetadataTargetRegionKeyType();
    }

    /**
     * Create an instance of {@link TimeSeriesGenericDataStructureRequestType }
     * 
     */
    public TimeSeriesGenericDataStructureRequestType createTimeSeriesGenericDataStructureRequestType() {
        return new TimeSeriesGenericDataStructureRequestType();
    }

    /**
     * Create an instance of {@link AgencySchemeRefType }
     * 
     */
    public AgencySchemeRefType createAgencySchemeRefType() {
        return new AgencySchemeRefType();
    }

    /**
     * Create an instance of {@link MetadataStructureReferenceType }
     * 
     */
    public MetadataStructureReferenceType createMetadataStructureReferenceType() {
        return new MetadataStructureReferenceType();
    }

    /**
     * Create an instance of {@link DataflowReferenceType }
     * 
     */
    public DataflowReferenceType createDataflowReferenceType() {
        return new DataflowReferenceType();
    }

    /**
     * Create an instance of {@link ReportingTaxonomyReferenceType }
     * 
     */
    public ReportingTaxonomyReferenceType createReportingTaxonomyReferenceType() {
        return new ReportingTaxonomyReferenceType();
    }

    /**
     * Create an instance of {@link ReportStructureReferenceType }
     * 
     */
    public ReportStructureReferenceType createReportStructureReferenceType() {
        return new ReportStructureReferenceType();
    }

    /**
     * Create an instance of {@link LocalMetadataStructureComponentRefType }
     * 
     */
    public LocalMetadataStructureComponentRefType createLocalMetadataStructureComponentRefType() {
        return new LocalMetadataStructureComponentRefType();
    }

    /**
     * Create an instance of {@link CodeReferenceType }
     * 
     */
    public CodeReferenceType createCodeReferenceType() {
        return new CodeReferenceType();
    }

    /**
     * Create an instance of {@link AttributeValueSetType }
     * 
     */
    public AttributeValueSetType createAttributeValueSetType() {
        return new AttributeValueSetType();
    }

    /**
     * Create an instance of {@link AnyLocalCodeReferenceType }
     * 
     */
    public AnyLocalCodeReferenceType createAnyLocalCodeReferenceType() {
        return new AnyLocalCodeReferenceType();
    }

    /**
     * Create an instance of {@link AttributeReferenceType }
     * 
     */
    public AttributeReferenceType createAttributeReferenceType() {
        return new AttributeReferenceType();
    }

    /**
     * Create an instance of {@link LocalDataConsumerReferenceType }
     * 
     */
    public LocalDataConsumerReferenceType createLocalDataConsumerReferenceType() {
        return new LocalDataConsumerReferenceType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "PrimaryMeasure")
    public JAXBElement<EmptyType> createPrimaryMeasure(EmptyType value) {
        return new JAXBElement<EmptyType>(_PrimaryMeasure_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ProcessStep")
    public JAXBElement<EmptyType> createProcessStep(EmptyType value) {
        return new JAXBElement<EmptyType>(_ProcessStep_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "GroupDimensionDescriptor")
    public JAXBElement<EmptyType> createGroupDimensionDescriptor(EmptyType value) {
        return new JAXBElement<EmptyType>(_GroupDimensionDescriptor_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "DataConsumer")
    public JAXBElement<EmptyType> createDataConsumer(EmptyType value) {
        return new JAXBElement<EmptyType>(_DataConsumer_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Agency")
    public JAXBElement<EmptyType> createAgency(EmptyType value) {
        return new JAXBElement<EmptyType>(_Agency_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ConceptMap")
    public JAXBElement<EmptyType> createConceptMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_ConceptMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "MeasureDescriptor")
    public JAXBElement<EmptyType> createMeasureDescriptor(EmptyType value) {
        return new JAXBElement<EmptyType>(_MeasureDescriptor_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "MetadataTarget")
    public JAXBElement<EmptyType> createMetadataTarget(EmptyType value) {
        return new JAXBElement<EmptyType>(_MetadataTarget_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Name")
    public JAXBElement<TextType> createName(TextType value) {
        return new JAXBElement<TextType>(_Name_QNAME, TextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Process")
    public JAXBElement<EmptyType> createProcess(EmptyType value) {
        return new JAXBElement<EmptyType>(_Process_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "AgencyScheme")
    public JAXBElement<EmptyType> createAgencyScheme(EmptyType value) {
        return new JAXBElement<EmptyType>(_AgencyScheme_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "StructureSet")
    public JAXBElement<EmptyType> createStructureSet(EmptyType value) {
        return new JAXBElement<EmptyType>(_StructureSet_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "DimensionDescriptorValuesTarget")
    public JAXBElement<EmptyType> createDimensionDescriptorValuesTarget(EmptyType value) {
        return new JAXBElement<EmptyType>(_DimensionDescriptorValuesTarget_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "IdentifiableObjectTarget")
    public JAXBElement<EmptyType> createIdentifiableObjectTarget(EmptyType value) {
        return new JAXBElement<EmptyType>(_IdentifiableObjectTarget_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "MeasureDimension")
    public JAXBElement<EmptyType> createMeasureDimension(EmptyType value) {
        return new JAXBElement<EmptyType>(_MeasureDimension_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Attribute")
    public JAXBElement<EmptyType> createAttribute(EmptyType value) {
        return new JAXBElement<EmptyType>(_Attribute_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ConstraintTarget")
    public JAXBElement<EmptyType> createConstraintTarget(EmptyType value) {
        return new JAXBElement<EmptyType>(_ConstraintTarget_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Metadataflow")
    public JAXBElement<EmptyType> createMetadataflow(EmptyType value) {
        return new JAXBElement<EmptyType>(_Metadataflow_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "DimensionDescriptor")
    public JAXBElement<EmptyType> createDimensionDescriptor(EmptyType value) {
        return new JAXBElement<EmptyType>(_DimensionDescriptor_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "DataStructure")
    public JAXBElement<EmptyType> createDataStructure(EmptyType value) {
        return new JAXBElement<EmptyType>(_DataStructure_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "HierarchicalCodelist")
    public JAXBElement<EmptyType> createHierarchicalCodelist(EmptyType value) {
        return new JAXBElement<EmptyType>(_HierarchicalCodelist_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ConceptScheme")
    public JAXBElement<EmptyType> createConceptScheme(EmptyType value) {
        return new JAXBElement<EmptyType>(_ConceptScheme_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "AttributeDescriptor")
    public JAXBElement<EmptyType> createAttributeDescriptor(EmptyType value) {
        return new JAXBElement<EmptyType>(_AttributeDescriptor_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ConceptSchemeMap")
    public JAXBElement<EmptyType> createConceptSchemeMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_ConceptSchemeMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Codelist")
    public JAXBElement<EmptyType> createCodelist(EmptyType value) {
        return new JAXBElement<EmptyType>(_Codelist_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "HierarchicalCode")
    public JAXBElement<EmptyType> createHierarchicalCode(EmptyType value) {
        return new JAXBElement<EmptyType>(_HierarchicalCode_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "CodelistMap")
    public JAXBElement<EmptyType> createCodelistMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_CodelistMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Code")
    public JAXBElement<EmptyType> createCode(EmptyType value) {
        return new JAXBElement<EmptyType>(_Code_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "DataSetTarget")
    public JAXBElement<EmptyType> createDataSetTarget(EmptyType value) {
        return new JAXBElement<EmptyType>(_DataSetTarget_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Categorisation")
    public JAXBElement<EmptyType> createCategorisation(EmptyType value) {
        return new JAXBElement<EmptyType>(_Categorisation_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "DataProvider")
    public JAXBElement<EmptyType> createDataProvider(EmptyType value) {
        return new JAXBElement<EmptyType>(_DataProvider_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Any")
    public JAXBElement<EmptyType> createAny(EmptyType value) {
        return new JAXBElement<EmptyType>(_Any_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Category")
    public JAXBElement<EmptyType> createCategory(EmptyType value) {
        return new JAXBElement<EmptyType>(_Category_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "OrganisationMap")
    public JAXBElement<EmptyType> createOrganisationMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_OrganisationMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XHTMLType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "StructuredText")
    public JAXBElement<XHTMLType> createStructuredText(XHTMLType value) {
        return new JAXBElement<XHTMLType>(_StructuredText_QNAME, XHTMLType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "MetadataStructure")
    public JAXBElement<EmptyType> createMetadataStructure(EmptyType value) {
        return new JAXBElement<EmptyType>(_MetadataStructure_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ReportPeriodTarget")
    public JAXBElement<EmptyType> createReportPeriodTarget(EmptyType value) {
        return new JAXBElement<EmptyType>(_ReportPeriodTarget_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "TimeDimension")
    public JAXBElement<EmptyType> createTimeDimension(EmptyType value) {
        return new JAXBElement<EmptyType>(_TimeDimension_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ReportingCategory")
    public JAXBElement<EmptyType> createReportingCategory(EmptyType value) {
        return new JAXBElement<EmptyType>(_ReportingCategory_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Hierarchy")
    public JAXBElement<EmptyType> createHierarchy(EmptyType value) {
        return new JAXBElement<EmptyType>(_Hierarchy_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ReportingTaxonomyMap")
    public JAXBElement<EmptyType> createReportingTaxonomyMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_ReportingTaxonomyMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "DataProviderScheme")
    public JAXBElement<EmptyType> createDataProviderScheme(EmptyType value) {
        return new JAXBElement<EmptyType>(_DataProviderScheme_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "MetadataSet")
    public JAXBElement<EmptyType> createMetadataSet(EmptyType value) {
        return new JAXBElement<EmptyType>(_MetadataSet_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ContentConstraint")
    public JAXBElement<EmptyType> createContentConstraint(EmptyType value) {
        return new JAXBElement<EmptyType>(_ContentConstraint_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "OrganisationUnitScheme")
    public JAXBElement<EmptyType> createOrganisationUnitScheme(EmptyType value) {
        return new JAXBElement<EmptyType>(_OrganisationUnitScheme_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "CategorySchemeMap")
    public JAXBElement<EmptyType> createCategorySchemeMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_CategorySchemeMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "StructureMap")
    public JAXBElement<EmptyType> createStructureMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_StructureMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Concept")
    public JAXBElement<EmptyType> createConcept(EmptyType value) {
        return new JAXBElement<EmptyType>(_Concept_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "AttachmentConstraint")
    public JAXBElement<EmptyType> createAttachmentConstraint(EmptyType value) {
        return new JAXBElement<EmptyType>(_AttachmentConstraint_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "CodeMap")
    public JAXBElement<EmptyType> createCodeMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_CodeMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "OrganisationSchemeMap")
    public JAXBElement<EmptyType> createOrganisationSchemeMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_OrganisationSchemeMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "MetadataAttribute")
    public JAXBElement<EmptyType> createMetadataAttribute(EmptyType value) {
        return new JAXBElement<EmptyType>(_MetadataAttribute_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Description")
    public JAXBElement<TextType> createDescription(TextType value) {
        return new JAXBElement<TextType>(_Description_QNAME, TextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Dataflow")
    public JAXBElement<EmptyType> createDataflow(EmptyType value) {
        return new JAXBElement<EmptyType>(_Dataflow_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Transition")
    public JAXBElement<EmptyType> createTransition(EmptyType value) {
        return new JAXBElement<EmptyType>(_Transition_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ProvisionAgreement")
    public JAXBElement<EmptyType> createProvisionAgreement(EmptyType value) {
        return new JAXBElement<EmptyType>(_ProvisionAgreement_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ReportingTaxonomy")
    public JAXBElement<EmptyType> createReportingTaxonomy(EmptyType value) {
        return new JAXBElement<EmptyType>(_ReportingTaxonomy_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Text")
    public JAXBElement<TextType> createText(TextType value) {
        return new JAXBElement<TextType>(_Text_QNAME, TextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Dimension")
    public JAXBElement<EmptyType> createDimension(EmptyType value) {
        return new JAXBElement<EmptyType>(_Dimension_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "CategoryScheme")
    public JAXBElement<EmptyType> createCategoryScheme(EmptyType value) {
        return new JAXBElement<EmptyType>(_CategoryScheme_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnnotationsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Annotations")
    public JAXBElement<AnnotationsType> createAnnotations(AnnotationsType value) {
        return new JAXBElement<AnnotationsType>(_Annotations_QNAME, AnnotationsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "HybridCodeMap")
    public JAXBElement<EmptyType> createHybridCodeMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_HybridCodeMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "Level")
    public JAXBElement<EmptyType> createLevel(EmptyType value) {
        return new JAXBElement<EmptyType>(_Level_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "DataConsumerScheme")
    public JAXBElement<EmptyType> createDataConsumerScheme(EmptyType value) {
        return new JAXBElement<EmptyType>(_DataConsumerScheme_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ComponentMap")
    public JAXBElement<EmptyType> createComponentMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_ComponentMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "HybridCodelistMap")
    public JAXBElement<EmptyType> createHybridCodelistMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_HybridCodelistMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ReportStructure")
    public JAXBElement<EmptyType> createReportStructure(EmptyType value) {
        return new JAXBElement<EmptyType>(_ReportStructure_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "ReportingCategoryMap")
    public JAXBElement<EmptyType> createReportingCategoryMap(EmptyType value) {
        return new JAXBElement<EmptyType>(_ReportingCategoryMap_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", name = "OrganisationUnit")
    public JAXBElement<EmptyType> createOrganisationUnit(EmptyType value) {
        return new JAXBElement<EmptyType>(_OrganisationUnit_QNAME, EmptyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "URN", scope = ReferenceType.class)
    public JAXBElement<String> createReferenceTypeURN(String value) {
        return new JAXBElement<String>(_ReferenceTypeURN_QNAME, String.class, ReferenceType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RefBaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Ref", scope = ReferenceType.class)
    public JAXBElement<RefBaseType> createReferenceTypeRef(RefBaseType value) {
        return new JAXBElement<RefBaseType>(_ReferenceTypeRef_QNAME, RefBaseType.class, ReferenceType.class, value);
    }

}
