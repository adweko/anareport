
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureUsageReferenceType;


/**
 * ProvisionAgreementWhereType defines the parameters of a provision agreement query. All supplied parameters must be matched in order for an object to satisfy the query.
 * 
 * <p>Java-Klasse f�r ProvisionAgreementWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProvisionAgreementWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ProvisionAgreementWhereBaseType">
 *       &lt;sequence>
 *         &lt;element name="StructureUsage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureUsageReferenceType" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvisionAgreementWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "structureUsage",
    "dataProvider"
})
public class ProvisionAgreementWhereType
    extends ProvisionAgreementWhereBaseType
{

    @XmlElement(name = "StructureUsage", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected StructureUsageReferenceType structureUsage;
    @XmlElement(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected DataProviderReferenceType dataProvider;

    /**
     * Ruft den Wert der structureUsage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureUsageReferenceType }
     *     
     */
    public StructureUsageReferenceType getStructureUsage() {
        return structureUsage;
    }

    /**
     * Legt den Wert der structureUsage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureUsageReferenceType }
     *     
     */
    public void setStructureUsage(StructureUsageReferenceType value) {
        this.structureUsage = value;
    }

    /**
     * Ruft den Wert der dataProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public DataProviderReferenceType getDataProvider() {
        return dataProvider;
    }

    /**
     * Legt den Wert der dataProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public void setDataProvider(DataProviderReferenceType value) {
        this.dataProvider = value;
    }

}
