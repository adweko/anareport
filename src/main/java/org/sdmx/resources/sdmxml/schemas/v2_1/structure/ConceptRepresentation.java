
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * ConceptRepresentation defines the core representation that are allowed for a concept. The text format allowed for a concept is that which is allowed for any non-target object component.
 * 
 * <p>Java-Klasse f�r ConceptRepresentation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConceptRepresentation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}RepresentationType">
 *       &lt;choice>
 *         &lt;element name="TextFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}BasicComponentTextFormatType"/>
 *         &lt;sequence>
 *           &lt;element name="Enumeration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistReferenceType"/>
 *           &lt;element name="EnumerationFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodededTextFormatType" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConceptRepresentation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class ConceptRepresentation
    extends RepresentationType
{


}
