
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DataStructureQueryType defines the structure of a data structure definition query. The parameters for the query are contained in the DataStructureWhere element. The References element is used to indicate how objects that reference or are referenced from the matched data structure definition should be returned.
 * 
 * <p>Java-Klasse f�r DataStructureQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataStructureQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructuralMetadataQueryType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableReturnDetailsType"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataStructureWhere"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataStructureQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class DataStructureQueryType
    extends StructuralMetadataQueryType
{


}
