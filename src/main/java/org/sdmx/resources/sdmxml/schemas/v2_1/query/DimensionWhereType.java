
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ConceptReferenceType;


/**
 * DimensionWhereType describes the structure of a dimension query. A dimension can be queried based on its identification, the concept from which it takes its semantic, the role it plays, and the code list it uses as the enumeration of its representation. This is an implicit set of "and" parameters, that is the conditions within this must all be met in order to return a match.
 * 
 * <p>Java-Klasse f�r DimensionWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DimensionWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataStructureComponentWhereType">
 *       &lt;sequence>
 *         &lt;element name="Role" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DimensionWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "role"
})
public class DimensionWhereType
    extends DataStructureComponentWhereType
{

    @XmlElement(name = "Role", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<ConceptReferenceType> role;

    /**
     * Gets the value of the role property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the role property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConceptReferenceType }
     * 
     * 
     */
    public List<ConceptReferenceType> getRole() {
        if (role == null) {
            role = new ArrayList<ConceptReferenceType>();
        }
        return this.role;
    }

}
