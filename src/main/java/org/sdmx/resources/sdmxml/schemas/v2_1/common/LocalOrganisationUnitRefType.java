
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * LocalOrganisationUnitRefType references an organisation unit locally where the reference to the organisation unit scheme which defines it is provided elsewhere.
 * 
 * <p>Java-Klasse f�r LocalOrganisationUnitRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LocalOrganisationUnitRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalOrganisationRefBaseType">
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationTypeCodelistType" fixed="OrganisationUnit" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocalOrganisationUnitRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class LocalOrganisationUnitRefType
    extends LocalOrganisationRefBaseType
{


}
