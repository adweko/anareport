
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CodedStatusMessageType;


/**
 * ErrorType describes the structure of an error response.
 * 
 * <p>Java-Klasse f�r ErrorType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErrorType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ErrorMessage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodedStatusMessageType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", propOrder = {
    "errorMessage"
})
public class ErrorType {

    @XmlElement(name = "ErrorMessage", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", required = true)
    protected List<CodedStatusMessageType> errorMessage;

    /**
     * Gets the value of the errorMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodedStatusMessageType }
     * 
     * 
     */
    public List<CodedStatusMessageType> getErrorMessage() {
        if (errorMessage == null) {
            errorMessage = new ArrayList<CodedStatusMessageType>();
        }
        return this.errorMessage;
    }

}
