
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AttachmentConstraintReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CategoryReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TimeDataType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TimeRangeValueType;


/**
 * DataParametersType defines the parameters for querying for data. This structure is refined by separate And/Or constructs which make logical restrictions on which parameters apply in such cases.
 * 
 * <p>Java-Klasse f�r DataParametersType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataParametersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataSetID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataStructureReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Dataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataflowReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Updated" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="ConceptValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ConceptValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RepresentationValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}CodeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DimensionValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DimensionValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TimeDimensionValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeDimensionValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttributeValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}AttributeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PrimaryMeasureValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}PrimaryMeasureValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraintReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TimeFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeDataType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Or" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersOrType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="And" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataParametersAndType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataParametersType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "dataSetID",
    "dataProvider",
    "dataStructure",
    "dataflow",
    "provisionAgreement",
    "category",
    "updated",
    "conceptValue",
    "representationValue",
    "dimensionValue",
    "timeDimensionValue",
    "attributeValue",
    "primaryMeasureValue",
    "attachmentConstraint",
    "timeFormat",
    "or",
    "and"
})
@XmlSeeAlso({
    DataParametersOrType.class,
    DataParametersAndType.class
})
public abstract class DataParametersType {

    @XmlElement(name = "DataSetID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<QueryIDType> dataSetID;
    @XmlElement(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<DataProviderReferenceType> dataProvider;
    @XmlElement(name = "DataStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<DataStructureReferenceType> dataStructure;
    @XmlElement(name = "Dataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<DataflowReferenceType> dataflow;
    @XmlElement(name = "ProvisionAgreement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<ProvisionAgreementReferenceType> provisionAgreement;
    @XmlElement(name = "Category", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<CategoryReferenceType> category;
    @XmlElement(name = "Updated", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<TimeRangeValueType> updated;
    @XmlElement(name = "ConceptValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<ConceptValueType> conceptValue;
    @XmlElement(name = "RepresentationValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<CodeValueType> representationValue;
    @XmlElement(name = "DimensionValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<DimensionValueType> dimensionValue;
    @XmlElement(name = "TimeDimensionValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<TimeDimensionValueType> timeDimensionValue;
    @XmlElement(name = "AttributeValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<AttributeValueType> attributeValue;
    @XmlElement(name = "PrimaryMeasureValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<PrimaryMeasureValueType> primaryMeasureValue;
    @XmlElement(name = "AttachmentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<AttachmentConstraintReferenceType> attachmentConstraint;
    @XmlElement(name = "TimeFormat", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    @XmlSchemaType(name = "NMTOKEN")
    protected List<TimeDataType> timeFormat;
    @XmlElement(name = "Or", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<DataParametersOrType> or;
    @XmlElement(name = "And", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<DataParametersAndType> and;

    /**
     * Gets the value of the dataSetID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataSetID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataSetID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryIDType }
     * 
     * 
     */
    public List<QueryIDType> getDataSetID() {
        if (dataSetID == null) {
            dataSetID = new ArrayList<QueryIDType>();
        }
        return this.dataSetID;
    }

    /**
     * Gets the value of the dataProvider property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataProvider property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataProvider().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataProviderReferenceType }
     * 
     * 
     */
    public List<DataProviderReferenceType> getDataProvider() {
        if (dataProvider == null) {
            dataProvider = new ArrayList<DataProviderReferenceType>();
        }
        return this.dataProvider;
    }

    /**
     * Gets the value of the dataStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataStructureReferenceType }
     * 
     * 
     */
    public List<DataStructureReferenceType> getDataStructure() {
        if (dataStructure == null) {
            dataStructure = new ArrayList<DataStructureReferenceType>();
        }
        return this.dataStructure;
    }

    /**
     * Gets the value of the dataflow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataflow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataflow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataflowReferenceType }
     * 
     * 
     */
    public List<DataflowReferenceType> getDataflow() {
        if (dataflow == null) {
            dataflow = new ArrayList<DataflowReferenceType>();
        }
        return this.dataflow;
    }

    /**
     * Gets the value of the provisionAgreement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the provisionAgreement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProvisionAgreement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProvisionAgreementReferenceType }
     * 
     * 
     */
    public List<ProvisionAgreementReferenceType> getProvisionAgreement() {
        if (provisionAgreement == null) {
            provisionAgreement = new ArrayList<ProvisionAgreementReferenceType>();
        }
        return this.provisionAgreement;
    }

    /**
     * Gets the value of the category property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the category property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryReferenceType }
     * 
     * 
     */
    public List<CategoryReferenceType> getCategory() {
        if (category == null) {
            category = new ArrayList<CategoryReferenceType>();
        }
        return this.category;
    }

    /**
     * Gets the value of the updated property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updated property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdated().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TimeRangeValueType }
     * 
     * 
     */
    public List<TimeRangeValueType> getUpdated() {
        if (updated == null) {
            updated = new ArrayList<TimeRangeValueType>();
        }
        return this.updated;
    }

    /**
     * Gets the value of the conceptValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conceptValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConceptValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConceptValueType }
     * 
     * 
     */
    public List<ConceptValueType> getConceptValue() {
        if (conceptValue == null) {
            conceptValue = new ArrayList<ConceptValueType>();
        }
        return this.conceptValue;
    }

    /**
     * Gets the value of the representationValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the representationValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRepresentationValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodeValueType }
     * 
     * 
     */
    public List<CodeValueType> getRepresentationValue() {
        if (representationValue == null) {
            representationValue = new ArrayList<CodeValueType>();
        }
        return this.representationValue;
    }

    /**
     * Gets the value of the dimensionValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dimensionValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDimensionValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DimensionValueType }
     * 
     * 
     */
    public List<DimensionValueType> getDimensionValue() {
        if (dimensionValue == null) {
            dimensionValue = new ArrayList<DimensionValueType>();
        }
        return this.dimensionValue;
    }

    /**
     * Gets the value of the timeDimensionValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timeDimensionValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimeDimensionValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TimeDimensionValueType }
     * 
     * 
     */
    public List<TimeDimensionValueType> getTimeDimensionValue() {
        if (timeDimensionValue == null) {
            timeDimensionValue = new ArrayList<TimeDimensionValueType>();
        }
        return this.timeDimensionValue;
    }

    /**
     * Gets the value of the attributeValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeValueType }
     * 
     * 
     */
    public List<AttributeValueType> getAttributeValue() {
        if (attributeValue == null) {
            attributeValue = new ArrayList<AttributeValueType>();
        }
        return this.attributeValue;
    }

    /**
     * Gets the value of the primaryMeasureValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the primaryMeasureValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrimaryMeasureValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrimaryMeasureValueType }
     * 
     * 
     */
    public List<PrimaryMeasureValueType> getPrimaryMeasureValue() {
        if (primaryMeasureValue == null) {
            primaryMeasureValue = new ArrayList<PrimaryMeasureValueType>();
        }
        return this.primaryMeasureValue;
    }

    /**
     * Gets the value of the attachmentConstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachmentConstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachmentConstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttachmentConstraintReferenceType }
     * 
     * 
     */
    public List<AttachmentConstraintReferenceType> getAttachmentConstraint() {
        if (attachmentConstraint == null) {
            attachmentConstraint = new ArrayList<AttachmentConstraintReferenceType>();
        }
        return this.attachmentConstraint;
    }

    /**
     * Gets the value of the timeFormat property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timeFormat property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimeFormat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TimeDataType }
     * 
     * 
     */
    public List<TimeDataType> getTimeFormat() {
        if (timeFormat == null) {
            timeFormat = new ArrayList<TimeDataType>();
        }
        return this.timeFormat;
    }

    /**
     * Gets the value of the or property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the or property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataParametersOrType }
     * 
     * 
     */
    public List<DataParametersOrType> getOr() {
        if (or == null) {
            or = new ArrayList<DataParametersOrType>();
        }
        return this.or;
    }

    /**
     * Gets the value of the and property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the and property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataParametersAndType }
     * 
     * 
     */
    public List<DataParametersAndType> getAnd() {
        if (and == null) {
            and = new ArrayList<DataParametersAndType>();
        }
        return this.and;
    }

}
