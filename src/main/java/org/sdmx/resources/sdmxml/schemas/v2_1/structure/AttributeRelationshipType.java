
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.EmptyType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalDimensionReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalGroupKeyDescriptorReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalPrimaryMeasureReferenceType;


/**
 * AttributeRelationshipType defines the structure for stating the relationship between an attribute and other data structure definition components.
 * 
 * <p>Java-Klasse f�r AttributeRelationshipType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AttributeRelationshipType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="None" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;sequence>
 *           &lt;element name="Dimension" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalDimensionReferenceType" maxOccurs="unbounded"/>
 *           &lt;element name="AttachmentGroup" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalGroupKeyDescriptorReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;element name="Group" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalGroupKeyDescriptorReferenceType"/>
 *         &lt;element name="PrimaryMeasure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalPrimaryMeasureReferenceType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeRelationshipType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "none",
    "dimension",
    "attachmentGroup",
    "group",
    "primaryMeasure"
})
public class AttributeRelationshipType {

    @XmlElement(name = "None", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected EmptyType none;
    @XmlElement(name = "Dimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<LocalDimensionReferenceType> dimension;
    @XmlElement(name = "AttachmentGroup", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<LocalGroupKeyDescriptorReferenceType> attachmentGroup;
    @XmlElement(name = "Group", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected LocalGroupKeyDescriptorReferenceType group;
    @XmlElement(name = "PrimaryMeasure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected LocalPrimaryMeasureReferenceType primaryMeasure;

    /**
     * Ruft den Wert der none-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getNone() {
        return none;
    }

    /**
     * Legt den Wert der none-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setNone(EmptyType value) {
        this.none = value;
    }

    /**
     * Gets the value of the dimension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dimension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDimension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalDimensionReferenceType }
     * 
     * 
     */
    public List<LocalDimensionReferenceType> getDimension() {
        if (dimension == null) {
            dimension = new ArrayList<LocalDimensionReferenceType>();
        }
        return this.dimension;
    }

    /**
     * Gets the value of the attachmentGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachmentGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachmentGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalGroupKeyDescriptorReferenceType }
     * 
     * 
     */
    public List<LocalGroupKeyDescriptorReferenceType> getAttachmentGroup() {
        if (attachmentGroup == null) {
            attachmentGroup = new ArrayList<LocalGroupKeyDescriptorReferenceType>();
        }
        return this.attachmentGroup;
    }

    /**
     * Ruft den Wert der group-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalGroupKeyDescriptorReferenceType }
     *     
     */
    public LocalGroupKeyDescriptorReferenceType getGroup() {
        return group;
    }

    /**
     * Legt den Wert der group-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalGroupKeyDescriptorReferenceType }
     *     
     */
    public void setGroup(LocalGroupKeyDescriptorReferenceType value) {
        this.group = value;
    }

    /**
     * Ruft den Wert der primaryMeasure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalPrimaryMeasureReferenceType }
     *     
     */
    public LocalPrimaryMeasureReferenceType getPrimaryMeasure() {
        return primaryMeasure;
    }

    /**
     * Legt den Wert der primaryMeasure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalPrimaryMeasureReferenceType }
     *     
     */
    public void setPrimaryMeasure(LocalPrimaryMeasureReferenceType value) {
        this.primaryMeasure = value;
    }

}
