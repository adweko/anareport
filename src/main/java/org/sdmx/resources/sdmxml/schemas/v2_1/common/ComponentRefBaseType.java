
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ComponentRefBaseType is an abstract base type for referencing a component contained in a component list within a structure.
 * 
 * <p>Java-Klasse f�r ComponentRefBaseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentRefBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ContainerChildObjectRefBaseType">
 *       &lt;attribute name="agencyID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedNCNameIDType" />
 *       &lt;attribute name="maintainableParentID" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *       &lt;attribute name="maintainableParentVersion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}VersionType" default="1.0" />
 *       &lt;attribute name="containerID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedIDType" />
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NestedIDType" />
 *       &lt;attribute name="local" type="{http://www.w3.org/2001/XMLSchema}boolean" fixed="false" />
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentTypeCodelistType" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructurePackageTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentRefBaseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlSeeAlso({
    DimensionRefType.class,
    TimeDimensionRefType.class,
    MetadataAttributeRefType.class,
    ConstraintTargetRefType.class,
    PrimaryMeasureRefType.class,
    DataSetTargetRefType.class,
    IdentifiableObjectTargetRefType.class,
    MeasureDimensionRefType.class,
    KeyDescriptorValuesTargetRefType.class,
    AttributeRefType.class,
    ReportPeriodTargetRefType.class
})
public abstract class ComponentRefBaseType
    extends ContainerChildObjectRefBaseType
{


}
