
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.TextType;


/**
 * NameableType is an abstract base type for  all nameable objects.
 * 
 * <p>Java-Klasse f�r NameableType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NameableType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}IdentifiableType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Name" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Description" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameableType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "name",
    "description"
})
@XmlSeeAlso({
    LevelBaseType.class,
    ProcessStepBaseType.class,
    StructureMapBaseType.class,
    HybridCodelistMapBaseType.class,
    HierarchyBaseType.class,
    ItemBaseType.class,
    ItemSchemeMapBaseType.class,
    VersionableType.class
})
public abstract class NameableType
    extends IdentifiableType
{

    @XmlElement(name = "Name", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", required = true)
    protected List<TextType> name;
    @XmlElement(name = "Description", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected List<TextType> description;

    /**
     * Name provides for a human-readable name for the object. This may be provided in multiple, parallel language-equivalent forms.Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getName() {
        if (name == null) {
            name = new ArrayList<TextType>();
        }
        return this.name;
    }

    /**
     * Description provides for a longer human-readable description of the object. This may be provided in multiple, parallel language-equivalent forms.Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getDescription() {
        if (description == null) {
            description = new ArrayList<TextType>();
        }
        return this.description;
    }

}
