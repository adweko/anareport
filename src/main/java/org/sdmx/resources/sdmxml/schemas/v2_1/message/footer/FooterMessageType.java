
package org.sdmx.resources.sdmxml.schemas.v2_1.message.footer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CodedStatusMessageType;


/**
 * FooterMessageType defines the structure of a message that is contained in the footer of a message. It is a status message that have a severity code of Error, Information, or Warning added to it.
 * 
 * <p>Java-Klasse f�r FooterMessageType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FooterMessageType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodedStatusMessageType">
 *       &lt;attribute name="severity" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer}SeverityCodeType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FooterMessageType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer")
public class FooterMessageType
    extends CodedStatusMessageType
{

    @XmlAttribute(name = "severity")
    protected SeverityCodeType severity;

    /**
     * Ruft den Wert der severity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SeverityCodeType }
     *     
     */
    public SeverityCodeType getSeverity() {
        return severity;
    }

    /**
     * Legt den Wert der severity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SeverityCodeType }
     *     
     */
    public void setSeverity(SeverityCodeType value) {
        this.severity = value;
    }

}
