
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureReferenceBaseType;


/**
 * StructureUsageType is an abstract base type for all structure usages. It contains a reference to a structure. Concrete instances of this type should restrict the type of structure referenced.
 * 
 * <p>Java-Klasse f�r StructureUsageType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureUsageType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MaintainableType">
 *       &lt;sequence>
 *         &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureReferenceBaseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureUsageType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "structure"
})
@XmlSeeAlso({
    MetadataflowType.class,
    DataflowType.class
})
public abstract class StructureUsageType
    extends MaintainableType
{

    @XmlElement(name = "Structure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected StructureReferenceBaseType structure;

    /**
     * Ruft den Wert der structure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureReferenceBaseType }
     *     
     */
    public StructureReferenceBaseType getStructure() {
        return structure;
    }

    /**
     * Legt den Wert der structure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureReferenceBaseType }
     *     
     */
    public void setStructure(StructureReferenceBaseType value) {
        this.structure = value;
    }

}
