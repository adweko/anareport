
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataTargetValueType describes the structure that is used to match reference metadata where a given metadata target's target object have particular values. If a value is not given for a target object which is part of the metadata target, it is assumed that all values are allowed for that target object. Thus, if no target object values are given in the entire metadata target, the query will simply match ant report where the reference metadata target is used. All target object value conditions must be met to constitute a match.
 * 
 * <p>Java-Klasse f�r MetadataTargetValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataTargetValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NCNameIDType"/>
 *         &lt;element name="TargetObjectValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TargetObjectValueType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataTargetValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "id",
    "targetObjectValue"
})
public class MetadataTargetValueType {

    @XmlElement(name = "ID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected String id;
    @XmlElement(name = "TargetObjectValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<TargetObjectValueType> targetObjectValue;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the targetObjectValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the targetObjectValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTargetObjectValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TargetObjectValueType }
     * 
     * 
     */
    public List<TargetObjectValueType> getTargetObjectValue() {
        if (targetObjectValue == null) {
            targetObjectValue = new ArrayList<TargetObjectValueType>();
        }
        return this.targetObjectValue;
    }

}
