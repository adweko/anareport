
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataType;


/**
 * TextFormatType defines the information for describing a full range of text formats and may place restrictions on the values of the other attributes, referred to as "facets".
 * 
 * <p>Java-Klasse f�r TextFormatType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TextFormatType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="textType" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataType" default="String" />
 *       &lt;attribute name="isSequence" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="interval" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="startValue" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="endValue" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="timeInterval" type="{http://www.w3.org/2001/XMLSchema}duration" />
 *       &lt;attribute name="startTime" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StandardTimePeriodType" />
 *       &lt;attribute name="endTime" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StandardTimePeriodType" />
 *       &lt;attribute name="minLength" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" />
 *       &lt;attribute name="maxLength" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" />
 *       &lt;attribute name="minValue" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="maxValue" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="decimals" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" />
 *       &lt;attribute name="pattern" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="isMultiLingual" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextFormatType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlSeeAlso({
    TargetObjectTextFormatType.class,
    BasicComponentTextFormatType.class
})
public class TextFormatType {

    @XmlAttribute(name = "textType")
    protected DataType textType;
    @XmlAttribute(name = "isSequence")
    protected Boolean isSequence;
    @XmlAttribute(name = "interval")
    protected BigDecimal interval;
    @XmlAttribute(name = "startValue")
    protected BigDecimal startValue;
    @XmlAttribute(name = "endValue")
    protected BigDecimal endValue;
    @XmlAttribute(name = "timeInterval")
    protected Duration timeInterval;
    @XmlAttribute(name = "startTime")
    protected List<String> startTime;
    @XmlAttribute(name = "endTime")
    protected List<String> endTime;
    @XmlAttribute(name = "minLength")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger minLength;
    @XmlAttribute(name = "maxLength")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxLength;
    @XmlAttribute(name = "minValue")
    protected BigDecimal minValue;
    @XmlAttribute(name = "maxValue")
    protected BigDecimal maxValue;
    @XmlAttribute(name = "decimals")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger decimals;
    @XmlAttribute(name = "pattern")
    protected String pattern;
    @XmlAttribute(name = "isMultiLingual")
    protected Boolean isMultiLingual;

    /**
     * Ruft den Wert der textType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataType }
     *     
     */
    public DataType getTextType() {
        if (textType == null) {
            return DataType.STRING;
        } else {
            return textType;
        }
    }

    /**
     * Legt den Wert der textType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataType }
     *     
     */
    public void setTextType(DataType value) {
        this.textType = value;
    }

    /**
     * Ruft den Wert der isSequence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSequence() {
        return isSequence;
    }

    /**
     * Legt den Wert der isSequence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSequence(Boolean value) {
        this.isSequence = value;
    }

    /**
     * Ruft den Wert der interval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInterval() {
        return interval;
    }

    /**
     * Legt den Wert der interval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInterval(BigDecimal value) {
        this.interval = value;
    }

    /**
     * Ruft den Wert der startValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStartValue() {
        return startValue;
    }

    /**
     * Legt den Wert der startValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStartValue(BigDecimal value) {
        this.startValue = value;
    }

    /**
     * Ruft den Wert der endValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEndValue() {
        return endValue;
    }

    /**
     * Legt den Wert der endValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEndValue(BigDecimal value) {
        this.endValue = value;
    }

    /**
     * Ruft den Wert der timeInterval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getTimeInterval() {
        return timeInterval;
    }

    /**
     * Legt den Wert der timeInterval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setTimeInterval(Duration value) {
        this.timeInterval = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the startTime property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStartTime().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getStartTime() {
        if (startTime == null) {
            startTime = new ArrayList<String>();
        }
        return this.startTime;
    }

    /**
     * Gets the value of the endTime property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the endTime property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEndTime().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEndTime() {
        if (endTime == null) {
            endTime = new ArrayList<String>();
        }
        return this.endTime;
    }

    /**
     * Ruft den Wert der minLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinLength() {
        return minLength;
    }

    /**
     * Legt den Wert der minLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinLength(BigInteger value) {
        this.minLength = value;
    }

    /**
     * Ruft den Wert der maxLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxLength() {
        return maxLength;
    }

    /**
     * Legt den Wert der maxLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxLength(BigInteger value) {
        this.maxLength = value;
    }

    /**
     * Ruft den Wert der minValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinValue() {
        return minValue;
    }

    /**
     * Legt den Wert der minValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinValue(BigDecimal value) {
        this.minValue = value;
    }

    /**
     * Ruft den Wert der maxValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxValue() {
        return maxValue;
    }

    /**
     * Legt den Wert der maxValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxValue(BigDecimal value) {
        this.maxValue = value;
    }

    /**
     * Ruft den Wert der decimals-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDecimals() {
        return decimals;
    }

    /**
     * Legt den Wert der decimals-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDecimals(BigInteger value) {
        this.decimals = value;
    }

    /**
     * Ruft den Wert der pattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * Legt den Wert der pattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPattern(String value) {
        this.pattern = value;
    }

    /**
     * Ruft den Wert der isMultiLingual-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsMultiLingual() {
        if (isMultiLingual == null) {
            return true;
        } else {
            return isMultiLingual;
        }
    }

    /**
     * Legt den Wert der isMultiLingual-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsMultiLingual(Boolean value) {
        this.isMultiLingual = value;
    }

}
