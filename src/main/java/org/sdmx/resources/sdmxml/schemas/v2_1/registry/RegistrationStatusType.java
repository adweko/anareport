
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * RegistrationStatusType describes the structure of a registration status.
 * 
 * <p>Java-Klasse f�r RegistrationStatusType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RegistrationStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}RegistrationType"/>
 *         &lt;element name="StatusMessage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}StatusMessageType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistrationStatusType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "registration",
    "statusMessage"
})
public class RegistrationStatusType {

    @XmlElement(name = "Registration", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected RegistrationType registration;
    @XmlElement(name = "StatusMessage", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected StatusMessageType statusMessage;

    /**
     * Ruft den Wert der registration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationType }
     *     
     */
    public RegistrationType getRegistration() {
        return registration;
    }

    /**
     * Legt den Wert der registration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationType }
     *     
     */
    public void setRegistration(RegistrationType value) {
        this.registration = value;
    }

    /**
     * Ruft den Wert der statusMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StatusMessageType }
     *     
     */
    public StatusMessageType getStatusMessage() {
        return statusMessage;
    }

    /**
     * Legt den Wert der statusMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusMessageType }
     *     
     */
    public void setStatusMessage(StatusMessageType value) {
        this.statusMessage = value;
    }

}
