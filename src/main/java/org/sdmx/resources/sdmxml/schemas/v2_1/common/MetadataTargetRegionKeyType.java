
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataTargetRegionKeyType is a type for providing a set of values for a target object in a metadata target of a re fence metadata report. A set of values or a time range can be provided for a report period target object. A collection of the respective types of references can be provided for data set reference and identifiable object reference target objects. For a key descriptor values target object, a collection of data keys can be provided.
 * 
 * <p>Java-Klasse f�r MetadataTargetRegionKeyType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataTargetRegionKeyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentValueSetType">
 *       &lt;choice>
 *         &lt;element name="Value" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleKeyValueType" maxOccurs="unbounded"/>
 *         &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType" maxOccurs="unbounded"/>
 *         &lt;element name="DataKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataKeyType" maxOccurs="unbounded"/>
 *         &lt;element name="Object" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType" maxOccurs="unbounded"/>
 *         &lt;element name="TimeRange" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SingleNCNameIDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataTargetRegionKeyType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class MetadataTargetRegionKeyType
    extends ComponentValueSetType
{


}
