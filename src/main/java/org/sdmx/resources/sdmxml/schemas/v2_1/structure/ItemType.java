
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalItemReferenceType;


/**
 * ItemType is an abstract base type for all items with in an item scheme. Concrete instances of this type may or may not utilize the nested item, but if so should restrict the actual types of item allowed.
 * 
 * <p>Java-Klasse f�r ItemType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ItemType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ItemBaseType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="Parent" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalItemReferenceType"/>
 *         &lt;sequence maxOccurs="unbounded">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Item"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "parent",
    "item"
})
@XmlSeeAlso({
    CategoryType.class,
    CodeType.class,
    ConceptBaseType.class,
    ReportingCategoryBaseType.class,
    BaseOrganisationType.class
})
public abstract class ItemType
    extends ItemBaseType
{

    @XmlElement(name = "Parent", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected LocalItemReferenceType parent;
    @XmlElementRef(name = "Item", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class, required = false)
    protected List<JAXBElement<? extends ItemType>> item;

    /**
     * Ruft den Wert der parent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalItemReferenceType }
     *     
     */
    public LocalItemReferenceType getParent() {
        return parent;
    }

    /**
     * Legt den Wert der parent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalItemReferenceType }
     *     
     */
    public void setParent(LocalItemReferenceType value) {
        this.parent = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CategoryType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationUnitType }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportingCategoryType }{@code >}
     * {@link JAXBElement }{@code <}{@link ConceptType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataProviderType }{@code >}
     * {@link JAXBElement }{@code <}{@link AgencyType }{@code >}
     * {@link JAXBElement }{@code <}{@link ItemType }{@code >}
     * {@link JAXBElement }{@code <}{@link DataConsumerType }{@code >}
     * {@link JAXBElement }{@code <}{@link CodeType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ItemType>> getItem() {
        if (item == null) {
            item = new ArrayList<JAXBElement<? extends ItemType>>();
        }
        return this.item;
    }

}
