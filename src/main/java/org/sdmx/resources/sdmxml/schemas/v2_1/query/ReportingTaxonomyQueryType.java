
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * ReportingTaxonomyQueryType defines the structure of a reporting taxonomy query. The parameters for the query are contained in the ReportingTaxonomyWhere element. The References element is used to indicate how objects that are referenced from the reporting taxonomy should be returned.
 * 
 * <p>Java-Klasse f�r ReportingTaxonomyQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReportingTaxonomyQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructuralMetadataQueryType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructureReturnDetailsType"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReportingTaxonomyWhere"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingTaxonomyQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class ReportingTaxonomyQueryType
    extends StructuralMetadataQueryType
{


}
