
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MeasureDimensionType defines the structure of the measure dimension. It is derived from the base dimension structure, but requires that a coded representation taken from a concept scheme is given.
 * 
 * <p>Java-Klasse f�r MeasureDimensionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MeasureDimensionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}BaseDimensionType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element name="ConceptIdentity" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType"/>
 *         &lt;element name="LocalRepresentation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MeasureDimensionRepresentationType"/>
 *         &lt;element name="ConceptRole" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ConceptReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DimensionTypeType" fixed="MeasureDimension" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeasureDimensionType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class MeasureDimensionType
    extends BaseDimensionType
{


}
