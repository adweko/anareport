
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r StructurePackageTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="StructurePackageTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}PackageTypeCodelistType">
 *     &lt;enumeration value="datastructure"/>
 *     &lt;enumeration value="metadatastructure"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StructurePackageTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(PackageTypeCodelistType.class)
public enum StructurePackageTypeCodelistType {

    @XmlEnumValue("datastructure")
    DATASTRUCTURE(PackageTypeCodelistType.DATASTRUCTURE),
    @XmlEnumValue("metadatastructure")
    METADATASTRUCTURE(PackageTypeCodelistType.METADATASTRUCTURE);
    private final PackageTypeCodelistType value;

    StructurePackageTypeCodelistType(PackageTypeCodelistType v) {
        value = v;
    }

    public PackageTypeCodelistType value() {
        return value;
    }

    public static StructurePackageTypeCodelistType fromValue(PackageTypeCodelistType v) {
        for (StructurePackageTypeCodelistType c: StructurePackageTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
