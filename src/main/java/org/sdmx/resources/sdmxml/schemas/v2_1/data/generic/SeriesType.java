
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;


/**
 * SeriesType defines a structure which is used to group a collection of observations which have a key in common. The key for a series is every dimension defined in the data structure definition, save the dimension declared to be at the observation level for this data set. In addition to observations, values can be provided for attributes which are associated with the dimensions which make up this series key (so long as the attributes do not specify a group attachment or also have an relationship with the observation dimension). It is possible for the series to contain only observations or only attribute values, or both.
 * 
 * <p>Java-Klasse f�r SeriesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SeriesType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="SeriesKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType"/>
 *         &lt;element name="Attributes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType" minOccurs="0"/>
 *         &lt;element name="Obs" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ObsType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeriesType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", propOrder = {
    "seriesKey",
    "attributes",
    "obs"
})
@XmlSeeAlso({
    TimeSeriesType.class
})
public class SeriesType
    extends AnnotableType
{

    @XmlElement(name = "SeriesKey", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", required = true)
    protected ValuesType seriesKey;
    @XmlElement(name = "Attributes", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected ValuesType attributes;
    @XmlElement(name = "Obs", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected List<ObsType> obs;

    /**
     * Ruft den Wert der seriesKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValuesType }
     *     
     */
    public ValuesType getSeriesKey() {
        return seriesKey;
    }

    /**
     * Legt den Wert der seriesKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuesType }
     *     
     */
    public void setSeriesKey(ValuesType value) {
        this.seriesKey = value;
    }

    /**
     * Ruft den Wert der attributes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValuesType }
     *     
     */
    public ValuesType getAttributes() {
        return attributes;
    }

    /**
     * Legt den Wert der attributes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuesType }
     *     
     */
    public void setAttributes(ValuesType value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the obs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the obs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObsType }
     * 
     * 
     */
    public List<ObsType> getObs() {
        if (obs == null) {
            obs = new ArrayList<ObsType>();
        }
        return this.obs;
    }

}
