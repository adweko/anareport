
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * NameableWhereType is an abstract base type that serves as the basis for any query for a nameable object.
 * 
 * <p>Java-Klasse f�r NameableWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NameableWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}IdentifiableWhereType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryTextType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameableWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "name",
    "description"
})
@XmlSeeAlso({
    ItemWhereType.class,
    VersionableWhereType.class
})
public abstract class NameableWhereType
    extends IdentifiableWhereType
{

    @XmlElement(name = "Name", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected QueryTextType name;
    @XmlElement(name = "Description", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected QueryTextType description;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QueryTextType }
     *     
     */
    public QueryTextType getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryTextType }
     *     
     */
    public void setName(QueryTextType value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QueryTextType }
     *     
     */
    public QueryTextType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryTextType }
     *     
     */
    public void setDescription(QueryTextType value) {
        this.description = value;
    }

}
