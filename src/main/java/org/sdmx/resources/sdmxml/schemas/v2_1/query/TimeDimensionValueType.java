
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeDimensionValueType is used to query for data where the time dimension has a particular value.
 * 
 * <p>Java-Klasse f�r TimeDimensionValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeDimensionValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}DataStructureComponentValueQueryType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NCNameIDType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeValue" maxOccurs="2"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeDimensionValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class TimeDimensionValueType
    extends DataStructureComponentValueQueryType
{


}
