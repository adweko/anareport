
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * GenericMetadataStructureType defines the structural information for a generic metadata message.
 * 
 * <p>Java-Klasse f�r GenericMetadataStructureType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GenericMetadataStructureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataStructureType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="ProvisionAgrement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType"/>
 *           &lt;element name="StructureUsage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataflowReferenceType"/>
 *           &lt;element name="Structure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataStructureReferenceType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericMetadataStructureType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class GenericMetadataStructureType
    extends MetadataStructureType
{


}
