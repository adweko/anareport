
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.StructureUsageReferenceType;


/**
 * ProvisionAgreementType describes the structure of a provision agreement. A provision agreement defines an agreement for a data provider to report data or reference metadata against a flow. Attributes which describe how the registry must behave when data or metadata is registered against this provision agreement are supplied.
 * 
 * <p>Java-Klasse f�r ProvisionAgreementType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProvisionAgreementType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MaintainableType">
 *       &lt;sequence>
 *         &lt;element name="StructureUsage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureUsageReferenceType"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvisionAgreementType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "structureUsage",
    "dataProvider"
})
public class ProvisionAgreementType
    extends MaintainableType
{

    @XmlElement(name = "StructureUsage", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected StructureUsageReferenceType structureUsage;
    @XmlElement(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected DataProviderReferenceType dataProvider;

    /**
     * Ruft den Wert der structureUsage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructureUsageReferenceType }
     *     
     */
    public StructureUsageReferenceType getStructureUsage() {
        return structureUsage;
    }

    /**
     * Legt den Wert der structureUsage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureUsageReferenceType }
     *     
     */
    public void setStructureUsage(StructureUsageReferenceType value) {
        this.structureUsage = value;
    }

    /**
     * Ruft den Wert der dataProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public DataProviderReferenceType getDataProvider() {
        return dataProvider;
    }

    /**
     * Legt den Wert der dataProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataProviderReferenceType }
     *     
     */
    public void setDataProvider(DataProviderReferenceType value) {
        this.dataProvider = value;
    }

}
