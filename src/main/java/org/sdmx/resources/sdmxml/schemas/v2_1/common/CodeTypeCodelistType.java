
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r CodeTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="CodeTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType">
 *     &lt;enumeration value="Code"/>
 *     &lt;enumeration value="HierarchicalCode"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CodeTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ObjectTypeCodelistType.class)
public enum CodeTypeCodelistType {

    @XmlEnumValue("Code")
    CODE(ObjectTypeCodelistType.CODE),
    @XmlEnumValue("HierarchicalCode")
    HIERARCHICAL_CODE(ObjectTypeCodelistType.HIERARCHICAL_CODE);
    private final ObjectTypeCodelistType value;

    CodeTypeCodelistType(ObjectTypeCodelistType v) {
        value = v;
    }

    public ObjectTypeCodelistType value() {
        return value;
    }

    public static CodeTypeCodelistType fromValue(ObjectTypeCodelistType v) {
        for (CodeTypeCodelistType c: CodeTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
