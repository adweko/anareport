
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ItemSchemeReferenceBaseType;


/**
 * ItemSchemeMapType is an abstract base type which forms the basis for mapping items between item schemes of the same type.
 * 
 * <p>Java-Klasse f�r ItemSchemeMapType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ItemSchemeMapType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ItemSchemeMapBaseType">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeReferenceBaseType"/>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeReferenceBaseType"/>
 *         &lt;sequence maxOccurs="unbounded">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ItemAssociation"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemSchemeMapType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "source",
    "target",
    "itemAssociation"
})
@XmlSeeAlso({
    ConceptSchemeMapType.class,
    CodelistMapType.class,
    CategorySchemeMapType.class,
    ReportingTaxonomyMapType.class,
    OrganisationSchemeMapType.class
})
public abstract class ItemSchemeMapType
    extends ItemSchemeMapBaseType
{

    @XmlElement(name = "Source", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected ItemSchemeReferenceBaseType source;
    @XmlElement(name = "Target", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected ItemSchemeReferenceBaseType target;
    @XmlElementRef(name = "ItemAssociation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = JAXBElement.class)
    protected List<JAXBElement<? extends ItemAssociationType>> itemAssociation;

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ItemSchemeReferenceBaseType }
     *     
     */
    public ItemSchemeReferenceBaseType getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemSchemeReferenceBaseType }
     *     
     */
    public void setSource(ItemSchemeReferenceBaseType value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der target-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ItemSchemeReferenceBaseType }
     *     
     */
    public ItemSchemeReferenceBaseType getTarget() {
        return target;
    }

    /**
     * Legt den Wert der target-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemSchemeReferenceBaseType }
     *     
     */
    public void setTarget(ItemSchemeReferenceBaseType value) {
        this.target = value;
    }

    /**
     * Gets the value of the itemAssociation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemAssociation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemAssociation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CategoryMapType }{@code >}
     * {@link JAXBElement }{@code <}{@link CodeMapType }{@code >}
     * {@link JAXBElement }{@code <}{@link ItemAssociationType }{@code >}
     * {@link JAXBElement }{@code <}{@link ConceptMapType }{@code >}
     * {@link JAXBElement }{@code <}{@link OrganisationMapType }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportingCategoryMapType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ItemAssociationType>> getItemAssociation() {
        if (itemAssociation == null) {
            itemAssociation = new ArrayList<JAXBElement<? extends ItemAssociationType>>();
        }
        return this.itemAssociation;
    }

}
