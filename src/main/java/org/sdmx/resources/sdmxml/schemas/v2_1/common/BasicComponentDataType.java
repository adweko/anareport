
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r BasicComponentDataType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="BasicComponentDataType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataType">
 *     &lt;enumeration value="String"/>
 *     &lt;enumeration value="Alpha"/>
 *     &lt;enumeration value="AlphaNumeric"/>
 *     &lt;enumeration value="Numeric"/>
 *     &lt;enumeration value="BigInteger"/>
 *     &lt;enumeration value="Integer"/>
 *     &lt;enumeration value="Long"/>
 *     &lt;enumeration value="Short"/>
 *     &lt;enumeration value="Decimal"/>
 *     &lt;enumeration value="Float"/>
 *     &lt;enumeration value="Double"/>
 *     &lt;enumeration value="Boolean"/>
 *     &lt;enumeration value="URI"/>
 *     &lt;enumeration value="Count"/>
 *     &lt;enumeration value="InclusiveValueRange"/>
 *     &lt;enumeration value="ExclusiveValueRange"/>
 *     &lt;enumeration value="Incremental"/>
 *     &lt;enumeration value="ObservationalTimePeriod"/>
 *     &lt;enumeration value="StandardTimePeriod"/>
 *     &lt;enumeration value="BasicTimePeriod"/>
 *     &lt;enumeration value="GregorianTimePeriod"/>
 *     &lt;enumeration value="GregorianYear"/>
 *     &lt;enumeration value="GregorianYearMonth"/>
 *     &lt;enumeration value="GregorianDay"/>
 *     &lt;enumeration value="ReportingTimePeriod"/>
 *     &lt;enumeration value="ReportingYear"/>
 *     &lt;enumeration value="ReportingSemester"/>
 *     &lt;enumeration value="ReportingTrimester"/>
 *     &lt;enumeration value="ReportingQuarter"/>
 *     &lt;enumeration value="ReportingMonth"/>
 *     &lt;enumeration value="ReportingWeek"/>
 *     &lt;enumeration value="ReportingDay"/>
 *     &lt;enumeration value="DateTime"/>
 *     &lt;enumeration value="TimeRange"/>
 *     &lt;enumeration value="Month"/>
 *     &lt;enumeration value="MonthDay"/>
 *     &lt;enumeration value="Day"/>
 *     &lt;enumeration value="Time"/>
 *     &lt;enumeration value="Duration"/>
 *     &lt;enumeration value="XHTML"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BasicComponentDataType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(DataType.class)
public enum BasicComponentDataType {

    @XmlEnumValue("String")
    STRING(DataType.STRING),
    @XmlEnumValue("Alpha")
    ALPHA(DataType.ALPHA),
    @XmlEnumValue("AlphaNumeric")
    ALPHA_NUMERIC(DataType.ALPHA_NUMERIC),
    @XmlEnumValue("Numeric")
    NUMERIC(DataType.NUMERIC),
    @XmlEnumValue("BigInteger")
    BIG_INTEGER(DataType.BIG_INTEGER),
    @XmlEnumValue("Integer")
    INTEGER(DataType.INTEGER),
    @XmlEnumValue("Long")
    LONG(DataType.LONG),
    @XmlEnumValue("Short")
    SHORT(DataType.SHORT),
    @XmlEnumValue("Decimal")
    DECIMAL(DataType.DECIMAL),
    @XmlEnumValue("Float")
    FLOAT(DataType.FLOAT),
    @XmlEnumValue("Double")
    DOUBLE(DataType.DOUBLE),
    @XmlEnumValue("Boolean")
    BOOLEAN(DataType.BOOLEAN),
    URI(DataType.URI),
    @XmlEnumValue("Count")
    COUNT(DataType.COUNT),
    @XmlEnumValue("InclusiveValueRange")
    INCLUSIVE_VALUE_RANGE(DataType.INCLUSIVE_VALUE_RANGE),
    @XmlEnumValue("ExclusiveValueRange")
    EXCLUSIVE_VALUE_RANGE(DataType.EXCLUSIVE_VALUE_RANGE),
    @XmlEnumValue("Incremental")
    INCREMENTAL(DataType.INCREMENTAL),
    @XmlEnumValue("ObservationalTimePeriod")
    OBSERVATIONAL_TIME_PERIOD(DataType.OBSERVATIONAL_TIME_PERIOD),
    @XmlEnumValue("StandardTimePeriod")
    STANDARD_TIME_PERIOD(DataType.STANDARD_TIME_PERIOD),
    @XmlEnumValue("BasicTimePeriod")
    BASIC_TIME_PERIOD(DataType.BASIC_TIME_PERIOD),
    @XmlEnumValue("GregorianTimePeriod")
    GREGORIAN_TIME_PERIOD(DataType.GREGORIAN_TIME_PERIOD),
    @XmlEnumValue("GregorianYear")
    GREGORIAN_YEAR(DataType.GREGORIAN_YEAR),
    @XmlEnumValue("GregorianYearMonth")
    GREGORIAN_YEAR_MONTH(DataType.GREGORIAN_YEAR_MONTH),
    @XmlEnumValue("GregorianDay")
    GREGORIAN_DAY(DataType.GREGORIAN_DAY),
    @XmlEnumValue("ReportingTimePeriod")
    REPORTING_TIME_PERIOD(DataType.REPORTING_TIME_PERIOD),
    @XmlEnumValue("ReportingYear")
    REPORTING_YEAR(DataType.REPORTING_YEAR),
    @XmlEnumValue("ReportingSemester")
    REPORTING_SEMESTER(DataType.REPORTING_SEMESTER),
    @XmlEnumValue("ReportingTrimester")
    REPORTING_TRIMESTER(DataType.REPORTING_TRIMESTER),
    @XmlEnumValue("ReportingQuarter")
    REPORTING_QUARTER(DataType.REPORTING_QUARTER),
    @XmlEnumValue("ReportingMonth")
    REPORTING_MONTH(DataType.REPORTING_MONTH),
    @XmlEnumValue("ReportingWeek")
    REPORTING_WEEK(DataType.REPORTING_WEEK),
    @XmlEnumValue("ReportingDay")
    REPORTING_DAY(DataType.REPORTING_DAY),
    @XmlEnumValue("DateTime")
    DATE_TIME(DataType.DATE_TIME),
    @XmlEnumValue("TimeRange")
    TIME_RANGE(DataType.TIME_RANGE),
    @XmlEnumValue("Month")
    MONTH(DataType.MONTH),
    @XmlEnumValue("MonthDay")
    MONTH_DAY(DataType.MONTH_DAY),
    @XmlEnumValue("Day")
    DAY(DataType.DAY),
    @XmlEnumValue("Time")
    TIME(DataType.TIME),
    @XmlEnumValue("Duration")
    DURATION(DataType.DURATION),
    XHTML(DataType.XHTML);
    private final DataType value;

    BasicComponentDataType(DataType v) {
        value = v;
    }

    public DataType value() {
        return value;
    }

    public static BasicComponentDataType fromValue(DataType v) {
        for (BasicComponentDataType c: BasicComponentDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
