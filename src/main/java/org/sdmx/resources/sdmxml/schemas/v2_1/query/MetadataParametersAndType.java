
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataParametersAndType refines the base data parameters to define a set of parameters joined by an "and" conditions. All of the parameters supplied in an instance of this type must be satisfied to result in a match. As a result of this condition, the maximum occurrence of some parameters has been reduced so as to not allow for impossible conditions to be specified (for example data cannot be matched is it is specified that the data set identifier should be "xyz" and the data identifier should be "abc".
 * 
 * <p>Java-Klasse f�r MetadataParametersAndType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataParametersAndType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataParametersType">
 *       &lt;sequence>
 *         &lt;element name="MetadataSetID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryIDType" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0"/>
 *         &lt;element name="MetadataStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataStructureReferenceType" minOccurs="0"/>
 *         &lt;element name="Metadataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataflowReferenceType" minOccurs="0"/>
 *         &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Updated" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" minOccurs="0"/>
 *         &lt;element name="ConceptValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ConceptValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RepresentationValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}CodeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MetadataTargetValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataTargetValueType" minOccurs="0"/>
 *         &lt;element name="ReportStructureValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReportStructureValueType" minOccurs="0"/>
 *         &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraintReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachedObject" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachedDataKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataKeyType" minOccurs="0"/>
 *         &lt;element name="AttachedDataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType" minOccurs="0"/>
 *         &lt;element name="AttachedReportingPeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType" minOccurs="0"/>
 *         &lt;element name="Or" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataParametersOrType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataParametersAndType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class MetadataParametersAndType
    extends MetadataParametersType
{


}
