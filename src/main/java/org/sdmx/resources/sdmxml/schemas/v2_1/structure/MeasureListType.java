
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MeasureListType describes the structure of the measure descriptor for a data structure definition. Only a primary may be defined.
 * 
 * <p>Java-Klasse f�r MeasureListType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MeasureListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ComponentListType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}PrimaryMeasure"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" fixed="MeasureDescriptor" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeasureListType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class MeasureListType
    extends ComponentListType
{


}
