
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ConstraintWhereType contains the parameters of a constraint query. All supplied parameters must be matched in order for an object to stratify the query.
 * 
 * <p>Java-Klasse f�r ConstraintWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConstraintWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ConstraintWhereBaseType">
 *       &lt;sequence>
 *         &lt;element name="ConstraintAttachmentWhere" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ConstraintAttachmentWhereType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="allowable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstraintWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "constraintAttachmentWhere"
})
public class ConstraintWhereType
    extends ConstraintWhereBaseType
{

    @XmlElement(name = "ConstraintAttachmentWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected ConstraintAttachmentWhereType constraintAttachmentWhere;
    @XmlAttribute(name = "allowable")
    protected Boolean allowable;

    /**
     * Ruft den Wert der constraintAttachmentWhere-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConstraintAttachmentWhereType }
     *     
     */
    public ConstraintAttachmentWhereType getConstraintAttachmentWhere() {
        return constraintAttachmentWhere;
    }

    /**
     * Legt den Wert der constraintAttachmentWhere-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstraintAttachmentWhereType }
     *     
     */
    public void setConstraintAttachmentWhere(ConstraintAttachmentWhereType value) {
        this.constraintAttachmentWhere = value;
    }

    /**
     * Ruft den Wert der allowable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowable() {
        return allowable;
    }

    /**
     * Legt den Wert der allowable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowable(Boolean value) {
        this.allowable = value;
    }

}
