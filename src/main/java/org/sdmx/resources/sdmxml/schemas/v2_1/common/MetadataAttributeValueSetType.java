
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataAttributeValueSetType defines the structure for providing values for a metadata attribute. If no values are provided, the attribute is implied to include/excluded from the region in which it is defined, with no regard to the value of the metadata attribute.
 * 
 * <p>Java-Klasse f�r MetadataAttributeValueSetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataAttributeValueSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentValueSetType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="Value" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleValueType" maxOccurs="unbounded"/>
 *         &lt;element name="TimeRange" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataAttributeValueSetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class MetadataAttributeValueSetType
    extends ComponentValueSetType
{


}
