
package org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;


/**
 * 
 * 			
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:xs="http://www.w3.org/2001/XMLSchema"&gt;SeriesType is the abstract type which defines a structure which is used to group a collection of observations which have a key in common. The key for a series is every dimension defined in the data structure definition, save the dimension declared to be at the observation level for this data set. In addition to observations, values can be provided for attributes which are associated with the dimensions which make up this series key (so long as the attributes do not specify a group attachment or also have an relationship with the observation dimension). It is possible for the series to contain only observations or only attribute values, or both.&lt;/p&gt;
 * </pre>
 * 
 * 			
 * 			
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common" xmlns:xs="http://www.w3.org/2001/XMLSchema"&gt;Data structure definition schemas will drive a type based on this that is specific to the data structure definition and the variation of the format being expressed in the schema. Both the dimension values which make up the key and the attribute values associated with the key dimensions will be represented with XML attributes. This is specified in the content model with the declaration of anyAttributes in the "local" namespace. The derived series type will refine this structure so that the attributes are explicit. The XML attributes will be given a name based on the attribute's identifier. These XML attributes will be unqualified (meaning they do not have a namespace associated with them). The dimension XML attributes will be required while the attribute XML attributes will be optional. To allow for generic processing, it is required that the only unqualified XML attributes in the derived group type be for the series dimensions and attributes declared in the data structure definition. If additional attributes are required, these should be qualified with a namespace so that a generic application can easily distinguish them as not being meant to represent a data structure definition dimension or attribute.&lt;/p&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java-Klasse f�r SeriesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SeriesType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="Obs" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType" maxOccurs="unbounded" minOccurs="0" form="unqualified"/>
 *       &lt;/sequence>
 *       &lt;attribute name="TIME_PERIOD" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObservationalTimePeriodType" />
 *       &lt;attribute name="REPORTING_YEAR_START_DAY" type="{http://www.w3.org/2001/XMLSchema}gMonthDay" />
 *       &lt;anyAttribute namespace=''/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeriesType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific", propOrder = {
    "obs"
})
@XmlSeeAlso({
    TimeSeriesType.class
})
public abstract class SeriesType
    extends AnnotableType
{

    @XmlElement(name = "Obs")
    protected List<ObsType> obs;
    @XmlAttribute(name = "TIME_PERIOD")
    protected List<String> timeperiod;
    @XmlAttribute(name = "REPORTING_YEAR_START_DAY")
    @XmlSchemaType(name = "gMonthDay")
    protected XMLGregorianCalendar reportingyearstartday;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the obs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the obs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObsType }
     * 
     * 
     */
    public List<ObsType> getObs() {
        if (obs == null) {
            obs = new ArrayList<ObsType>();
        }
        return this.obs;
    }

    /**
     * Gets the value of the timeperiod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timeperiod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTIMEPERIOD().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTIMEPERIOD() {
        if (timeperiod == null) {
            timeperiod = new ArrayList<String>();
        }
        return this.timeperiod;
    }

    /**
     * Ruft den Wert der reportingyearstartday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getREPORTINGYEARSTARTDAY() {
        return reportingyearstartday;
    }

    /**
     * Legt den Wert der reportingyearstartday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setREPORTINGYEARSTARTDAY(XMLGregorianCalendar value) {
        this.reportingyearstartday = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
