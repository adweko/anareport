
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeDimensionRepresentationType defines the representation for the time dimension. Enumerated values are not allowed.
 * 
 * <p>Java-Klasse f�r TimeDimensionRepresentationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeDimensionRepresentationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}SimpleDataStructureRepresentationType">
 *       &lt;sequence>
 *         &lt;element name="TextFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}TimeTextFormatType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeDimensionRepresentationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class TimeDimensionRepresentationType
    extends SimpleDataStructureRepresentationType
{


}
