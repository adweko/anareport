
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MaintainableTypeCodelistType;


/**
 * <p>Java-Klasse f�r OrganisationSchemeTypeCodeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganisationSchemeTypeCodeType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableTypeCodelistType">
 *     &lt;enumeration value="OrganisationScheme"/>
 *     &lt;enumeration value="AgencyScheme"/>
 *     &lt;enumeration value="DataConsumerScheme"/>
 *     &lt;enumeration value="DataProviderScheme"/>
 *     &lt;enumeration value="OrganisationUnitScheme"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrganisationSchemeTypeCodeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
@XmlEnum(MaintainableTypeCodelistType.class)
public enum OrganisationSchemeTypeCodeType {

    @XmlEnumValue("OrganisationScheme")
    ORGANISATION_SCHEME(MaintainableTypeCodelistType.ORGANISATION_SCHEME),
    @XmlEnumValue("AgencyScheme")
    AGENCY_SCHEME(MaintainableTypeCodelistType.AGENCY_SCHEME),
    @XmlEnumValue("DataConsumerScheme")
    DATA_CONSUMER_SCHEME(MaintainableTypeCodelistType.DATA_CONSUMER_SCHEME),
    @XmlEnumValue("DataProviderScheme")
    DATA_PROVIDER_SCHEME(MaintainableTypeCodelistType.DATA_PROVIDER_SCHEME),
    @XmlEnumValue("OrganisationUnitScheme")
    ORGANISATION_UNIT_SCHEME(MaintainableTypeCodelistType.ORGANISATION_UNIT_SCHEME);
    private final MaintainableTypeCodelistType value;

    OrganisationSchemeTypeCodeType(MaintainableTypeCodelistType v) {
        value = v;
    }

    public MaintainableTypeCodelistType value() {
        return value;
    }

    public static OrganisationSchemeTypeCodeType fromValue(MaintainableTypeCodelistType v) {
        for (OrganisationSchemeTypeCodeType c: OrganisationSchemeTypeCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
