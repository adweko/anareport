
package org.sdmx.resources.sdmxml.schemas.v2_1.metadata.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;


/**
 * ReportType contains a set of report attributes and identifies a target objects] to which they apply.
 * 
 * <p>Java-Klasse f�r ReportType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReportType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/generic}TargetType"/>
 *         &lt;element name="AttributeSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/generic}AttributeSetType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/generic", propOrder = {
    "target",
    "attributeSet"
})
public class ReportType
    extends AnnotableType
{

    @XmlElement(name = "Target", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/generic", required = true)
    protected TargetType target;
    @XmlElement(name = "AttributeSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/metadata/generic", required = true)
    protected AttributeSetType attributeSet;
    @XmlAttribute(name = "id", required = true)
    protected String id;

    /**
     * Ruft den Wert der target-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TargetType }
     *     
     */
    public TargetType getTarget() {
        return target;
    }

    /**
     * Legt den Wert der target-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TargetType }
     *     
     */
    public void setTarget(TargetType value) {
        this.target = value;
    }

    /**
     * Ruft den Wert der attributeSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributeSetType }
     *     
     */
    public AttributeSetType getAttributeSet() {
        return attributeSet;
    }

    /**
     * Legt den Wert der attributeSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeSetType }
     *     
     */
    public void setAttributeSet(AttributeSetType value) {
        this.attributeSet = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
