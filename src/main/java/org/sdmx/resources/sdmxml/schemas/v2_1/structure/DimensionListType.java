
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * DimensionListType describes the key descriptor for a data structure definition. The order of the declaration of child dimensions is significant: it is used to describe the order in which they will appear in data formats for which key values are supplied in an ordered fashion (exclusive of the time dimension, which is not represented as a member of the ordered key). Any data structure definition which uses the time dimension should also declare a frequency dimension, conventionally the first dimension in the key (the set of ordered non-time dimensions). If is not necessary to assign a time dimension, as data can be organised in any fashion required.
 * 
 * <p>Java-Klasse f�r DimensionListType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DimensionListType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DimensionListBaseType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Dimension"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MeasureDimension"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}TimeDimension"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DimensionListType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "dimensionOrMeasureDimensionOrTimeDimension"
})
public class DimensionListType
    extends DimensionListBaseType
{

    @XmlElements({
        @XmlElement(name = "Dimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = DimensionType.class),
        @XmlElement(name = "MeasureDimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = MeasureDimensionType.class),
        @XmlElement(name = "TimeDimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = TimeDimensionType.class)
    })
    protected List<BaseDimensionType> dimensionOrMeasureDimensionOrTimeDimension;

    /**
     * Gets the value of the dimensionOrMeasureDimensionOrTimeDimension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dimensionOrMeasureDimensionOrTimeDimension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDimensionOrMeasureDimensionOrTimeDimension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DimensionType }
     * {@link MeasureDimensionType }
     * {@link TimeDimensionType }
     * 
     * 
     */
    public List<BaseDimensionType> getDimensionOrMeasureDimensionOrTimeDimension() {
        if (dimensionOrMeasureDimensionOrTimeDimension == null) {
            dimensionOrMeasureDimensionOrTimeDimension = new ArrayList<BaseDimensionType>();
        }
        return this.dimensionOrMeasureDimensionOrTimeDimension;
    }

}
