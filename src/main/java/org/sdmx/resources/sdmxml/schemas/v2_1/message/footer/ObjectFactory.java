
package org.sdmx.resources.sdmxml.schemas.v2_1.message.footer;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.sdmx.resources.sdmxml.schemas.v2_1.message.footer package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Footer_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer", "Footer");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.sdmx.resources.sdmxml.schemas.v2_1.message.footer
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FooterType }
     * 
     */
    public FooterType createFooterType() {
        return new FooterType();
    }

    /**
     * Create an instance of {@link FooterMessageType }
     * 
     */
    public FooterMessageType createFooterMessageType() {
        return new FooterMessageType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FooterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer", name = "Footer")
    public JAXBElement<FooterType> createFooter(FooterType value) {
        return new JAXBElement<FooterType>(_Footer_QNAME, FooterType.class, null, value);
    }

}
