
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * CubeRegionKeyType is a type for providing a set of values for a dimension for the purpose of defining a data cube region. A set of distinct value can be provided, or if this dimension is represented as time, and time range can be specified.
 * 
 * <p>Java-Klasse f�r CubeRegionKeyType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CubeRegionKeyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentValueSetType">
 *       &lt;choice>
 *         &lt;element name="Value" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleValueType" maxOccurs="unbounded"/>
 *         &lt;element name="TimeRange" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeRangeValueType"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SingleNCNameIDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CubeRegionKeyType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class CubeRegionKeyType
    extends ComponentValueSetType
{


}
