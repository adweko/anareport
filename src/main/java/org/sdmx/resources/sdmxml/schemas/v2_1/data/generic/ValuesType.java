
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ValuesType is a general structure which contains a collection of data structure definition component values. This type is used to provide both key and attribute collection values.
 * 
 * <p>Java-Klasse f�r ValuesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ValuesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Value" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ComponentValueType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValuesType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", propOrder = {
    "value"
})
public class ValuesType {

    @XmlElement(name = "Value", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", required = true)
    protected List<ComponentValueType> value;

    /**
     * Gets the value of the value property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the value property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComponentValueType }
     * 
     * 
     */
    public List<ComponentValueType> getValue() {
        if (value == null) {
            value = new ArrayList<ComponentValueType>();
        }
        return this.value;
    }

}
