
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * This is a type which describes a structure for holding all of the various dedicated registry interface message types.
 * 
 * <p>Java-Klasse f�r RegistryInterfaceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RegistryInterfaceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}MessageType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}BasicHeaderType"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SubmitRegistrationsRequest" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmitRegistrationsRequestType"/>
 *           &lt;element name="SubmitRegistrationsResponse" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmitRegistrationsResponseType"/>
 *           &lt;element name="QueryRegistrationRequest" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}QueryRegistrationRequestType"/>
 *           &lt;element name="QueryRegistrationResponse" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}QueryRegistrationResponseType"/>
 *           &lt;element name="SubmitStructureRequest" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmitStructureRequestType"/>
 *           &lt;element name="SubmitStructureResponse" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmitStructureResponseType"/>
 *           &lt;element name="SubmitSubscriptionsRequest" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmitSubscriptionsRequestType"/>
 *           &lt;element name="SubmitSubscriptionsResponse" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmitSubscriptionsResponseType"/>
 *           &lt;element name="QuerySubscriptionRequest" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}QuerySubscriptionRequestType"/>
 *           &lt;element name="QuerySubscriptionResponse" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}QuerySubscriptionResponseType"/>
 *           &lt;element name="NotifyRegistryEvent" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}NotifyRegistryEventType"/>
 *         &lt;/choice>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer}Footer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistryInterfaceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
@XmlSeeAlso({
    NotifyRegistryEventType.class,
    SubmitRegistrationsRequestType.class,
    SubmitSubscriptionsResponseType.class,
    QueryRegistrationRequestType.class,
    SubmitSubscriptionsRequestType.class,
    SubmitStructureRequestType.class,
    QuerySubscriptionRequestType.class,
    SubmitStructureResponseType.class,
    QuerySubscriptionResponseType.class,
    QueryRegistrationResponseType.class,
    SubmitRegistrationsResponseType.class
})
public class RegistryInterfaceType
    extends MessageType
{


}
