
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ConstraintTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ConstraintTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableTypeCodelistType">
 *     &lt;enumeration value="AttachmentConstraint"/>
 *     &lt;enumeration value="ContentConstraint"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ConstraintTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(MaintainableTypeCodelistType.class)
public enum ConstraintTypeCodelistType {

    @XmlEnumValue("AttachmentConstraint")
    ATTACHMENT_CONSTRAINT(MaintainableTypeCodelistType.ATTACHMENT_CONSTRAINT),
    @XmlEnumValue("ContentConstraint")
    CONTENT_CONSTRAINT(MaintainableTypeCodelistType.CONTENT_CONSTRAINT);
    private final MaintainableTypeCodelistType value;

    ConstraintTypeCodelistType(MaintainableTypeCodelistType v) {
        value = v;
    }

    public MaintainableTypeCodelistType value() {
        return value;
    }

    public static ConstraintTypeCodelistType fromValue(MaintainableTypeCodelistType v) {
        for (ConstraintTypeCodelistType c: ConstraintTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
