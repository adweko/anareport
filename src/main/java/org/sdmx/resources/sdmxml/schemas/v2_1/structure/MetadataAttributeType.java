
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataAttributeType describes the structure of a metadata attribute. The metadata attribute takes its semantic, and in some cases it representation, from its concept identity. A metadata attribute may be coded (via the local representation), uncoded (via the text format), or take no value. In addition to this value, the metadata attribute may also specify subordinate metadata attributes. If a metadata attribute only serves the purpose of containing subordinate metadata attributes, then the isPresentational attribute should be used. Otherwise, it is assumed to also take a value. If the metadata attribute does take a value, and a representation is not defined, it will be inherited from the concept it takes its semantic from. The optional id on the metadata attribute uniquely identifies it within the metadata structured definition. If this id is not supplied, its value is assumed to be that of the concept referenced from the concept identity. Note that a metadata attribute (as identified by the id attribute) definition  must be unique across the entire metadata structure definition (including target identifier, identifier component, and report structure ids). A metadata attribute may be used in multiple report structures and at different levels, but the content (value and/or child metadata attributes and their cardinality) of the metadata attribute cannot change.
 * 
 * <p>Java-Klasse f�r MetadataAttributeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataAttributeType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataAttributeBaseType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="minOccurs" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" default="1" />
 *       &lt;attribute name="maxOccurs" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OccurenceType" default="1" />
 *       &lt;attribute name="isPresentational" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataAttributeType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "metadataAttribute"
})
public class MetadataAttributeType
    extends MetadataAttributeBaseType
{

    @XmlElement(name = "MetadataAttribute", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<MetadataAttributeType> metadataAttribute;
    @XmlAttribute(name = "minOccurs")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger minOccurs;
    @XmlAttribute(name = "maxOccurs")
    protected String maxOccurs;
    @XmlAttribute(name = "isPresentational")
    protected Boolean isPresentational;

    /**
     * Gets the value of the metadataAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadataAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadataAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadataAttributeType }
     * 
     * 
     */
    public List<MetadataAttributeType> getMetadataAttribute() {
        if (metadataAttribute == null) {
            metadataAttribute = new ArrayList<MetadataAttributeType>();
        }
        return this.metadataAttribute;
    }

    /**
     * Ruft den Wert der minOccurs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinOccurs() {
        if (minOccurs == null) {
            return new BigInteger("1");
        } else {
            return minOccurs;
        }
    }

    /**
     * Legt den Wert der minOccurs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinOccurs(BigInteger value) {
        this.minOccurs = value;
    }

    /**
     * Ruft den Wert der maxOccurs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxOccurs() {
        if (maxOccurs == null) {
            return "1";
        } else {
            return maxOccurs;
        }
    }

    /**
     * Legt den Wert der maxOccurs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxOccurs(String value) {
        this.maxOccurs = value;
    }

    /**
     * Ruft den Wert der isPresentational-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsPresentational() {
        if (isPresentational == null) {
            return false;
        } else {
            return isPresentational;
        }
    }

    /**
     * Legt den Wert der isPresentational-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPresentational(Boolean value) {
        this.isPresentational = value;
    }

}
