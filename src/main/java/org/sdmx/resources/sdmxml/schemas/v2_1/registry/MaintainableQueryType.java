
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * MaintainableQueryType describes the structure of a query for a maintainable object.
 * 
 * <p>Java-Klasse f�r MaintainableQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MaintainableQueryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableQueryType">
 *       &lt;attribute name="agencyID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}NestedIDQueryType" default="%" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintainableQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
public class MaintainableQueryType
    extends VersionableQueryType
{

    @XmlAttribute(name = "agencyID")
    protected String agencyID;

    /**
     * Ruft den Wert der agencyID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyID() {
        if (agencyID == null) {
            return "%";
        } else {
            return agencyID;
        }
    }

    /**
     * Legt den Wert der agencyID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyID(String value) {
        this.agencyID = value;
    }

}
