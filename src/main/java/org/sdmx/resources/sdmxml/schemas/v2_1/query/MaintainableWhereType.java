
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MaintainableTypeCodelistType;


/**
 * MaintainableQueryType is an abstract base type that serves as the basis for any query for a maintainable object.
 * 
 * <p>Java-Klasse f�r MaintainableWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MaintainableWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}VersionableWhereType">
 *       &lt;sequence>
 *         &lt;element name="AgencyID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}QueryNestedIDType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableTypeCodelistType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintainableWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "agencyID"
})
@XmlSeeAlso({
    StructuresWhereType.class,
    ProcessWhereBaseType.class,
    HierarchicalCodelistWhereBaseType.class,
    StructureSetWhereBaseType.class,
    CategorisationWhereBaseType.class,
    ConstraintWhereBaseType.class,
    StructureUsageWhereType.class,
    StructureWhereType.class,
    ItemSchemeWhereType.class,
    ProvisionAgreementWhereBaseType.class
})
public abstract class MaintainableWhereType
    extends VersionableWhereType
{

    @XmlElement(name = "AgencyID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected QueryNestedIDType agencyID;
    @XmlAttribute(name = "type")
    protected MaintainableTypeCodelistType type;

    /**
     * Ruft den Wert der agencyID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QueryNestedIDType }
     *     
     */
    public QueryNestedIDType getAgencyID() {
        return agencyID;
    }

    /**
     * Legt den Wert der agencyID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryNestedIDType }
     *     
     */
    public void setAgencyID(QueryNestedIDType value) {
        this.agencyID = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaintainableTypeCodelistType }
     *     
     */
    public MaintainableTypeCodelistType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainableTypeCodelistType }
     *     
     */
    public void setType(MaintainableTypeCodelistType value) {
        this.type = value;
    }

}
