
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataKeyValueType is a type for providing a target object value for the purpose of defining a distinct metadata key. Only a single value can be provided for the target object.
 * 
 * <p>Java-Klasse f�r MetadataKeyValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataKeyValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DinstinctKeyValueType">
 *       &lt;choice>
 *         &lt;element name="Value" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleKeyValueType"/>
 *         &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SetReferenceType"/>
 *         &lt;element name="DataKey" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataKeyType"/>
 *         &lt;element name="Object" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectReferenceType"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SingleNCNameIDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataKeyValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class MetadataKeyValueType
    extends DinstinctKeyValueType
{


}
