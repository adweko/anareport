
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r ComponentListTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ComponentListTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ObjectTypeCodelistType">
 *     &lt;enumeration value="AttributeDescriptor"/>
 *     &lt;enumeration value="DimensionDescriptor"/>
 *     &lt;enumeration value="GroupDimensionDescriptor"/>
 *     &lt;enumeration value="MeasureDescriptor"/>
 *     &lt;enumeration value="MetadataTarget"/>
 *     &lt;enumeration value="ReportStructure"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ComponentListTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ObjectTypeCodelistType.class)
public enum ComponentListTypeCodelistType {

    @XmlEnumValue("AttributeDescriptor")
    ATTRIBUTE_DESCRIPTOR(ObjectTypeCodelistType.ATTRIBUTE_DESCRIPTOR),
    @XmlEnumValue("DimensionDescriptor")
    DIMENSION_DESCRIPTOR(ObjectTypeCodelistType.DIMENSION_DESCRIPTOR),
    @XmlEnumValue("GroupDimensionDescriptor")
    GROUP_DIMENSION_DESCRIPTOR(ObjectTypeCodelistType.GROUP_DIMENSION_DESCRIPTOR),
    @XmlEnumValue("MeasureDescriptor")
    MEASURE_DESCRIPTOR(ObjectTypeCodelistType.MEASURE_DESCRIPTOR),
    @XmlEnumValue("MetadataTarget")
    METADATA_TARGET(ObjectTypeCodelistType.METADATA_TARGET),
    @XmlEnumValue("ReportStructure")
    REPORT_STRUCTURE(ObjectTypeCodelistType.REPORT_STRUCTURE);
    private final ObjectTypeCodelistType value;

    ComponentListTypeCodelistType(ObjectTypeCodelistType v) {
        value = v;
    }

    public ObjectTypeCodelistType value() {
        return value;
    }

    public static ComponentListTypeCodelistType fromValue(ObjectTypeCodelistType v) {
        for (ComponentListTypeCodelistType c: ComponentListTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
