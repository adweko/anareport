
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SetReferenceType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.sdmx.resources.sdmxml.schemas.v2_1.query package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PrimaryMeasureWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "PrimaryMeasureWhere");
    private final static QName _NumericValue_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "NumericValue");
    private final static QName _ProcessWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ProcessWhere");
    private final static QName _DataStructureWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "DataStructureWhere");
    private final static QName _HierarchicalCodelistWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "HierarchicalCodelistWhere");
    private final static QName _CategorySchemeWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "CategorySchemeWhere");
    private final static QName _Value_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "Value");
    private final static QName _CategorisationWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "CategorisationWhere");
    private final static QName _TimeValue_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "TimeValue");
    private final static QName _ComponentListWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ComponentListWhere");
    private final static QName _DataflowWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "DataflowWhere");
    private final static QName _ComponentWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ComponentWhere");
    private final static QName _GroupDimensionWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "GroupDimensionWhere");
    private final static QName _ConceptWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ConceptWhere");
    private final static QName _OrganisationWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "OrganisationWhere");
    private final static QName _StructuresWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "StructuresWhere");
    private final static QName _CategoryWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "CategoryWhere");
    private final static QName _TextValue_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "TextValue");
    private final static QName _MetadataTargetWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "MetadataTargetWhere");
    private final static QName _MetadataAttributeWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "MetadataAttributeWhere");
    private final static QName _AttributeWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "AttributeWhere");
    private final static QName _CodeWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "CodeWhere");
    private final static QName _GroupWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "GroupWhere");
    private final static QName _DimensionWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "DimensionWhere");
    private final static QName _ConceptSchemeWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ConceptSchemeWhere");
    private final static QName _ProvisionAgreementWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ProvisionAgreementWhere");
    private final static QName _ConstraintWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ConstraintWhere");
    private final static QName _MetadataflowWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "MetadataflowWhere");
    private final static QName _ReportStructureWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ReportStructureWhere");
    private final static QName _ReportingTaxonomyWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ReportingTaxonomyWhere");
    private final static QName _TimeDimensionWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "TimeDimensionWhere");
    private final static QName _TargetObjectWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "TargetObjectWhere");
    private final static QName _OrganisationSchemeWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "OrganisationSchemeWhere");
    private final static QName _StructureSetWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "StructureSetWhere");
    private final static QName _ReportingCategoryWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ReportingCategoryWhere");
    private final static QName _CodelistWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "CodelistWhere");
    private final static QName _MetadataStructureWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "MetadataStructureWhere");
    private final static QName _MeasureDimensionWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "MeasureDimensionWhere");
    private final static QName _ItemWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ItemWhere");
    private final static QName _StructuralMetadataWhere_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "StructuralMetadataWhere");
    private final static QName _ConstraintAttachmentWhereTypeDataStructure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "DataStructure");
    private final static QName _ConstraintAttachmentWhereTypeMetadataStructure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "MetadataStructure");
    private final static QName _ConstraintAttachmentWhereTypeMetadataSet_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "MetadataSet");
    private final static QName _ConstraintAttachmentWhereTypeDataflow_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "Dataflow");
    private final static QName _ConstraintAttachmentWhereTypeDataSourceURL_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "DataSourceURL");
    private final static QName _ConstraintAttachmentWhereTypeDataProvider_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "DataProvider");
    private final static QName _ConstraintAttachmentWhereTypeMetadataflow_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "Metadataflow");
    private final static QName _ConstraintAttachmentWhereTypeProvisionAgreement_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "ProvisionAgreement");
    private final static QName _ConstraintAttachmentWhereTypeDataSet_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", "DataSet");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.sdmx.resources.sdmxml.schemas.v2_1.query
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConstraintWhereType }
     * 
     */
    public ConstraintWhereType createConstraintWhereType() {
        return new ConstraintWhereType();
    }

    /**
     * Create an instance of {@link TargetObjectWhereType }
     * 
     */
    public TargetObjectWhereType createTargetObjectWhereType() {
        return new TargetObjectWhereType();
    }

    /**
     * Create an instance of {@link OrganisationWhereType }
     * 
     */
    public OrganisationWhereType createOrganisationWhereType() {
        return new OrganisationWhereType();
    }

    /**
     * Create an instance of {@link MetadataStructureWhereType }
     * 
     */
    public MetadataStructureWhereType createMetadataStructureWhereType() {
        return new MetadataStructureWhereType();
    }

    /**
     * Create an instance of {@link GroupWhereType }
     * 
     */
    public GroupWhereType createGroupWhereType() {
        return new GroupWhereType();
    }

    /**
     * Create an instance of {@link OrganisationSchemeWhereType }
     * 
     */
    public OrganisationSchemeWhereType createOrganisationSchemeWhereType() {
        return new OrganisationSchemeWhereType();
    }

    /**
     * Create an instance of {@link MetadataflowWhereType }
     * 
     */
    public MetadataflowWhereType createMetadataflowWhereType() {
        return new MetadataflowWhereType();
    }

    /**
     * Create an instance of {@link CodelistWhereType }
     * 
     */
    public CodelistWhereType createCodelistWhereType() {
        return new CodelistWhereType();
    }

    /**
     * Create an instance of {@link TimeDimensionWhereType }
     * 
     */
    public TimeDimensionWhereType createTimeDimensionWhereType() {
        return new TimeDimensionWhereType();
    }

    /**
     * Create an instance of {@link ProvisionAgreementWhereType }
     * 
     */
    public ProvisionAgreementWhereType createProvisionAgreementWhereType() {
        return new ProvisionAgreementWhereType();
    }

    /**
     * Create an instance of {@link CodeWhereType }
     * 
     */
    public CodeWhereType createCodeWhereType() {
        return new CodeWhereType();
    }

    /**
     * Create an instance of {@link ConceptWhereType }
     * 
     */
    public ConceptWhereType createConceptWhereType() {
        return new ConceptWhereType();
    }

    /**
     * Create an instance of {@link DimensionWhereType }
     * 
     */
    public DimensionWhereType createDimensionWhereType() {
        return new DimensionWhereType();
    }

    /**
     * Create an instance of {@link TimePeriodValueType }
     * 
     */
    public TimePeriodValueType createTimePeriodValueType() {
        return new TimePeriodValueType();
    }

    /**
     * Create an instance of {@link ConceptSchemeWhereType }
     * 
     */
    public ConceptSchemeWhereType createConceptSchemeWhereType() {
        return new ConceptSchemeWhereType();
    }

    /**
     * Create an instance of {@link AttributeWhereType }
     * 
     */
    public AttributeWhereType createAttributeWhereType() {
        return new AttributeWhereType();
    }

    /**
     * Create an instance of {@link ReportingCategoryWhereType }
     * 
     */
    public ReportingCategoryWhereType createReportingCategoryWhereType() {
        return new ReportingCategoryWhereType();
    }

    /**
     * Create an instance of {@link ReportStructureWhereType }
     * 
     */
    public ReportStructureWhereType createReportStructureWhereType() {
        return new ReportStructureWhereType();
    }

    /**
     * Create an instance of {@link MetadataAttributeWhereType }
     * 
     */
    public MetadataAttributeWhereType createMetadataAttributeWhereType() {
        return new MetadataAttributeWhereType();
    }

    /**
     * Create an instance of {@link CategorisationWhereType }
     * 
     */
    public CategorisationWhereType createCategorisationWhereType() {
        return new CategorisationWhereType();
    }

    /**
     * Create an instance of {@link MetadataTargetWhereType }
     * 
     */
    public MetadataTargetWhereType createMetadataTargetWhereType() {
        return new MetadataTargetWhereType();
    }

    /**
     * Create an instance of {@link ReportingTaxonomyWhereType }
     * 
     */
    public ReportingTaxonomyWhereType createReportingTaxonomyWhereType() {
        return new ReportingTaxonomyWhereType();
    }

    /**
     * Create an instance of {@link DataStructureWhereType }
     * 
     */
    public DataStructureWhereType createDataStructureWhereType() {
        return new DataStructureWhereType();
    }

    /**
     * Create an instance of {@link HierarchicalCodelistWhereType }
     * 
     */
    public HierarchicalCodelistWhereType createHierarchicalCodelistWhereType() {
        return new HierarchicalCodelistWhereType();
    }

    /**
     * Create an instance of {@link CategoryWhereType }
     * 
     */
    public CategoryWhereType createCategoryWhereType() {
        return new CategoryWhereType();
    }

    /**
     * Create an instance of {@link MeasureDimensionWhereType }
     * 
     */
    public MeasureDimensionWhereType createMeasureDimensionWhereType() {
        return new MeasureDimensionWhereType();
    }

    /**
     * Create an instance of {@link CategorySchemeWhereType }
     * 
     */
    public CategorySchemeWhereType createCategorySchemeWhereType() {
        return new CategorySchemeWhereType();
    }

    /**
     * Create an instance of {@link QueryTextType }
     * 
     */
    public QueryTextType createQueryTextType() {
        return new QueryTextType();
    }

    /**
     * Create an instance of {@link SimpleValueType }
     * 
     */
    public SimpleValueType createSimpleValueType() {
        return new SimpleValueType();
    }

    /**
     * Create an instance of {@link StructuresWhereType }
     * 
     */
    public StructuresWhereType createStructuresWhereType() {
        return new StructuresWhereType();
    }

    /**
     * Create an instance of {@link PrimaryMeasureWhereType }
     * 
     */
    public PrimaryMeasureWhereType createPrimaryMeasureWhereType() {
        return new PrimaryMeasureWhereType();
    }

    /**
     * Create an instance of {@link DataflowWhereType }
     * 
     */
    public DataflowWhereType createDataflowWhereType() {
        return new DataflowWhereType();
    }

    /**
     * Create an instance of {@link NumericValueType }
     * 
     */
    public NumericValueType createNumericValueType() {
        return new NumericValueType();
    }

    /**
     * Create an instance of {@link ProcessWhereType }
     * 
     */
    public ProcessWhereType createProcessWhereType() {
        return new ProcessWhereType();
    }

    /**
     * Create an instance of {@link StructureSetWhereType }
     * 
     */
    public StructureSetWhereType createStructureSetWhereType() {
        return new StructureSetWhereType();
    }

    /**
     * Create an instance of {@link DimensionValueType }
     * 
     */
    public DimensionValueType createDimensionValueType() {
        return new DimensionValueType();
    }

    /**
     * Create an instance of {@link DataflowQueryType }
     * 
     */
    public DataflowQueryType createDataflowQueryType() {
        return new DataflowQueryType();
    }

    /**
     * Create an instance of {@link MetadataParametersAndType }
     * 
     */
    public MetadataParametersAndType createMetadataParametersAndType() {
        return new MetadataParametersAndType();
    }

    /**
     * Create an instance of {@link DataParametersOrType }
     * 
     */
    public DataParametersOrType createDataParametersOrType() {
        return new DataParametersOrType();
    }

    /**
     * Create an instance of {@link StructureSetQueryType }
     * 
     */
    public StructureSetQueryType createStructureSetQueryType() {
        return new StructureSetQueryType();
    }

    /**
     * Create an instance of {@link ProcessStepWhereType }
     * 
     */
    public ProcessStepWhereType createProcessStepWhereType() {
        return new ProcessStepWhereType();
    }

    /**
     * Create an instance of {@link QueryStringType }
     * 
     */
    public QueryStringType createQueryStringType() {
        return new QueryStringType();
    }

    /**
     * Create an instance of {@link GenericDataReturnDetailsType }
     * 
     */
    public GenericDataReturnDetailsType createGenericDataReturnDetailsType() {
        return new GenericDataReturnDetailsType();
    }

    /**
     * Create an instance of {@link ReferencesType }
     * 
     */
    public ReferencesType createReferencesType() {
        return new ReferencesType();
    }

    /**
     * Create an instance of {@link OrganisationSchemeQueryType }
     * 
     */
    public OrganisationSchemeQueryType createOrganisationSchemeQueryType() {
        return new OrganisationSchemeQueryType();
    }

    /**
     * Create an instance of {@link HierarchicalCodelistQueryType }
     * 
     */
    public HierarchicalCodelistQueryType createHierarchicalCodelistQueryType() {
        return new HierarchicalCodelistQueryType();
    }

    /**
     * Create an instance of {@link ProcessQueryType }
     * 
     */
    public ProcessQueryType createProcessQueryType() {
        return new ProcessQueryType();
    }

    /**
     * Create an instance of {@link PrimaryMeasureValueType }
     * 
     */
    public PrimaryMeasureValueType createPrimaryMeasureValueType() {
        return new PrimaryMeasureValueType();
    }

    /**
     * Create an instance of {@link DataQueryType }
     * 
     */
    public DataQueryType createDataQueryType() {
        return new DataQueryType();
    }

    /**
     * Create an instance of {@link ReportStructureValueType }
     * 
     */
    public ReportStructureValueType createReportStructureValueType() {
        return new ReportStructureValueType();
    }

    /**
     * Create an instance of {@link ProvisionAgreementQueryType }
     * 
     */
    public ProvisionAgreementQueryType createProvisionAgreementQueryType() {
        return new ProvisionAgreementQueryType();
    }

    /**
     * Create an instance of {@link QueryNestedIDType }
     * 
     */
    public QueryNestedIDType createQueryNestedIDType() {
        return new QueryNestedIDType();
    }

    /**
     * Create an instance of {@link TimeDimensionValueType }
     * 
     */
    public TimeDimensionValueType createTimeDimensionValueType() {
        return new TimeDimensionValueType();
    }

    /**
     * Create an instance of {@link MetadataAttributeValueType }
     * 
     */
    public MetadataAttributeValueType createMetadataAttributeValueType() {
        return new MetadataAttributeValueType();
    }

    /**
     * Create an instance of {@link MetadataReturnDetailsType }
     * 
     */
    public MetadataReturnDetailsType createMetadataReturnDetailsType() {
        return new MetadataReturnDetailsType();
    }

    /**
     * Create an instance of {@link MetadataflowQueryType }
     * 
     */
    public MetadataflowQueryType createMetadataflowQueryType() {
        return new MetadataflowQueryType();
    }

    /**
     * Create an instance of {@link DataStructureRequestType }
     * 
     */
    public DataStructureRequestType createDataStructureRequestType() {
        return new DataStructureRequestType();
    }

    /**
     * Create an instance of {@link MappedObjectReferenceType }
     * 
     */
    public MappedObjectReferenceType createMappedObjectReferenceType() {
        return new MappedObjectReferenceType();
    }

    /**
     * Create an instance of {@link ReportingTaxonomyQueryType }
     * 
     */
    public ReportingTaxonomyQueryType createReportingTaxonomyQueryType() {
        return new ReportingTaxonomyQueryType();
    }

    /**
     * Create an instance of {@link AnnotationWhereType }
     * 
     */
    public AnnotationWhereType createAnnotationWhereType() {
        return new AnnotationWhereType();
    }

    /**
     * Create an instance of {@link MetadataQueryType }
     * 
     */
    public MetadataQueryType createMetadataQueryType() {
        return new MetadataQueryType();
    }

    /**
     * Create an instance of {@link ConceptValueType }
     * 
     */
    public ConceptValueType createConceptValueType() {
        return new ConceptValueType();
    }

    /**
     * Create an instance of {@link MaintainableReturnDetailsType }
     * 
     */
    public MaintainableReturnDetailsType createMaintainableReturnDetailsType() {
        return new MaintainableReturnDetailsType();
    }

    /**
     * Create an instance of {@link GenericTimeSeriesDataReturnDetailsType }
     * 
     */
    public GenericTimeSeriesDataReturnDetailsType createGenericTimeSeriesDataReturnDetailsType() {
        return new GenericTimeSeriesDataReturnDetailsType();
    }

    /**
     * Create an instance of {@link StructuresQueryType }
     * 
     */
    public StructuresQueryType createStructuresQueryType() {
        return new StructuresQueryType();
    }

    /**
     * Create an instance of {@link CategorisationQueryType }
     * 
     */
    public CategorisationQueryType createCategorisationQueryType() {
        return new CategorisationQueryType();
    }

    /**
     * Create an instance of {@link TargetObjectValueType }
     * 
     */
    public TargetObjectValueType createTargetObjectValueType() {
        return new TargetObjectValueType();
    }

    /**
     * Create an instance of {@link CategorySchemeQueryType }
     * 
     */
    public CategorySchemeQueryType createCategorySchemeQueryType() {
        return new CategorySchemeQueryType();
    }

    /**
     * Create an instance of {@link DataStructureComponentWhereType }
     * 
     */
    public DataStructureComponentWhereType createDataStructureComponentWhereType() {
        return new DataStructureComponentWhereType();
    }

    /**
     * Create an instance of {@link DataStructureQueryType }
     * 
     */
    public DataStructureQueryType createDataStructureQueryType() {
        return new DataStructureQueryType();
    }

    /**
     * Create an instance of {@link ConceptSchemeQueryType }
     * 
     */
    public ConceptSchemeQueryType createConceptSchemeQueryType() {
        return new ConceptSchemeQueryType();
    }

    /**
     * Create an instance of {@link DataSchemaQueryType }
     * 
     */
    public DataSchemaQueryType createDataSchemaQueryType() {
        return new DataSchemaQueryType();
    }

    /**
     * Create an instance of {@link AttributeValueType }
     * 
     */
    public AttributeValueType createAttributeValueType() {
        return new AttributeValueType();
    }

    /**
     * Create an instance of {@link ConstraintAttachmentWhereType }
     * 
     */
    public ConstraintAttachmentWhereType createConstraintAttachmentWhereType() {
        return new ConstraintAttachmentWhereType();
    }

    /**
     * Create an instance of {@link MetadataParametersOrType }
     * 
     */
    public MetadataParametersOrType createMetadataParametersOrType() {
        return new MetadataParametersOrType();
    }

    /**
     * Create an instance of {@link DataReturnDetailsType }
     * 
     */
    public DataReturnDetailsType createDataReturnDetailsType() {
        return new DataReturnDetailsType();
    }

    /**
     * Create an instance of {@link MetadataSchemaQueryType }
     * 
     */
    public MetadataSchemaQueryType createMetadataSchemaQueryType() {
        return new MetadataSchemaQueryType();
    }

    /**
     * Create an instance of {@link TimeSeriesDataReturnDetailsType }
     * 
     */
    public TimeSeriesDataReturnDetailsType createTimeSeriesDataReturnDetailsType() {
        return new TimeSeriesDataReturnDetailsType();
    }

    /**
     * Create an instance of {@link DataParametersAndType }
     * 
     */
    public DataParametersAndType createDataParametersAndType() {
        return new DataParametersAndType();
    }

    /**
     * Create an instance of {@link MappedObjectRefType }
     * 
     */
    public MappedObjectRefType createMappedObjectRefType() {
        return new MappedObjectRefType();
    }

    /**
     * Create an instance of {@link CodeValueType }
     * 
     */
    public CodeValueType createCodeValueType() {
        return new CodeValueType();
    }

    /**
     * Create an instance of {@link GenericTimeSeriesDataQueryType }
     * 
     */
    public GenericTimeSeriesDataQueryType createGenericTimeSeriesDataQueryType() {
        return new GenericTimeSeriesDataQueryType();
    }

    /**
     * Create an instance of {@link TimeSeriesDataQueryType }
     * 
     */
    public TimeSeriesDataQueryType createTimeSeriesDataQueryType() {
        return new TimeSeriesDataQueryType();
    }

    /**
     * Create an instance of {@link QueryIDType }
     * 
     */
    public QueryIDType createQueryIDType() {
        return new QueryIDType();
    }

    /**
     * Create an instance of {@link CodelistQueryType }
     * 
     */
    public CodelistQueryType createCodelistQueryType() {
        return new CodelistQueryType();
    }

    /**
     * Create an instance of {@link MetadataTargetValueType }
     * 
     */
    public MetadataTargetValueType createMetadataTargetValueType() {
        return new MetadataTargetValueType();
    }

    /**
     * Create an instance of {@link StructureReturnDetailsType }
     * 
     */
    public StructureReturnDetailsType createStructureReturnDetailsType() {
        return new StructureReturnDetailsType();
    }

    /**
     * Create an instance of {@link InputOrOutputObjectType }
     * 
     */
    public InputOrOutputObjectType createInputOrOutputObjectType() {
        return new InputOrOutputObjectType();
    }

    /**
     * Create an instance of {@link MetadataStructureQueryType }
     * 
     */
    public MetadataStructureQueryType createMetadataStructureQueryType() {
        return new MetadataStructureQueryType();
    }

    /**
     * Create an instance of {@link GenericDataQueryType }
     * 
     */
    public GenericDataQueryType createGenericDataQueryType() {
        return new GenericDataQueryType();
    }

    /**
     * Create an instance of {@link MappedObjectType }
     * 
     */
    public MappedObjectType createMappedObjectType() {
        return new MappedObjectType();
    }

    /**
     * Create an instance of {@link ConstraintQueryType }
     * 
     */
    public ConstraintQueryType createConstraintQueryType() {
        return new ConstraintQueryType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrimaryMeasureWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "PrimaryMeasureWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentWhere")
    public JAXBElement<PrimaryMeasureWhereType> createPrimaryMeasureWhere(PrimaryMeasureWhereType value) {
        return new JAXBElement<PrimaryMeasureWhereType>(_PrimaryMeasureWhere_QNAME, PrimaryMeasureWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumericValueType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "NumericValue")
    public JAXBElement<NumericValueType> createNumericValue(NumericValueType value) {
        return new JAXBElement<NumericValueType>(_NumericValue_QNAME, NumericValueType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ProcessWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<ProcessWhereType> createProcessWhere(ProcessWhereType value) {
        return new JAXBElement<ProcessWhereType>(_ProcessWhere_QNAME, ProcessWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataStructureWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "DataStructureWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<DataStructureWhereType> createDataStructureWhere(DataStructureWhereType value) {
        return new JAXBElement<DataStructureWhereType>(_DataStructureWhere_QNAME, DataStructureWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HierarchicalCodelistWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "HierarchicalCodelistWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<HierarchicalCodelistWhereType> createHierarchicalCodelistWhere(HierarchicalCodelistWhereType value) {
        return new JAXBElement<HierarchicalCodelistWhereType>(_HierarchicalCodelistWhere_QNAME, HierarchicalCodelistWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategorySchemeWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "CategorySchemeWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<CategorySchemeWhereType> createCategorySchemeWhere(CategorySchemeWhereType value) {
        return new JAXBElement<CategorySchemeWhereType>(_CategorySchemeWhere_QNAME, CategorySchemeWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleValueType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "Value")
    public JAXBElement<SimpleValueType> createValue(SimpleValueType value) {
        return new JAXBElement<SimpleValueType>(_Value_QNAME, SimpleValueType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategorisationWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "CategorisationWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<CategorisationWhereType> createCategorisationWhere(CategorisationWhereType value) {
        return new JAXBElement<CategorisationWhereType>(_CategorisationWhere_QNAME, CategorisationWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimePeriodValueType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "TimeValue")
    public JAXBElement<TimePeriodValueType> createTimeValue(TimePeriodValueType value) {
        return new JAXBElement<TimePeriodValueType>(_TimeValue_QNAME, TimePeriodValueType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ComponentListWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ComponentListWhere")
    public JAXBElement<ComponentListWhereType> createComponentListWhere(ComponentListWhereType value) {
        return new JAXBElement<ComponentListWhereType>(_ComponentListWhere_QNAME, ComponentListWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataflowWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "DataflowWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<DataflowWhereType> createDataflowWhere(DataflowWhereType value) {
        return new JAXBElement<DataflowWhereType>(_DataflowWhere_QNAME, DataflowWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ComponentWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ComponentWhere")
    public JAXBElement<ComponentWhereType> createComponentWhere(ComponentWhereType value) {
        return new JAXBElement<ComponentWhereType>(_ComponentWhere_QNAME, ComponentWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DimensionWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "GroupDimensionWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentWhere")
    public JAXBElement<DimensionWhereType> createGroupDimensionWhere(DimensionWhereType value) {
        return new JAXBElement<DimensionWhereType>(_GroupDimensionWhere_QNAME, DimensionWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConceptWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ConceptWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ItemWhere")
    public JAXBElement<ConceptWhereType> createConceptWhere(ConceptWhereType value) {
        return new JAXBElement<ConceptWhereType>(_ConceptWhere_QNAME, ConceptWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrganisationWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "OrganisationWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ItemWhere")
    public JAXBElement<OrganisationWhereType> createOrganisationWhere(OrganisationWhereType value) {
        return new JAXBElement<OrganisationWhereType>(_OrganisationWhere_QNAME, OrganisationWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructuresWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "StructuresWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<StructuresWhereType> createStructuresWhere(StructuresWhereType value) {
        return new JAXBElement<StructuresWhereType>(_StructuresWhere_QNAME, StructuresWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "CategoryWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ItemWhere")
    public JAXBElement<CategoryWhereType> createCategoryWhere(CategoryWhereType value) {
        return new JAXBElement<CategoryWhereType>(_CategoryWhere_QNAME, CategoryWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryTextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "TextValue")
    public JAXBElement<QueryTextType> createTextValue(QueryTextType value) {
        return new JAXBElement<QueryTextType>(_TextValue_QNAME, QueryTextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataTargetWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "MetadataTargetWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentListWhere")
    public JAXBElement<MetadataTargetWhereType> createMetadataTargetWhere(MetadataTargetWhereType value) {
        return new JAXBElement<MetadataTargetWhereType>(_MetadataTargetWhere_QNAME, MetadataTargetWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataAttributeWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "MetadataAttributeWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentWhere")
    public JAXBElement<MetadataAttributeWhereType> createMetadataAttributeWhere(MetadataAttributeWhereType value) {
        return new JAXBElement<MetadataAttributeWhereType>(_MetadataAttributeWhere_QNAME, MetadataAttributeWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "AttributeWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentWhere")
    public JAXBElement<AttributeWhereType> createAttributeWhere(AttributeWhereType value) {
        return new JAXBElement<AttributeWhereType>(_AttributeWhere_QNAME, AttributeWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodeWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "CodeWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ItemWhere")
    public JAXBElement<CodeWhereType> createCodeWhere(CodeWhereType value) {
        return new JAXBElement<CodeWhereType>(_CodeWhere_QNAME, CodeWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "GroupWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentListWhere")
    public JAXBElement<GroupWhereType> createGroupWhere(GroupWhereType value) {
        return new JAXBElement<GroupWhereType>(_GroupWhere_QNAME, GroupWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DimensionWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "DimensionWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentWhere")
    public JAXBElement<DimensionWhereType> createDimensionWhere(DimensionWhereType value) {
        return new JAXBElement<DimensionWhereType>(_DimensionWhere_QNAME, DimensionWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConceptSchemeWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ConceptSchemeWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<ConceptSchemeWhereType> createConceptSchemeWhere(ConceptSchemeWhereType value) {
        return new JAXBElement<ConceptSchemeWhereType>(_ConceptSchemeWhere_QNAME, ConceptSchemeWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisionAgreementWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ProvisionAgreementWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<ProvisionAgreementWhereType> createProvisionAgreementWhere(ProvisionAgreementWhereType value) {
        return new JAXBElement<ProvisionAgreementWhereType>(_ProvisionAgreementWhere_QNAME, ProvisionAgreementWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConstraintWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ConstraintWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<ConstraintWhereType> createConstraintWhere(ConstraintWhereType value) {
        return new JAXBElement<ConstraintWhereType>(_ConstraintWhere_QNAME, ConstraintWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataflowWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "MetadataflowWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<MetadataflowWhereType> createMetadataflowWhere(MetadataflowWhereType value) {
        return new JAXBElement<MetadataflowWhereType>(_MetadataflowWhere_QNAME, MetadataflowWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportStructureWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ReportStructureWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentListWhere")
    public JAXBElement<ReportStructureWhereType> createReportStructureWhere(ReportStructureWhereType value) {
        return new JAXBElement<ReportStructureWhereType>(_ReportStructureWhere_QNAME, ReportStructureWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportingTaxonomyWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ReportingTaxonomyWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<ReportingTaxonomyWhereType> createReportingTaxonomyWhere(ReportingTaxonomyWhereType value) {
        return new JAXBElement<ReportingTaxonomyWhereType>(_ReportingTaxonomyWhere_QNAME, ReportingTaxonomyWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimeDimensionWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "TimeDimensionWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentWhere")
    public JAXBElement<TimeDimensionWhereType> createTimeDimensionWhere(TimeDimensionWhereType value) {
        return new JAXBElement<TimeDimensionWhereType>(_TimeDimensionWhere_QNAME, TimeDimensionWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TargetObjectWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "TargetObjectWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentWhere")
    public JAXBElement<TargetObjectWhereType> createTargetObjectWhere(TargetObjectWhereType value) {
        return new JAXBElement<TargetObjectWhereType>(_TargetObjectWhere_QNAME, TargetObjectWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrganisationSchemeWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "OrganisationSchemeWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<OrganisationSchemeWhereType> createOrganisationSchemeWhere(OrganisationSchemeWhereType value) {
        return new JAXBElement<OrganisationSchemeWhereType>(_OrganisationSchemeWhere_QNAME, OrganisationSchemeWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureSetWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "StructureSetWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<StructureSetWhereType> createStructureSetWhere(StructureSetWhereType value) {
        return new JAXBElement<StructureSetWhereType>(_StructureSetWhere_QNAME, StructureSetWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportingCategoryWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ReportingCategoryWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ItemWhere")
    public JAXBElement<ReportingCategoryWhereType> createReportingCategoryWhere(ReportingCategoryWhereType value) {
        return new JAXBElement<ReportingCategoryWhereType>(_ReportingCategoryWhere_QNAME, ReportingCategoryWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodelistWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "CodelistWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<CodelistWhereType> createCodelistWhere(CodelistWhereType value) {
        return new JAXBElement<CodelistWhereType>(_CodelistWhere_QNAME, CodelistWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataStructureWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "MetadataStructureWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "StructuralMetadataWhere")
    public JAXBElement<MetadataStructureWhereType> createMetadataStructureWhere(MetadataStructureWhereType value) {
        return new JAXBElement<MetadataStructureWhereType>(_MetadataStructureWhere_QNAME, MetadataStructureWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeasureDimensionWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "MeasureDimensionWhere", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", substitutionHeadName = "ComponentWhere")
    public JAXBElement<MeasureDimensionWhereType> createMeasureDimensionWhere(MeasureDimensionWhereType value) {
        return new JAXBElement<MeasureDimensionWhereType>(_MeasureDimensionWhere_QNAME, MeasureDimensionWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ItemWhere")
    public JAXBElement<ItemWhereType> createItemWhere(ItemWhereType value) {
        return new JAXBElement<ItemWhereType>(_ItemWhere_QNAME, ItemWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainableWhereType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "StructuralMetadataWhere")
    public JAXBElement<MaintainableWhereType> createStructuralMetadataWhere(MaintainableWhereType value) {
        return new JAXBElement<MaintainableWhereType>(_StructuralMetadataWhere_QNAME, MaintainableWhereType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataStructureReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "DataStructure", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<DataStructureReferenceType> createConstraintAttachmentWhereTypeDataStructure(DataStructureReferenceType value) {
        return new JAXBElement<DataStructureReferenceType>(_ConstraintAttachmentWhereTypeDataStructure_QNAME, DataStructureReferenceType.class, ConstraintAttachmentWhereType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataStructureReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "MetadataStructure", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<MetadataStructureReferenceType> createConstraintAttachmentWhereTypeMetadataStructure(MetadataStructureReferenceType value) {
        return new JAXBElement<MetadataStructureReferenceType>(_ConstraintAttachmentWhereTypeMetadataStructure_QNAME, MetadataStructureReferenceType.class, ConstraintAttachmentWhereType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "MetadataSet", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<SetReferenceType> createConstraintAttachmentWhereTypeMetadataSet(SetReferenceType value) {
        return new JAXBElement<SetReferenceType>(_ConstraintAttachmentWhereTypeMetadataSet_QNAME, SetReferenceType.class, ConstraintAttachmentWhereType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataflowReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "Dataflow", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<DataflowReferenceType> createConstraintAttachmentWhereTypeDataflow(DataflowReferenceType value) {
        return new JAXBElement<DataflowReferenceType>(_ConstraintAttachmentWhereTypeDataflow_QNAME, DataflowReferenceType.class, ConstraintAttachmentWhereType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "DataSourceURL", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<String> createConstraintAttachmentWhereTypeDataSourceURL(String value) {
        return new JAXBElement<String>(_ConstraintAttachmentWhereTypeDataSourceURL_QNAME, String.class, ConstraintAttachmentWhereType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataProviderReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "DataProvider", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<DataProviderReferenceType> createConstraintAttachmentWhereTypeDataProvider(DataProviderReferenceType value) {
        return new JAXBElement<DataProviderReferenceType>(_ConstraintAttachmentWhereTypeDataProvider_QNAME, DataProviderReferenceType.class, ConstraintAttachmentWhereType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataflowReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "Metadataflow", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<MetadataflowReferenceType> createConstraintAttachmentWhereTypeMetadataflow(MetadataflowReferenceType value) {
        return new JAXBElement<MetadataflowReferenceType>(_ConstraintAttachmentWhereTypeMetadataflow_QNAME, MetadataflowReferenceType.class, ConstraintAttachmentWhereType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisionAgreementReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "ProvisionAgreement", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<ProvisionAgreementReferenceType> createConstraintAttachmentWhereTypeProvisionAgreement(ProvisionAgreementReferenceType value) {
        return new JAXBElement<ProvisionAgreementReferenceType>(_ConstraintAttachmentWhereTypeProvisionAgreement_QNAME, ProvisionAgreementReferenceType.class, ConstraintAttachmentWhereType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", name = "DataSet", scope = ConstraintAttachmentWhereType.class)
    public JAXBElement<SetReferenceType> createConstraintAttachmentWhereTypeDataSet(SetReferenceType value) {
        return new JAXBElement<SetReferenceType>(_ConstraintAttachmentWhereTypeDataSet_QNAME, SetReferenceType.class, ConstraintAttachmentWhereType.class, value);
    }

}
