
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ActionType;


/**
 * NotifyRegistryEventType describes the structure a registry notification, in response to a subscription to a registry event. At a minimum, the event time, a reference to the change object, a reference to the underlying subscription triggering the notification, and the action that took place on the object are sent. In addition, the full details of the object may be provided at the discretion of the registry. In the event that the details are not sent, it will be possible to query for the details of the changed object using the reference provided.
 * 
 * <p>Java-Klasse f�r NotifyRegistryEventType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NotifyRegistryEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;choice>
 *           &lt;element name="ObjectURN" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *           &lt;element name="RegistrationID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType"/>
 *         &lt;/choice>
 *         &lt;element name="SubscriptionURN" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *         &lt;element name="EventAction" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ActionType"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="StructuralEvent" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}StructuralEventType"/>
 *           &lt;element name="RegistrationEvent" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}RegistrationEventType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NotifyRegistryEventType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "eventTime",
    "objectURN",
    "registrationID",
    "subscriptionURN",
    "eventAction",
    "structuralEvent",
    "registrationEvent"
})
public class NotifyRegistryEventType {

    @XmlElement(name = "EventTime", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventTime;
    @XmlElement(name = "ObjectURN", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    @XmlSchemaType(name = "anyURI")
    protected String objectURN;
    @XmlElement(name = "RegistrationID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected String registrationID;
    @XmlElement(name = "SubscriptionURN", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    @XmlSchemaType(name = "anyURI")
    protected String subscriptionURN;
    @XmlElement(name = "EventAction", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected ActionType eventAction;
    @XmlElement(name = "StructuralEvent", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected StructuralEventType structuralEvent;
    @XmlElement(name = "RegistrationEvent", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected RegistrationEventType registrationEvent;

    /**
     * Ruft den Wert der eventTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventTime() {
        return eventTime;
    }

    /**
     * Legt den Wert der eventTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventTime(XMLGregorianCalendar value) {
        this.eventTime = value;
    }

    /**
     * Ruft den Wert der objectURN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectURN() {
        return objectURN;
    }

    /**
     * Legt den Wert der objectURN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectURN(String value) {
        this.objectURN = value;
    }

    /**
     * Ruft den Wert der registrationID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationID() {
        return registrationID;
    }

    /**
     * Legt den Wert der registrationID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationID(String value) {
        this.registrationID = value;
    }

    /**
     * Ruft den Wert der subscriptionURN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriptionURN() {
        return subscriptionURN;
    }

    /**
     * Legt den Wert der subscriptionURN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriptionURN(String value) {
        this.subscriptionURN = value;
    }

    /**
     * Ruft den Wert der eventAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getEventAction() {
        return eventAction;
    }

    /**
     * Legt den Wert der eventAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setEventAction(ActionType value) {
        this.eventAction = value;
    }

    /**
     * Ruft den Wert der structuralEvent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StructuralEventType }
     *     
     */
    public StructuralEventType getStructuralEvent() {
        return structuralEvent;
    }

    /**
     * Legt den Wert der structuralEvent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructuralEventType }
     *     
     */
    public void setStructuralEvent(StructuralEventType value) {
        this.structuralEvent = value;
    }

    /**
     * Ruft den Wert der registrationEvent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationEventType }
     *     
     */
    public RegistrationEventType getRegistrationEvent() {
        return registrationEvent;
    }

    /**
     * Legt den Wert der registrationEvent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationEventType }
     *     
     */
    public void setRegistrationEvent(RegistrationEventType value) {
        this.registrationEvent = value;
    }

}
