
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataKeySetType defines a collection of metadata keys (identifier component values).
 * 
 * <p>Java-Klasse f�r MetadataKeySetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataKeySetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}KeySetType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataKeyType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataKeySetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class MetadataKeySetType
    extends KeySetType
{


}
