
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MaintainableReturnDetailsType defines the structure for the return details of a non-item scheme maintainable object. It eliminates the detail options that are specific to searching an item scheme
 * 
 * <p>Java-Klasse f�r MaintainableReturnDetailsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MaintainableReturnDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructureReturnDetailsType">
 *       &lt;sequence>
 *         &lt;element name="References" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReferencesType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="detail" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MaintainableReturnDetailType" default="Full" />
 *       &lt;attribute name="returnMatchedArtefact" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintainableReturnDetailsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class MaintainableReturnDetailsType
    extends StructureReturnDetailsType
{


}
