
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataStructureReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataflowReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.QueryableDataSourceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SetReferenceType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.sdmx.resources.sdmxml.schemas.v2_1.structure package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GroupDimension_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "GroupDimension");
    private final static QName _ReportingYearStartDay_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ReportingYearStartDay");
    private final static QName _DataStructureComponents_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "DataStructureComponents");
    private final static QName _Structures_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Structures");
    private final static QName _ComponentList_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ComponentList");
    private final static QName _MeasureDimension_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "MeasureDimension");
    private final static QName _CategoryMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "CategoryMap");
    private final static QName _Group_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Group");
    private final static QName _Attribute_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Attribute");
    private final static QName _Code_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Code");
    private final static QName _DataSetTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "DataSetTarget");
    private final static QName _AttributeList_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "AttributeList");
    private final static QName _DataConsumer_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "DataConsumer");
    private final static QName _Agency_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Agency");
    private final static QName _Organisation_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Organisation");
    private final static QName _PrimaryMeasure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "PrimaryMeasure");
    private final static QName _Component_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Component");
    private final static QName _MetadataStructureComponents_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "MetadataStructureComponents");
    private final static QName _IdentifiableObjectTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "IdentifiableObjectTarget");
    private final static QName _ConceptMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ConceptMap");
    private final static QName _MetadataTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "MetadataTarget");
    private final static QName _Dimension_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Dimension");
    private final static QName _CodeMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "CodeMap");
    private final static QName _MetadataAttribute_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "MetadataAttribute");
    private final static QName _KeyDescriptorValuesTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "KeyDescriptorValuesTarget");
    private final static QName _ConstraintContentTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ConstraintContentTarget");
    private final static QName _Grouping_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Grouping");
    private final static QName _ReportingCategoryMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ReportingCategoryMap");
    private final static QName _ReportStructure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ReportStructure");
    private final static QName _OrganisationUnit_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "OrganisationUnit");
    private final static QName _Item_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Item");
    private final static QName _ReportPeriodTarget_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ReportPeriodTarget");
    private final static QName _DataProvider_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "DataProvider");
    private final static QName _ItemAssociation_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ItemAssociation");
    private final static QName _OrganisationMap_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "OrganisationMap");
    private final static QName _Category_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Category");
    private final static QName _MeasureList_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "MeasureList");
    private final static QName _Concept_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Concept");
    private final static QName _DimensionList_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "DimensionList");
    private final static QName _TimeDimension_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "TimeDimension");
    private final static QName _ReportingCategory_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ReportingCategory");
    private final static QName _ContactTypeURI_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "URI");
    private final static QName _ContactTypeFax_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Fax");
    private final static QName _ContactTypeEmail_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Email");
    private final static QName _ContactTypeTelephone_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Telephone");
    private final static QName _ContactTypeX400_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "X400");
    private final static QName _ConstraintAttachmentTypeDataStructure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "DataStructure");
    private final static QName _ConstraintAttachmentTypeMetadataStructure_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "MetadataStructure");
    private final static QName _ConstraintAttachmentTypeQueryableDataSource_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "QueryableDataSource");
    private final static QName _ConstraintAttachmentTypeDataflow_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Dataflow");
    private final static QName _ConstraintAttachmentTypeMetadataSet_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "MetadataSet");
    private final static QName _ConstraintAttachmentTypeDataSet_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "DataSet");
    private final static QName _ConstraintAttachmentTypeMetadataflow_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "Metadataflow");
    private final static QName _ConstraintAttachmentTypeProvisionAgreement_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "ProvisionAgreement");
    private final static QName _ConstraintAttachmentTypeSimpleDataSource_QNAME = new QName("http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", "SimpleDataSource");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.sdmx.resources.sdmxml.schemas.v2_1.structure
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GroupType }
     * 
     */
    public GroupType createGroupType() {
        return new GroupType();
    }

    /**
     * Create an instance of {@link OrganisationMapType }
     * 
     */
    public OrganisationMapType createOrganisationMapType() {
        return new OrganisationMapType();
    }

    /**
     * Create an instance of {@link CategoryType }
     * 
     */
    public CategoryType createCategoryType() {
        return new CategoryType();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link AttributeListType }
     * 
     */
    public AttributeListType createAttributeListType() {
        return new AttributeListType();
    }

    /**
     * Create an instance of {@link MetadataAttributeType }
     * 
     */
    public MetadataAttributeType createMetadataAttributeType() {
        return new MetadataAttributeType();
    }

    /**
     * Create an instance of {@link CategoryMapType }
     * 
     */
    public CategoryMapType createCategoryMapType() {
        return new CategoryMapType();
    }

    /**
     * Create an instance of {@link MeasureDimensionType }
     * 
     */
    public MeasureDimensionType createMeasureDimensionType() {
        return new MeasureDimensionType();
    }

    /**
     * Create an instance of {@link KeyDescriptorValuesTargetType }
     * 
     */
    public KeyDescriptorValuesTargetType createKeyDescriptorValuesTargetType() {
        return new KeyDescriptorValuesTargetType();
    }

    /**
     * Create an instance of {@link DataProviderType }
     * 
     */
    public DataProviderType createDataProviderType() {
        return new DataProviderType();
    }

    /**
     * Create an instance of {@link MetadataTargetType }
     * 
     */
    public MetadataTargetType createMetadataTargetType() {
        return new MetadataTargetType();
    }

    /**
     * Create an instance of {@link ReportingCategoryType }
     * 
     */
    public ReportingCategoryType createReportingCategoryType() {
        return new ReportingCategoryType();
    }

    /**
     * Create an instance of {@link CodeMapType }
     * 
     */
    public CodeMapType createCodeMapType() {
        return new CodeMapType();
    }

    /**
     * Create an instance of {@link DimensionListType }
     * 
     */
    public DimensionListType createDimensionListType() {
        return new DimensionListType();
    }

    /**
     * Create an instance of {@link PrimaryMeasureType }
     * 
     */
    public PrimaryMeasureType createPrimaryMeasureType() {
        return new PrimaryMeasureType();
    }

    /**
     * Create an instance of {@link StructuresType }
     * 
     */
    public StructuresType createStructuresType() {
        return new StructuresType();
    }

    /**
     * Create an instance of {@link TimeDimensionType }
     * 
     */
    public TimeDimensionType createTimeDimensionType() {
        return new TimeDimensionType();
    }

    /**
     * Create an instance of {@link ConceptMapType }
     * 
     */
    public ConceptMapType createConceptMapType() {
        return new ConceptMapType();
    }

    /**
     * Create an instance of {@link ConceptType }
     * 
     */
    public ConceptType createConceptType() {
        return new ConceptType();
    }

    /**
     * Create an instance of {@link ReportingYearStartDayType }
     * 
     */
    public ReportingYearStartDayType createReportingYearStartDayType() {
        return new ReportingYearStartDayType();
    }

    /**
     * Create an instance of {@link OrganisationUnitType }
     * 
     */
    public OrganisationUnitType createOrganisationUnitType() {
        return new OrganisationUnitType();
    }

    /**
     * Create an instance of {@link AgencyType }
     * 
     */
    public AgencyType createAgencyType() {
        return new AgencyType();
    }

    /**
     * Create an instance of {@link DataStructureComponentsType }
     * 
     */
    public DataStructureComponentsType createDataStructureComponentsType() {
        return new DataStructureComponentsType();
    }

    /**
     * Create an instance of {@link DataSetTargetType }
     * 
     */
    public DataSetTargetType createDataSetTargetType() {
        return new DataSetTargetType();
    }

    /**
     * Create an instance of {@link ReportPeriodTargetType }
     * 
     */
    public ReportPeriodTargetType createReportPeriodTargetType() {
        return new ReportPeriodTargetType();
    }

    /**
     * Create an instance of {@link ReportingCategoryMapType }
     * 
     */
    public ReportingCategoryMapType createReportingCategoryMapType() {
        return new ReportingCategoryMapType();
    }

    /**
     * Create an instance of {@link DimensionType }
     * 
     */
    public DimensionType createDimensionType() {
        return new DimensionType();
    }

    /**
     * Create an instance of {@link CodeType }
     * 
     */
    public CodeType createCodeType() {
        return new CodeType();
    }

    /**
     * Create an instance of {@link IdentifiableObjectTargetType }
     * 
     */
    public IdentifiableObjectTargetType createIdentifiableObjectTargetType() {
        return new IdentifiableObjectTargetType();
    }

    /**
     * Create an instance of {@link ReportStructureType }
     * 
     */
    public ReportStructureType createReportStructureType() {
        return new ReportStructureType();
    }

    /**
     * Create an instance of {@link DataConsumerType }
     * 
     */
    public DataConsumerType createDataConsumerType() {
        return new DataConsumerType();
    }

    /**
     * Create an instance of {@link GroupDimensionType }
     * 
     */
    public GroupDimensionType createGroupDimensionType() {
        return new GroupDimensionType();
    }

    /**
     * Create an instance of {@link MeasureListType }
     * 
     */
    public MeasureListType createMeasureListType() {
        return new MeasureListType();
    }

    /**
     * Create an instance of {@link ConstraintContentTargetType }
     * 
     */
    public ConstraintContentTargetType createConstraintContentTargetType() {
        return new ConstraintContentTargetType();
    }

    /**
     * Create an instance of {@link MetadataStructureComponentsType }
     * 
     */
    public MetadataStructureComponentsType createMetadataStructureComponentsType() {
        return new MetadataStructureComponentsType();
    }

    /**
     * Create an instance of {@link LevelType }
     * 
     */
    public LevelType createLevelType() {
        return new LevelType();
    }

    /**
     * Create an instance of {@link MetadataKeySetType }
     * 
     */
    public MetadataKeySetType createMetadataKeySetType() {
        return new MetadataKeySetType();
    }

    /**
     * Create an instance of {@link ProcessesType }
     * 
     */
    public ProcessesType createProcessesType() {
        return new ProcessesType();
    }

    /**
     * Create an instance of {@link MetadataflowType }
     * 
     */
    public MetadataflowType createMetadataflowType() {
        return new MetadataflowType();
    }

    /**
     * Create an instance of {@link ContentConstraintType }
     * 
     */
    public ContentConstraintType createContentConstraintType() {
        return new ContentConstraintType();
    }

    /**
     * Create an instance of {@link ConceptSchemeMapType }
     * 
     */
    public ConceptSchemeMapType createConceptSchemeMapType() {
        return new ConceptSchemeMapType();
    }

    /**
     * Create an instance of {@link HierarchicalCodeType }
     * 
     */
    public HierarchicalCodeType createHierarchicalCodeType() {
        return new HierarchicalCodeType();
    }

    /**
     * Create an instance of {@link KeyDescriptorValuesRepresentationType }
     * 
     */
    public KeyDescriptorValuesRepresentationType createKeyDescriptorValuesRepresentationType() {
        return new KeyDescriptorValuesRepresentationType();
    }

    /**
     * Create an instance of {@link ProvisionAgreementType }
     * 
     */
    public ProvisionAgreementType createProvisionAgreementType() {
        return new ProvisionAgreementType();
    }

    /**
     * Create an instance of {@link ReportingYearStartDayRepresentationType }
     * 
     */
    public ReportingYearStartDayRepresentationType createReportingYearStartDayRepresentationType() {
        return new ReportingYearStartDayRepresentationType();
    }

    /**
     * Create an instance of {@link AttachmentConstraintAttachmentType }
     * 
     */
    public AttachmentConstraintAttachmentType createAttachmentConstraintAttachmentType() {
        return new AttachmentConstraintAttachmentType();
    }

    /**
     * Create an instance of {@link CodededTextFormatType }
     * 
     */
    public CodededTextFormatType createCodededTextFormatType() {
        return new CodededTextFormatType();
    }

    /**
     * Create an instance of {@link SimpleComponentTextFormatType }
     * 
     */
    public SimpleComponentTextFormatType createSimpleComponentTextFormatType() {
        return new SimpleComponentTextFormatType();
    }

    /**
     * Create an instance of {@link SimpleDataStructureRepresentationType }
     * 
     */
    public SimpleDataStructureRepresentationType createSimpleDataStructureRepresentationType() {
        return new SimpleDataStructureRepresentationType();
    }

    /**
     * Create an instance of {@link StructureMapType }
     * 
     */
    public StructureMapType createStructureMapType() {
        return new StructureMapType();
    }

    /**
     * Create an instance of {@link MetadataStructuresType }
     * 
     */
    public MetadataStructuresType createMetadataStructuresType() {
        return new MetadataStructuresType();
    }

    /**
     * Create an instance of {@link IdentifiableObjectTextFormatType }
     * 
     */
    public IdentifiableObjectTextFormatType createIdentifiableObjectTextFormatType() {
        return new IdentifiableObjectTextFormatType();
    }

    /**
     * Create an instance of {@link ProcessType }
     * 
     */
    public ProcessType createProcessType() {
        return new ProcessType();
    }

    /**
     * Create an instance of {@link DataflowType }
     * 
     */
    public DataflowType createDataflowType() {
        return new DataflowType();
    }

    /**
     * Create an instance of {@link IncludedCodelistReferenceType }
     * 
     */
    public IncludedCodelistReferenceType createIncludedCodelistReferenceType() {
        return new IncludedCodelistReferenceType();
    }

    /**
     * Create an instance of {@link ProcessStepType }
     * 
     */
    public ProcessStepType createProcessStepType() {
        return new ProcessStepType();
    }

    /**
     * Create an instance of {@link CodingTextFormatType }
     * 
     */
    public CodingTextFormatType createCodingTextFormatType() {
        return new CodingTextFormatType();
    }

    /**
     * Create an instance of {@link CodelistMapType }
     * 
     */
    public CodelistMapType createCodelistMapType() {
        return new CodelistMapType();
    }

    /**
     * Create an instance of {@link DataflowsType }
     * 
     */
    public DataflowsType createDataflowsType() {
        return new DataflowsType();
    }

    /**
     * Create an instance of {@link CategorisationType }
     * 
     */
    public CategorisationType createCategorisationType() {
        return new CategorisationType();
    }

    /**
     * Create an instance of {@link ConceptRepresentation }
     * 
     */
    public ConceptRepresentation createConceptRepresentation() {
        return new ConceptRepresentation();
    }

    /**
     * Create an instance of {@link ComputationType }
     * 
     */
    public ComputationType createComputationType() {
        return new ComputationType();
    }

    /**
     * Create an instance of {@link TargetObjectTextFormatType }
     * 
     */
    public TargetObjectTextFormatType createTargetObjectTextFormatType() {
        return new TargetObjectTextFormatType();
    }

    /**
     * Create an instance of {@link StructureSetsType }
     * 
     */
    public StructureSetsType createStructureSetsType() {
        return new StructureSetsType();
    }

    /**
     * Create an instance of {@link DataSetRepresentationType }
     * 
     */
    public DataSetRepresentationType createDataSetRepresentationType() {
        return new DataSetRepresentationType();
    }

    /**
     * Create an instance of {@link OrganisationSchemesType }
     * 
     */
    public OrganisationSchemesType createOrganisationSchemesType() {
        return new OrganisationSchemesType();
    }

    /**
     * Create an instance of {@link TextFormatType }
     * 
     */
    public TextFormatType createTextFormatType() {
        return new TextFormatType();
    }

    /**
     * Create an instance of {@link ContactType }
     * 
     */
    public ContactType createContactType() {
        return new ContactType();
    }

    /**
     * Create an instance of {@link CodelistType }
     * 
     */
    public CodelistType createCodelistType() {
        return new CodelistType();
    }

    /**
     * Create an instance of {@link ReleaseCalendarType }
     * 
     */
    public ReleaseCalendarType createReleaseCalendarType() {
        return new ReleaseCalendarType();
    }

    /**
     * Create an instance of {@link ReportingTaxonomyType }
     * 
     */
    public ReportingTaxonomyType createReportingTaxonomyType() {
        return new ReportingTaxonomyType();
    }

    /**
     * Create an instance of {@link MeasureDimensionRepresentationType }
     * 
     */
    public MeasureDimensionRepresentationType createMeasureDimensionRepresentationType() {
        return new MeasureDimensionRepresentationType();
    }

    /**
     * Create an instance of {@link ReportingYearStartDayTextFormatType }
     * 
     */
    public ReportingYearStartDayTextFormatType createReportingYearStartDayTextFormatType() {
        return new ReportingYearStartDayTextFormatType();
    }

    /**
     * Create an instance of {@link TransitionType }
     * 
     */
    public TransitionType createTransitionType() {
        return new TransitionType();
    }

    /**
     * Create an instance of {@link DataStructureType }
     * 
     */
    public DataStructureType createDataStructureType() {
        return new DataStructureType();
    }

    /**
     * Create an instance of {@link TimeTextFormatType }
     * 
     */
    public TimeTextFormatType createTimeTextFormatType() {
        return new TimeTextFormatType();
    }

    /**
     * Create an instance of {@link MetadataStructureComponentsBaseType }
     * 
     */
    public MetadataStructureComponentsBaseType createMetadataStructureComponentsBaseType() {
        return new MetadataStructureComponentsBaseType();
    }

    /**
     * Create an instance of {@link ValueMappingType }
     * 
     */
    public ValueMappingType createValueMappingType() {
        return new ValueMappingType();
    }

    /**
     * Create an instance of {@link ValueMapType }
     * 
     */
    public ValueMapType createValueMapType() {
        return new ValueMapType();
    }

    /**
     * Create an instance of {@link CategorisationsType }
     * 
     */
    public CategorisationsType createCategorisationsType() {
        return new CategorisationsType();
    }

    /**
     * Create an instance of {@link ReportingTaxonomiesType }
     * 
     */
    public ReportingTaxonomiesType createReportingTaxonomiesType() {
        return new ReportingTaxonomiesType();
    }

    /**
     * Create an instance of {@link NonFacetedTextFormatType }
     * 
     */
    public NonFacetedTextFormatType createNonFacetedTextFormatType() {
        return new NonFacetedTextFormatType();
    }

    /**
     * Create an instance of {@link ConceptsType }
     * 
     */
    public ConceptsType createConceptsType() {
        return new ConceptsType();
    }

    /**
     * Create an instance of {@link DataConsumerSchemeType }
     * 
     */
    public DataConsumerSchemeType createDataConsumerSchemeType() {
        return new DataConsumerSchemeType();
    }

    /**
     * Create an instance of {@link ConstraintRepresentationType }
     * 
     */
    public ConstraintRepresentationType createConstraintRepresentationType() {
        return new ConstraintRepresentationType();
    }

    /**
     * Create an instance of {@link AgencySchemeType }
     * 
     */
    public AgencySchemeType createAgencySchemeType() {
        return new AgencySchemeType();
    }

    /**
     * Create an instance of {@link CategorySchemeMapType }
     * 
     */
    public CategorySchemeMapType createCategorySchemeMapType() {
        return new CategorySchemeMapType();
    }

    /**
     * Create an instance of {@link CodelistsType }
     * 
     */
    public CodelistsType createCodelistsType() {
        return new CodelistsType();
    }

    /**
     * Create an instance of {@link HybridCodeMapType }
     * 
     */
    public HybridCodeMapType createHybridCodeMapType() {
        return new HybridCodeMapType();
    }

    /**
     * Create an instance of {@link KeyDescriptorValuesTextFormatType }
     * 
     */
    public KeyDescriptorValuesTextFormatType createKeyDescriptorValuesTextFormatType() {
        return new KeyDescriptorValuesTextFormatType();
    }

    /**
     * Create an instance of {@link RepresentationMapType }
     * 
     */
    public RepresentationMapType createRepresentationMapType() {
        return new RepresentationMapType();
    }

    /**
     * Create an instance of {@link OrganisationUnitSchemeType }
     * 
     */
    public OrganisationUnitSchemeType createOrganisationUnitSchemeType() {
        return new OrganisationUnitSchemeType();
    }

    /**
     * Create an instance of {@link HybridCodelistMapType }
     * 
     */
    public HybridCodelistMapType createHybridCodelistMapType() {
        return new HybridCodelistMapType();
    }

    /**
     * Create an instance of {@link DataSetTextFormatType }
     * 
     */
    public DataSetTextFormatType createDataSetTextFormatType() {
        return new DataSetTextFormatType();
    }

    /**
     * Create an instance of {@link MetadataflowsType }
     * 
     */
    public MetadataflowsType createMetadataflowsType() {
        return new MetadataflowsType();
    }

    /**
     * Create an instance of {@link InputOutputType }
     * 
     */
    public InputOutputType createInputOutputType() {
        return new InputOutputType();
    }

    /**
     * Create an instance of {@link HierarchyType }
     * 
     */
    public HierarchyType createHierarchyType() {
        return new HierarchyType();
    }

    /**
     * Create an instance of {@link AttributeRelationshipType }
     * 
     */
    public AttributeRelationshipType createAttributeRelationshipType() {
        return new AttributeRelationshipType();
    }

    /**
     * Create an instance of {@link ComponentMapType }
     * 
     */
    public ComponentMapType createComponentMapType() {
        return new ComponentMapType();
    }

    /**
     * Create an instance of {@link ISOConceptReferenceType }
     * 
     */
    public ISOConceptReferenceType createISOConceptReferenceType() {
        return new ISOConceptReferenceType();
    }

    /**
     * Create an instance of {@link IdentifiableObjectRepresentationType }
     * 
     */
    public IdentifiableObjectRepresentationType createIdentifiableObjectRepresentationType() {
        return new IdentifiableObjectRepresentationType();
    }

    /**
     * Create an instance of {@link ReportPeriodRepresentationType }
     * 
     */
    public ReportPeriodRepresentationType createReportPeriodRepresentationType() {
        return new ReportPeriodRepresentationType();
    }

    /**
     * Create an instance of {@link TimeDimensionRepresentationType }
     * 
     */
    public TimeDimensionRepresentationType createTimeDimensionRepresentationType() {
        return new TimeDimensionRepresentationType();
    }

    /**
     * Create an instance of {@link DataStructuresType }
     * 
     */
    public DataStructuresType createDataStructuresType() {
        return new DataStructuresType();
    }

    /**
     * Create an instance of {@link ReportingTaxonomyMapType }
     * 
     */
    public ReportingTaxonomyMapType createReportingTaxonomyMapType() {
        return new ReportingTaxonomyMapType();
    }

    /**
     * Create an instance of {@link ProvisionAgreementsType }
     * 
     */
    public ProvisionAgreementsType createProvisionAgreementsType() {
        return new ProvisionAgreementsType();
    }

    /**
     * Create an instance of {@link ConstraintsType }
     * 
     */
    public ConstraintsType createConstraintsType() {
        return new ConstraintsType();
    }

    /**
     * Create an instance of {@link ContentConstraintAttachmentType }
     * 
     */
    public ContentConstraintAttachmentType createContentConstraintAttachmentType() {
        return new ContentConstraintAttachmentType();
    }

    /**
     * Create an instance of {@link DataProviderSchemeType }
     * 
     */
    public DataProviderSchemeType createDataProviderSchemeType() {
        return new DataProviderSchemeType();
    }

    /**
     * Create an instance of {@link MetadataAttributeRepresentationType }
     * 
     */
    public MetadataAttributeRepresentationType createMetadataAttributeRepresentationType() {
        return new MetadataAttributeRepresentationType();
    }

    /**
     * Create an instance of {@link ConstraintTextFormatType }
     * 
     */
    public ConstraintTextFormatType createConstraintTextFormatType() {
        return new ConstraintTextFormatType();
    }

    /**
     * Create an instance of {@link StructureSetType }
     * 
     */
    public StructureSetType createStructureSetType() {
        return new StructureSetType();
    }

    /**
     * Create an instance of {@link OrganisationSchemeMapType }
     * 
     */
    public OrganisationSchemeMapType createOrganisationSchemeMapType() {
        return new OrganisationSchemeMapType();
    }

    /**
     * Create an instance of {@link DataKeySetType }
     * 
     */
    public DataKeySetType createDataKeySetType() {
        return new DataKeySetType();
    }

    /**
     * Create an instance of {@link BasicComponentTextFormatType }
     * 
     */
    public BasicComponentTextFormatType createBasicComponentTextFormatType() {
        return new BasicComponentTextFormatType();
    }

    /**
     * Create an instance of {@link AttachmentConstraintType }
     * 
     */
    public AttachmentConstraintType createAttachmentConstraintType() {
        return new AttachmentConstraintType();
    }

    /**
     * Create an instance of {@link MetadataStructureType }
     * 
     */
    public MetadataStructureType createMetadataStructureType() {
        return new MetadataStructureType();
    }

    /**
     * Create an instance of {@link CategorySchemesType }
     * 
     */
    public CategorySchemesType createCategorySchemesType() {
        return new CategorySchemesType();
    }

    /**
     * Create an instance of {@link HierarchicalCodelistsType }
     * 
     */
    public HierarchicalCodelistsType createHierarchicalCodelistsType() {
        return new HierarchicalCodelistsType();
    }

    /**
     * Create an instance of {@link ConceptSchemeType }
     * 
     */
    public ConceptSchemeType createConceptSchemeType() {
        return new ConceptSchemeType();
    }

    /**
     * Create an instance of {@link CategorySchemeType }
     * 
     */
    public CategorySchemeType createCategorySchemeType() {
        return new CategorySchemeType();
    }

    /**
     * Create an instance of {@link HierarchicalCodelistType }
     * 
     */
    public HierarchicalCodelistType createHierarchicalCodelistType() {
        return new HierarchicalCodelistType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupDimensionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "GroupDimension", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<GroupDimensionType> createGroupDimension(GroupDimensionType value) {
        return new JAXBElement<GroupDimensionType>(_GroupDimension_QNAME, GroupDimensionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportingYearStartDayType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ReportingYearStartDay", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<ReportingYearStartDayType> createReportingYearStartDay(ReportingYearStartDayType value) {
        return new JAXBElement<ReportingYearStartDayType>(_ReportingYearStartDay_QNAME, ReportingYearStartDayType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataStructureComponentsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "DataStructureComponents", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Grouping")
    public JAXBElement<DataStructureComponentsType> createDataStructureComponents(DataStructureComponentsType value) {
        return new JAXBElement<DataStructureComponentsType>(_DataStructureComponents_QNAME, DataStructureComponentsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructuresType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Structures")
    public JAXBElement<StructuresType> createStructures(StructuresType value) {
        return new JAXBElement<StructuresType>(_Structures_QNAME, StructuresType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ComponentListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ComponentList")
    public JAXBElement<ComponentListType> createComponentList(ComponentListType value) {
        return new JAXBElement<ComponentListType>(_ComponentList_QNAME, ComponentListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeasureDimensionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "MeasureDimension", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<MeasureDimensionType> createMeasureDimension(MeasureDimensionType value) {
        return new JAXBElement<MeasureDimensionType>(_MeasureDimension_QNAME, MeasureDimensionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryMapType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "CategoryMap", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ItemAssociation")
    public JAXBElement<CategoryMapType> createCategoryMap(CategoryMapType value) {
        return new JAXBElement<CategoryMapType>(_CategoryMap_QNAME, CategoryMapType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Group", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ComponentList")
    public JAXBElement<GroupType> createGroup(GroupType value) {
        return new JAXBElement<GroupType>(_Group_QNAME, GroupType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Attribute", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<AttributeType> createAttribute(AttributeType value) {
        return new JAXBElement<AttributeType>(_Attribute_QNAME, AttributeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Code", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Item")
    public JAXBElement<CodeType> createCode(CodeType value) {
        return new JAXBElement<CodeType>(_Code_QNAME, CodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSetTargetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "DataSetTarget", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<DataSetTargetType> createDataSetTarget(DataSetTargetType value) {
        return new JAXBElement<DataSetTargetType>(_DataSetTarget_QNAME, DataSetTargetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "AttributeList", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ComponentList")
    public JAXBElement<AttributeListType> createAttributeList(AttributeListType value) {
        return new JAXBElement<AttributeListType>(_AttributeList_QNAME, AttributeListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataConsumerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "DataConsumer", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Organisation")
    public JAXBElement<DataConsumerType> createDataConsumer(DataConsumerType value) {
        return new JAXBElement<DataConsumerType>(_DataConsumer_QNAME, DataConsumerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgencyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Agency", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Organisation")
    public JAXBElement<AgencyType> createAgency(AgencyType value) {
        return new JAXBElement<AgencyType>(_Agency_QNAME, AgencyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrganisationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Organisation", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Item")
    public JAXBElement<OrganisationType> createOrganisation(OrganisationType value) {
        return new JAXBElement<OrganisationType>(_Organisation_QNAME, OrganisationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrimaryMeasureType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "PrimaryMeasure", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<PrimaryMeasureType> createPrimaryMeasure(PrimaryMeasureType value) {
        return new JAXBElement<PrimaryMeasureType>(_PrimaryMeasure_QNAME, PrimaryMeasureType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ComponentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Component")
    public JAXBElement<ComponentType> createComponent(ComponentType value) {
        return new JAXBElement<ComponentType>(_Component_QNAME, ComponentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataStructureComponentsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "MetadataStructureComponents", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Grouping")
    public JAXBElement<MetadataStructureComponentsType> createMetadataStructureComponents(MetadataStructureComponentsType value) {
        return new JAXBElement<MetadataStructureComponentsType>(_MetadataStructureComponents_QNAME, MetadataStructureComponentsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentifiableObjectTargetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "IdentifiableObjectTarget", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<IdentifiableObjectTargetType> createIdentifiableObjectTarget(IdentifiableObjectTargetType value) {
        return new JAXBElement<IdentifiableObjectTargetType>(_IdentifiableObjectTarget_QNAME, IdentifiableObjectTargetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConceptMapType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ConceptMap", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ItemAssociation")
    public JAXBElement<ConceptMapType> createConceptMap(ConceptMapType value) {
        return new JAXBElement<ConceptMapType>(_ConceptMap_QNAME, ConceptMapType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataTargetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "MetadataTarget", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ComponentList")
    public JAXBElement<MetadataTargetType> createMetadataTarget(MetadataTargetType value) {
        return new JAXBElement<MetadataTargetType>(_MetadataTarget_QNAME, MetadataTargetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DimensionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Dimension", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<DimensionType> createDimension(DimensionType value) {
        return new JAXBElement<DimensionType>(_Dimension_QNAME, DimensionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodeMapType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "CodeMap", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ItemAssociation")
    public JAXBElement<CodeMapType> createCodeMap(CodeMapType value) {
        return new JAXBElement<CodeMapType>(_CodeMap_QNAME, CodeMapType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataAttributeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "MetadataAttribute", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<MetadataAttributeType> createMetadataAttribute(MetadataAttributeType value) {
        return new JAXBElement<MetadataAttributeType>(_MetadataAttribute_QNAME, MetadataAttributeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link KeyDescriptorValuesTargetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "KeyDescriptorValuesTarget", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<KeyDescriptorValuesTargetType> createKeyDescriptorValuesTarget(KeyDescriptorValuesTargetType value) {
        return new JAXBElement<KeyDescriptorValuesTargetType>(_KeyDescriptorValuesTarget_QNAME, KeyDescriptorValuesTargetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConstraintContentTargetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ConstraintContentTarget", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<ConstraintContentTargetType> createConstraintContentTarget(ConstraintContentTargetType value) {
        return new JAXBElement<ConstraintContentTargetType>(_ConstraintContentTarget_QNAME, ConstraintContentTargetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupingType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Grouping")
    public JAXBElement<GroupingType> createGrouping(GroupingType value) {
        return new JAXBElement<GroupingType>(_Grouping_QNAME, GroupingType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportingCategoryMapType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ReportingCategoryMap", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ItemAssociation")
    public JAXBElement<ReportingCategoryMapType> createReportingCategoryMap(ReportingCategoryMapType value) {
        return new JAXBElement<ReportingCategoryMapType>(_ReportingCategoryMap_QNAME, ReportingCategoryMapType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportStructureType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ReportStructure", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ComponentList")
    public JAXBElement<ReportStructureType> createReportStructure(ReportStructureType value) {
        return new JAXBElement<ReportStructureType>(_ReportStructure_QNAME, ReportStructureType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrganisationUnitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "OrganisationUnit", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Organisation")
    public JAXBElement<OrganisationUnitType> createOrganisationUnit(OrganisationUnitType value) {
        return new JAXBElement<OrganisationUnitType>(_OrganisationUnit_QNAME, OrganisationUnitType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Item")
    public JAXBElement<ItemType> createItem(ItemType value) {
        return new JAXBElement<ItemType>(_Item_QNAME, ItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportPeriodTargetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ReportPeriodTarget", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<ReportPeriodTargetType> createReportPeriodTarget(ReportPeriodTargetType value) {
        return new JAXBElement<ReportPeriodTargetType>(_ReportPeriodTarget_QNAME, ReportPeriodTargetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataProviderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "DataProvider", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Organisation")
    public JAXBElement<DataProviderType> createDataProvider(DataProviderType value) {
        return new JAXBElement<DataProviderType>(_DataProvider_QNAME, DataProviderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemAssociationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ItemAssociation")
    public JAXBElement<ItemAssociationType> createItemAssociation(ItemAssociationType value) {
        return new JAXBElement<ItemAssociationType>(_ItemAssociation_QNAME, ItemAssociationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrganisationMapType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "OrganisationMap", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ItemAssociation")
    public JAXBElement<OrganisationMapType> createOrganisationMap(OrganisationMapType value) {
        return new JAXBElement<OrganisationMapType>(_OrganisationMap_QNAME, OrganisationMapType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Category", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Item")
    public JAXBElement<CategoryType> createCategory(CategoryType value) {
        return new JAXBElement<CategoryType>(_Category_QNAME, CategoryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeasureListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "MeasureList", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ComponentList")
    public JAXBElement<MeasureListType> createMeasureList(MeasureListType value) {
        return new JAXBElement<MeasureListType>(_MeasureList_QNAME, MeasureListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConceptType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Concept", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Item")
    public JAXBElement<ConceptType> createConcept(ConceptType value) {
        return new JAXBElement<ConceptType>(_Concept_QNAME, ConceptType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DimensionListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "DimensionList", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "ComponentList")
    public JAXBElement<DimensionListType> createDimensionList(DimensionListType value) {
        return new JAXBElement<DimensionListType>(_DimensionList_QNAME, DimensionListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimeDimensionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "TimeDimension", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Component")
    public JAXBElement<TimeDimensionType> createTimeDimension(TimeDimensionType value) {
        return new JAXBElement<TimeDimensionType>(_TimeDimension_QNAME, TimeDimensionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportingCategoryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ReportingCategory", substitutionHeadNamespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", substitutionHeadName = "Item")
    public JAXBElement<ReportingCategoryType> createReportingCategory(ReportingCategoryType value) {
        return new JAXBElement<ReportingCategoryType>(_ReportingCategory_QNAME, ReportingCategoryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "URI", scope = ContactType.class)
    public JAXBElement<String> createContactTypeURI(String value) {
        return new JAXBElement<String>(_ContactTypeURI_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Fax", scope = ContactType.class)
    public JAXBElement<String> createContactTypeFax(String value) {
        return new JAXBElement<String>(_ContactTypeFax_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Email", scope = ContactType.class)
    public JAXBElement<String> createContactTypeEmail(String value) {
        return new JAXBElement<String>(_ContactTypeEmail_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Telephone", scope = ContactType.class)
    public JAXBElement<String> createContactTypeTelephone(String value) {
        return new JAXBElement<String>(_ContactTypeTelephone_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "X400", scope = ContactType.class)
    public JAXBElement<String> createContactTypeX400(String value) {
        return new JAXBElement<String>(_ContactTypeX400_QNAME, String.class, ContactType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataStructureReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "DataStructure", scope = ConstraintAttachmentType.class)
    public JAXBElement<DataStructureReferenceType> createConstraintAttachmentTypeDataStructure(DataStructureReferenceType value) {
        return new JAXBElement<DataStructureReferenceType>(_ConstraintAttachmentTypeDataStructure_QNAME, DataStructureReferenceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataStructureReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "MetadataStructure", scope = ConstraintAttachmentType.class)
    public JAXBElement<MetadataStructureReferenceType> createConstraintAttachmentTypeMetadataStructure(MetadataStructureReferenceType value) {
        return new JAXBElement<MetadataStructureReferenceType>(_ConstraintAttachmentTypeMetadataStructure_QNAME, MetadataStructureReferenceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryableDataSourceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "QueryableDataSource", scope = ConstraintAttachmentType.class)
    public JAXBElement<QueryableDataSourceType> createConstraintAttachmentTypeQueryableDataSource(QueryableDataSourceType value) {
        return new JAXBElement<QueryableDataSourceType>(_ConstraintAttachmentTypeQueryableDataSource_QNAME, QueryableDataSourceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataflowReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Dataflow", scope = ConstraintAttachmentType.class)
    public JAXBElement<DataflowReferenceType> createConstraintAttachmentTypeDataflow(DataflowReferenceType value) {
        return new JAXBElement<DataflowReferenceType>(_ConstraintAttachmentTypeDataflow_QNAME, DataflowReferenceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "MetadataSet", scope = ConstraintAttachmentType.class)
    public JAXBElement<SetReferenceType> createConstraintAttachmentTypeMetadataSet(SetReferenceType value) {
        return new JAXBElement<SetReferenceType>(_ConstraintAttachmentTypeMetadataSet_QNAME, SetReferenceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataProviderReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "DataProvider", scope = ConstraintAttachmentType.class)
    public JAXBElement<DataProviderReferenceType> createConstraintAttachmentTypeDataProvider(DataProviderReferenceType value) {
        return new JAXBElement<DataProviderReferenceType>(_DataProvider_QNAME, DataProviderReferenceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "DataSet", scope = ConstraintAttachmentType.class)
    public JAXBElement<SetReferenceType> createConstraintAttachmentTypeDataSet(SetReferenceType value) {
        return new JAXBElement<SetReferenceType>(_ConstraintAttachmentTypeDataSet_QNAME, SetReferenceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataflowReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "Metadataflow", scope = ConstraintAttachmentType.class)
    public JAXBElement<MetadataflowReferenceType> createConstraintAttachmentTypeMetadataflow(MetadataflowReferenceType value) {
        return new JAXBElement<MetadataflowReferenceType>(_ConstraintAttachmentTypeMetadataflow_QNAME, MetadataflowReferenceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvisionAgreementReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "ProvisionAgreement", scope = ConstraintAttachmentType.class)
    public JAXBElement<ProvisionAgreementReferenceType> createConstraintAttachmentTypeProvisionAgreement(ProvisionAgreementReferenceType value) {
        return new JAXBElement<ProvisionAgreementReferenceType>(_ConstraintAttachmentTypeProvisionAgreement_QNAME, ProvisionAgreementReferenceType.class, ConstraintAttachmentType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", name = "SimpleDataSource", scope = ConstraintAttachmentType.class)
    public JAXBElement<String> createConstraintAttachmentTypeSimpleDataSource(String value) {
        return new JAXBElement<String>(_ConstraintAttachmentTypeSimpleDataSource_QNAME, String.class, ConstraintAttachmentType.class, value);
    }

}
