
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * KeyDescriptorRefType contains a reference to the key descriptor within a data structure definition.
 * 
 * <p>Java-Klasse f�r KeyDescriptorRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="KeyDescriptorRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentListRefBaseType">
 *       &lt;attribute name="id" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType" fixed="DIMENSION_DESCRIPTOR" />
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentListTypeCodelistType" fixed="DimensionDescriptor" />
 *       &lt;attribute name="package" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructurePackageTypeCodelistType" fixed="datastructure" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyDescriptorRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class KeyDescriptorRefType
    extends ComponentListRefBaseType
{


}
