
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalComponentListComponentReferenceType;


/**
 * ComponentMapType defines the structure for relating a component in a source structure to a component in a target structure.
 * 
 * <p>Java-Klasse f�r ComponentMapType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentMapType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalComponentListComponentReferenceType"/>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalComponentListComponentReferenceType"/>
 *         &lt;element name="RepresentationMapping" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}RepresentationMapType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentMapType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "source",
    "target",
    "representationMapping"
})
public class ComponentMapType
    extends AnnotableType
{

    @XmlElement(name = "Source", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected LocalComponentListComponentReferenceType source;
    @XmlElement(name = "Target", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected LocalComponentListComponentReferenceType target;
    @XmlElement(name = "RepresentationMapping", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected RepresentationMapType representationMapping;

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalComponentListComponentReferenceType }
     *     
     */
    public LocalComponentListComponentReferenceType getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalComponentListComponentReferenceType }
     *     
     */
    public void setSource(LocalComponentListComponentReferenceType value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der target-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalComponentListComponentReferenceType }
     *     
     */
    public LocalComponentListComponentReferenceType getTarget() {
        return target;
    }

    /**
     * Legt den Wert der target-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalComponentListComponentReferenceType }
     *     
     */
    public void setTarget(LocalComponentListComponentReferenceType value) {
        this.target = value;
    }

    /**
     * Ruft den Wert der representationMapping-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RepresentationMapType }
     *     
     */
    public RepresentationMapType getRepresentationMapping() {
        return representationMapping;
    }

    /**
     * Legt den Wert der representationMapping-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RepresentationMapType }
     *     
     */
    public void setRepresentationMapping(RepresentationMapType value) {
        this.representationMapping = value;
    }

}
