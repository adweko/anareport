
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r DataType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="DataType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="String"/>
 *     &lt;enumeration value="Alpha"/>
 *     &lt;enumeration value="AlphaNumeric"/>
 *     &lt;enumeration value="Numeric"/>
 *     &lt;enumeration value="BigInteger"/>
 *     &lt;enumeration value="Integer"/>
 *     &lt;enumeration value="Long"/>
 *     &lt;enumeration value="Short"/>
 *     &lt;enumeration value="Decimal"/>
 *     &lt;enumeration value="Float"/>
 *     &lt;enumeration value="Double"/>
 *     &lt;enumeration value="Boolean"/>
 *     &lt;enumeration value="URI"/>
 *     &lt;enumeration value="Count"/>
 *     &lt;enumeration value="InclusiveValueRange"/>
 *     &lt;enumeration value="ExclusiveValueRange"/>
 *     &lt;enumeration value="Incremental"/>
 *     &lt;enumeration value="ObservationalTimePeriod"/>
 *     &lt;enumeration value="StandardTimePeriod"/>
 *     &lt;enumeration value="BasicTimePeriod"/>
 *     &lt;enumeration value="GregorianTimePeriod"/>
 *     &lt;enumeration value="GregorianYear"/>
 *     &lt;enumeration value="GregorianYearMonth"/>
 *     &lt;enumeration value="GregorianDay"/>
 *     &lt;enumeration value="ReportingTimePeriod"/>
 *     &lt;enumeration value="ReportingYear"/>
 *     &lt;enumeration value="ReportingSemester"/>
 *     &lt;enumeration value="ReportingTrimester"/>
 *     &lt;enumeration value="ReportingQuarter"/>
 *     &lt;enumeration value="ReportingMonth"/>
 *     &lt;enumeration value="ReportingWeek"/>
 *     &lt;enumeration value="ReportingDay"/>
 *     &lt;enumeration value="DateTime"/>
 *     &lt;enumeration value="TimeRange"/>
 *     &lt;enumeration value="Month"/>
 *     &lt;enumeration value="MonthDay"/>
 *     &lt;enumeration value="Day"/>
 *     &lt;enumeration value="Time"/>
 *     &lt;enumeration value="Duration"/>
 *     &lt;enumeration value="XHTML"/>
 *     &lt;enumeration value="KeyValues"/>
 *     &lt;enumeration value="IdentifiableReference"/>
 *     &lt;enumeration value="DataSetReference"/>
 *     &lt;enumeration value="AttachmentConstraintReference"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DataType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum DataType {


    /**
     * A string datatype corresponding to W3C XML Schema's xs:string datatype.
     * 
     */
    @XmlEnumValue("String")
    STRING("String"),

    /**
     * A string datatype which only allows for the simple aplhabetic charcter set of A-z.
     * 
     */
    @XmlEnumValue("Alpha")
    ALPHA("Alpha"),

    /**
     * A string datatype which only allows for the simple alphabetic character set of A-z plus the simple numeric character set of 0-9.
     * 
     */
    @XmlEnumValue("AlphaNumeric")
    ALPHA_NUMERIC("AlphaNumeric"),

    /**
     * A string datatype which only allows for the simple numeric character set of 0-9. This format is not treated as an integer, and therefore can having leading zeros.
     * 
     */
    @XmlEnumValue("Numeric")
    NUMERIC("Numeric"),

    /**
     * An integer datatype corresponding to W3C XML Schema's xs:integer datatype.
     * 
     */
    @XmlEnumValue("BigInteger")
    BIG_INTEGER("BigInteger"),

    /**
     * An integer datatype corresponding to W3C XML Schema's xs:int datatype.
     * 
     */
    @XmlEnumValue("Integer")
    INTEGER("Integer"),

    /**
     * A numeric datatype corresponding to W3C XML Schema's xs:long datatype.
     * 
     */
    @XmlEnumValue("Long")
    LONG("Long"),

    /**
     * A numeric datatype corresponding to W3C XML Schema's xs:short datatype.
     * 
     */
    @XmlEnumValue("Short")
    SHORT("Short"),

    /**
     * A numeric datatype corresponding to W3C XML Schema's xs:decimal datatype.
     * 
     */
    @XmlEnumValue("Decimal")
    DECIMAL("Decimal"),

    /**
     * A numeric datatype corresponding to W3C XML Schema's xs:float datatype.
     * 
     */
    @XmlEnumValue("Float")
    FLOAT("Float"),

    /**
     * A numeric datatype corresponding to W3C XML Schema's xs:double datatype.
     * 
     */
    @XmlEnumValue("Double")
    DOUBLE("Double"),

    /**
     * A datatype corresponding to W3C XML Schema's xs:boolean datatype.
     * 
     */
    @XmlEnumValue("Boolean")
    BOOLEAN("Boolean"),

    /**
     * A datatype corresponding to W3C XML Schema's xs:anyURI datatype.
     * 
     */
    URI("URI"),

    /**
     * A simple incrementing Integer type. The isSequence facet must be set to true, and the interval facet must be set to "1".
     * 
     */
    @XmlEnumValue("Count")
    COUNT("Count"),

    /**
     * This value indicates that the startValue and endValue attributes provide the inclusive boundaries of a numeric range of type xs:decimal.
     * 
     */
    @XmlEnumValue("InclusiveValueRange")
    INCLUSIVE_VALUE_RANGE("InclusiveValueRange"),

    /**
     * This value indicates that the startValue and endValue attributes provide the exclusive boundaries of a numeric range, of type xs:decimal.
     * 
     */
    @XmlEnumValue("ExclusiveValueRange")
    EXCLUSIVE_VALUE_RANGE("ExclusiveValueRange"),

    /**
     * This value indicates that the value increments according to the value provided in the interval facet, and has a true value for the isSequence facet.
     * 
     */
    @XmlEnumValue("Incremental")
    INCREMENTAL("Incremental"),

    /**
     * Observational time periods are the superset of all time periods in SDMX. It is the union of the standard time periods (i.e. Gregorian time periods, the reporting time periods, and date time) and a time range.
     * 
     */
    @XmlEnumValue("ObservationalTimePeriod")
    OBSERVATIONAL_TIME_PERIOD("ObservationalTimePeriod"),

    /**
     * Standard time periods is a superset of distinct time period in SDMX. It is the union of the basic time periods (i.e. the Gregorian time periods and date time) and the reporting time periods.
     * 
     */
    @XmlEnumValue("StandardTimePeriod")
    STANDARD_TIME_PERIOD("StandardTimePeriod"),

    /**
     * BasicTimePeriod time periods is a superset of the Gregorian time periods and a date time.
     * 
     */
    @XmlEnumValue("BasicTimePeriod")
    BASIC_TIME_PERIOD("BasicTimePeriod"),

    /**
     * Gregorian time periods correspond to calendar periods and are represented in ISO-8601 formats. This is the union of the year, year month, and date formats.
     * 
     */
    @XmlEnumValue("GregorianTimePeriod")
    GREGORIAN_TIME_PERIOD("GregorianTimePeriod"),

    /**
     * A Gregorian time period corresponding to W3C XML Schema's xs:gYear datatype, which is based on ISO-8601.
     * 
     */
    @XmlEnumValue("GregorianYear")
    GREGORIAN_YEAR("GregorianYear"),

    /**
     * A time datatype corresponding to W3C XML Schema's xs:gYearMonth datatype, which is based on ISO-8601.
     * 
     */
    @XmlEnumValue("GregorianYearMonth")
    GREGORIAN_YEAR_MONTH("GregorianYearMonth"),

    /**
     * A time datatype corresponding to W3C XML Schema's xs:date datatype, which is based on ISO-8601.
     * 
     */
    @XmlEnumValue("GregorianDay")
    GREGORIAN_DAY("GregorianDay"),

    /**
     * Reporting time periods represent periods of a standard length within a reporting year, where to start of the year (defined as a month and day) must be defined elsewhere or it is assumed to be January 1. This is the union of the reporting year, semester, trimester, quarter, month, week, and day.
     * 
     */
    @XmlEnumValue("ReportingTimePeriod")
    REPORTING_TIME_PERIOD("ReportingTimePeriod"),

    /**
     * A reporting year represents a period of 1 year (P1Y) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingYearType.
     * 
     */
    @XmlEnumValue("ReportingYear")
    REPORTING_YEAR("ReportingYear"),

    /**
     * A reporting semester represents a period of 6 months (P6M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingSemesterType.
     * 
     */
    @XmlEnumValue("ReportingSemester")
    REPORTING_SEMESTER("ReportingSemester"),

    /**
     * A reporting trimester represents a period of 4 months (P4M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingTrimesterType.
     * 
     */
    @XmlEnumValue("ReportingTrimester")
    REPORTING_TRIMESTER("ReportingTrimester"),

    /**
     * A reporting quarter represents a period of 3 months (P3M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingQuarterType.
     * 
     */
    @XmlEnumValue("ReportingQuarter")
    REPORTING_QUARTER("ReportingQuarter"),

    /**
     * A reporting month represents a period of 1 month (P1M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingMonthType.
     * 
     */
    @XmlEnumValue("ReportingMonth")
    REPORTING_MONTH("ReportingMonth"),

    /**
     * A reporting week represents a period of 7 days (P7D) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingWeekType.
     * 
     */
    @XmlEnumValue("ReportingWeek")
    REPORTING_WEEK("ReportingWeek"),

    /**
     * A reporting day represents a period of 1 day (P1D) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingDayType.
     * 
     */
    @XmlEnumValue("ReportingDay")
    REPORTING_DAY("ReportingDay"),

    /**
     * A time datatype corresponding to W3C XML Schema's xs:dateTime datatype.
     * 
     */
    @XmlEnumValue("DateTime")
    DATE_TIME("DateTime"),

    /**
     * TimeRange defines a time period by providing a distinct start (date or date time) and a duration.
     * 
     */
    @XmlEnumValue("TimeRange")
    TIME_RANGE("TimeRange"),

    /**
     * A time datatype corresponding to W3C XML Schema's xs:gMonth datatype.
     * 
     */
    @XmlEnumValue("Month")
    MONTH("Month"),

    /**
     * A time datatype corresponding to W3C XML Schema's xs:gMonthDay datatype.
     * 
     */
    @XmlEnumValue("MonthDay")
    MONTH_DAY("MonthDay"),

    /**
     * A time datatype corresponding to W3C XML Schema's xs:gDay datatype.
     * 
     */
    @XmlEnumValue("Day")
    DAY("Day"),

    /**
     * A time datatype corresponding to W3C XML Schema's xs:time datatype.
     * 
     */
    @XmlEnumValue("Time")
    TIME("Time"),

    /**
     * A time datatype corresponding to W3C XML Schema's xs:duration datatype.
     * 
     */
    @XmlEnumValue("Duration")
    DURATION("Duration"),

    /**
     * This value indicates that the content of the component can contain XHTML markup.
     * 
     */
    XHTML("XHTML"),

    /**
     * This value indicates that the content of the component will be data key (a set of dimension references and values for the dimensions).
     * 
     */
    @XmlEnumValue("KeyValues")
    KEY_VALUES("KeyValues"),

    /**
     * This value indicates that the content of the component will be complete reference (either URN or full set of reference fields) to an Identifiable object in the SDMX Information Model.
     * 
     */
    @XmlEnumValue("IdentifiableReference")
    IDENTIFIABLE_REFERENCE("IdentifiableReference"),

    /**
     * This value indicates that the content of the component will be reference to a data provider, which is actually a formal reference to a data provider and a data set identifier value.
     * 
     */
    @XmlEnumValue("DataSetReference")
    DATA_SET_REFERENCE("DataSetReference"),

    /**
     * This value indicates that the content of the component will be reference to an attachment constraint, which is actually a combination of a collection of full or partial key values and a reference to a data set or data structure, usage, or provision agreement.
     * 
     */
    @XmlEnumValue("AttachmentConstraintReference")
    ATTACHMENT_CONSTRAINT_REFERENCE("AttachmentConstraintReference");
    private final String value;

    DataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DataType fromValue(String v) {
        for (DataType c: DataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
