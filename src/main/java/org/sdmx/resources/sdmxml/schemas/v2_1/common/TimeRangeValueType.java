
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeRangeValueType allows a time period value to be expressed as a range. It can be expressed as the period before a period, after a period, or between two periods. Each of these properties can specify their inclusion in regards to the range.
 * 
 * <p>Java-Klasse f�r TimeRangeValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeRangeValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="BeforePeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimePeriodRangeType"/>
 *         &lt;element name="AfterPeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimePeriodRangeType"/>
 *         &lt;sequence>
 *           &lt;element name="StartPeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimePeriodRangeType"/>
 *           &lt;element name="EndPeriod" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimePeriodRangeType"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeRangeValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common", propOrder = {
    "beforePeriod",
    "afterPeriod",
    "startPeriod",
    "endPeriod"
})
public class TimeRangeValueType {

    @XmlElement(name = "BeforePeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected TimePeriodRangeType beforePeriod;
    @XmlElement(name = "AfterPeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected TimePeriodRangeType afterPeriod;
    @XmlElement(name = "StartPeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected TimePeriodRangeType startPeriod;
    @XmlElement(name = "EndPeriod", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
    protected TimePeriodRangeType endPeriod;

    /**
     * Ruft den Wert der beforePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimePeriodRangeType }
     *     
     */
    public TimePeriodRangeType getBeforePeriod() {
        return beforePeriod;
    }

    /**
     * Legt den Wert der beforePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimePeriodRangeType }
     *     
     */
    public void setBeforePeriod(TimePeriodRangeType value) {
        this.beforePeriod = value;
    }

    /**
     * Ruft den Wert der afterPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimePeriodRangeType }
     *     
     */
    public TimePeriodRangeType getAfterPeriod() {
        return afterPeriod;
    }

    /**
     * Legt den Wert der afterPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimePeriodRangeType }
     *     
     */
    public void setAfterPeriod(TimePeriodRangeType value) {
        this.afterPeriod = value;
    }

    /**
     * Ruft den Wert der startPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimePeriodRangeType }
     *     
     */
    public TimePeriodRangeType getStartPeriod() {
        return startPeriod;
    }

    /**
     * Legt den Wert der startPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimePeriodRangeType }
     *     
     */
    public void setStartPeriod(TimePeriodRangeType value) {
        this.startPeriod = value;
    }

    /**
     * Ruft den Wert der endPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimePeriodRangeType }
     *     
     */
    public TimePeriodRangeType getEndPeriod() {
        return endPeriod;
    }

    /**
     * Legt den Wert der endPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimePeriodRangeType }
     *     
     */
    public void setEndPeriod(TimePeriodRangeType value) {
        this.endPeriod = value;
    }

}
