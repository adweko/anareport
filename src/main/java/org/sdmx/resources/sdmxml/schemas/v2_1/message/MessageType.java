
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.message.footer.FooterType;


/**
 * MessageType is an abstract type which is used by all of the messages, to allow inheritance of common features. Every message consists of a mandatory header, followed by optional payload (which may occur multiple times), and finally an optional footer section for conveying error, warning, and informational messages.
 * 
 * <p>Java-Klasse f�r MessageType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MessageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}BaseHeaderType"/>
 *         &lt;any namespace='http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message' maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer}Footer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", propOrder = {
    "header",
    "any",
    "footer"
})
@XmlSeeAlso({
    ProvisionAgreementQueryType.class,
    CategorySchemeQueryType.class,
    StructureSpecificMetadataType.class,
    ConstraintQueryType.class,
    StructureSpecificDataType.class,
    DataflowQueryType.class,
    MetadataStructureQueryType.class,
    CategorisationQueryType.class,
    DataSchemaQueryType.class,
    ConceptSchemeQueryType.class,
    OrganisationSchemeQueryType.class,
    StructuresQueryType.class,
    StructureType.class,
    MetadataQueryType.class,
    DataStructureQueryType.class,
    HierarchicalCodelistQueryType.class,
    MetadataflowQueryType.class,
    GenericDataType.class,
    StructureSetQueryType.class,
    ProcessQueryType.class,
    DataQueryType.class,
    RegistryInterfaceType.class,
    GenericMetadataType.class,
    MetadataSchemaQueryType.class,
    CodelistQueryType.class,
    ReportingTaxonomyQueryType.class
})
public abstract class MessageType {

    @XmlElement(name = "Header", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message", required = true)
    protected BaseHeaderType header;
    @XmlAnyElement(lax = true)
    protected List<Object> any;
    @XmlElement(name = "Footer", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer")
    protected FooterType footer;

    /**
     * Ruft den Wert der header-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BaseHeaderType }
     *     
     */
    public BaseHeaderType getHeader() {
        return header;
    }

    /**
     * Legt den Wert der header-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseHeaderType }
     *     
     */
    public void setHeader(BaseHeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

    /**
     * Ruft den Wert der footer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FooterType }
     *     
     */
    public FooterType getFooter() {
        return footer;
    }

    /**
     * Legt den Wert der footer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FooterType }
     *     
     */
    public void setFooter(FooterType value) {
        this.footer = value;
    }

}
