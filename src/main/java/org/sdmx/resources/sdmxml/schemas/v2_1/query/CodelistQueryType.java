
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * CodelistQueryType defines the structure of a codelist query. The parameters for the query are contained in the CodelistWhere element. The References element is used to indicate how objects that reference the matched codelist should be returned.
 * 
 * <p>Java-Klasse f�r CodelistQueryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CodelistQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructuralMetadataQueryType">
 *       &lt;sequence>
 *         &lt;element name="ReturnDetails" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}StructureReturnDetailsType"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}CodelistWhere"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodelistQueryType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
public class CodelistQueryType
    extends StructuralMetadataQueryType
{


}
