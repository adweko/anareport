
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataAttributeValueType describes the structure that is used to match reference metadata where a metadata attribute has a particular value. Metadata attribute value queries can be nested for querying nested metadata attributes. If no value is provided, then simply the presence of the metadata attribute within the given context will result in a match. All nested metadata attribute value conditions must be met to constitute a match.
 * 
 * <p>Java-Klasse f�r MetadataAttributeValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataAttributeValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}NCNameIDType"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}Value"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TextValue" maxOccurs="unbounded"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}NumericValue" maxOccurs="2"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}TimeValue" maxOccurs="2"/>
 *         &lt;/choice>
 *         &lt;element name="MetadataAttributeValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataAttributeValueType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataAttributeValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "id",
    "value",
    "textValue",
    "numericValue",
    "timeValue",
    "metadataAttributeValue"
})
public class MetadataAttributeValueType {

    @XmlElement(name = "ID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", required = true)
    protected String id;
    @XmlElement(name = "Value", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected SimpleValueType value;
    @XmlElement(name = "TextValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<QueryTextType> textValue;
    @XmlElement(name = "NumericValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<NumericValueType> numericValue;
    @XmlElement(name = "TimeValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<TimePeriodValueType> timeValue;
    @XmlElement(name = "MetadataAttributeValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<MetadataAttributeValueType> metadataAttributeValue;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SimpleValueType }
     *     
     */
    public SimpleValueType getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleValueType }
     *     
     */
    public void setValue(SimpleValueType value) {
        this.value = value;
    }

    /**
     * Gets the value of the textValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the textValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryTextType }
     * 
     * 
     */
    public List<QueryTextType> getTextValue() {
        if (textValue == null) {
            textValue = new ArrayList<QueryTextType>();
        }
        return this.textValue;
    }

    /**
     * Gets the value of the numericValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the numericValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNumericValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NumericValueType }
     * 
     * 
     */
    public List<NumericValueType> getNumericValue() {
        if (numericValue == null) {
            numericValue = new ArrayList<NumericValueType>();
        }
        return this.numericValue;
    }

    /**
     * Gets the value of the timeValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timeValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimeValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TimePeriodValueType }
     * 
     * 
     */
    public List<TimePeriodValueType> getTimeValue() {
        if (timeValue == null) {
            timeValue = new ArrayList<TimePeriodValueType>();
        }
        return this.timeValue;
    }

    /**
     * Gets the value of the metadataAttributeValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadataAttributeValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadataAttributeValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadataAttributeValueType }
     * 
     * 
     */
    public List<MetadataAttributeValueType> getMetadataAttributeValue() {
        if (metadataAttributeValue == null) {
            metadataAttributeValue = new ArrayList<MetadataAttributeValueType>();
        }
        return this.metadataAttributeValue;
    }

}
