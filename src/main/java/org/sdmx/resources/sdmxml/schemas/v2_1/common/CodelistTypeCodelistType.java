
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r CodelistTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="CodelistTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MaintainableTypeCodelistType">
 *     &lt;enumeration value="Codelist"/>
 *     &lt;enumeration value="HierarchicalCodelist"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CodelistTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(MaintainableTypeCodelistType.class)
public enum CodelistTypeCodelistType {

    @XmlEnumValue("Codelist")
    CODELIST(MaintainableTypeCodelistType.CODELIST),
    @XmlEnumValue("HierarchicalCodelist")
    HIERARCHICAL_CODELIST(MaintainableTypeCodelistType.HIERARCHICAL_CODELIST);
    private final MaintainableTypeCodelistType value;

    CodelistTypeCodelistType(MaintainableTypeCodelistType v) {
        value = v;
    }

    public MaintainableTypeCodelistType value() {
        return value;
    }

    public static CodelistTypeCodelistType fromValue(MaintainableTypeCodelistType v) {
        for (CodelistTypeCodelistType c: CodelistTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
