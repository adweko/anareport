
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SubmitRegistrationsResponseType describes the structure of a registration response. For each submitted registration in the request, a registration status is provided. The status elements should be provided in the same order as the submitted registrations, although each status will echo the request to ensure accurate processing of the responses.
 * 
 * <p>Java-Klasse f�r SubmitRegistrationsResponseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitRegistrationsResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegistrationStatus" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}RegistrationStatusType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitRegistrationsResponseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "registrationStatus"
})
public class SubmitRegistrationsResponseType {

    @XmlElement(name = "RegistrationStatus", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected List<RegistrationStatusType> registrationStatus;

    /**
     * Gets the value of the registrationStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the registrationStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegistrationStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegistrationStatusType }
     * 
     * 
     */
    public List<RegistrationStatusType> getRegistrationStatus() {
        if (registrationStatus == null) {
            registrationStatus = new ArrayList<RegistrationStatusType>();
        }
        return this.registrationStatus;
    }

}
