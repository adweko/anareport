
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AttachmentConstraintReferenceType;


/**
 * GroupType describes the structure of a group descriptor in a data structure definition. A group may consist of a of partial key, or collection of distinct cube regions or key sets to which attributes may be attached. The purpose of a group is to specify attributes values which have the same value based on some common dimensionality. All groups declared in the data structure must be unique - that is, you may not have duplicate partial keys. All groups must be given unique identifiers.
 * 
 * <p>Java-Klasse f�r GroupType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GroupType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}GroupBaseType">
 *       &lt;choice>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}GroupDimension" maxOccurs="unbounded"/>
 *         &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AttachmentConstraintReferenceType"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "groupDimension",
    "attachmentConstraint"
})
public class GroupType
    extends GroupBaseType
{

    @XmlElement(name = "GroupDimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected List<GroupDimensionType> groupDimension;
    @XmlElement(name = "AttachmentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected AttachmentConstraintReferenceType attachmentConstraint;

    /**
     * Gets the value of the groupDimension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupDimension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupDimension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupDimensionType }
     * 
     * 
     */
    public List<GroupDimensionType> getGroupDimension() {
        if (groupDimension == null) {
            groupDimension = new ArrayList<GroupDimensionType>();
        }
        return this.groupDimension;
    }

    /**
     * Ruft den Wert der attachmentConstraint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentConstraintReferenceType }
     *     
     */
    public AttachmentConstraintReferenceType getAttachmentConstraint() {
        return attachmentConstraint;
    }

    /**
     * Legt den Wert der attachmentConstraint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentConstraintReferenceType }
     *     
     */
    public void setAttachmentConstraint(AttachmentConstraintReferenceType value) {
        this.attachmentConstraint = value;
    }

}
