
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r RangeOperatorType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="RangeOperatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="greaterThanOrEqual"/>
 *     &lt;enumeration value="lessThanOrEqual"/>
 *     &lt;enumeration value="greaterThan"/>
 *     &lt;enumeration value="lessThan"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RangeOperatorType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum
public enum RangeOperatorType {


    /**
     * (>=) - value must be greater than or equal to the value supplied.
     * 
     */
    @XmlEnumValue("greaterThanOrEqual")
    GREATER_THAN_OR_EQUAL("greaterThanOrEqual"),

    /**
     * (<=) - value must be less than or equal to the value supplied.
     * 
     */
    @XmlEnumValue("lessThanOrEqual")
    LESS_THAN_OR_EQUAL("lessThanOrEqual"),

    /**
     * (>) - value must be greater than the value supplied.
     * 
     */
    @XmlEnumValue("greaterThan")
    GREATER_THAN("greaterThan"),

    /**
     * (<) - value must be less than the value supplied.
     * 
     */
    @XmlEnumValue("lessThan")
    LESS_THAN("lessThan");
    private final String value;

    RangeOperatorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RangeOperatorType fromValue(String v) {
        for (RangeOperatorType c: RangeOperatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
