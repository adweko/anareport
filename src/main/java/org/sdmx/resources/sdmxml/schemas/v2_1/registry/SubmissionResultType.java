
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SubmissionResultType provides the status of the structural object submission request. It will identify the object submitted, report back the action requested, and convey the status and any error messages which are relevant to the submission.
 * 
 * <p>Java-Klasse f�r SubmissionResultType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmissionResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubmittedStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmittedStructureType"/>
 *         &lt;element name="StatusMessage" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}StatusMessageType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionResultType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "submittedStructure",
    "statusMessage"
})
public class SubmissionResultType {

    @XmlElement(name = "SubmittedStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected SubmittedStructureType submittedStructure;
    @XmlElement(name = "StatusMessage", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected StatusMessageType statusMessage;

    /**
     * Ruft den Wert der submittedStructure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SubmittedStructureType }
     *     
     */
    public SubmittedStructureType getSubmittedStructure() {
        return submittedStructure;
    }

    /**
     * Legt den Wert der submittedStructure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmittedStructureType }
     *     
     */
    public void setSubmittedStructure(SubmittedStructureType value) {
        this.submittedStructure = value;
    }

    /**
     * Ruft den Wert der statusMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StatusMessageType }
     *     
     */
    public StatusMessageType getStatusMessage() {
        return statusMessage;
    }

    /**
     * Legt den Wert der statusMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusMessageType }
     *     
     */
    public void setStatusMessage(StatusMessageType value) {
        this.statusMessage = value;
    }

}
