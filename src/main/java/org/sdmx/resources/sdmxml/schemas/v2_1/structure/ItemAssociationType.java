
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalItemReferenceType;


/**
 * ItemAssociationType is an abstract type which defines the relationship between two items from the source and target item schemes of an item scheme map.
 * 
 * <p>Java-Klasse f�r ItemAssociationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ItemAssociationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="Source" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalItemReferenceType"/>
 *         &lt;element name="Target" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalItemReferenceType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemAssociationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "source",
    "target"
})
@XmlSeeAlso({
    OrganisationMapType.class,
    CategoryMapType.class,
    CodeMapType.class,
    ConceptMapType.class,
    ReportingCategoryMapType.class
})
public abstract class ItemAssociationType
    extends AnnotableType
{

    @XmlElement(name = "Source", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected LocalItemReferenceType source;
    @XmlElement(name = "Target", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected LocalItemReferenceType target;

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalItemReferenceType }
     *     
     */
    public LocalItemReferenceType getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalItemReferenceType }
     *     
     */
    public void setSource(LocalItemReferenceType value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der target-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalItemReferenceType }
     *     
     */
    public LocalItemReferenceType getTarget() {
        return target;
    }

    /**
     * Legt den Wert der target-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalItemReferenceType }
     *     
     */
    public void setTarget(LocalItemReferenceType value) {
        this.target = value;
    }

}
