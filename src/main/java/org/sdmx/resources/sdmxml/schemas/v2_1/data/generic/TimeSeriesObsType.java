
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeSeriesObsType defines the structure of a time series observation. The observation must be provided a value for the time dimension. This time value should disambiguate the observation within the series in which it is defined (i.e. there should not be another observation with the same time value). The observation can contain an observed value and/or attribute values.
 * 
 * <p>Java-Klasse f�r TimeSeriesObsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeSeriesObsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element name="ObsDimension" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}TimeValueType"/>
 *         &lt;element name="ObsValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ObsValueType" minOccurs="0"/>
 *         &lt;element name="Attributes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeSeriesObsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
public class TimeSeriesObsType
    extends ObsType
{


}
