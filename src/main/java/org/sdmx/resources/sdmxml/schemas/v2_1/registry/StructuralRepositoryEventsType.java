
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.EmptyType;


/**
 * StructuralRepositoryEventsType details the structural events for the subscription. At least one maintenance agency must be specified, although it may be given a wildcard value (meaning the subscription is for the structural events of all agencies). This can also be a list of agencies to allow the subscription to subscribe the events of more than one agency. It should be noted that when doing so, all of the subsequent objects are assumed to apply to every agency in the list. The subscription is then refined by detailing the structural objects maintained by the agency for which the subscription should apply. It is possible to explicitly select all object events, all objects of given types, or to individually list out specific objects. Note that for any object, it is also possible to provide an explicit URN to reference a distinct object. In this case, the reference to maintenance agency described above is ignored. Although it is not required, if specific objects are being referenced by explicit URNs, it is good practice to list the agencies.
 * 
 * <p>Java-Klasse f�r StructuralRepositoryEventsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructuralRepositoryEventsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}NestedIDQueryType" maxOccurs="unbounded"/>
 *         &lt;choice>
 *           &lt;element name="AllEvents" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *           &lt;choice maxOccurs="unbounded">
 *             &lt;element name="AgencyScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="DataConsmerScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="DataProviderScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="OrganisationUnitScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="Dataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="Metadataflow" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="CategoryScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="Categorisation" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}IdentifiableObjectEventType"/>
 *             &lt;element name="Codelist" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="HierarchicalCodelist" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="ConceptScheme" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="MetadataStructureDefinition" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="KeyFamily" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="StructureSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="ReportingTaxonomy" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="Process" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="AttachmentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="ContentConstraint" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *             &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}VersionableObjectEventType"/>
 *           &lt;/choice>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" fixed="STRUCTURE" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuralRepositoryEventsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "agencyID",
    "allEvents",
    "agencySchemeOrDataConsmerSchemeOrDataProviderScheme"
})
public class StructuralRepositoryEventsType {

    @XmlElement(name = "AgencyID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true, defaultValue = "%")
    protected List<String> agencyID;
    @XmlElement(name = "AllEvents", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected EmptyType allEvents;
    @XmlElementRefs({
        @XmlElementRef(name = "Metadataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "HierarchicalCodelist", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ProvisionAgreement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataProviderScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "KeyFamily", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "MetadataStructureDefinition", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ContentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataConsmerScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "OrganisationUnitScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Categorisation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Process", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Dataflow", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ReportingTaxonomy", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AgencyScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "CategoryScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ConceptScheme", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Codelist", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "AttachmentConstraint", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "StructureSet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> agencySchemeOrDataConsmerSchemeOrDataProviderScheme;
    @XmlAttribute(name = "TYPE")
    protected String type;

    /**
     * Gets the value of the agencyID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agencyID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgencyID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAgencyID() {
        if (agencyID == null) {
            agencyID = new ArrayList<String>();
        }
        return this.agencyID;
    }

    /**
     * Ruft den Wert der allEvents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAllEvents() {
        return allEvents;
    }

    /**
     * Legt den Wert der allEvents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAllEvents(EmptyType value) {
        this.allEvents = value;
    }

    /**
     * Gets the value of the agencySchemeOrDataConsmerSchemeOrDataProviderScheme property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agencySchemeOrDataConsmerSchemeOrDataProviderScheme property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgencySchemeOrDataConsmerSchemeOrDataProviderScheme().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link IdentifiableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link VersionableObjectEventType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getAgencySchemeOrDataConsmerSchemeOrDataProviderScheme() {
        if (agencySchemeOrDataConsmerSchemeOrDataProviderScheme == null) {
            agencySchemeOrDataConsmerSchemeOrDataProviderScheme = new ArrayList<JAXBElement<?>>();
        }
        return this.agencySchemeOrDataConsmerSchemeOrDataProviderScheme;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTYPE() {
        if (type == null) {
            return "STRUCTURE";
        } else {
            return type;
        }
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTYPE(String value) {
        this.type = value;
    }

}
