
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * TimePeriodValueType describes the structure of a time period query. A value is provided as the content in the SDMX time period format.
 * 
 * <p>Java-Klasse f�r TimePeriodValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimePeriodValueType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common>ObservationalTimePeriodType">
 *       &lt;attribute name="operator" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeOperatorType" default="equal" />
 *       &lt;attribute name="reportingYearStartDay" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}ReportingYearStartDayQueryType" default="Any" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimePeriodValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "value"
})
public class TimePeriodValueType {

    @XmlValue
    protected List<String> value;
    @XmlAttribute(name = "operator")
    protected String operator;
    @XmlAttribute(name = "reportingYearStartDay")
    protected String reportingYearStartDay;

    /**
     * ObservationalTimePeriodType specifies a distinct time period or point in time in SDMX. The time period can either be a Gregorian calendar period, a standard reporting period, a distinct point in time, or a time range with a specific date and duration.Gets the value of the value property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the value property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getValue() {
        if (value == null) {
            value = new ArrayList<String>();
        }
        return this.value;
    }

    /**
     * Ruft den Wert der operator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperator() {
        if (operator == null) {
            return "equal";
        } else {
            return operator;
        }
    }

    /**
     * Legt den Wert der operator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperator(String value) {
        this.operator = value;
    }

    /**
     * Ruft den Wert der reportingYearStartDay-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingYearStartDay() {
        if (reportingYearStartDay == null) {
            return "Any";
        } else {
            return reportingYearStartDay;
        }
    }

    /**
     * Legt den Wert der reportingYearStartDay-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingYearStartDay(String value) {
        this.reportingYearStartDay = value;
    }

}
