
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CubeRegionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.MetadataTargetRegionType;


/**
 * ConstraintType is an abstract base type that specific types of constraints (content and attachment) restrict and extend to describe their details. The inclusion of a key or region in a constraint is determined by first processing the included key sets, and then removing those keys defined in the excluded key sets. If no included key sets are defined, then it is assumed the all possible keys or regions are included, and any excluded key or regions are removed from this complete set.
 * 
 * <p>Java-Klasse f�r ConstraintType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ConstraintType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConstraintBaseType">
 *       &lt;sequence>
 *         &lt;element name="ConstraintAttachment" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ConstraintAttachmentType" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="DataKeySet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}DataKeySetType"/>
 *           &lt;element name="MetadataKeySet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}MetadataKeySetType"/>
 *           &lt;element name="CubeRegion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CubeRegionType"/>
 *           &lt;element name="MetadataTargetRegion" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}MetadataTargetRegionType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstraintType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "constraintAttachment",
    "dataKeySetOrMetadataKeySetOrCubeRegion"
})
@XmlSeeAlso({
    ContentConstraintBaseType.class,
    AttachmentConstraintType.class
})
public abstract class ConstraintType
    extends ConstraintBaseType
{

    @XmlElement(name = "ConstraintAttachment", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ConstraintAttachmentType constraintAttachment;
    @XmlElements({
        @XmlElement(name = "DataKeySet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = DataKeySetType.class),
        @XmlElement(name = "MetadataKeySet", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = MetadataKeySetType.class),
        @XmlElement(name = "CubeRegion", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = CubeRegionType.class),
        @XmlElement(name = "MetadataTargetRegion", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", type = MetadataTargetRegionType.class)
    })
    protected List<Object> dataKeySetOrMetadataKeySetOrCubeRegion;

    /**
     * Ruft den Wert der constraintAttachment-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConstraintAttachmentType }
     *     
     */
    public ConstraintAttachmentType getConstraintAttachment() {
        return constraintAttachment;
    }

    /**
     * Legt den Wert der constraintAttachment-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstraintAttachmentType }
     *     
     */
    public void setConstraintAttachment(ConstraintAttachmentType value) {
        this.constraintAttachment = value;
    }

    /**
     * Gets the value of the dataKeySetOrMetadataKeySetOrCubeRegion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataKeySetOrMetadataKeySetOrCubeRegion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataKeySetOrMetadataKeySetOrCubeRegion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataKeySetType }
     * {@link MetadataKeySetType }
     * {@link CubeRegionType }
     * {@link MetadataTargetRegionType }
     * 
     * 
     */
    public List<Object> getDataKeySetOrMetadataKeySetOrCubeRegion() {
        if (dataKeySetOrMetadataKeySetOrCubeRegion == null) {
            dataKeySetOrMetadataKeySetOrCubeRegion = new ArrayList<Object>();
        }
        return this.dataKeySetOrMetadataKeySetOrCubeRegion;
    }

}
