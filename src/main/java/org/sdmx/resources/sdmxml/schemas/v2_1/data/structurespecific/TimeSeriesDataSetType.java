
package org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeSeriesDataSetType is the abstract type which defines the base structure for any data structure definition specific time series based data set. A derived data set type will be created that is specific to a data structure definition. Unlike the base format, only one variation of this is allowed for a data structure definition. This variation is the time dimension as the observation dimension. Data is organised into a collection of time series. Because this derivation is achieved using restriction, data sets conforming to this type will inherently conform to the base data set structure as well. In fact, data structure specific here will be identical to data in the base data set when the time dimension is the observation dimension, even for the derived data set types. This means that the data contained in this structure can be processed in exactly the same manner as the base structure. The same rules for derivation as the base data set type apply to this specialized data set.
 * 
 * <p>Java-Klasse f�r TimeSeriesDataSetType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeSeriesDataSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}DataSetType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0" form="unqualified"/>
 *         &lt;element name="Group" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}GroupType" maxOccurs="unbounded" minOccurs="0" form="unqualified"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="Series" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}TimeSeriesType" maxOccurs="unbounded" form="unqualified"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}SetAttributeGroup"/>
 *       &lt;anyAttribute namespace=''/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeSeriesDataSetType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific")
public abstract class TimeSeriesDataSetType
    extends DataSetType
{


}
