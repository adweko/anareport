
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataAttributeRepresentationType defines the possible local representations of a metadata attribute.
 * 
 * <p>Java-Klasse f�r MetadataAttributeRepresentationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataAttributeRepresentationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}RepresentationType">
 *       &lt;choice>
 *         &lt;element name="TextFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}BasicComponentTextFormatType"/>
 *         &lt;sequence>
 *           &lt;element name="Enumeration" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CodelistReferenceType"/>
 *           &lt;element name="EnumerationFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}CodededTextFormatType" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataAttributeRepresentationType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class MetadataAttributeRepresentationType
    extends RepresentationType
{


}
