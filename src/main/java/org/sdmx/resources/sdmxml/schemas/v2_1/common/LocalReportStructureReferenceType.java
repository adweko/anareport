
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * LocalReportStructureReferenceType is a type for referencing a report structure locally, where the reference to the metadata structure definition which defines it is provided in another context (for example the metadata structure definition in which the reference occurs).
 * 
 * <p>Java-Klasse f�r LocalReportStructureReferenceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LocalReportStructureReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalComponentListReferenceType">
 *       &lt;sequence>
 *         &lt;element name="Ref" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalReportStructureRefType" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocalReportStructureReferenceType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class LocalReportStructureReferenceType
    extends LocalComponentListReferenceType
{


}
