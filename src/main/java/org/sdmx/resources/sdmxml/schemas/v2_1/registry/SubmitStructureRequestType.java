
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ActionType;
import org.sdmx.resources.sdmxml.schemas.v2_1.structure.StructuresType;


/**
 * SubmitStructureRequestType describes the structure of a structure submission. Structural components are provided either in-line or referenced via a SDMX-ML Structure message external to the registry. A default action and external reference resolution action are all provided for each of the contained components, but can be overridden on a per component basis.
 * 
 * <p>Java-Klasse f�r SubmitStructureRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitStructureRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="StructureLocation" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *           &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}Structures"/>
 *         &lt;/choice>
 *         &lt;element name="SubmittedStructure" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmittedStructureType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="action" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ActionType" default="Append" />
 *       &lt;attribute name="externalDependencies" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitStructureRequestType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "structureLocation",
    "structures",
    "submittedStructure"
})
public class SubmitStructureRequestType {

    @XmlElement(name = "StructureLocation", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    @XmlSchemaType(name = "anyURI")
    protected String structureLocation;
    @XmlElement(name = "Structures", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected StructuresType structures;
    @XmlElement(name = "SubmittedStructure", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected List<SubmittedStructureType> submittedStructure;
    @XmlAttribute(name = "action")
    protected ActionType action;
    @XmlAttribute(name = "externalDependencies")
    protected Boolean externalDependencies;

    /**
     * Ruft den Wert der structureLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureLocation() {
        return structureLocation;
    }

    /**
     * Legt den Wert der structureLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureLocation(String value) {
        this.structureLocation = value;
    }

    /**
     * Structures allows for the inline definition of structural components for submission.
     * 
     * @return
     *     possible object is
     *     {@link StructuresType }
     *     
     */
    public StructuresType getStructures() {
        return structures;
    }

    /**
     * Legt den Wert der structures-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StructuresType }
     *     
     */
    public void setStructures(StructuresType value) {
        this.structures = value;
    }

    /**
     * Gets the value of the submittedStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the submittedStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubmittedStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubmittedStructureType }
     * 
     * 
     */
    public List<SubmittedStructureType> getSubmittedStructure() {
        if (submittedStructure == null) {
            submittedStructure = new ArrayList<SubmittedStructureType>();
        }
        return this.submittedStructure;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getAction() {
        if (action == null) {
            return ActionType.APPEND;
        } else {
            return action;
        }
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setAction(ActionType value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der externalDependencies-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isExternalDependencies() {
        if (externalDependencies == null) {
            return false;
        } else {
            return externalDependencies;
        }
    }

    /**
     * Legt den Wert der externalDependencies-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExternalDependencies(Boolean value) {
        this.externalDependencies = value;
    }

}
