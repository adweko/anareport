
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DataProviderSchemeRefType contains a set of reference fields for a data provider scheme.
 * 
 * <p>Java-Klasse f�r DataProviderSchemeRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataProviderSchemeRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationSchemeRefBaseType">
 *       &lt;attribute name="class" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}OrganisationSchemeTypeCodelistType" fixed="DataProviderScheme" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataProviderSchemeRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class DataProviderSchemeRefType
    extends OrganisationSchemeRefBaseType
{


}
