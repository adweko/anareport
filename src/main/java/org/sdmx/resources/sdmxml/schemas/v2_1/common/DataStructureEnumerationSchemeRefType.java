
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * DataStructureEnumerationSchemeRefType contains the reference fields for referencing any item scheme that is allowable as the enumeration of the representation of a data structure definition component.
 * 
 * <p>Java-Klasse f�r DataStructureEnumerationSchemeRefType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataStructureEnumerationSchemeRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemeRefType">
 *       &lt;attribute name="class" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DimensionEumerationSchemeTypeCodelistType" />
 *       &lt;attribute name="package" use="required" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ItemSchemePackageTypeCodelistType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataStructureEnumerationSchemeRefType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
public class DataStructureEnumerationSchemeRefType
    extends ItemSchemeRefType
{


}
