
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalMetadataTargetReferenceType;


/**
 * ReportStructureType describes the structure of a report structure. It comprises a set of metadata attributes that can be defined as a hierarchy, and identifies the potential attachment of these attributes to an object by referencing a target identifier.
 * 
 * <p>Java-Klasse f�r ReportStructureType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReportStructureType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ReportStructureBaseType">
 *       &lt;sequence>
 *         &lt;element name="MetadataTarget" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalMetadataTargetReferenceType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportStructureType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "metadataTarget"
})
public class ReportStructureType
    extends ReportStructureBaseType
{

    @XmlElement(name = "MetadataTarget", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", required = true)
    protected List<LocalMetadataTargetReferenceType> metadataTarget;

    /**
     * Gets the value of the metadataTarget property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadataTarget property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadataTarget().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalMetadataTargetReferenceType }
     * 
     * 
     */
    public List<LocalMetadataTargetReferenceType> getMetadataTarget() {
        if (metadataTarget == null) {
            metadataTarget = new ArrayList<LocalMetadataTargetReferenceType>();
        }
        return this.metadataTarget;
    }

}
