
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SubmitStructureResponseType describes the structure of a response to a structure submission. For each submitted structure, a Result will be returned.
 * 
 * <p>Java-Klasse f�r SubmitStructureResponseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitStructureResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubmissionResult" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}SubmissionResultType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitStructureResponseType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "submissionResult"
})
public class SubmitStructureResponseType {

    @XmlElement(name = "SubmissionResult", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", required = true)
    protected List<SubmissionResultType> submissionResult;

    /**
     * Gets the value of the submissionResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the submissionResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubmissionResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubmissionResultType }
     * 
     * 
     */
    public List<SubmissionResultType> getSubmissionResult() {
        if (submissionResult == null) {
            submissionResult = new ArrayList<SubmissionResultType>();
        }
        return this.submissionResult;
    }

}
