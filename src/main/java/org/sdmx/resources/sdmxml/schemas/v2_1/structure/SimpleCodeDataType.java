
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.SimpleDataType;


/**
 * <p>Java-Klasse f�r SimpleCodeDataType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SimpleCodeDataType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}SimpleDataType">
 *     &lt;enumeration value="Alpha"/>
 *     &lt;enumeration value="AlphaNumeric"/>
 *     &lt;enumeration value="Numeric"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SimpleCodeDataType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
@XmlEnum(SimpleDataType.class)
public enum SimpleCodeDataType {

    @XmlEnumValue("Alpha")
    ALPHA(SimpleDataType.ALPHA),
    @XmlEnumValue("AlphaNumeric")
    ALPHA_NUMERIC(SimpleDataType.ALPHA_NUMERIC),
    @XmlEnumValue("Numeric")
    NUMERIC(SimpleDataType.NUMERIC);
    private final SimpleDataType value;

    SimpleCodeDataType(SimpleDataType v) {
        value = v;
    }

    public SimpleDataType value() {
        return value;
    }

    public static SimpleCodeDataType fromValue(SimpleDataType v) {
        for (SimpleCodeDataType c: SimpleCodeDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
