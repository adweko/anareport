
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.LocalCodelistMapReferenceType;


/**
 * RepresentationMapType describes the structure of the mapping of the value of a source to component to a target component. Either a reference to another map defined within the containing structure set or a description of the source and target text formats must be provided. Note that for key family components, only a reference to a codelist map is relevant, since that is the only type of coded representation allowed in a key family.
 * 
 * <p>Java-Klasse f�r RepresentationMapType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RepresentationMapType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="CodelistMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}LocalCodelistMapReferenceType"/>
 *         &lt;sequence>
 *           &lt;element name="ToTextFormat" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}TextFormatType"/>
 *           &lt;element name="ToValueType" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ToValueTypeType"/>
 *         &lt;/sequence>
 *         &lt;element name="ValueMap" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}ValueMapType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepresentationMapType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure", propOrder = {
    "codelistMap",
    "toTextFormat",
    "toValueType",
    "valueMap"
})
public class RepresentationMapType {

    @XmlElement(name = "CodelistMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected LocalCodelistMapReferenceType codelistMap;
    @XmlElement(name = "ToTextFormat", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected TextFormatType toTextFormat;
    @XmlElement(name = "ToValueType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    @XmlSchemaType(name = "NMTOKEN")
    protected ToValueTypeType toValueType;
    @XmlElement(name = "ValueMap", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
    protected ValueMapType valueMap;

    /**
     * Ruft den Wert der codelistMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocalCodelistMapReferenceType }
     *     
     */
    public LocalCodelistMapReferenceType getCodelistMap() {
        return codelistMap;
    }

    /**
     * Legt den Wert der codelistMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalCodelistMapReferenceType }
     *     
     */
    public void setCodelistMap(LocalCodelistMapReferenceType value) {
        this.codelistMap = value;
    }

    /**
     * Ruft den Wert der toTextFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextFormatType }
     *     
     */
    public TextFormatType getToTextFormat() {
        return toTextFormat;
    }

    /**
     * Legt den Wert der toTextFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextFormatType }
     *     
     */
    public void setToTextFormat(TextFormatType value) {
        this.toTextFormat = value;
    }

    /**
     * Ruft den Wert der toValueType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ToValueTypeType }
     *     
     */
    public ToValueTypeType getToValueType() {
        return toValueType;
    }

    /**
     * Legt den Wert der toValueType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ToValueTypeType }
     *     
     */
    public void setToValueType(ToValueTypeType value) {
        this.toValueType = value;
    }

    /**
     * Ruft den Wert der valueMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValueMapType }
     *     
     */
    public ValueMapType getValueMap() {
        return valueMap;
    }

    /**
     * Legt den Wert der valueMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueMapType }
     *     
     */
    public void setValueMap(ValueMapType value) {
        this.valueMap = value;
    }

}
