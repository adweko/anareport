
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r TargetObjectTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="TargetObjectTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ComponentTypeCodelistType">
 *     &lt;enumeration value="ConstraintTarget"/>
 *     &lt;enumeration value="DataSetTarget"/>
 *     &lt;enumeration value="IdentifiableObjectTarget"/>
 *     &lt;enumeration value="DimensionDescriptorValuesTarget"/>
 *     &lt;enumeration value="ReportPeriodTarget"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TargetObjectTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(ComponentTypeCodelistType.class)
public enum TargetObjectTypeCodelistType {

    @XmlEnumValue("ConstraintTarget")
    CONSTRAINT_TARGET(ComponentTypeCodelistType.CONSTRAINT_TARGET),
    @XmlEnumValue("DataSetTarget")
    DATA_SET_TARGET(ComponentTypeCodelistType.DATA_SET_TARGET),
    @XmlEnumValue("IdentifiableObjectTarget")
    IDENTIFIABLE_OBJECT_TARGET(ComponentTypeCodelistType.IDENTIFIABLE_OBJECT_TARGET),
    @XmlEnumValue("DimensionDescriptorValuesTarget")
    DIMENSION_DESCRIPTOR_VALUES_TARGET(ComponentTypeCodelistType.DIMENSION_DESCRIPTOR_VALUES_TARGET),
    @XmlEnumValue("ReportPeriodTarget")
    REPORT_PERIOD_TARGET(ComponentTypeCodelistType.REPORT_PERIOD_TARGET);
    private final ComponentTypeCodelistType value;

    TargetObjectTypeCodelistType(ComponentTypeCodelistType v) {
        value = v;
    }

    public ComponentTypeCodelistType value() {
        return value;
    }

    public static TargetObjectTypeCodelistType fromValue(ComponentTypeCodelistType v) {
        for (TargetObjectTypeCodelistType c: TargetObjectTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
