
package org.sdmx.resources.sdmxml.schemas.v2_1.data.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.AnnotableType;


/**
 * ObsType defines the structure of a grouped observation. The observation must be provided a value for the dimension which is declared to be at the observation level for this data set. This dimension value should disambiguate the observation within the series in which it is defined (i.e. there should not be another observation with the same dimension value). The observation can contain an observed value and/or attribute values.
 * 
 * <p>Java-Klasse f�r ObsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}AnnotableType">
 *       &lt;sequence>
 *         &lt;element name="ObsDimension" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}BaseValueType"/>
 *         &lt;element name="ObsValue" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ObsValueType" minOccurs="0"/>
 *         &lt;element name="Attributes" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ValuesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", propOrder = {
    "obsDimension",
    "obsValue",
    "attributes"
})
@XmlSeeAlso({
    TimeSeriesObsType.class
})
public class ObsType
    extends AnnotableType
{

    @XmlElement(name = "ObsDimension", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic", required = true)
    protected BaseValueType obsDimension;
    @XmlElement(name = "ObsValue", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected ObsValueType obsValue;
    @XmlElement(name = "Attributes", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic")
    protected ValuesType attributes;

    /**
     * Ruft den Wert der obsDimension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BaseValueType }
     *     
     */
    public BaseValueType getObsDimension() {
        return obsDimension;
    }

    /**
     * Legt den Wert der obsDimension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseValueType }
     *     
     */
    public void setObsDimension(BaseValueType value) {
        this.obsDimension = value;
    }

    /**
     * Ruft den Wert der obsValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObsValueType }
     *     
     */
    public ObsValueType getObsValue() {
        return obsValue;
    }

    /**
     * Legt den Wert der obsValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObsValueType }
     *     
     */
    public void setObsValue(ObsValueType value) {
        this.obsValue = value;
    }

    /**
     * Ruft den Wert der attributes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ValuesType }
     *     
     */
    public ValuesType getAttributes() {
        return attributes;
    }

    /**
     * Legt den Wert der attributes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuesType }
     *     
     */
    public void setAttributes(ValuesType value) {
        this.attributes = value;
    }

}
