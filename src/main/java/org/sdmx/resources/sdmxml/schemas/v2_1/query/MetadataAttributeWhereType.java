
package org.sdmx.resources.sdmxml.schemas.v2_1.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MetadataAttributeWhereType describes the parameters for a metadata attribute. A metadata attribute can be queried based on its identification, the concept from which it takes its semantic, and an item scheme it uses as its representation. Nested metadata attributes allow for the querying of metadata attributes explicitly at nested level, although a top level metadata attribute query will be processed by querying metadata attributes at any level. This is an implicit set of "and" parameters, that is the conditions within this must all be met in order to return a match.
 * 
 * <p>Java-Klasse f�r MetadataAttributeWhereType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MetadataAttributeWhereType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataAttributeWhereBaseType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query}MetadataAttributeWhere" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataAttributeWhereType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query", propOrder = {
    "metadataAttributeWhere"
})
public class MetadataAttributeWhereType
    extends MetadataAttributeWhereBaseType
{

    @XmlElement(name = "MetadataAttributeWhere", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/query")
    protected List<MetadataAttributeWhereType> metadataAttributeWhere;

    /**
     * Gets the value of the metadataAttributeWhere property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadataAttributeWhere property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadataAttributeWhere().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadataAttributeWhereType }
     * 
     * 
     */
    public List<MetadataAttributeWhereType> getMetadataAttributeWhere() {
        if (metadataAttributeWhere == null) {
            metadataAttributeWhere = new ArrayList<MetadataAttributeWhereType>();
        }
        return this.metadataAttributeWhere;
    }

}
