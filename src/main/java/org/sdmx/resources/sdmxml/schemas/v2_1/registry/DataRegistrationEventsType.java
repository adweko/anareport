
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.CategoryReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.DataProviderReferenceType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.EmptyType;
import org.sdmx.resources.sdmxml.schemas.v2_1.common.ProvisionAgreementReferenceType;


/**
 * DataRegistrationEventsType details the data registration events for the subscription. It is possible to subscribe to all data registration events in the repository, or specific events for; single registrations, provision agreements, data providers, data flows, key families, and categories that categorize data flows or key families.
 * 
 * <p>Java-Klasse f�r DataRegistrationEventsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DataRegistrationEventsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="AllEvents" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}EmptyType"/>
 *         &lt;choice maxOccurs="unbounded">
 *           &lt;element name="RegistrationID" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}IDType"/>
 *           &lt;element name="ProvisionAgreement" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}ProvisionAgreementReferenceType"/>
 *           &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType"/>
 *           &lt;element name="DataflowReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}MaintainableEventType"/>
 *           &lt;element name="KeyFamilyReference" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}MaintainableEventType"/>
 *           &lt;element name="Category" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}CategoryReferenceType"/>
 *         &lt;/choice>
 *       &lt;/choice>
 *       &lt;attribute name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" fixed="DATA" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataRegistrationEventsType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "allEvents",
    "registrationIDOrProvisionAgreementOrDataProvider"
})
public class DataRegistrationEventsType {

    @XmlElement(name = "AllEvents", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry")
    protected EmptyType allEvents;
    @XmlElementRefs({
        @XmlElementRef(name = "DataflowReference", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Category", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "RegistrationID", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "DataProvider", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "KeyFamilyReference", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ProvisionAgreement", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> registrationIDOrProvisionAgreementOrDataProvider;
    @XmlAttribute(name = "TYPE")
    protected String type;

    /**
     * Ruft den Wert der allEvents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmptyType }
     *     
     */
    public EmptyType getAllEvents() {
        return allEvents;
    }

    /**
     * Legt den Wert der allEvents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyType }
     *     
     */
    public void setAllEvents(EmptyType value) {
        this.allEvents = value;
    }

    /**
     * Gets the value of the registrationIDOrProvisionAgreementOrDataProvider property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the registrationIDOrProvisionAgreementOrDataProvider property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegistrationIDOrProvisionAgreementOrDataProvider().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link MaintainableEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link MaintainableEventType }{@code >}
     * {@link JAXBElement }{@code <}{@link CategoryReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link ProvisionAgreementReferenceType }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link DataProviderReferenceType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getRegistrationIDOrProvisionAgreementOrDataProvider() {
        if (registrationIDOrProvisionAgreementOrDataProvider == null) {
            registrationIDOrProvisionAgreementOrDataProvider = new ArrayList<JAXBElement<?>>();
        }
        return this.registrationIDOrProvisionAgreementOrDataProvider;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTYPE() {
        if (type == null) {
            return "DATA";
        } else {
            return type;
        }
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTYPE(String value) {
        this.type = value;
    }

}
