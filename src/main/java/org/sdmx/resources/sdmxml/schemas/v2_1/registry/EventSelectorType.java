
package org.sdmx.resources.sdmxml.schemas.v2_1.registry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * EventSelectorType describes the details of the events for a subscription. It allows subscribers to specify registry and repository events for which they wish to receive notifications.
 * 
 * <p>Java-Klasse f�r EventSelectorType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EventSelectorType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="3">
 *         &lt;element name="StructuralRepositoryEvents" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}StructuralRepositoryEventsType"/>
 *         &lt;element name="DataRegistrationEvents" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}DataRegistrationEventsType"/>
 *         &lt;element name="MetadataRegistrationEvents" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry}MetadataRegistrationEventsType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventSelectorType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", propOrder = {
    "structuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents"
})
public class EventSelectorType {

    @XmlElements({
        @XmlElement(name = "StructuralRepositoryEvents", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = StructuralRepositoryEventsType.class),
        @XmlElement(name = "DataRegistrationEvents", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = DataRegistrationEventsType.class),
        @XmlElement(name = "MetadataRegistrationEvents", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/registry", type = MetadataRegistrationEventsType.class)
    })
    protected List<Object> structuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents;

    /**
     * Gets the value of the structuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the structuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStructuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StructuralRepositoryEventsType }
     * {@link DataRegistrationEventsType }
     * {@link MetadataRegistrationEventsType }
     * 
     * 
     */
    public List<Object> getStructuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents() {
        if (structuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents == null) {
            structuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents = new ArrayList<Object>();
        }
        return this.structuralRepositoryEventsOrDataRegistrationEventsOrMetadataRegistrationEvents;
    }

}
