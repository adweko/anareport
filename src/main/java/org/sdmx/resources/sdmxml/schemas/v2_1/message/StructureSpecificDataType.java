
package org.sdmx.resources.sdmxml.schemas.v2_1.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * StructureSpecificDataType defines the structure of the structure specific data message. Note that the data set payload type is abstract, and therefore it will have to be assigned a type in an instance. This type must be derived from the base type referenced. This base type defines a general structure which can be followed to allow for generic processing of the data even if the exact details of the data structure specific format are not known.
 * 
 * <p>Java-Klasse f�r StructureSpecificDataType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StructureSpecificDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}MessageType">
 *       &lt;sequence>
 *         &lt;element name="Header" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message}StructureSpecificDataHeaderType"/>
 *         &lt;element name="DataSet" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}DataSetType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer}Footer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureSpecificDataType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message")
@XmlSeeAlso({
    StructureSpecificTimeSeriesDataType.class
})
public class StructureSpecificDataType
    extends MessageType
{


}
