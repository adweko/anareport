
package org.sdmx.resources.sdmxml.schemas.v2_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r StructureTypeCodelistType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="StructureTypeCodelistType">
 *   &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StructureOrUsageTypeCodelistType">
 *     &lt;enumeration value="DataStructure"/>
 *     &lt;enumeration value="MetadataStructure"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StructureTypeCodelistType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common")
@XmlEnum(StructureOrUsageTypeCodelistType.class)
public enum StructureTypeCodelistType {

    @XmlEnumValue("DataStructure")
    DATA_STRUCTURE(StructureOrUsageTypeCodelistType.DATA_STRUCTURE),
    @XmlEnumValue("MetadataStructure")
    METADATA_STRUCTURE(StructureOrUsageTypeCodelistType.METADATA_STRUCTURE);
    private final StructureOrUsageTypeCodelistType value;

    StructureTypeCodelistType(StructureOrUsageTypeCodelistType v) {
        value = v;
    }

    public StructureOrUsageTypeCodelistType value() {
        return value;
    }

    public static StructureTypeCodelistType fromValue(StructureOrUsageTypeCodelistType v) {
        for (StructureTypeCodelistType c: StructureTypeCodelistType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
