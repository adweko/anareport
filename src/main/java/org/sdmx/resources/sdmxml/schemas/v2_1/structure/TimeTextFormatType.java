
package org.sdmx.resources.sdmxml.schemas.v2_1.structure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeTextFormat is a restricted version of the SimpleComponentTextFormatType that only allows time based format and specifies a default ObservationalTimePeriod representation and facets of a start and end time.
 * 
 * <p>Java-Klasse f�r TimeTextFormatType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TimeTextFormatType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure}SimpleComponentTextFormatType">
 *       &lt;attribute name="textType" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}TimeDataType" default="ObservationalTimePeriod" />
 *       &lt;attribute name="startTime" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StandardTimePeriodType" />
 *       &lt;attribute name="endTime" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}StandardTimePeriodType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeTextFormatType", namespace = "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure")
public class TimeTextFormatType
    extends SimpleComponentTextFormatType
{


}
