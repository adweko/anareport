
package de.bundesbank.statistik.riad.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_RIAD_HDR_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_RIAD_HDR_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="RPRTNG_AGNT_CD" use="required" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_BLZ" />
 *       &lt;attribute name="DT_RFRNC" use="required" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_YYYY_MM" />
 *       &lt;attribute name="SRVY_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_SRVY_ID" fixed="RIAD" />
 *       &lt;attribute name="PRT_MSSG" use="required" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_PRT_MSSG" />
 *       &lt;attribute name="IS_LST_PRT_MSSG" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_RIAD_HDR_C", namespace = "http://www.bundesbank.de/statistik/riad/v2")
public class ObsBBKRIADHDRC
    extends ObsType
{


}
