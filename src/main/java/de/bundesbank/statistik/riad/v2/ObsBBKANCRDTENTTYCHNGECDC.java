
package de.bundesbank.statistik.riad.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_ENTTY_CHNGE_CD_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_ENTTY_CHNGE_CD_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="TYP_OLD_CP_ID" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="1" />
 *       &lt;attribute name="OLD_CP_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID" />
 *       &lt;attribute name="TYP_NEW_CP_ID" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="1" />
 *       &lt;attribute name="NEW_CP_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_ENTTY_CHNGE_CD_C", namespace = "http://www.bundesbank.de/statistik/riad/v2")
public class ObsBBKANCRDTENTTYCHNGECDC
    extends ObsType
{


}
