
package de.bundesbank.statistik.riad.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_ENTTY_RFRNC_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_ENTTY_RFRNC_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="TYP_CP_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_CP_ID" />
 *       &lt;attribute name="CP_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID" />
 *       &lt;attribute name="LEI" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_LEI_NA" />
 *       &lt;attribute name="TYP_HD_OFFC_UNDRTKNG_ID" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_CP_ID" />
 *       &lt;attribute name="HD_OFFC_UNDRTKNG_ID" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID" />
 *       &lt;attribute name="TYP_IMMDT_PRNT_UNDRTKNG_ID" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_CP_ID_PRTC" />
 *       &lt;attribute name="IMMDT_PRNT_UNDRTKNG_ID" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID_NA" />
 *       &lt;attribute name="TYP_ULTMT_PRNT_UNDRTKNG_ID" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_CP_ID_PRTC" />
 *       &lt;attribute name="ULTMT_PRNT_UNDRTKNG_ID" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID_NA" />
 *       &lt;attribute name="NM_ENTTY" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_255" />
 *       &lt;attribute name="STRT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_LTN_EXT_255_NA" />
 *       &lt;attribute name="CTY" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_LTN_EXT_255_NA" />
 *       &lt;attribute name="TRRTRL_UNT" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_NUTS3_NA" />
 *       &lt;attribute name="PSTL_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_LTN_EXT_255_NA" />
 *       &lt;attribute name="CNTRY" type="{http://www.bundesbank.de/statistik/ecb/codelists/v2}CL_ECBSDD_ISO3166_DSJNT_IO" />
 *       &lt;attribute name="LGL_FRM" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_LGL_FRM_NA" />
 *       &lt;attribute name="INSTTTNL_SCTR" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_INSTTTNL_SCTR" />
 *       &lt;attribute name="KUSY" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_KUSY" />
 *       &lt;attribute name="ECNMC_ACTVTY" type="{http://www.bundesbank.de/statistik/ecb/codelists/v2}CL_ECBSDD_NACE_LVLS2TO4_STGNG" />
 *       &lt;attribute name="LGL_PRCDNG_STTS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_LGL_PRCDNG_STTS_NA" />
 *       &lt;attribute name="LGL_PRCDNG_STTS_DT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="ENTRPRS_SZ" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_SZ_NA" />
 *       &lt;attribute name="ENTRPRS_SZ_DT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="NMBR_EMPLYS" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="BLNC_SHT_TTL_CRRNCY" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="ANNL_TRNVR_CRRNCY" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ALL_2D_NA" />
 *       &lt;attribute name="ACCNTNG_FRMWRK_SL" type="{http://www.bundesbank.de/statistik/ecb/codelists/v2}CL_ECBSDD_ACCNTNG_FRMWRK_RIAD_CLLCTN" />
 *       &lt;attribute name="ENTTY_RIAD_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_50" />
 *       &lt;attribute name="ISIN" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_ISIN_12" />
 *       &lt;attribute name="AT_FB_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_6D_L_3D" />
 *       &lt;attribute name="AT_GEM_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_5D" />
 *       &lt;attribute name="AT_IDENT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1_TO_8" />
 *       &lt;attribute name="AT_LAE_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1" />
 *       &lt;attribute name="AT_ZVR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1_TO_10" />
 *       &lt;attribute name="AVID" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="BE_KBO_BCE_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="BE_OND_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_0_9" />
 *       &lt;attribute name="BG_BULSTAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9_10_13" />
 *       &lt;attribute name="BG_OTHER_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_255" />
 *       &lt;attribute name="BG_UIC_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9_13" />
 *       &lt;attribute name="BG_VAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_BG_9_10" />
 *       &lt;attribute name="BIC" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_11D_OR_L_OR_8_D_OR_L" />
 *       &lt;attribute name="BLMBRG_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="BR_CNPJ_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_2D_3D_3D_4D_2D" />
 *       &lt;attribute name="BVD_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="CA_BN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9" />
 *       &lt;attribute name="CH_UID_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_CHE_3D_3D_3D" />
 *       &lt;attribute name="CN_CC_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_18_L_OR_D" />
 *       &lt;attribute name="CY_DRCOR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_COP_8D" />
 *       &lt;attribute name="CY_GG_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_S13_8D" />
 *       &lt;attribute name="CY_IF_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_CYIF_4D" />
 *       &lt;attribute name="CY_NOTAP_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NotApplicable" />
 *       &lt;attribute name="CY_PF_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_PF_4D" />
 *       &lt;attribute name="CY_TIC_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8D_1L" />
 *       &lt;attribute name="CY_VAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_D_7D_L" />
 *       &lt;attribute name="CZ_ICO_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="CZ_NID_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8_OR_10" />
 *       &lt;attribute name="DE_BAK_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_BAK" />
 *       &lt;attribute name="DE_BAKISG_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_7_8" />
 *       &lt;attribute name="DE_BAKISN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_7_8" />
 *       &lt;attribute name="DE_BLZ" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_BLZ" />
 *       &lt;attribute name="DE_DESTATIS_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="DE_GNR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_GnR_6D_3L_1L_4D" />
 *       &lt;attribute name="DE_HRA_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_HRA_6D_3L_1L_4D" />
 *       &lt;attribute name="DE_HRB_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_HRB_6D_3L_1L_4D" />
 *       &lt;attribute name="DE_NOTAP_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NotApplicable" />
 *       &lt;attribute name="DE_PR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_PR_6D_3L_1L_4D" />
 *       &lt;attribute name="DE_TAX_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_13D" />
 *       &lt;attribute name="DE_VAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_DE_9D" />
 *       &lt;attribute name="DE_VR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_VR_6D_3L_1L_4D" />
 *       &lt;attribute name="DK_CVR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="DK_FT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_VAR_LEN_D_DASH" />
 *       &lt;attribute name="DK_NOTAP_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NotApplicable" />
 *       &lt;attribute name="DK_SE_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="DUNS_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="EE_FON_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1_TO_4" />
 *       &lt;attribute name="EE_RG_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="EIOPA_ENTTY_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="ES_NIF_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9D_OR_L" />
 *       &lt;attribute name="FI_ALV_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_FI_8D" />
 *       &lt;attribute name="FI_NOTAP_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NotApplicable" />
 *       &lt;attribute name="FI_SIRA_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8D_3D" />
 *       &lt;attribute name="FI_Y_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8D_DASH" />
 *       &lt;attribute name="FR_CIB" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_5D" />
 *       &lt;attribute name="FR_RNA_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1L_9D" />
 *       &lt;attribute name="FR_SIREN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9" />
 *       &lt;attribute name="FVC_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_255" />
 *       &lt;attribute name="GB_CRN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8D_OR_SC6D_or_NL6D" />
 *       &lt;attribute name="GB_UTR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_10D_OR_9DK" />
 *       &lt;attribute name="GEN_IPF_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GEN_NBR_ENTTY_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GEN_NCB_ENTTY_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GEN_NSA_ENTTY_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GEN_NSI_ENTTY_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GEN_OTHER_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_511" />
 *       &lt;attribute name="GEN_PS_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GEN_TAX_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GEN_TRD_RGSTR_ENTTY_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GEN_VAT_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="GR_AFM_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9" />
 *       &lt;attribute name="GR_IMO_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_7" />
 *       &lt;attribute name="HK_BR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8D_3D_2D_2D_L" />
 *       &lt;attribute name="HR_MB_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="HR_MBS_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_0_OR_1_8D" />
 *       &lt;attribute name="HR_OIB_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_11" />
 *       &lt;attribute name="HU_CEG_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_2D-2D-6D" />
 *       &lt;attribute name="HU_FB_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_FB5D_OR_FB3D_1L_2D" />
 *       &lt;attribute name="HU_KOZ_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_HU_8D" />
 *       &lt;attribute name="HU_TOR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="IE_CRO_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="IN_CIN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_21D_OR_L" />
 *       &lt;attribute name="IN_PAN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_10D_OR_L" />
 *       &lt;attribute name="IFS_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_255" />
 *       &lt;attribute name="IT_ABI_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="IT_CCIAA_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_2L_7D" />
 *       &lt;attribute name="IT_CF_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_11" />
 *       &lt;attribute name="IT_UCITS_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_MAX_7D" />
 *       &lt;attribute name="JP_CN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1D_12D" />
 *       &lt;attribute name="LEID" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="LT_INV_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1L_3D" />
 *       &lt;attribute name="LT_JAR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9" />
 *       &lt;attribute name="LU_IF_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1L_6D_C_5D" />
 *       &lt;attribute name="LU_RCS_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_A_J_D" />
 *       &lt;attribute name="LU_VAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="LV_FON_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_LVAF_6_OR_LVIF_6_OR_LV_11" />
 *       &lt;attribute name="LV_NBR_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_11" />
 *       &lt;attribute name="LV_VAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_LV_11D" />
 *       &lt;attribute name="MC_CIB" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_5D" />
 *       &lt;attribute name="MC_NIS_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_4D_1L_5D" />
 *       &lt;attribute name="MC_RCI_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_2D_P_S_5D" />
 *       &lt;attribute name="MT_CNUM_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="MT_OLE_CD" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_LTN_EXT_50" />
 *       &lt;attribute name="MT_VAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="MX_RFC_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_3L_6D_3D_OR_L" />
 *       &lt;attribute name="NL_KVK_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="NL_RSIN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9" />
 *       &lt;attribute name="PL_KRS_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_VAR_LEN" />
 *       &lt;attribute name="PL_NIP_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_10" />
 *       &lt;attribute name="PL_REGON_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_14_OR_9" />
 *       &lt;attribute name="PL_VAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_PL_10" />
 *       &lt;attribute name="PT_FSA_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_MAX_6D" />
 *       &lt;attribute name="PT_NIF_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_9" />
 *       &lt;attribute name="RO_CUI_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_RO_10" />
 *       &lt;attribute name="RO_TAX_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_RO_10" />
 *       &lt;attribute name="RO_TRN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_J_D_D_D" />
 *       &lt;attribute name="RU_INN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_10" />
 *       &lt;attribute name="SE_FIN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_5D" />
 *       &lt;attribute name="SE_MOM_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_SE_12" />
 *       &lt;attribute name="SE_ORG_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_10D_DASH" />
 *       &lt;attribute name="SI_DAV_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="SI_DDV_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_SI_8" />
 *       &lt;attribute name="SI_MAT_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_10" />
 *       &lt;attribute name="SK_ICO_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_8" />
 *       &lt;attribute name="SK_IF_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_SK_8D_3L_2D" />
 *       &lt;attribute name="TR_VKN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_1D_TO_10D" />
 *       &lt;attribute name="US_EIN_CD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ID_2D_7D" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_ENTTY_RFRNC_C", namespace = "http://www.bundesbank.de/statistik/riad/v2")
public class ObsBBKANCRDTENTTYRFRNCC
    extends ObsType
{


}
