
package de.bundesbank.statistik.ecb.codelists.v2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r CL_ECBSDD_ISO3166_DSJNT_IO.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="CL_ECBSDD_ISO3166_DSJNT_IO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AD"/>
 *     &lt;enumeration value="AE"/>
 *     &lt;enumeration value="AF"/>
 *     &lt;enumeration value="AG"/>
 *     &lt;enumeration value="AI"/>
 *     &lt;enumeration value="AL"/>
 *     &lt;enumeration value="AM"/>
 *     &lt;enumeration value="AO"/>
 *     &lt;enumeration value="AQ"/>
 *     &lt;enumeration value="AR"/>
 *     &lt;enumeration value="AS"/>
 *     &lt;enumeration value="AT"/>
 *     &lt;enumeration value="AU"/>
 *     &lt;enumeration value="AW"/>
 *     &lt;enumeration value="AX"/>
 *     &lt;enumeration value="AZ"/>
 *     &lt;enumeration value="BA"/>
 *     &lt;enumeration value="BB"/>
 *     &lt;enumeration value="BD"/>
 *     &lt;enumeration value="BE"/>
 *     &lt;enumeration value="BF"/>
 *     &lt;enumeration value="BG"/>
 *     &lt;enumeration value="BH"/>
 *     &lt;enumeration value="BI"/>
 *     &lt;enumeration value="BJ"/>
 *     &lt;enumeration value="BL"/>
 *     &lt;enumeration value="BM"/>
 *     &lt;enumeration value="BN"/>
 *     &lt;enumeration value="BO"/>
 *     &lt;enumeration value="BQ"/>
 *     &lt;enumeration value="BR"/>
 *     &lt;enumeration value="BS"/>
 *     &lt;enumeration value="BT"/>
 *     &lt;enumeration value="BV"/>
 *     &lt;enumeration value="BW"/>
 *     &lt;enumeration value="BY"/>
 *     &lt;enumeration value="BZ"/>
 *     &lt;enumeration value="CA"/>
 *     &lt;enumeration value="CC"/>
 *     &lt;enumeration value="CD"/>
 *     &lt;enumeration value="CF"/>
 *     &lt;enumeration value="CG"/>
 *     &lt;enumeration value="CH"/>
 *     &lt;enumeration value="CI"/>
 *     &lt;enumeration value="CK"/>
 *     &lt;enumeration value="CL"/>
 *     &lt;enumeration value="CM"/>
 *     &lt;enumeration value="CN"/>
 *     &lt;enumeration value="CO"/>
 *     &lt;enumeration value="CR"/>
 *     &lt;enumeration value="CU"/>
 *     &lt;enumeration value="CV"/>
 *     &lt;enumeration value="CW"/>
 *     &lt;enumeration value="CX"/>
 *     &lt;enumeration value="CY"/>
 *     &lt;enumeration value="CZ"/>
 *     &lt;enumeration value="DE"/>
 *     &lt;enumeration value="DJ"/>
 *     &lt;enumeration value="DK"/>
 *     &lt;enumeration value="DM"/>
 *     &lt;enumeration value="DO"/>
 *     &lt;enumeration value="DZ"/>
 *     &lt;enumeration value="E$"/>
 *     &lt;enumeration value="EC"/>
 *     &lt;enumeration value="EE"/>
 *     &lt;enumeration value="EG"/>
 *     &lt;enumeration value="EH"/>
 *     &lt;enumeration value="ER"/>
 *     &lt;enumeration value="ES"/>
 *     &lt;enumeration value="ET"/>
 *     &lt;enumeration value="FI"/>
 *     &lt;enumeration value="FJ"/>
 *     &lt;enumeration value="FK"/>
 *     &lt;enumeration value="FM"/>
 *     &lt;enumeration value="FO"/>
 *     &lt;enumeration value="FR"/>
 *     &lt;enumeration value="GA"/>
 *     &lt;enumeration value="GB"/>
 *     &lt;enumeration value="GD"/>
 *     &lt;enumeration value="GE"/>
 *     &lt;enumeration value="GF"/>
 *     &lt;enumeration value="GG"/>
 *     &lt;enumeration value="GH"/>
 *     &lt;enumeration value="GI"/>
 *     &lt;enumeration value="GL"/>
 *     &lt;enumeration value="GM"/>
 *     &lt;enumeration value="GN"/>
 *     &lt;enumeration value="GP"/>
 *     &lt;enumeration value="GQ"/>
 *     &lt;enumeration value="GR"/>
 *     &lt;enumeration value="GS"/>
 *     &lt;enumeration value="GT"/>
 *     &lt;enumeration value="GU"/>
 *     &lt;enumeration value="GW"/>
 *     &lt;enumeration value="GY"/>
 *     &lt;enumeration value="HK"/>
 *     &lt;enumeration value="HM"/>
 *     &lt;enumeration value="HN"/>
 *     &lt;enumeration value="HR"/>
 *     &lt;enumeration value="HT"/>
 *     &lt;enumeration value="HU"/>
 *     &lt;enumeration value="ID"/>
 *     &lt;enumeration value="IE"/>
 *     &lt;enumeration value="IL"/>
 *     &lt;enumeration value="IM"/>
 *     &lt;enumeration value="IN"/>
 *     &lt;enumeration value="IO"/>
 *     &lt;enumeration value="IQ"/>
 *     &lt;enumeration value="IR"/>
 *     &lt;enumeration value="IS"/>
 *     &lt;enumeration value="IT"/>
 *     &lt;enumeration value="JE"/>
 *     &lt;enumeration value="JM"/>
 *     &lt;enumeration value="JO"/>
 *     &lt;enumeration value="JP"/>
 *     &lt;enumeration value="KE"/>
 *     &lt;enumeration value="KG"/>
 *     &lt;enumeration value="KH"/>
 *     &lt;enumeration value="KI"/>
 *     &lt;enumeration value="KM"/>
 *     &lt;enumeration value="KN"/>
 *     &lt;enumeration value="KP"/>
 *     &lt;enumeration value="KR"/>
 *     &lt;enumeration value="KW"/>
 *     &lt;enumeration value="KY"/>
 *     &lt;enumeration value="KZ"/>
 *     &lt;enumeration value="LA"/>
 *     &lt;enumeration value="LB"/>
 *     &lt;enumeration value="LC"/>
 *     &lt;enumeration value="LI"/>
 *     &lt;enumeration value="LK"/>
 *     &lt;enumeration value="LR"/>
 *     &lt;enumeration value="LS"/>
 *     &lt;enumeration value="LT"/>
 *     &lt;enumeration value="LU"/>
 *     &lt;enumeration value="LV"/>
 *     &lt;enumeration value="LY"/>
 *     &lt;enumeration value="MA"/>
 *     &lt;enumeration value="MC"/>
 *     &lt;enumeration value="MD"/>
 *     &lt;enumeration value="ME"/>
 *     &lt;enumeration value="MF"/>
 *     &lt;enumeration value="MG"/>
 *     &lt;enumeration value="MH"/>
 *     &lt;enumeration value="MK"/>
 *     &lt;enumeration value="ML"/>
 *     &lt;enumeration value="MM"/>
 *     &lt;enumeration value="MN"/>
 *     &lt;enumeration value="MO"/>
 *     &lt;enumeration value="MP"/>
 *     &lt;enumeration value="MQ"/>
 *     &lt;enumeration value="MR"/>
 *     &lt;enumeration value="MS"/>
 *     &lt;enumeration value="MT"/>
 *     &lt;enumeration value="MU"/>
 *     &lt;enumeration value="MV"/>
 *     &lt;enumeration value="MW"/>
 *     &lt;enumeration value="MX"/>
 *     &lt;enumeration value="MY"/>
 *     &lt;enumeration value="MZ"/>
 *     &lt;enumeration value="N$"/>
 *     &lt;enumeration value="NA"/>
 *     &lt;enumeration value="NC"/>
 *     &lt;enumeration value="NE"/>
 *     &lt;enumeration value="NF"/>
 *     &lt;enumeration value="NG"/>
 *     &lt;enumeration value="NI"/>
 *     &lt;enumeration value="NL"/>
 *     &lt;enumeration value="NO"/>
 *     &lt;enumeration value="NP"/>
 *     &lt;enumeration value="NR"/>
 *     &lt;enumeration value="NU"/>
 *     &lt;enumeration value="NZ"/>
 *     &lt;enumeration value="OM"/>
 *     &lt;enumeration value="PA"/>
 *     &lt;enumeration value="PE"/>
 *     &lt;enumeration value="PF"/>
 *     &lt;enumeration value="PG"/>
 *     &lt;enumeration value="PH"/>
 *     &lt;enumeration value="PK"/>
 *     &lt;enumeration value="PL"/>
 *     &lt;enumeration value="PM"/>
 *     &lt;enumeration value="PN"/>
 *     &lt;enumeration value="PR"/>
 *     &lt;enumeration value="PS"/>
 *     &lt;enumeration value="PT"/>
 *     &lt;enumeration value="PW"/>
 *     &lt;enumeration value="PY"/>
 *     &lt;enumeration value="QA"/>
 *     &lt;enumeration value="RE"/>
 *     &lt;enumeration value="RO"/>
 *     &lt;enumeration value="RS"/>
 *     &lt;enumeration value="RU"/>
 *     &lt;enumeration value="RW"/>
 *     &lt;enumeration value="SA"/>
 *     &lt;enumeration value="SB"/>
 *     &lt;enumeration value="SC"/>
 *     &lt;enumeration value="SD"/>
 *     &lt;enumeration value="SE"/>
 *     &lt;enumeration value="SG"/>
 *     &lt;enumeration value="SH"/>
 *     &lt;enumeration value="SI"/>
 *     &lt;enumeration value="SJ"/>
 *     &lt;enumeration value="SK"/>
 *     &lt;enumeration value="SL"/>
 *     &lt;enumeration value="SM"/>
 *     &lt;enumeration value="SN"/>
 *     &lt;enumeration value="SO"/>
 *     &lt;enumeration value="SR"/>
 *     &lt;enumeration value="SS"/>
 *     &lt;enumeration value="ST"/>
 *     &lt;enumeration value="SV"/>
 *     &lt;enumeration value="SX"/>
 *     &lt;enumeration value="SY"/>
 *     &lt;enumeration value="SZ"/>
 *     &lt;enumeration value="TC"/>
 *     &lt;enumeration value="TD"/>
 *     &lt;enumeration value="TF"/>
 *     &lt;enumeration value="TG"/>
 *     &lt;enumeration value="TH"/>
 *     &lt;enumeration value="TJ"/>
 *     &lt;enumeration value="TK"/>
 *     &lt;enumeration value="TL"/>
 *     &lt;enumeration value="TM"/>
 *     &lt;enumeration value="TN"/>
 *     &lt;enumeration value="TO"/>
 *     &lt;enumeration value="TR"/>
 *     &lt;enumeration value="TT"/>
 *     &lt;enumeration value="TV"/>
 *     &lt;enumeration value="TW"/>
 *     &lt;enumeration value="TZ"/>
 *     &lt;enumeration value="UA"/>
 *     &lt;enumeration value="UG"/>
 *     &lt;enumeration value="UM"/>
 *     &lt;enumeration value="US"/>
 *     &lt;enumeration value="UY"/>
 *     &lt;enumeration value="UZ"/>
 *     &lt;enumeration value="VA"/>
 *     &lt;enumeration value="VC"/>
 *     &lt;enumeration value="VE"/>
 *     &lt;enumeration value="VG"/>
 *     &lt;enumeration value="VI"/>
 *     &lt;enumeration value="VN"/>
 *     &lt;enumeration value="VU"/>
 *     &lt;enumeration value="WF"/>
 *     &lt;enumeration value="WS"/>
 *     &lt;enumeration value="YE"/>
 *     &lt;enumeration value="YT"/>
 *     &lt;enumeration value="ZA"/>
 *     &lt;enumeration value="ZM"/>
 *     &lt;enumeration value="ZW"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CL_ECBSDD_ISO3166_DSJNT_IO", namespace = "http://www.bundesbank.de/statistik/ecb/codelists/v2")
@XmlEnum
public enum CLECBSDDISO3166DSJNTIO {


    /**
     * Andorra
     * 
     */
    AD("AD"),

    /**
     * United Arab Emirates (the)
     * 
     */
    AE("AE"),

    /**
     * Afghanistan
     * 
     */
    AF("AF"),

    /**
     * Antigua and Barbuda
     * 
     */
    AG("AG"),

    /**
     * Anguilla
     * 
     */
    AI("AI"),

    /**
     * Albania
     * 
     */
    AL("AL"),

    /**
     * Armenia
     * 
     */
    AM("AM"),

    /**
     * Angola
     * 
     */
    AO("AO"),

    /**
     * Antarctica
     * 
     */
    AQ("AQ"),

    /**
     * Argentina
     * 
     */
    AR("AR"),

    /**
     * American Samoa
     * 
     */
    AS("AS"),

    /**
     * Austria
     * 
     */
    AT("AT"),

    /**
     * Australia
     * 
     */
    AU("AU"),

    /**
     * Aruba
     * 
     */
    AW("AW"),

    /**
     * Aland Islands
     * 
     */
    AX("AX"),

    /**
     * Azerbaijan
     * 
     */
    AZ("AZ"),

    /**
     * Bosnia and Herzegovina
     * 
     */
    BA("BA"),

    /**
     * Barbados
     * 
     */
    BB("BB"),

    /**
     * Bangladesh
     * 
     */
    BD("BD"),

    /**
     * Belgium
     * 
     */
    BE("BE"),

    /**
     * Burkina Faso
     * 
     */
    BF("BF"),

    /**
     * Bulgaria
     * 
     */
    BG("BG"),

    /**
     * Bahrain
     * 
     */
    BH("BH"),

    /**
     * Burundi
     * 
     */
    BI("BI"),

    /**
     * Benin
     * 
     */
    BJ("BJ"),

    /**
     * Saint Barth�lemy
     * 
     */
    BL("BL"),

    /**
     * Bermuda
     * 
     */
    BM("BM"),

    /**
     * Brunei Darussalam
     * 
     */
    BN("BN"),

    /**
     * Bolivia (Plurinational State of)
     * 
     */
    BO("BO"),

    /**
     * Bonaire, Saint Eustatius and Saba
     * 
     */
    BQ("BQ"),

    /**
     * Brazil
     * 
     */
    BR("BR"),

    /**
     * Bahamas (the)
     * 
     */
    BS("BS"),

    /**
     * Bhutan
     * 
     */
    BT("BT"),

    /**
     * Bouvet Island
     * 
     */
    BV("BV"),

    /**
     * Botswana
     * 
     */
    BW("BW"),

    /**
     * Belarus
     * 
     */
    BY("BY"),

    /**
     * Belize
     * 
     */
    BZ("BZ"),

    /**
     * Canada
     * 
     */
    CA("CA"),

    /**
     * Cocos (Keeling) Islands (the)
     * 
     */
    CC("CC"),

    /**
     * Congo (the Democratic Republic of the)
     * 
     */
    CD("CD"),

    /**
     * Central African Republic (the)
     * 
     */
    CF("CF"),

    /**
     * Congo (the)
     * 
     */
    CG("CG"),

    /**
     * Switzerland
     * 
     */
    CH("CH"),

    /**
     * Cote d'Ivoire
     * 
     */
    CI("CI"),

    /**
     * Cook Islands (the)
     * 
     */
    CK("CK"),

    /**
     * Chile
     * 
     */
    CL("CL"),

    /**
     * Cameroon
     * 
     */
    CM("CM"),

    /**
     * China
     * 
     */
    CN("CN"),

    /**
     * Colombia
     * 
     */
    CO("CO"),

    /**
     * Costa Rica
     * 
     */
    CR("CR"),

    /**
     * Cuba
     * 
     */
    CU("CU"),

    /**
     * Cabo Verde
     * 
     */
    CV("CV"),

    /**
     * Curacao
     * 
     */
    CW("CW"),

    /**
     * Christmas Island
     * 
     */
    CX("CX"),

    /**
     * Cyprus
     * 
     */
    CY("CY"),

    /**
     * Czechia
     * 
     */
    CZ("CZ"),

    /**
     * Germany
     * 
     */
    DE("DE"),

    /**
     * Djibouti
     * 
     */
    DJ("DJ"),

    /**
     * Denmark
     * 
     */
    DK("DK"),

    /**
     * Dominica
     * 
     */
    DM("DM"),

    /**
     * Dominican Republic (the)
     * 
     */
    DO("DO"),

    /**
     * Algeria
     * 
     */
    DZ("DZ"),

    /**
     * European International Organisations
     * 
     */
    @XmlEnumValue("E$")
    E_$("E$"),

    /**
     * Ecuador
     * 
     */
    EC("EC"),

    /**
     * Estonia
     * 
     */
    EE("EE"),

    /**
     * Egypt
     * 
     */
    EG("EG"),

    /**
     * Western Sahara
     * 
     */
    EH("EH"),

    /**
     * Eritrea
     * 
     */
    ER("ER"),

    /**
     * Spain
     * 
     */
    ES("ES"),

    /**
     * Ethiopia
     * 
     */
    ET("ET"),

    /**
     * Finland
     * 
     */
    FI("FI"),

    /**
     * Fiji
     * 
     */
    FJ("FJ"),

    /**
     * Falkland Islands (the) [Malvinas]
     * 
     */
    FK("FK"),

    /**
     * Micronesia (Federated States of)
     * 
     */
    FM("FM"),

    /**
     * Faroe Islands (the)
     * 
     */
    FO("FO"),

    /**
     * France
     * 
     */
    FR("FR"),

    /**
     * Gabon
     * 
     */
    GA("GA"),

    /**
     * United Kingdom of Great Britain and Northern Ireland (the)
     * 
     */
    GB("GB"),

    /**
     * Grenada
     * 
     */
    GD("GD"),

    /**
     * Georgia
     * 
     */
    GE("GE"),

    /**
     * French Guiana
     * 
     */
    GF("GF"),

    /**
     * Guernsey
     * 
     */
    GG("GG"),

    /**
     * Ghana
     * 
     */
    GH("GH"),

    /**
     * Gibraltar
     * 
     */
    GI("GI"),

    /**
     * Greenland
     * 
     */
    GL("GL"),

    /**
     * Gambia (the)
     * 
     */
    GM("GM"),

    /**
     * Guinea
     * 
     */
    GN("GN"),

    /**
     * Guadeloupe
     * 
     */
    GP("GP"),

    /**
     * Equatorial Guinea
     * 
     */
    GQ("GQ"),

    /**
     * Greece
     * 
     */
    GR("GR"),

    /**
     * South Georgia and the South Sandwich Islands
     * 
     */
    GS("GS"),

    /**
     * Guatemala
     * 
     */
    GT("GT"),

    /**
     * Guam
     * 
     */
    GU("GU"),

    /**
     * Guinea-Bissau
     * 
     */
    GW("GW"),

    /**
     * Guyana
     * 
     */
    GY("GY"),

    /**
     * Hong Kong
     * 
     */
    HK("HK"),

    /**
     * Heard Island and McDonald Islands
     * 
     */
    HM("HM"),

    /**
     * Honduras
     * 
     */
    HN("HN"),

    /**
     * Croatia
     * 
     */
    HR("HR"),

    /**
     * Haiti
     * 
     */
    HT("HT"),

    /**
     * Hungary
     * 
     */
    HU("HU"),

    /**
     * Indonesia
     * 
     */
    ID("ID"),

    /**
     * Ireland
     * 
     */
    IE("IE"),

    /**
     * Israel
     * 
     */
    IL("IL"),

    /**
     * Isle of Man
     * 
     */
    IM("IM"),

    /**
     * India
     * 
     */
    IN("IN"),

    /**
     * British Indian Ocean Territory (the)
     * 
     */
    IO("IO"),

    /**
     * Iraq
     * 
     */
    IQ("IQ"),

    /**
     * Iran (Islamic Republic of)
     * 
     */
    IR("IR"),

    /**
     * Iceland
     * 
     */
    IS("IS"),

    /**
     * Italy
     * 
     */
    IT("IT"),

    /**
     * Jersey
     * 
     */
    JE("JE"),

    /**
     * Jamaica
     * 
     */
    JM("JM"),

    /**
     * Jordan
     * 
     */
    JO("JO"),

    /**
     * Japan
     * 
     */
    JP("JP"),

    /**
     * Kenya
     * 
     */
    KE("KE"),

    /**
     * Kyrgyzstan
     * 
     */
    KG("KG"),

    /**
     * Cambodia
     * 
     */
    KH("KH"),

    /**
     * Kiribati
     * 
     */
    KI("KI"),

    /**
     * Comoros (the)
     * 
     */
    KM("KM"),

    /**
     * Saint Kitts and Nevis
     * 
     */
    KN("KN"),

    /**
     * Korea (the Democratic People's Republic of)
     * 
     */
    KP("KP"),

    /**
     * Korea (the Republic of)
     * 
     */
    KR("KR"),

    /**
     * Kuwait
     * 
     */
    KW("KW"),

    /**
     * Cayman Islands (the)
     * 
     */
    KY("KY"),

    /**
     * Kazakhstan
     * 
     */
    KZ("KZ"),

    /**
     * Lao People's Democratic Republic (the)
     * 
     */
    LA("LA"),

    /**
     * Lebanon
     * 
     */
    LB("LB"),

    /**
     * Saint Lucia
     * 
     */
    LC("LC"),

    /**
     * Liechtenstein
     * 
     */
    LI("LI"),

    /**
     * Sri Lanka
     * 
     */
    LK("LK"),

    /**
     * Liberia
     * 
     */
    LR("LR"),

    /**
     * Lesotho
     * 
     */
    LS("LS"),

    /**
     * Lithuania
     * 
     */
    LT("LT"),

    /**
     * Luxembourg
     * 
     */
    LU("LU"),

    /**
     * Latvia
     * 
     */
    LV("LV"),

    /**
     * Libya
     * 
     */
    LY("LY"),

    /**
     * Morocco
     * 
     */
    MA("MA"),

    /**
     * Monaco
     * 
     */
    MC("MC"),

    /**
     * Moldova (the Republic of)
     * 
     */
    MD("MD"),

    /**
     * Montenegro
     * 
     */
    ME("ME"),

    /**
     * Saint Martin (French part)
     * 
     */
    MF("MF"),

    /**
     * Madagascar
     * 
     */
    MG("MG"),

    /**
     * Marshall Islands (the)
     * 
     */
    MH("MH"),

    /**
     * Macedonia (the former Yugoslav Republic of)
     * 
     */
    MK("MK"),

    /**
     * Mali
     * 
     */
    ML("ML"),

    /**
     * Myanmar
     * 
     */
    MM("MM"),

    /**
     * Mongolia
     * 
     */
    MN("MN"),

    /**
     * Macao
     * 
     */
    MO("MO"),

    /**
     * Northern Mariana Islands (the)
     * 
     */
    MP("MP"),

    /**
     * Martinique
     * 
     */
    MQ("MQ"),

    /**
     * Mauritania
     * 
     */
    MR("MR"),

    /**
     * Montserrat
     * 
     */
    MS("MS"),

    /**
     * Malta
     * 
     */
    MT("MT"),

    /**
     * Mauritius
     * 
     */
    MU("MU"),

    /**
     * Maldives
     * 
     */
    MV("MV"),

    /**
     * Malawi
     * 
     */
    MW("MW"),

    /**
     * Mexico
     * 
     */
    MX("MX"),

    /**
     * Malaysia
     * 
     */
    MY("MY"),

    /**
     * Mozambique
     * 
     */
    MZ("MZ"),

    /**
     * Non-European International Organisations
     * 
     */
    @XmlEnumValue("N$")
    N_$("N$"),

    /**
     * Namibia
     * 
     */
    NA("NA"),

    /**
     * New Caledonia
     * 
     */
    NC("NC"),

    /**
     * Niger (the)
     * 
     */
    NE("NE"),

    /**
     * Norfolk Island
     * 
     */
    NF("NF"),

    /**
     * Nigeria
     * 
     */
    NG("NG"),

    /**
     * Nicaragua
     * 
     */
    NI("NI"),

    /**
     * Netherlands (the)
     * 
     */
    NL("NL"),

    /**
     * Norway
     * 
     */
    NO("NO"),

    /**
     * Nepal
     * 
     */
    NP("NP"),

    /**
     * Nauru
     * 
     */
    NR("NR"),

    /**
     * Niue
     * 
     */
    NU("NU"),

    /**
     * New Zealand
     * 
     */
    NZ("NZ"),

    /**
     * Oman
     * 
     */
    OM("OM"),

    /**
     * Panama
     * 
     */
    PA("PA"),

    /**
     * Peru
     * 
     */
    PE("PE"),

    /**
     * French Polynesia
     * 
     */
    PF("PF"),

    /**
     * Papua New Guinea
     * 
     */
    PG("PG"),

    /**
     * Philippines (the)
     * 
     */
    PH("PH"),

    /**
     * Pakistan
     * 
     */
    PK("PK"),

    /**
     * Poland
     * 
     */
    PL("PL"),

    /**
     * Saint Pierre and Miquelon
     * 
     */
    PM("PM"),

    /**
     * Pitcairn
     * 
     */
    PN("PN"),

    /**
     * Puerto Rico
     * 
     */
    PR("PR"),

    /**
     * Palestine, State of
     * 
     */
    PS("PS"),

    /**
     * Portugal
     * 
     */
    PT("PT"),

    /**
     * Palau
     * 
     */
    PW("PW"),

    /**
     * Paraguay
     * 
     */
    PY("PY"),

    /**
     * Qatar
     * 
     */
    QA("QA"),

    /**
     * R�union
     * 
     */
    RE("RE"),

    /**
     * Romania
     * 
     */
    RO("RO"),

    /**
     * Serbia
     * 
     */
    RS("RS"),

    /**
     * Russian Federation (the)
     * 
     */
    RU("RU"),

    /**
     * Rwanda
     * 
     */
    RW("RW"),

    /**
     * Saudi Arabia
     * 
     */
    SA("SA"),

    /**
     * Solomon Islands
     * 
     */
    SB("SB"),

    /**
     * Seychelles
     * 
     */
    SC("SC"),

    /**
     * Sudan (the)
     * 
     */
    SD("SD"),

    /**
     * Sweden
     * 
     */
    SE("SE"),

    /**
     * Singapore
     * 
     */
    SG("SG"),

    /**
     * Saint Helena, Ascension and Tristan da Cunha
     * 
     */
    SH("SH"),

    /**
     * Slovenia
     * 
     */
    SI("SI"),

    /**
     * Svalbard and Jan Mayen
     * 
     */
    SJ("SJ"),

    /**
     * Slovakia
     * 
     */
    SK("SK"),

    /**
     * Sierra Leone
     * 
     */
    SL("SL"),

    /**
     * San Marino
     * 
     */
    SM("SM"),

    /**
     * Senegal
     * 
     */
    SN("SN"),

    /**
     * Somalia
     * 
     */
    SO("SO"),

    /**
     * Suriname
     * 
     */
    SR("SR"),

    /**
     * South Sudan
     * 
     */
    SS("SS"),

    /**
     * Sao Tome and Principe
     * 
     */
    ST("ST"),

    /**
     * El Salvador
     * 
     */
    SV("SV"),

    /**
     * Sint Maarten (Dutch part)
     * 
     */
    SX("SX"),

    /**
     * Syrian Arab Republic
     * 
     */
    SY("SY"),

    /**
     * Swaziland
     * 
     */
    SZ("SZ"),

    /**
     * Turks and Caicos Islands (the)
     * 
     */
    TC("TC"),

    /**
     * Chad
     * 
     */
    TD("TD"),

    /**
     * French Southern Territories (the)
     * 
     */
    TF("TF"),

    /**
     * Togo
     * 
     */
    TG("TG"),

    /**
     * Thailand
     * 
     */
    TH("TH"),

    /**
     * Tajikistan
     * 
     */
    TJ("TJ"),

    /**
     * Tokelau
     * 
     */
    TK("TK"),

    /**
     * Timor-Leste
     * 
     */
    TL("TL"),

    /**
     * Turkmenistan
     * 
     */
    TM("TM"),

    /**
     * Tunisia
     * 
     */
    TN("TN"),

    /**
     * Tonga
     * 
     */
    TO("TO"),

    /**
     * Turkey
     * 
     */
    TR("TR"),

    /**
     * Trinidad and Tobago
     * 
     */
    TT("TT"),

    /**
     * Tuvalu
     * 
     */
    TV("TV"),

    /**
     * Taiwan (Province of China)
     * 
     */
    TW("TW"),

    /**
     * Tanzania, United Republic of
     * 
     */
    TZ("TZ"),

    /**
     * Ukraine
     * 
     */
    UA("UA"),

    /**
     * Uganda
     * 
     */
    UG("UG"),

    /**
     * United States Minor Outlying Islands (the)
     * 
     */
    UM("UM"),

    /**
     * United States of America (the)
     * 
     */
    US("US"),

    /**
     * Uruguay
     * 
     */
    UY("UY"),

    /**
     * Uzbekistan
     * 
     */
    UZ("UZ"),

    /**
     * Holy See (the)
     * 
     */
    VA("VA"),

    /**
     * Saint Vincent and the Grenadines
     * 
     */
    VC("VC"),

    /**
     * Venezuela (Bolivarian Republic of)
     * 
     */
    VE("VE"),

    /**
     * Virgin Islands (British)
     * 
     */
    VG("VG"),

    /**
     * Virgin Islands (U.S.)
     * 
     */
    VI("VI"),

    /**
     * Viet Nam
     * 
     */
    VN("VN"),

    /**
     * Vanuatu
     * 
     */
    VU("VU"),

    /**
     * Wallis and Futuna
     * 
     */
    WF("WF"),

    /**
     * Samoa
     * 
     */
    WS("WS"),

    /**
     * Yemen
     * 
     */
    YE("YE"),

    /**
     * Mayotte
     * 
     */
    YT("YT"),

    /**
     * South Africa
     * 
     */
    ZA("ZA"),

    /**
     * Zambia
     * 
     */
    ZM("ZM"),

    /**
     * Zimbabwe
     * 
     */
    ZW("ZW");
    private final String value;

    CLECBSDDISO3166DSJNTIO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CLECBSDDISO3166DSJNTIO fromValue(String v) {
        for (CLECBSDDISO3166DSJNTIO c: CLECBSDDISO3166DSJNTIO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
