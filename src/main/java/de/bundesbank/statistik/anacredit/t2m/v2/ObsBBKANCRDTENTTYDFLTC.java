
package de.bundesbank.statistik.anacredit.t2m.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_ENTTY_DFLT_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_ENTTY_DFLT_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="CP_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID" />
 *       &lt;attribute name="TYP_CP_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_CP_ID" />
 *       &lt;attribute name="DFLT_STTS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_CRDT_QLTY_DFLT_STTS_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="DT_DFLT_STTS" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_ENTTY_DFLT_C", namespace = "http://www.bundesbank.de/statistik/anacredit/t2m/v2")
public class ObsBBKANCRDTENTTYDFLTC
    extends ObsType
{


}
