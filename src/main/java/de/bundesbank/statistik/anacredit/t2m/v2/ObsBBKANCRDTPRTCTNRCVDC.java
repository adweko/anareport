
package de.bundesbank.statistik.anacredit.t2m.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_PRTCTN_RCVD_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_PRTCTN_RCVD_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PRTCTN_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="TYP_PRTCTN" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_PRTCTN_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="PRTCTN_VL" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="TYP_PRTCTN_VL" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_PRTCTN_VL_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="PRTCTN_VLTN_APPRCH" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_PRTCTN_VLTN_APPRCH_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="RL_ESTT_CLLTRL_LCTN" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_ISO3166_NUTS_DSJNT_NA" />
 *       &lt;attribute name="DT_PRTCTN_VL" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="DT_MTRTY_PRTCTN" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="ORGNL_PRTCTN_VL" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="DT_ORGNL_PRTCTN_VL" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_PRTCTN_RCVD_C", namespace = "http://www.bundesbank.de/statistik/anacredit/t2m/v2")
public class ObsBBKANCRDTPRTCTNRCVDC
    extends ObsType
{


}
