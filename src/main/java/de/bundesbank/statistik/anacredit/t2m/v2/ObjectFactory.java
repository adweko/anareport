
package de.bundesbank.statistik.anacredit.t2m.v2;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.bundesbank.statistik.anacredit.t2m.v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.bundesbank.statistik.anacredit.t2m.v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTHDRC }
     * 
     */
    public ObsBBKANCRDTHDRC createObsBBKANCRDTHDRC() {
        return new ObsBBKANCRDTHDRC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTPRTCTNPRVDRC }
     * 
     */
    public ObsBBKANCRDTPRTCTNPRVDRC createObsBBKANCRDTPRTCTNPRVDRC() {
        return new ObsBBKANCRDTPRTCTNPRVDRC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTPRTCTNRCVDC }
     * 
     */
    public ObsBBKANCRDTPRTCTNRCVDC createObsBBKANCRDTPRTCTNRCVDC() {
        return new ObsBBKANCRDTPRTCTNRCVDC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTENTTYRSKC }
     * 
     */
    public ObsBBKANCRDTENTTYRSKC createObsBBKANCRDTENTTYRSKC() {
        return new ObsBBKANCRDTENTTYRSKC();
    }

    /**
     * Create an instance of {@link BBKANCRDTHDRC }
     * 
     */
    public BBKANCRDTHDRC createBBKANCRDTHDRC() {
        return new BBKANCRDTHDRC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTINSTRMNTPRTCTNRCVDC }
     * 
     */
    public ObsBBKANCRDTINSTRMNTPRTCTNRCVDC createObsBBKANCRDTINSTRMNTPRTCTNRCVDC() {
        return new ObsBBKANCRDTINSTRMNTPRTCTNRCVDC();
    }

    /**
     * Create an instance of {@link BBKANCRDTPRTCTNPRVDRC }
     * 
     */
    public BBKANCRDTPRTCTNPRVDRC createBBKANCRDTPRTCTNPRVDRC() {
        return new BBKANCRDTPRTCTNPRVDRC();
    }

    /**
     * Create an instance of {@link BBKANCRDTENTTYDFLTC }
     * 
     */
    public BBKANCRDTENTTYDFLTC createBBKANCRDTENTTYDFLTC() {
        return new BBKANCRDTENTTYDFLTC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTENTTYDFLTC }
     * 
     */
    public ObsBBKANCRDTENTTYDFLTC createObsBBKANCRDTENTTYDFLTC() {
        return new ObsBBKANCRDTENTTYDFLTC();
    }

    /**
     * Create an instance of {@link BBKANCRDTENTTYRSKC }
     * 
     */
    public BBKANCRDTENTTYRSKC createBBKANCRDTENTTYRSKC() {
        return new BBKANCRDTENTTYRSKC();
    }

    /**
     * Create an instance of {@link BBKANCRDTINSTRMNTPRTCTNRCVDC }
     * 
     */
    public BBKANCRDTINSTRMNTPRTCTNRCVDC createBBKANCRDTINSTRMNTPRTCTNRCVDC() {
        return new BBKANCRDTINSTRMNTPRTCTNRCVDC();
    }

    /**
     * Create an instance of {@link BBKANCRDTPRTCTNRCVDC }
     * 
     */
    public BBKANCRDTPRTCTNRCVDC createBBKANCRDTPRTCTNRCVDC() {
        return new BBKANCRDTPRTCTNRCVDC();
    }

}
