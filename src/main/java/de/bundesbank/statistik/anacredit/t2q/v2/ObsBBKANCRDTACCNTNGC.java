
package de.bundesbank.statistik.anacredit.t2q.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_ACCNTNG_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_ACCNTNG_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="CNTRCT_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="INSTRMNT_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="ACCNTNG_CLSSFCTN" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_ACCNTNG_CLSSFCTN_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="RCGNTN_STTS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_RCGNTN_STTS_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="ACCMLTD_WRTFFS" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="ACCMLTD_IMPRMNT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="IMPRMNT_STTS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_CRDT_QLTY_IMPRMNT_STTS_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="IMPRMNT_ASSSSMNT_MTHD" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_IMPRMNT_ASSSSMNT_MTHD_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="SRC_ENCMBRNC" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_SRC_ENCMBRNC_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="ACCMLTD_CHNGS_FV_CR" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ALL_2D_NA" />
 *       &lt;attribute name="PRFRMNG_STTS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_CRDT_QLTY_PRFRMNG_STTS_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="DT_PRFRMNG_STTS" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="PRVSNS_OFF_BLNC_SHT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="FRBRNC_STTS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_FRBRNC_STTS_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="DT_FRBRNC_STTS" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="CMLTV_RCVRS_SNC_DFLT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="PRDNTL_PRTFL" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_PRDNTL_PRTFL_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="CRRYNG_AMNT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ALL_2D_NA" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_ACCNTNG_C", namespace = "http://www.bundesbank.de/statistik/anacredit/t2q/v2")
public class ObsBBKANCRDTACCNTNGC
    extends ObsType
{


}
