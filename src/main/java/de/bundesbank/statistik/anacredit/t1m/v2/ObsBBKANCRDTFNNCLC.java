
package de.bundesbank.statistik.anacredit.t1m.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_FNNCL_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_FNNCL_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="CNTRCT_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="INSTRMNT_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="ANNLSD_AGRD_RT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ALL_6D_NA" />
 *       &lt;attribute name="DT_NXT_INTRST_RT_RST" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="DFLT_STTS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_CRDT_QLTY_DFLT_STTS_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="DT_DFLT_STTS" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="TRNSFRRD_AMNT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="ARRRS" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="DT_PST_D" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="TYP_SCRTSTN" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_TRNSFR_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="OTSTNDNG_NMNL_AMNT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="ACCRD_INTRST" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ALL_2D_NA" />
 *       &lt;attribute name="OFF_BLNC_SHT_AMNT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_FNNCL_C", namespace = "http://www.bundesbank.de/statistik/anacredit/t1m/v2")
public class ObsBBKANCRDTFNNCLC
    extends ObsType
{


}
