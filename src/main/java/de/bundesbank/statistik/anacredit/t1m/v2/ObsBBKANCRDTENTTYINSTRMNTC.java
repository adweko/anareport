
package de.bundesbank.statistik.anacredit.t1m.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_ENTTY_INSTRMNT_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_ENTTY_INSTRMNT_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="CP_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID_NA" />
 *       &lt;attribute name="TYP_CP_ID" use="required" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_CP_ID_PRTC" />
 *       &lt;attribute name="CNTRCT_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="INSTRMNT_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="ENTTY_RL" use="required" type="{http://www.bundesbank.de/statistik/ecb/codelists/v2}CL_ECBSDD_ENTTY_RL_ANCRDT_CLLCTN" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_ENTTY_INSTRMNT_C", namespace = "http://www.bundesbank.de/statistik/anacredit/t1m/v2")
public class ObsBBKANCRDTENTTYINSTRMNTC
    extends ObsType
{


}
