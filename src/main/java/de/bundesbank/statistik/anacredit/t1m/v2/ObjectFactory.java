
package de.bundesbank.statistik.anacredit.t1m.v2;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.bundesbank.statistik.anacredit.t1m.v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.bundesbank.statistik.anacredit.t1m.v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTJNTLBLTSC }
     * 
     */
    public ObsBBKANCRDTJNTLBLTSC createObsBBKANCRDTJNTLBLTSC() {
        return new ObsBBKANCRDTJNTLBLTSC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTINSTRMNTC }
     * 
     */
    public ObsBBKANCRDTINSTRMNTC createObsBBKANCRDTINSTRMNTC() {
        return new ObsBBKANCRDTINSTRMNTC();
    }

    /**
     * Create an instance of {@link BBKANCRDTINSTRMNTC }
     * 
     */
    public BBKANCRDTINSTRMNTC createBBKANCRDTINSTRMNTC() {
        return new BBKANCRDTINSTRMNTC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTHDRC }
     * 
     */
    public ObsBBKANCRDTHDRC createObsBBKANCRDTHDRC() {
        return new ObsBBKANCRDTHDRC();
    }

    /**
     * Create an instance of {@link BBKANCRDTENTTYINSTRMNTC }
     * 
     */
    public BBKANCRDTENTTYINSTRMNTC createBBKANCRDTENTTYINSTRMNTC() {
        return new BBKANCRDTENTTYINSTRMNTC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTFNNCLC }
     * 
     */
    public ObsBBKANCRDTFNNCLC createObsBBKANCRDTFNNCLC() {
        return new ObsBBKANCRDTFNNCLC();
    }

    /**
     * Create an instance of {@link BBKANCRDTHDRC }
     * 
     */
    public BBKANCRDTHDRC createBBKANCRDTHDRC() {
        return new BBKANCRDTHDRC();
    }

    /**
     * Create an instance of {@link BBKANCRDTJNTLBLTSC }
     * 
     */
    public BBKANCRDTJNTLBLTSC createBBKANCRDTJNTLBLTSC() {
        return new BBKANCRDTJNTLBLTSC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTENTTYINSTRMNTC }
     * 
     */
    public ObsBBKANCRDTENTTYINSTRMNTC createObsBBKANCRDTENTTYINSTRMNTC() {
        return new ObsBBKANCRDTENTTYINSTRMNTC();
    }

    /**
     * Create an instance of {@link BBKANCRDTFNNCLC }
     * 
     */
    public BBKANCRDTFNNCLC createBBKANCRDTFNNCLC() {
        return new BBKANCRDTFNNCLC();
    }

}
