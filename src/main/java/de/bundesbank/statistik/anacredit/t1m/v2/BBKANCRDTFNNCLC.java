
package de.bundesbank.statistik.anacredit.t1m.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.DataSetType;


/**
 * <p>Java-Klasse f�r BBK_ANCRDT_FNNCL_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BBK_ANCRDT_FNNCL_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}DataSetType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *         &lt;element name="DataProvider" type="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}DataProviderReferenceType" minOccurs="0" form="unqualified"/>
 *         &lt;choice>
 *           &lt;element name="Obs" type="{http://www.bundesbank.de/statistik/anacredit/t1m/v2}ObsBBK_ANCRDT_FNNCL_C" maxOccurs="unbounded" form="unqualified"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BBK_ANCRDT_FNNCL_C", namespace = "http://www.bundesbank.de/statistik/anacredit/t1m/v2")
public class BBKANCRDTFNNCLC
    extends DataSetType
{


}
