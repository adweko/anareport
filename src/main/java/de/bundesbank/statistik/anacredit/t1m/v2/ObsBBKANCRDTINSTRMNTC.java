
package de.bundesbank.statistik.anacredit.t1m.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_INSTRMNT_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_INSTRMNT_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="CNTRCT_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="INSTRMNT_ID" use="required" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="TYP_INSTRMNT" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_INSTRMNT_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="TYP_AMRTSTN" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_AMRTSTN_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="CRRNCY_DNMNTN" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_ISO4217_NA" />
 *       &lt;attribute name="FDCRY" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_FDCRY_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="DT_INCPTN" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="DT_END_INTRST_ONLY" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="INTRST_RT_CP" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ALL_6D_NA" />
 *       &lt;attribute name="INTRST_RT_FLR" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ALL_6D_NA" />
 *       &lt;attribute name="INTRST_RT_RST_FRQNCY" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_FRQNCY_INTRST_RT_RST_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="INTRST_RT_SPRD" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_ALL_6D_NA" />
 *       &lt;attribute name="TYP_INTRST_RT" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_INTRST_RT_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="DT_LGL_FNL_MTRTY" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="CMMTMNT_INCPTN" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *       &lt;attribute name="PYMNT_FRQNCY" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_FRQNCY_PYMNT_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="PRJCT_FNNC_LN" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_PRJCT_FNNC_LN_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="PRPS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_PRPS_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="RCRS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_RCRSE_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="RFRNC_RT" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_RFRNC_RT_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="DT_STTLMNT" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_FLL_YYYY_MM_DD_NA" />
 *       &lt;attribute name="SBRDNTD_DBT" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_SBRDNTD_DBT_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="SYNDCTD_CNTRCT_ID" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID_NA" />
 *       &lt;attribute name="RPYMNT_RGHTS" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_RPYMNT_RGHTS_ANCRDT_CLLCTN_NA" />
 *       &lt;attribute name="FV_CHNG_CR_BFR_PRCHS" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_NN_NGTV_2D_NA" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_INSTRMNT_C", namespace = "http://www.bundesbank.de/statistik/anacredit/t1m/v2")
public class ObsBBKANCRDTINSTRMNTC
    extends ObsType
{


}
