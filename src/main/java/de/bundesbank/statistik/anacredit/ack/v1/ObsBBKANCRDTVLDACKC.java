
package de.bundesbank.statistik.anacredit.ack.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_VLD_ACK_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_VLD_ACK_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="VLDTN_ID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CNDTN_IDS" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CP_ID" type="{http://www.bundesbank.de/statistik/bbk/commontypes/v2}BBK_CP_ID" />
 *       &lt;attribute name="TYP_CP_ID" type="{http://www.bundesbank.de/statistik/bbk/codelists/v2}CL_BBK_TYP_CP_ID" />
 *       &lt;attribute name="CNTRCT_ID" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="INSTRMNT_ID" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *       &lt;attribute name="PRTCTN_ID" type="{http://www.bundesbank.de/statistik/ecb/commontypes/v2}ECBSDD_RSTRCTD_ID_60" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_VLD_ACK_C", namespace = "http://www.bundesbank.de/statistik/anacredit/ack/v1")
public class ObsBBKANCRDTVLDACKC
    extends ObsType
{


}
