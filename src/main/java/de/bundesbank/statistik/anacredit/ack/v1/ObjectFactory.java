
package de.bundesbank.statistik.anacredit.ack.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.bundesbank.statistik.anacredit.ack.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.bundesbank.statistik.anacredit.ack.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BBKANCRDTACKHDRC }
     * 
     */
    public BBKANCRDTACKHDRC createBBKANCRDTACKHDRC() {
        return new BBKANCRDTACKHDRC();
    }

    /**
     * Create an instance of {@link BBKANCRDTVLDACKC }
     * 
     */
    public BBKANCRDTVLDACKC createBBKANCRDTVLDACKC() {
        return new BBKANCRDTVLDACKC();
    }

    /**
     * Create an instance of {@link BBKANCRDTVLDACKXMLC }
     * 
     */
    public BBKANCRDTVLDACKXMLC createBBKANCRDTVLDACKXMLC() {
        return new BBKANCRDTVLDACKXMLC();
    }

    /**
     * Create an instance of {@link BBKANCRDTACKMSSGIDC }
     * 
     */
    public BBKANCRDTACKMSSGIDC createBBKANCRDTACKMSSGIDC() {
        return new BBKANCRDTACKMSSGIDC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTVLDACKC }
     * 
     */
    public ObsBBKANCRDTVLDACKC createObsBBKANCRDTVLDACKC() {
        return new ObsBBKANCRDTVLDACKC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTACKMSSGIDC }
     * 
     */
    public ObsBBKANCRDTACKMSSGIDC createObsBBKANCRDTACKMSSGIDC() {
        return new ObsBBKANCRDTACKMSSGIDC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTACKHDRC }
     * 
     */
    public ObsBBKANCRDTACKHDRC createObsBBKANCRDTACKHDRC() {
        return new ObsBBKANCRDTACKHDRC();
    }

    /**
     * Create an instance of {@link ObsBBKANCRDTVLDACKXMLC }
     * 
     */
    public ObsBBKANCRDTVLDACKXMLC createObsBBKANCRDTVLDACKXMLC() {
        return new ObsBBKANCRDTVLDACKXMLC();
    }

}
