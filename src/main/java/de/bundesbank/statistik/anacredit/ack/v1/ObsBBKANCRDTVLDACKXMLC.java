
package de.bundesbank.statistik.anacredit.ack.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.sdmx.resources.sdmxml.schemas.v2_1.data.structurespecific.ObsType;


/**
 * <p>Java-Klasse f�r ObsBBK_ANCRDT_VLD_ACK_XML_C complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObsBBK_ANCRDT_VLD_ACK_XML_C">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/structurespecific}ObsType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common}Annotations" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ERR_ID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="XML_CLMN" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="XML_RW" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="ERR_SVRTY" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ERR_MSSG" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObsBBK_ANCRDT_VLD_ACK_XML_C", namespace = "http://www.bundesbank.de/statistik/anacredit/ack/v1")
public class ObsBBKANCRDTVLDACKXMLC
    extends ObsType
{


}
