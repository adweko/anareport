
package de.bundesbank.statistik.bbk.codelists.v2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r CL_BBK_SRVY_ID.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="CL_BBK_SRVY_ID">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ANCRDT_T1M"/>
 *     &lt;enumeration value="ANCRDT_T2M"/>
 *     &lt;enumeration value="ANCRDT_T2Q"/>
 *     &lt;enumeration value="RIAD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CL_BBK_SRVY_ID", namespace = "http://www.bundesbank.de/statistik/bbk/codelists/v2")
@XmlEnum
public enum CLBBKSRVYID {


    /**
     * Reporting Type T1M
     * 
     */
    @XmlEnumValue("ANCRDT_T1M")
    ANCRDT_T_1_M("ANCRDT_T1M"),

    /**
     * Reporting Type T2M
     * 
     */
    @XmlEnumValue("ANCRDT_T2M")
    ANCRDT_T_2_M("ANCRDT_T2M"),

    /**
     * Reporting Type T2Q
     * 
     */
    @XmlEnumValue("ANCRDT_T2Q")
    ANCRDT_T_2_Q("ANCRDT_T2Q"),

    /**
     * Reporting Type RIAD
     * 
     */
    RIAD("RIAD");
    private final String value;

    CLBBKSRVYID(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CLBBKSRVYID fromValue(String v) {
        for (CLBBKSRVYID c: CLBBKSRVYID.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
