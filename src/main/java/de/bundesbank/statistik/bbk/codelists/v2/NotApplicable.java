
package de.bundesbank.statistik.bbk.codelists.v2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r NotApplicable.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="NotApplicable">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NOT_APPL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NotApplicable", namespace = "http://www.bundesbank.de/statistik/bbk/codelists/v2")
@XmlEnum
public enum NotApplicable {

    NOT_APPL;

    public String value() {
        return name();
    }

    public static NotApplicable fromValue(String v) {
        return valueOf(v);
    }

}
