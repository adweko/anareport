
package de.bundesbank.statistik.bbk.commontypes.v2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r BBK_NotApplicable.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="BBK_NotApplicable">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NOT_APPL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BBK_NotApplicable", namespace = "http://www.bundesbank.de/statistik/bbk/commontypes/v2")
@XmlEnum
public enum BBKNotApplicable {

    NOT_APPL;

    public String value() {
        return name();
    }

    public static BBKNotApplicable fromValue(String v) {
        return valueOf(v);
    }

}
